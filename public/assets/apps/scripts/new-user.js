function validateEmail(sEmail) {
	var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
	if (filter.test(sEmail)) {
			return true;
	}
	else {
	return false;
	}
	}
/**
 * validate name
 * @param user_name
 * @returns {Boolean}
 */
function validateName(user_name) {
	
	var filter =/^[ A-Za-z@.0-9]*$/;
	if (filter.test(user_name)) {
			return true;
	}
	else {
	return false;
	}
	}
/**
 * validate password
 * @param password
 * @returns {Boolean}
 */
function valPassword(){
	var password=($('#new_password').val()).length;
	if(password < 4){
		$("#passmsg").show();
	}else{
		$("#passmsg").hide();
	}
}
/**
 *validate valFullName
 * @param FullName
 * @returns {Boolean}
 */
function valFullName(){
	var filter =/^[ A-Za-z ]*$/;
	var full_name =$('#new_full_name').val();
	if (!filter.test(full_name)) {
		$("#fullnamemsg").show();
	}else {
		$("#fullnamemsg").hide();
	}
}
/**
*validate updated valFullName
* @param FullName
* @returns {Boolean}
*/
function valFullNameUpdated(){
	var filter =/^[ A-Za-z ]*$/;
	var full_name =$('#full_name_id').val();
	if (!filter.test(full_name)) {
		$("#fullnamemsgup").show();
	}else {
		$("#fullnamemsgup").hide();
	}
}
function checkUserExist(){
	var user_name=$('#new_user_name').val();
	$.ajax({
		type: "post",
		url: $('#url_path').val() + '/vns-username-check',
		data: {user_name:user_name},
		dataType: 'json',
			success: function(jsonResponse){
				if(jsonResponse.success == false){
				$('#update_button').prop('disabled',true);
				$("#usernamemsg").show();
				document.getElementById("create").disabled = true;
				return false;
				}else if(jsonResponse.success == true){
				$('#update_button').prop('disabled',false);
				$("#usernamemsg").hide();
				document.getElementById("create").disabled = false;
				return true;
				}
			}
	});
}

/*function checkUpdatedUserExist(){
	var user_id = $('#map_id').val();
	var user_name=$('#user_name_id').val();
	$.ajax({
		type: "post",
		url: $('#url_path').val() + '/vns-updated-username-check',
		data: {user_name:user_name,user_id:user_id},
		dataType: 'json',
			success: function(jsonResponse){
				if(jsonResponse.success == false){
				$('#update_button').prop('disabled',true);	
				$("#usernamemsgup").show();
				document.getElementById("create").disabled = true;
				return false;
				}else if(jsonResponse.success == true){
				$('#update_button').prop('disabled',false);
				$("#usernamemsgup").hide();
				document.getElementById("create").disabled = false;
				return true;
				}
			}
	});
}
*/


function emailExist(){
	var user_id = $('#map_id').val();
	var email_id=$('#email_id').val();
	$.ajax({
		type: "post",
		url: $('#url_path').val() + '/vns-updated-email-check',
		data: {email_id:email_id,user_id:user_id},
		dataType: 'json',
			success: function(jsonResponse){
				if(jsonResponse.success == false){
				$("#useremailup").show();
				$('#update_button').prop('disabled',true);
				document.getElementById("create").disabled = true;
				return false;
				}else if(jsonResponse.success == true){
				$('#update_button').prop('disabled',false);
				$("#useremailup").hide();
				document.getElementById("create").disabled = false;
				return true;
				}
			}
	});
}

function clearMsg(){
	$("#usernamemsg").hide();
}

function checkEmailExist(){
	var email_id=$('#new_email_id').val();
	$.ajax({
		type: "post",
		url: $('#url_path').val() + '/vns-email-check',
		data: {email_id:email_id},
		dataType: 'json',
			success: function(jsonResponse){
				if(jsonResponse.success == false){
				$("#emailmsg").show();
				$('#update_button').prop('disabled',true);
				document.getElementById("create").disabled = true;
				return false;
				}else if(jsonResponse.success == true){
				$('#update_button').prop('disabled',false);
				$("#emailmsg").hide();
				document.getElementById("create").disabled = false;
				return true;
				}
			}
	});
}


function createNewUser(obj){

var password = $("#new_password").val();
var user_name=$('#new_user_name').val();
var email_id=$('#new_email_id').val();
var full_name=$('#new_full_name').val();
var admin_user = $('input[name=admin]:checked').val();
var dept=$('#create_user').val();
if(user_name=='' || full_name==''||email_id==''||password=='')
{
	$("#top_msg1").removeClass("alert alert-info").addClass(
	"alert alert-danger").text("Fill all the mandatory fields");
$("#top_msg1").show();
$("#top_msg1").fadeOut(3000);
}

else if(validateEmail(email_id)==false)
{
	$("#top_msg1").removeClass("alert alert-info").addClass(
	"alert alert-danger").text("Enter valid email id");
$("#top_msg1").show();
$("#top_msg1").fadeOut(3000);
}else if(validateName(user_name)==false)
{
	$("#top_msg1").removeClass("alert alert-info").addClass(
	"alert alert-danger").text("Enter valid user_name");
$("#top_msg1").show();
$("#top_msg1").fadeOut(3000);
}
else{
$('#top_msg1').removeClass().addClass("alert alert-info").text('Please wait');
$("#top_msg1").show();
$('#create').prop('disabled',true);
setTimeout(function() {$('#new_user_id').modal('hide');}, 3000);

setTimeout(function() {$("#top_msg").show().addClass("alert alert-success").text('User added successfully');},3000);

setTimeout(function(){
	   window.location.reload(1);
	}, 4000);

			$.ajax({
				type: "post",
				url: $('#url_path').val() + '/vns-user-create',

				data: {user_name:user_name,password:password,email_id:email_id,full_name:full_name,admin_user:admin_user,department:dept},
				
				dataType: 'json',
				success: function(jsonResponse){
					
					if (jsonResponse.msg == undefined || jsonResponse.msg == null
							|| jsonResponse.msg.length == 0) {
						$("#top_msg").removeClass("alert alert-info").addClass(
								"alert alert-danger").text(jsonResponse.msg);
						$('#new_user_id').modal('hide');
						$("#top_msg").show();
						jQuery('.scroll-to-top').click();
					} else {
						$("#top_msg").removeClass("alert alert-danger").addClass(
								"alert alert-success").text(jsonResponse.msg);
						
						$('#new_user_id').modal('hide');

						$("#top_msg").show();
						jQuery('.scroll-to-top').click();

					}

					setTimeout(function() {
						window.location.reload(1);
					}, 5000);
			    
				}
				});
	
		}
}

function populateDeleteModal(obj){
$("#map_id1").val($(obj).closest("tr").find('#id').text());
}


function populateUpdateModal(obj){
	
 	$("#map_id").val($(obj).closest("tr").find('#id').text()); //role-subpermission id 
 	$("#user_name_id").val($(obj).closest("tr").find('#user_name').text());
 	$("#full_name_id").val($(obj).closest("tr").find('#full_name').text());
 	$("#email_id").val($(obj).closest("tr").find('#email').text());
}



function deleteMapping(obj){

	$.ajax({
		type: "post",
		url: $('#url_path').val() + '/vns-user-delete',

		data: {id:$("#map_id1").val()},
		
		dataType: 'json',
		success: function(jsonResponse){
			
			if (jsonResponse.msg == undefined || jsonResponse.msg == null
					|| jsonResponse.msg.length == 0) {
				$("#top_msg").removeClass("alert alert-info").addClass(
						"alert alert-danger").text(jsonResponse.msg);
				$("#top_msg").show();
				jQuery('.scroll-to-top').click();
			} else {
				$("#top_msg").removeClass("alert alert-danger").addClass(
						"alert alert-success").text(jsonResponse.msg);
				$("#top_msg").show();
				jQuery('.scroll-to-top').click();

			}

			setTimeout(function() {
				window.location.reload(1);
			}, 5000);	    
		}
		});
	
}


function clearFields(){
	
	$('#create').prop('disabled',false);
	document.getElementById('new_user_name').value = "";
	document.getElementById('new_password').value = "";
	document.getElementById('new_full_name').value = "";
	document.getElementById('new_email_id').value = "";

	$("#usernamemsg").hide();
	$("#emailmsg").hide();
	
}

function showDropdown(){
	  $("#dd").show();
}
function hideDropdown(){
	  $("#dd").hide();
}

function updateUserDetails() {
var id= $("#map_id").val();
var user_name = $("#user_name_id").val();
var full_name = $("#full_name_id").val(); 
var email = $("#email_id").val();
   
if(user_name=='' || full_name==''||email=='')
{
	$("#top_msg2").removeClass("alert alert-info").addClass(
	"alert alert-danger").text("Fill all the fields");
$("#top_msg2").show();
$("#top_msg2").fadeOut(3000);
}
 
else if(validateEmail(email)==false)
{
	$("#top_msg2").removeClass("alert alert-info").addClass(
	"alert alert-danger").text("Enter valid email id");
$("#top_msg2").show();
$("#top_msg2").fadeOut(3000);
}
else{
	document.getElementById("update_button").disabled = true;
$.ajax({
	type: "post",
	url: $('#url_path').val() + '/vns-user-update',

	data: {id:id,user_name:user_name,full_name:full_name,email:email},
	
	dataType: 'json',
	success: function(jsonResponse){
		


		if (jsonResponse.msg == undefined || jsonResponse.msg == null
				|| jsonResponse.msg.length == 0) {
			$("#top_msg").removeClass("alert alert-info").addClass(
					"alert alert-danger").text(jsonResponse.msg);
        $('#update').modal('hide');
			$("#top_msg").show();
			jQuery('.scroll-to-top').click();
		} else {
			$("#top_msg").removeClass("alert alert-danger").addClass(
					"alert alert-success").text(jsonResponse.msg);
			$('#update').modal('hide');
			$("#top_msg").show();
			jQuery('.scroll-to-top').click();

		}

		setTimeout(function() {
			window.location.reload(1);
		}, 5000);

	
 
	    
	}
	});
}
}
