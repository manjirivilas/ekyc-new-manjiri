
/*
 * For Creating Role
 */

function createRole(){
	
if($("#create_rolename").val()=='')
{
	$("#top_msg1").removeClass("alert alert-info").addClass(
	"alert alert-danger").text('Fill all the mandatory fields');
$("#top_msg1").show();
$("#top_msg1").fadeOut(3000);

}
else
{
	$('#create_new_modal').modal('hide');
	  $.ajax({
			type: "post",
			url: $('#url_path').val() + '/admin-create-role',
			data: {role_name:$("#create_rolename").val() ,role_description:$("#create_desc").val()  },
		
	  dataType: 'json',
		success: function(jsonResponse){

			
			if (jsonResponse.msg == undefined || jsonResponse.msg == null
					|| jsonResponse.msg.length == 0) {
				$("#top_msg").removeClass("alert alert-info").addClass(
						"alert alert-danger").text(jsonResponse.msg);
				$("#top_msg").show();
				jQuery('.scroll-to-top').click();
			} else {
				$("#top_msg").removeClass("alert alert-danger").addClass(
						"alert alert-success").text(jsonResponse.msg);
				$("#top_msg").show();
				//$('#create_new_modal').modal('hide');
				jQuery('.scroll-to-top').click();

			}
			setTimeout(function() {
				window.location.reload(1);
			}, 3000);
		}
			});

}
}
/* End of Function to Create new role*/







/*Populate Delete Modal*/
 
function populateDeleteModal(obj) {
	$("#delete_roleid").val($(obj).closest("tr").find('#role_id').text());
}

/*End of Populate Delete Modal*/




/* Populate Update Modal*/
function populateUpdateModal(obj) {
	$("#update_roleid").val($(obj).closest("tr").find('#role_id').text());
	
	$("#update_rolename").val($(obj).closest("tr").find('#role_name').text());
	$("#update_desc").val($(obj).closest("tr").find('#description').text());
	
}
/*End of Populate Update Modal*/
/**
 * check if role exist while creating new role
 */

function checkRoleNameExist(){
	var role=$('#create_rolename').val();
	$.ajax({
		type: "post",
		url: $('#url_path').val() + '/admin-rolename-check',
		data: {role:role},
		dataType: 'json',
			success: function(jsonResponse){
				if(jsonResponse.success == false){
				$("#rolenamemsg").show();
				document.getElementById("create").disabled = true;
				return false;
				}else if(jsonResponse.success == true){
				$("#rolenamemsg").hide();
				document.getElementById("create").disabled = false;
				return true;
				}
			}
	});
}



function roleExist(obj){
	var role_id = $('#update_roleid').val();
	var role_name=$('#update_rolename').val();
	$.ajax({
		type: "post",
		url: $('#url_path').val() + '/admin-updatedrole-check',
		data: {role_id:role_id,role_name:role_name},
		dataType: 'json',
			success: function(jsonResponse){
				if(jsonResponse.success == false){
				$("#role_error").show();
				$('#update_button').prop('disabled',true);
				//document.getElementById("create").disabled = true;
				return false;
				}else if(jsonResponse.success == true){
				$('#update_button').prop('disabled',false);
				$("#role_error").hide();
				//document.getElementById("create").disabled = false;
				return true;
				}
			}
	});
}


/*Ajax function to Update Role*/
function updateRole() {
		  $.ajax({
			type: "post",
			url: $('#url_path').val() + '/update-role',
			data: {role_id:$("#update_roleid").val(),role_name:	$("#update_rolename").val() ,role_description:$("#update_desc").val()},
			dataType: 'json',
			success: function(jsonResponse){
			if (jsonResponse.msg == undefined || jsonResponse.msg == null
					|| jsonResponse.msg.length == 0) {
				$("#top_msg").removeClass("alert alert-info").addClass(
						"alert alert-danger").text(jsonResponse.msg);
				$("#top_msg").show();
				jQuery('.scroll-to-top').click();
			} else {
				$("#top_msg").removeClass("alert alert-danger").addClass(
						"alert alert-success").text(jsonResponse.msg);
				$("#top_msg").show();
				jQuery('.scroll-to-top').click();

			}
			setTimeout(function() {
				window.location.reload(1);
			}, 5000);
	    
		}
			});
	
}
/*End of Ajax function to Update Role*/

/***
 * delete role
 */
function deleteRole(obj){
  $.ajax({
		type: "post",
		url: $('#url_path').val() + '/delete-role',
	data: {role_id: $('#delete_roleid').val() },

		
  dataType: 'json',
	success: function(jsonResponse){
		if (jsonResponse.msg == undefined || jsonResponse.msg == null
				|| jsonResponse.msg.length == 0) {
			$("#top_msg").removeClass("alert alert-info").addClass(
					"alert alert-danger").text(jsonResponse.msg);
			$("#top_msg").show();
			jQuery('.scroll-to-top').click();
		} else {
			$("#top_msg").removeClass("alert alert-danger").addClass(
					"alert alert-success").text(jsonResponse.msg);
			$("#top_msg").show();
			jQuery('.scroll-to-top').click();

		}

		setTimeout(function() {
			window.location.reload(1);
		}, 5000);

	    
	}
		});
}


function clear_data (){

	$("#create_rolename").val('');
	$("#create_desc").val('');
}
