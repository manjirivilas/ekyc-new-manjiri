$('#datepicker').click(function() {
	$('.offer').hide();
	$('#optionsRadios1').prop('checked', false);
	$('#optionsRadios2').prop('checked', false);
	$('#optionsRadios3').prop('checked', false);
	$('.code').hide();
	$('.free_trades').hide();
});

$('#datepicker_1').click(function() {
	$('.offer').hide();
	$('#optionsRadios1').prop('checked', false);
	$('#optionsRadios2').prop('checked', false);
	$('#optionsRadios3').prop('checked', false);
	$('.code').hide();
	$('.free_trades').hide();
});

function displayVoucher() {
	$('#text2').val('');
	$('#tradesCode').val('');
	$('#tradesNo').val('');

	$('.offer').hide();
	$('.code').show();
		$('.free_trades').hide();
}
function displayOffer() {
	$('#text3').val('');
	$('#tradesCode').val('');
	$('#tradesNo').val('');
	$('#text4').val('');
	$('#text5').val('');

	$('.code').hide();
	$('.offer').show();
		$('.free_trades').hide();
}
function displayTrades() {
	$('#text2').val('');
	$('#text3').val('');
	$('#text5').val('');
	$('#text4').val('');

	$('.code').hide();
	$('.offer').hide();
	$('.free_trades').show();
}
function validateform() {

	var d = new Date().toISOString().slice(0, 10);
	$("#top_msg").hide();
	var text = $('#text1').val();
	var form_date = $('#datepicker').val();
	var to_date = $('#datepicker_1').val();

	if (text != '' && form_date >= d && to_date > form_date) {
		if ($('#optionsRadios1').is(':checked')) {
		//	alert('haahaha');
			var discount = $('#text2').val();
			if (discount <= 100 && discount >= 1) {
				var offer_array = [ text, form_date, to_date, discount ];
				offer_ajax(offer_array);

			}

			else {
				$("#top_msg").removeClass("alert alert-info").addClass("alert alert-danger").text("Enter proper discount amount");
				$("#top_msg").show();
				jQuery('.scroll-to-top').click();
			}

		}
		if ($('#optionsRadios2').is(':checked')) {
			var offer = parseInt($('#text3').val());
			var min_val = parseInt($('#text4').val());
			var code = $('#text5').val();

			if (offer > 0 && offer <= min_val && code != '') {
				var voucher_array = [ text, form_date, to_date, code, offer,
						min_val ];
				offer_ajax(voucher_array);
			} else {
				$("#top_msg").removeClass("alert alert-info").addClass("alert alert-danger").text("Enter proper code,min value and offer amount");
				$("#top_msg").show();
				jQuery('.scroll-to-top').click();
			}

		}
		if ($('#optionsRadios3').is(':checked')) {
			var trades = $('#tradesNo').val();
			var trades_code = $('#tradesCode').val();
			var trades_pricingPlan = $('#pricingPlan').val();
			var reg_trades= /^[0-9]{1,2}$/ ;

				if (trades_pricingPlan == 0)
				{
					$("#top_msg").removeClass("alert alert-info").addClass("alert alert-danger").text("Please select pricing plan.");
					$("#top_msg").show();
					$("#top_msg").fadeOut(5000);
					jQuery('.scroll-to-top').click();
				}
			  else if(reg_trades.test(trades) == true){
					var voucher_array = [ text, form_date, to_date,trades,trades_code,trades_pricingPlan,1];
					offer_ajax(voucher_array);
				}
			 else {
				$("#top_msg").removeClass("alert alert-info").addClass("alert alert-danger").text("Only positive and upto two decimal numbers are accepted.");
				$("#top_msg").show();
				$("#top_msg").fadeOut(5000);
				jQuery('.scroll-to-top').click();
			}

		}

	} else if (text != '' && (form_date < d || to_date < form_date)) {

		$("#top_msg").removeClass("alert alert-info").addClass(
				"alert alert-danger").text(
				"Select proper form date and to date");

		$("#top_msg").show();
		$("#top_msg").fadeOut(5000);
		jQuery('.scroll-to-top').click()
	}

	else {
		$("#top_msg").removeClass("alert alert-info").addClass("alert alert-danger").text("Fill all the required (*) fields ");

		$("#top_msg").show();
		$("#top_msg").fadeOut(5000);
		jQuery('.scroll-to-top').click();
	}
}



function offer_ajax(offer_array) {
	$.ajax({
		type: "post",
		url:$('#url_path').val()+'/e-kyc-offers',
//		url: '{{ URL::route("get-offer-details") }}',
		data: {offer:offer_array},
	//	dataType: 'json',
		success: function(jsonResponse){

			if(jsonResponse.flag==1)
			{
			$( "#top_msg" ).removeClass( "alert alert-info" ).addClass( "alert alert-success" ).text(jsonResponse.msg);
			$( "#top_msg" ).show();
		    jQuery('.scroll-to-top').click();
		    setTimeout(function(){
	    	   window.location.reload(1);
	    	}, 5000);
			}
			else
			{
				if(jsonResponse.flag==0 )
					{

					if(typeof JSON.stringify(jsonResponse.msg) == 'string') {
						var arr_offer=[];
						 arr_offer.push('<tr><th>Name</th><th>From Date</th><th>To Date</th><th>Discount</th><th>Code</th><th>Offer</th><th>Min. value</th></tr>');
							for (var i=0;i<jsonResponse.msg.length;i++) {
							  if(jsonResponse.msg[i].discount!=null)
								  {
								  $('#myModal').modal('show');

								  arr_offer.push('<tr> ');
								  arr_offer.push('<td> ' +jsonResponse.msg[i].name + '</td>');
								  arr_offer.push('<td> ' +jsonResponse.msg[i].from_date + '</td>');
								  arr_offer.push('<td> ' +jsonResponse.msg[i].to_date + '</td>');
								  arr_offer.push('<td> ' +jsonResponse.msg[i].discount + '</td>');
								  arr_offer.push('<td> NA </td>');
								  arr_offer.push('<td> NA </td>');
								  arr_offer.push('<td> NA </td>');
								  arr_offer.push('</tr> ');

								  }
							  else
								  {

								  $('#myModal').modal('show');
								  arr_offer.push('<tr> ');
								  arr_offer.push('<td> ' +jsonResponse.msg[i].name + '</td>');
								  arr_offer.push('<td> ' +jsonResponse.msg[i].from_date + '</td>');
								  arr_offer.push('<td> ' +jsonResponse.msg[i].to_date + '</td>');
								  arr_offer.push('<td> NA</td>');
								  arr_offer.push('<td>  ' +jsonResponse.msg[i].code + ' </td>');
								  arr_offer.push('<td>  ' +jsonResponse.msg[i].off + '</td>');
								  arr_offer.push('<td>  ' +jsonResponse.msg[i].min_val + ' </td>');
								  arr_offer.push('</tr> ');
								  }
							  }


						 $(".modal-body #excelDataTable").html( arr_offer );

						}
					else
						{
						$( "#top_msg" ).removeClass( "alert alert-danger" ).addClass( "alert alert-danger" ).text(jsonResponse.msg);
					    $( "#top_msg" ).show();
					    jQuery('.scroll-to-top').click();
						}
					}




			}

		}
	});
	}

function updateOffers()
{

	var d = new Date().toISOString().slice(0, 10);
	$("#top_msg").hide();
	var text = $('#text1').val();
	var form_date = $('#datepicker').val();
	var to_date = $('#datepicker_1').val();

	if (text != '' && form_date >= d && to_date > form_date) {
		if ($('#optionsRadios1').is(':checked')) {
			var discount = $('#text2').val();
			var offer_array = [ text, form_date, to_date, discount ];
				updateAjax(offer_array);

			}

		}
		if ($('#optionsRadios2').is(':checked')) {
			var offer = parseInt($('#text3').val());
			var min_val = parseInt($('#text4').val());
			var code = $('#text5').val();
			var voucher_array = [ text, form_date, to_date, code, offer,min_val ];
			updateAjax(voucher_array);
			}

		}

function updateAjax(offers)
{
	$.ajax({
		type: "post",
		url:$('#url_path').val()+'/update-ekyc-existing-offer',
		data: {offer:offers},
		dataType: 'json',
		success: function(jsonResponse){
			if(jsonResponse.msg == undefined || jsonResponse.msg == null || jsonResponse.msg.length == 0)
			{
			$( "#top_msg" ).removeClass( "alert alert-info" ).addClass( "alert alert-danger" ).text(jsonResponse.msg);
			$( "#top_msg" ).show();
		    jQuery('.scroll-to-top').click();
			}
			else
			{
				$( "#top_msg" ).removeClass( "alert alert-danger" ).addClass( "alert alert-success" ).text(jsonResponse.msg);
			    $( "#top_msg" ).show();
			    jQuery('.scroll-to-top').click();

			}

		    setTimeout(function(){
		    	   window.location.reload(1);
		    	}, 5000);

		}
	});

}
