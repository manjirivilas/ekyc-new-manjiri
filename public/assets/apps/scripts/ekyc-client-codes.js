
$("#submit").click(function(){
	
	if(!$('#submit').is('[disabled=disabled]'))
	{

		$( "#top_msg" ).removeClass().addClass( "alert alert-info" ).text('Please wait...');	
		$( "#top_msg" ).show();
	

	 $.ajax({
		type: "POST",
// 		url: '{!! URL::route("fetch-client-code") !!}',
		url: $("#hdnroute").val()+'/post-offline-client-codes',
		data: $("#client-code-form").serialize(),
		dataType: 'json',
		
		success: function(jsonResponse){

if(jsonResponse.status == 'fail'){

	 $('#top_msg').text('');
	
	var array_response =  new Array();
    var user = 0;


    $('#top_msg').show().addClass('alert-danger').removeClass('alert-success');
    
	$.each( jsonResponse.msg, function( key, value ) {
// 		  alert( key + ": " + value );
		array_response[user] =  new Array (
				value
				);

		setTimeout(function(){ $('#top_msg').slideUp(); $('#top_msg ').text('');}, 5000);
		 $('#anchors').text('');

	$('#top_msg').append('<li style="margin-top:-10px;"><p class="alert-alert-danger">'+value+'</p>  </li>');


		
// 		 $('#top_msg').text(array_response[user]);

		 user++;
		 
			});
	
// 	alert(array_response[0]);
}


if(jsonResponse.status == 'success'){


		 $('#top_msg').show().addClass('alert-success').removeClass('alert-danger');
		 $('#top_msg').text('The Available Client-Codes are displayed below');
			 
// 	alert(array_response[0]);


// $('#client-code-form input,select,a').attr('readonly', true);
// $('#client-code-form input,select,a').attr('disabled', true);

		 $('#fetched_codes').show();
// 		 $('#fetched_codes').text("");
// 		 $('#fetched_codes').text('Below are the available client-codes');

		 $('#anchors').text('');
			$.each( jsonResponse.msg, function( key, value ) {
// 				$('#anchors').append('<li><a data-toggle="modal" data-target="#myModal" data-title="Registering client-code :" id='+value+' href="#anchor-'+value+'" value='+value+' onClick="register_code(this)">'+value+'</a>  </li>');
					
						$('#anchors').append('<div class="col-md-2"><a data-toggle="modal" data-target="#myModal" data-title="Registering client-code :" id='+value+' href="#anchor-'+value+'" value='+value+' onClick="register_code(this)">'+value+'</a>  </div>');
					
			});
}

		}
	}); 
}
});

function register_code(data){
	$("#client_name").text($('#name').val());
	$("#client_state").text($('#state').val());
	$("#client_city").text($('#city').val());
	$("#client_code").text(data.text);
	$("#pan_card1").text($('#pan_card').val());
}


function registerClientCode(){


	 $.ajax({
			type: "POST",
//	 		url: '{!! URL::route("fetch-client-code") !!}',
			url: $("#hdnroute").val()+'/register-offline-client-codes',
			data: {name:$('#name').val(),state:$('#state').val(),city:$('#city').val(),code:$('#client_code').text(),pan_card:$('#pan_card').val()},
			dataType: 'json',
			
			success: function(jsonResponse){
				
				if(jsonResponse.status == 'fail'){

					 $('#top_msg').text('');
						
					var array_response =  new Array();
				    var user = 0;

					 $('#anchors').text('');

				    $('#top_msg').show().addClass('alert-danger').removeClass('alert-success');
				    
					$.each( jsonResponse.msg, function( key, value ) {
//				 		  alert( key + ": " + value );
						array_response[user] =  new Array (
								value
								);

						setTimeout(function(){ $('#top_msg').slideUp(); $('#top_msg ').text('');}, 5000);

					$('#top_msg').append('<li style="margin-top:-10px;"><p class="alert-alert-danger">'+value+'</p>  </li>');

//				 		 $('#top_msg').text(array_response[user]);
						 user++;
						 
							});
}		
				if(jsonResponse.status == 'success'){

					 $('#top_msg').show().addClass('alert-info').removeClass('alert-danger');
					 $('#top_msg').text('Client-code '+$("#client_code").text()+' Registered and Assigned to '+$("#name").val()+' of '+$("#city").val()+', '+$("#state").val()+' location.');
					 
					
				 	$('#client-code-form input,select,a').attr('readonly', false);
					$('#client-code-form input,select,a').attr('disabled', false);

					$('#client-code-form input,select').val('');
					$('#anchors a').val("");
					$('#fetched_codes').hide();
					
					
					}
				
				}

			});


}




$("#pan_card").blur(function(){
//    alert($('#pan_card').val());
    
    $.ajax({
		type: "POST",
// 		url: '{!! URL::route("fetch-client-code") !!}',
		url: $("#hdnroute").val()+'/check-client-id-exist-against-pancard',
		data: {pan_card:$('#pan_card').val()},
		dataType: 'json',
		
		success: function(jsonResponse){
			

			if(jsonResponse.msg == undefined || jsonResponse.msg == null || jsonResponse.msg.length == 0)
			{
			$( "#top_msg" ).removeClass( "alert alert-info" ).addClass( "alert alert-danger" ).text(jsonResponse.msg);	
			$( "#top_msg" ).show();	  
		    jQuery('.scroll-to-top').click();
			}
			else
			{
				if(jsonResponse.msg!=1 &&jsonResponse.msg!=0)
					{
					if(jsonResponse.status=='fail')
						{
						
				   $( "#top_msg" ).removeClass().addClass( "alert alert-danger" ).text(jsonResponse.msg);
						}
					else{
					  $( "#top_msg" ).removeClass().addClass( "alert alert-info" ).text(jsonResponse.msg);
					   $('#name').val('');
					    $('#state').val('');
					    $('#city').val('');
					    $('#fetched_codes').hide();
					}
			    $( "#top_msg" ).show();	  
			    jQuery('.scroll-to-top').click();
			    $('#name').attr('disabled', true);
			    $('#state').attr('disabled', true);
			    $('#city').attr('disabled', true);
			    $('#submit').attr('disabled', true);
			    
					}
				else
					{
					$( "#top_msg" ).hide();	 
					 $('#name').attr('disabled', false);
					    $('#state').attr('disabled', false);
					    $('#city').attr('disabled', false);
					    $('#submit').attr('disabled', false);
					}
				
				
			}
			
		  	
	    
		
		}

		});
});

