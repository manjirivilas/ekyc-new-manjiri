$(document).ready(function () {



      $("form[name='userSignUp']").validate({
    // Specify validation rules
    rules: {
      // The key name on the left side is the name attribute
      // of an input field. Validation rules are defined
      // on the right side
      name: {
        required: true,
        characterspaces : true,
        minlength:3,
        maxLen:[100]
        

      },
      mobile: {
        required: true,
        digits:10,
        mobilestart:true,
        minlength: 10,
        maxlength:10
      },
      email: {
        required: true,
        // Specify that email should be validated
        // by the built-in "email" rule
        email: true
      },
    },
    // Specify validation error messages
    messages: {
      name: {
        required: "Please enter your Full Name",
        minlength: jQuery.validator.format("Please enter at least {0} characters."),
        maxLen: "You have reached the maximum number of characters.",
        characterspaces : "Only alphabetical characters"
        
      },
      mobile: {
        required: "Please enter your Mobile No.",
        digits: "Please enter only digits.",
        mobilestart : "Mobile Should start either with 7|8|9",
        minlength: jQuery.validator.format("Please enter at least {0} characters."),
        maxlength: jQuery.validator.format("Please enter no more than {0} characters.")

        
      },
      email: "Please enter a valid email address"
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
    submitHandler: function(form) {
      form.submit();
    }
  });


  $("#loginForm").validate({
    // Specify validation rules
    rules: {
      // The key name on the left side is the name attribute
      // of an input field. Validation rules are defined
      // on the right side
      email: {
        required: true,
        // Specify that email should be validated
        // by the built-in "email" rule
        email: true
      },
      password: {
        required: true,
        minlength:6
      }
    },
    // Specify validation error messages
    messages: {
      email: "Please enter a valid email address",

      password: {
        required: "Please enter password",
        minlength: jQuery.validator.format("Please enter at least {0} characters.")
      }
      
      
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
    submitHandler: function(form) {
      form.submit();
    }
  });


  $("#forgotPassword").validate({
    // Specify validation rules
    rules: {
      // The key name on the left side is the name attribute
      // of an input field. Validation rules are defined
      // on the right side
      email: {
        required: true,
        // Specify that email should be validated
        // by the built-in "email" rule
        email: true
      }
    },
    // Specify validation error messages
    messages: {
      email: "Please enter a valid email address",

    
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
    submitHandler: function(form) {
      form.submit();
    }
  });

  $("#changePasswordForm").validate({
    // Specify validation rules
    rules: {
      // The key name on the left side is the name attribute
      // of an input field. Validation rules are defined
      // on the right side
      old_password: {
        required: true,
        minlength:6
      },
      password: {
        required: true,
        minlength:6
      },
      password_confirmation: {
        required: true,
        minlength:6
      }
    },
    // Specify validation error messages
    messages: {
      old_password: {
        required: "Please enter old password",
        minlength: jQuery.validator.format("Please enter at least {0} characters.")
      },
      password: {
        required: "Please enter password",
        minlength: jQuery.validator.format("Please enter at least {0} characters.")
      },
      password_confirmation: {
        required: "Please enter confirm password",
        minlength: jQuery.validator.format("Please enter at least {0} characters.")
      }
    
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
    submitHandler: function(form) {
      form.submit();
    }
  });


  $(".password-validate").validate({
    // Specify validation rules
    rules: {
      // The key name on the left side is the name attribute
      // of an input field. Validation rules are defined
      // on the right side
       password: {
        required: true,
        minlength:6
      },
      password_confirmation: {
        required: true,
        minlength:6
      }
    },
    // Specify validation error messages
    messages: {
      password: {
        required: "Please enter password",
        minlength: jQuery.validator.format("Please enter at least {0} characters.")
      },
      password_confirmation: {
        required: "Please enter confirm password",
        minlength: jQuery.validator.format("Please enter at least {0} characters.")
      }
    
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
    submitHandler: function(form) {
      form.submit();
    }
  });
});




