$(document).ready(function () {


  $("#aadharDetail").validate({
    // Specify validation rules
    rules: {
      // The key name on the left side is the name attribute
      // of an input field. Validation rules are defined
      // on the right side
      adhaar: {
        required: true,
        digits:10,
        minlength: 12,
      }
    },
    // Specify validation error messages
    messages: {
      adhaar: {
       required: "Please enter your Aadhar Number",
        digits: "Please enter only digits.",
        minlength: jQuery.validator.format("Please enter at least {0} characters.")
      
      }

    
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
    submitHandler: function(form) {
      form.submit();
    }
  });


$("#panDetail").validate({
    // Specify validation rules
    rules: {
      // The key name on the left side is the name attribute
      // of an input field. Validation rules are defined
      // on the right side
      pan_no: {
        required: true,
        panvalidate:true,
        minlength: 10,
      }
    },
    // Specify validation error messages
    messages: {
      pan_no: {
       required: "Please enter your PAN",
        panvalidate: "Please enter valid PAN.",
        minlength: jQuery.validator.format("Please enter at least {0} characters.")
        
      }

    
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
    submitHandler: function(form) {
      form.submit();
    }
  });

$("#fetchOTP").validate({
    // Specify validation rules
    rules: {
      // The key name on the left side is the name attribute
      // of an input field. Validation rules are defined
      // on the right side
      pin: {
        required: true,
        digits: 6,
        minlength: 6,
      }
    },
    // Specify validation error messages
    messages: {
      pin: {
       required: "Please enter One Time Password",
        digits: "Please enter only digits.",
        minlength: jQuery.validator.format("Please enter at least {0} characters.")
        
      }

    
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
    submitHandler: function(form) {
      form.submit();
    }
  });


$("#personalDetailsForm").validate({
    // Specify validation rules
    rules: {
      fathername: {
        required: true,
        characterspaces : true,
        minlength:3
      },
      mothername: {
        required: true,
        characterspaces : true,
        minlength:3
      },
      birthdate: {
        required: true
      },
      email: {
        required: true,
        email: true
      },
      mobile: {
        required: true,
        digits:10,
        mobilestart:true,
        minlength: 10,
        maxlength:10
      },
      address: {
        required: true,
        minlength:4
      },
      address1: {
        required: true,
        minlength:4
      },
      address2: {
        required: true,
        minlength:4
      },
      city: {
        required: true
      },
      state: {
        required: true
      },
      pincode: {
        required: true,
        digits:10,
        minlength: 6
      },
      address_proof: {
        required: true
      },
      permanent_address: {
        required: true,
        minlength:4
      },
      permanent_address1: {
        required: true,
        minlength:4
      },
      permanent_address2: {
        required: true,
        minlength:4
      },
      permanent_city: {
        required: true
      },
      permanent_state: {
        required: true
      },
      permanent_pincode: {
        required: true,
        digits:10,
        minlength: 6
      },
      permanent_address_proof: {
        required: true
      },
      annual_income: {
        required: true
      },
      net_worth: {
        required: true,
        digits:10,
        minlength:2
      },
      birthdate_2: {
        required: true
      },
      occupation: {
        required: true
      }
    },
    // Specify validation error messages
    messages: {
      fathername: {
        required: "Please enter your Father Name",
        minlength: jQuery.validator.format("Please enter at least {0} characters."),
        maxLen: "You have reached the maximum number of characters.",
        characterspaces : "Only alphabetical characters"
      },
      mothername: {
        required: "Please enter your Mother Name",
        minlength: jQuery.validator.format("Please enter at least {0} characters."),
        maxLen: "You have reached the maximum number of characters.",
        characterspaces : "Only alphabetical characters"
      },
      birthdate: {
        required: "Please enter Date of Birth" 
      },
      email: "Please enter a valid email address",
      mobile: {
        required: "Please enter your Mobile No.",
        digits: "Please enter only digits.",
        mobilestart : "Mobile Should start either with 7|8|9",
        minlength: jQuery.validator.format("Please enter at least {0} characters."),
        maxlength: jQuery.validator.format("Please enter no more than {0} characters.")
      },
      address: {
        required: "Please enter your Address",
        minlength: jQuery.validator.format("Please enter at least {0} characters.")
      },
      address1: {
        required: "Please enter your Address 1",
        minlength: jQuery.validator.format("Please enter at least {0} characters.")
      },
      address2: {
        required: "Please enter your Address 2",
        minlength: jQuery.validator.format("Please enter at least {0} characters.")
      },
      city: {
        required: "Please enter your city"
      },
      state: {
        required: "Please enter your Address 1"
      },
      pincode: {
        required: "Please enter your pincode",
        digits:"Please enter only digits.",
        minlength: jQuery.validator.format("Please enter at least {0} characters.")
      },
      address_proof: {
        required: "Please enter your Address 1"
      },
      permanent_address: {
        required: "Please enter your Address",
        minlength: jQuery.validator.format("Please enter at least {0} characters.")
      },
      permanent_address1: {
        required: "Please enter your Address 1",
        minlength: jQuery.validator.format("Please enter at least {0} characters.")
      },
      permanent_address2: {
        required: "Please enter your Address 2",
        minlength: jQuery.validator.format("Please enter at least {0} characters.")
      },
      permanent_city: {
        required: "Please enter your city"
      },
      permanent_state: {
        required: "Please enter your Address 1"
      },
      permanent_pincode: {
        required: "Please enter your pincode",
        digits:"Please enter only digits.",
        minlength: jQuery.validator.format("Please enter at least {0} characters.")
      },
      permanent_address_proof: {
        required: "Please enter your Address 1"
      },
      annual_income: {
        required: "Please select Annual Income"
      },
      net_worth: {
        required: "Please enter Net worth",
        digits:"Please enter only Numbers.",
        minlength:jQuery.validator.format("Please enter at least {0} characters.")
      },
      birthdate_2: {
        required: "Please select As on date"
      },
      occupation: {
        required: "Please select Occupation"
      }
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
    submitHandler: function(form) {
      form.submit();
    }
  });


/*
$("#tradingDetailsForm").validate({


rules: {
    Trading_exp: {
      required: true,
    
      year: {
      depends: function() {
          return $("#bonus-material").is(":checked");


                              // return ($('[name="Trading_exp"]:checked').val('1')?'true':'false'); 





      }
      }
      }
      
      
    
  },



    // rules: {
    //   year: {
        // required: true
        // {

        //   depends: function(){
        //     return $("#trade_exp_yes").is(':checked');
        //   }
        // }
      // }
      
    // },

    messages: {
        year : {
          required : "Please select Year"
        }
    },
    submitHandler: function(form) {
      form.submit();
    }

});
*/

$("#tradingDetailsForm").validate({
  rules: {
    year: {
        required: function(element) { 
                                  if($('[name="Trading_exp"]:checked').val('1'))
                                    { 
                                      if($('[name="year"]').val('0') && $('[name="month"]').val('0') )
                                      {
                                        // alert($('[name="year"]').val('0'));
                                         return true;
                                      }
                                        // return true;
                                    }
                                  else
                                    {
                                      return false;
                                    }
        }

      },
      month: {
        required: function(element) { 
                                  if($('[name="Trading_exp"]:checked').val('1')){return true}else{return false};
        }

      }

      },

    messages: {
        year : {
          required : "Please select Year"
        },
        month : {
          required : "Please select Month"
        }
    },
    submitHandler: function(form) {
      form.submit();
    }
  
});



});


// alert($('[name="Trading_exp"]:checked').val('1')?'true':'false');
 // alert($('[name="Trading_exp"]:checked').val());

