function populateUpdateModal(obj){
	$("#map_id").val($(obj).closest("tr").find('#id').text()); //role-subpermission id 

	var y = $(obj).closest("tr").find('#role_id').text();
 	$('[name="role_list_id"] option[value='+y+']').prop("selected",true);
	$("#role_name").val($(obj).closest("tr").find('#role_name').text());

	
	var x = $(obj).closest("tr").find('#user_id').text();
	$('[name="user_list_id"] option[value='+x+']').prop("selected",true);

}

function populateDeleteModal(obj){
	$("#map_id1").val($(obj).closest("tr").find('#id').text());
	}

function deleteMapping(){

	$.ajax({
		type: "post",
		url: $('#url_path').val() + '/role-user-delete',

		data: {user_role_map_id:$("#map_id1").val()},
		
		dataType: 'json',
		success: function(jsonResponse){
			
			if (jsonResponse.msg == undefined || jsonResponse.msg == null
					|| jsonResponse.msg.length == 0) {
				$("#top_msg").removeClass("alert alert-info").addClass(
						"alert alert-danger").text(jsonResponse.msg);
				$("#top_msg").show();
				jQuery('.scroll-to-top').click();
			} else {
				$("#top_msg").removeClass("alert alert-danger").addClass(
						"alert alert-success").text(jsonResponse.msg);
				$("#top_msg").show();
				jQuery('.scroll-to-top').click();

			}

			setTimeout(function() {
				window.location.reload(1);
			}, 5000);
	 
		    
		}
		});
	
}

function updateRoleUser() {
	var id= $("#map_id").val();
	var role = document.getElementById("role_list_id");
	var rolename = role.options[role.selectedIndex].text;   
	var user_list_id = document.getElementById("user_list_id");
	var user_name = user_list_id.options[user_list_id.selectedIndex].text; 


	$.ajax({
		type: "post",
		url: $('#url_path').val() + '/role-user-update',

		data: {id:id,rolename:rolename,user_name:user_name},
		
		dataType: 'json',
		success: function(jsonResponse){
			


			if (jsonResponse.msg == undefined || jsonResponse.msg == null
					|| jsonResponse.msg.length == 0) {
				$("#top_msg").removeClass("alert alert-info").addClass(
						"alert alert-danger").text(jsonResponse.msg);
				$("#top_msg").show();
				jQuery('.scroll-to-top').click();
			}else if (jsonResponse.msg == 'Role-user mapping is already exist'){
				$("#top_msg").removeClass("alert alert-success").addClass(
				"alert alert-danger").text(jsonResponse.msg);
					$("#top_msg").show();
						jQuery('.scroll-to-top').click();
			} else {
				$("#top_msg").removeClass("alert alert-danger").addClass(
						"alert alert-success").text(jsonResponse.msg);
				$("#top_msg").show();
				jQuery('.scroll-to-top').click();

			}

			setTimeout(function() {
				window.location.reload(1);
			}, 5000);

		
	 
		    
		}
		});
	}



function createMapping(obj){
			$.ajax({
				type: "post",
				url: $('#url_path').val() + '/role-user-create-admin',

				data: {role_id:$("#create_role_id").val(),user_id:$("#create_users_id").val()},
				
				dataType: 'json',
				success: function(jsonResponse){
					
					if (jsonResponse.msg == undefined || jsonResponse.msg == null
							|| jsonResponse.msg.length == 0) {
						$("#top_msg").removeClass("alert alert-info").addClass(
								"alert alert-danger").text(jsonResponse.msg);
						$("#top_msg").show();
						jQuery('.scroll-to-top').click();
					}else if (jsonResponse.msg == 'Role-user mapping is already exist'){
						$("#top_msg").removeClass("alert alert-success").addClass(
						"alert alert-danger").text(jsonResponse.msg);
							$("#top_msg").show();
								jQuery('.scroll-to-top').click();
					} else {
						$("#top_msg").removeClass("alert alert-danger").addClass(
								"alert alert-success").text(jsonResponse.msg);
						$("#top_msg").show();
						jQuery('.scroll-to-top').click();

					}

					setTimeout(function() {
						window.location.reload(1);
					}, 5000);
			    
				}
				});
	
		}