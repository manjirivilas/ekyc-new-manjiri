$('#id').hide();
$('#client_id').hide();
$('#regulation_id').hide();

function rejectionReason() {
	$('#reason').show();
}
function approve() {
	$('#reason').hide();

}

function validateForm()
{
	if ($('#optionsRadios1').is(':checked')) {
		$('#myModal').modal('hide');
	$.ajax({
		type: "post",
		url:$('#url_path').val()+'/approve',
		data: {id:$('#id').val(),client_id:$('#client_id').val(),regulation_id:$('#regulation_id').val()},
		dataType: 'json',
		success: function(jsonResponse){
			if(jsonResponse.msg == undefined || jsonResponse.msg == null || jsonResponse.msg.length == 0)
			{
			$( "#top_msg" ).removeClass( "alert alert-info" ).addClass( "alert alert-danger" ).text(jsonResponse.msg);	
			$( "#top_msg" ).show();	  
		    jQuery('.scroll-to-top').click();
			}
			else
			{
				$( "#top_msg" ).removeClass( "alert alert-danger" ).addClass( "alert alert-success" ).text(jsonResponse.msg);
			    $( "#top_msg" ).show();	  
			    jQuery('.scroll-to-top').click();
				
			}
			
		    setTimeout(function(){
		    	   window.location.reload(1);
		    	}, 5000);	
	    
		}
	});
	
	}
	else if ($('#optionsRadios2').is(':checked')) 
	{
		if($('#reason').val()!='')
		{
			$('#myModal').modal('hide');
$.ajax({
	type: "post",
	url:$('#url_path').val()+'/reject',
	data: {id:$('#id').val(),client_id:$('#client_id').val(),reason:$('#reason').val(),regulation_id:$('#regulation_id').val()},
	dataType: 'json',
	success: function(jsonResponse){
		if(jsonResponse.msg == undefined || jsonResponse.msg == null || jsonResponse.msg.length == 0)
		{
		$( "#top_msg" ).removeClass( "alert alert-info" ).addClass( "alert alert-danger" ).text(jsonResponse.msg);	
		$( "#top_msg" ).show();	  
	    jQuery('.scroll-to-top').click();
		}
		else
		{
			$( "#top_msg" ).removeClass( "alert alert-danger" ).addClass( "alert alert-success" ).text(jsonResponse.msg);
		    $( "#top_msg" ).show();	  
		    jQuery('.scroll-to-top').click();
			
		}
		
	    setTimeout(function(){
	    	   window.location.reload(1);
	    	}, 5000);	
    
	}
});
		}
		else
		{
			$( "#top_msg1" ).removeClass( "alert alert-info" ).addClass( "alert alert-danger" ).text('Enter rejection reason');	
			$( "#top_msg1" ).show();	  
			 $('#myModal').scrollTop(0);
			 $("#top_msg1").fadeOut(3000);
		}	
   }

	else
	{
		$( "#top_msg1" ).removeClass( "alert alert-info" ).addClass( "alert alert-danger" ).text('Please approve or reject fatca regulation');	
		$( "#top_msg1" ).show();	  

		 $('#myModal').scrollTop(0);
		 $("#top_msg1").fadeOut(3000);

  }
}
