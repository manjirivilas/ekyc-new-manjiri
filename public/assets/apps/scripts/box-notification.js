

function validateform() {

	var notification = $("input:radio[name='membership']:checked").val();
	var title = $('#text1').val();
	var from_date = $('#datepicker').val();
	var to_date = $('#datepicker_1').val();
	var link = $('#text2').val();
	var desc = $('#desc').val();
	var d = new Date().toISOString().slice(0, 10);

	if (notification == '' || title == '' || from_date == '' || to_date == ''
			|| desc == '') {
		$("#top_msg").removeClass().addClass("alert alert-danger").text(
				"Fill all the required (*) fields ");
		$("#top_msg").show();
		jQuery('.scroll-to-top').click();
		return false;

	}
	else if (from_date < d) {
		$("#top_msg").removeClass().addClass("alert alert-danger").text(
				"From date should be current or future date ");
		$("#top_msg").show();
		jQuery('.scroll-to-top').click();
		return false;

	}

	else if (to_date < from_date) {
		$("#top_msg").removeClass().addClass("alert alert-danger").text(
				"To date should be current or future date");
		$("#top_msg").show();
		jQuery('.scroll-to-top').click();
		return false;

	}
	else {
		if(link!==""){
		
		if (isUrlValid(link) == false) {
			$("#top_msg").removeClass("alert alert-info").addClass(
					"alert alert-danger").text("Enter URL in proper format");
			$("#top_msg").show();
			jQuery('.scroll-to-top').click();
			return false;
			
		}
		}
		
	}
	
	
		var arr_notification = [ title, from_date, to_date, link, desc,
				notification ]
		// alert(arr_notification);
		$.ajax({
			type : "post",
			url : $('#url_path').val() + '/box-notification',
			// url: '{{ URL::route("get-offer-details") }}',
			data : {
				notification : arr_notification
			},
			dataType : 'json',
			success : function(jsonResponse) {

				if (jsonResponse.msg == undefined || jsonResponse.msg == null
						|| jsonResponse.msg.length == 0) {
					$("#top_msg").removeClass("alert alert-info").addClass(
							"alert alert-danger").text(jsonResponse.msg);
					$("#top_msg").show();
					jQuery('.scroll-to-top').click();
				} else {
					$("#top_msg").removeClass("alert alert-danger").addClass(
							"alert alert-success").text(jsonResponse.msg);
					$("#top_msg").show();
					jQuery('.scroll-to-top').click();

				}

				setTimeout(function() {
					window.location.reload(1);
				}, 5000);

			}
		});



}
function isUrlValid(url) {
	return /^(https?|s?ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i
			.test(url);
}


function showErrormessages(){
	
	$("#top_msg").show();
	
	
}
function hideErrormessages(){
	
	$("#top_msg").hide();
	
}
function notificationFromDate() {

	hideErrormessages()
	var d = new Date().toISOString().slice(0, 10);
	var from_date = $('#datepicker').val();
	var to_date = $('#datepicker_1').val();
	
	if (from_date < d) {
		$("#top_msg").removeClass().addClass("alert alert-danger").text(
				"From date should be current or future date");
		showErrormessages();
		jQuery('.scroll-to-top').click();
	}
	
	if (to_date < from_date && to_date != '') {
		$("#top_msg").removeClass().addClass("alert alert-danger").text(
				"To date should be current or future date");
		showErrormessages();
		jQuery('.scroll-to-top').click();
	}
	
	
}

$('#datepicker').click(function(){
    $('#ui-datepicker-div').css("z-index", "+99");
  });

$('#datepicker_1').click(function(){
	   $('#ui-datepicker-div').css("z-index", "+99");
	  });



