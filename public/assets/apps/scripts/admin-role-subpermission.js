function createMapping(obj){
			$.ajax({
				type: "post",
				url: $('#url_path').val() + '/admin-rolesubpermission-create',

				data: {role_id:$("#create_role_id").val(),sub_permission_id:$("#create_subpermission_id").val()},
				
				dataType: 'json',
				success: function(jsonResponse){
					
					if (jsonResponse.msg == undefined || jsonResponse.msg == null
							|| jsonResponse.msg.length == 0) {
						$("#top_msg").removeClass("alert alert-info").addClass(
								"alert alert-danger").text(jsonResponse.msg);
						$("#top_msg").show();
						jQuery('.scroll-to-top').click();
					}else if (jsonResponse.msg == 'Role-subpermission mapping is already exist'){
						$("#top_msg").removeClass("alert alert-success").addClass(
						"alert alert-danger").text(jsonResponse.msg);
							$("#top_msg").show();
								jQuery('.scroll-to-top').click();
					} else {
						$("#top_msg").removeClass("alert alert-danger").addClass(
								"alert alert-success").text(jsonResponse.msg);
						$("#top_msg").show();
						jQuery('.scroll-to-top').click();

					}

					setTimeout(function() {
						window.location.reload(1);
					}, 5000);
			    
				}
				});
	
		}

function populateDeleteModal(obj){
	$("#map_id1").val($(obj).closest("tr").find('#id').text());
	}

function deleteMapping(obj){
	$.ajax({
		type: "post",
		url: $('#url_path').val() + '/role-subpermission-delete',

		data: {sub_permission_id:$("#map_id1").val()},
		
		dataType: 'json',
		success: function(jsonResponse){
			
			if (jsonResponse.msg == undefined || jsonResponse.msg == null
					|| jsonResponse.msg.length == 0) {
				$("#top_msg").removeClass("alert alert-info").addClass(
						"alert alert-danger").text(jsonResponse.msg);
				$("#top_msg").show();
				jQuery('.scroll-to-top').click();
			} else {
				$("#top_msg").removeClass("alert alert-danger").addClass(
						"alert alert-success").text(jsonResponse.msg);
				$("#top_msg").show();
				jQuery('.scroll-to-top').click();

			}

			setTimeout(function() {
				window.location.reload(1);
			}, 5000);
	 
		    
		}
		});
	
}

function populateUpdateModal(obj){
	
 	$("#map_id").val($(obj).closest("tr").find('#id').text()); //role-subpermission id 

	var y = $(obj).closest("tr").find('#role_id').text();
 	$('[name="role_subpermisson"] option[value='+y+']').prop("selected",true);
	$("#role_name").val($(obj).closest("tr").find('#role_name').text());

	
	var x = $(obj).closest("tr").find('#sub_permission_id').text();
	$('[name="sub_permission"] option[value='+x+']').prop("selected",true);

}

function updateSubPermission() {
	var id= $("#map_id").val();
	var role = document.getElementById("role_subpermisson");
	var rolename = role.options[role.selectedIndex].text;    
	var sub_permission = document.getElementById("sub_permission");
	var sub_permission_name = sub_permission.options[sub_permission.selectedIndex].text;    

	$.ajax({
		type: "post",
		url: $('#url_path').val() + '/role-subpermission-update',

		data: {id:id,rolename:rolename,sub_permission_name:sub_permission_name},
		
		dataType: 'json',
		success: function(jsonResponse){
			


			if (jsonResponse.msg == undefined || jsonResponse.msg == null
					|| jsonResponse.msg.length == 0) {
				$("#top_msg").removeClass("alert alert-info").addClass(
						"alert alert-danger").text(jsonResponse.msg);
				$("#top_msg").show();
				jQuery('.scroll-to-top').click();
			} else if (jsonResponse.msg == 'Role-subpermission mapping is already exist'){
				$("#top_msg").removeClass("alert alert-success").addClass(
				"alert alert-danger").text(jsonResponse.msg);
					$("#top_msg").show();
						jQuery('.scroll-to-top').click();
			} else {
				$("#top_msg").removeClass("alert alert-danger").addClass(
						"alert alert-success").text(jsonResponse.msg);
				$("#top_msg").show();
				jQuery('.scroll-to-top').click();

			}

			setTimeout(function() {
				window.location.reload(1);
			}, 5000);

		
	 
		    
		}
		});
	}