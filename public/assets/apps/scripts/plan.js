var mf_nominee_flag=0;div_status = 2;

$(document).ready(function () {
$('#holder').css("border", "1px solid #01b792");
$('#holdermf').css("border", "1px solid #01b792");


// Select plan
$("#tradechk").change(function () {
    if($('#tradechk').is(':checked') == true){

      $('#holder').css("border", "1px solid #01b792");
    }
    else
    {
      $('#holder').css("border", "1px solid #f4f5f6");
    }

});

$("#mfchk").change(function () {
    if($('#mfchk').is(':checked') == true){
      $('#holdermf').css("border", "1px solid #01b792");
    }
    else
    {
      $('#holdermf').css("border", "1px solid #f4f5f6");
      $("#eistac").css("display", "block");
    }

});



// Select products i.e. demat and existing account

$("#dematchk").change(function () {

  if($('#dematchk').is(':checked') == true){
    $('#dmaholder').css("border", "1px solid #01b792");
    $("#ndmaholder").css("display", "none");
    $(".add_nondemat_sec").css("display", "none");
    $("#existac").css("display", "none");
    $("#dematac").css("display", "block");

  }
  else
  {
    $( "#nondematchk" ).prop( "checked", true );
    $('#dmaholder').css("border", "1px solid #f4f5f6");
    $("#ndmaholder").css("display", "block");
    $(".add_nondemat_sec").css("display", "block");
    $("#existac").css("display", "block");
    $("#dematac").css("display", "none");
  }

});

$("#nondematchk").change(function () {
  if($('#dematchk').is(':checked') == true){

  }
  else
  {
    $( "#dematchk" ).prop( "checked", true );
    $('#dmaholder').css("border", "1px solid #01b792");
      $("#ndmaholder").css("display", "none");
    $(".add_nondemat_sec").css("display", "none");
    $("#existac").css("display", "none");
    $("#dematac").css("display", "block");
  }

});


$("#segment-equity").click(function () {
     $('input:checkbox[class="equity_chk"]').not(this).prop('checked', this.checked);
 });


 $("#segment-commodity").click(function () {
     $('input:checkbox[class="commodity-chk"]').not(this).prop('checked', this.checked);
 });

 $("#segment-currency").click(function () {
     $('input:checkbox[class="currency-chk"]').not(this).prop('checked', this.checked);
 });

/*select segment options js*/
 $("#select-segment-list1 li").click(function(e){
     e.preventDefault();
     var selText = $(this).find(".select-seg-opt").text();
     $("#select-segment1 .select-segment-txt").val(selText);
 });

$('#nominee_datetime').datetimepicker({
                icons: {
                    time: "fa fa-clock-o",
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down"
                },
                format: 'DD.MM.YYYY'
              });

$('#nominee_datetime_1').datetimepicker({
                icons: {
                    time: "fa fa-clock-o",
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down"
                },
                format: 'DD.MM.YYYY'
              });

$('#nominee_datetime_2').datetimepicker({
                icons: {
                    time: "fa fa-clock-o",
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down"
                },
                format: 'DD.MM.YYYY'
              });

 

 $("#nominee-count").text(div_status);

   $("body").on('click', ".add-det", function(){

    mf_nominee_flag++;
    $('#nominee_flag').val(mf_nominee_flag);


    add_nominee_div(mf_nominee_flag);
   

});


$("body").on('click', ".add-remove-nom2", function(){

  remove_nominee_div(mf_nominee_flag,"2");

// mf_nominee_flag --;
});

$("body").on('click', ".add-remove-nom1", function(){

  remove_nominee_div(mf_nominee_flag,"1");
  

});

$("#select-segment-list2 li").click(function(e){
     e.preventDefault();
     var selText = $(this).find(".select-seg-opt").text();
     $("#select-segment2 .select-segment-txt").val(selText);
 });

 $("#select-segment-list3 li").click(function(e){
     e.preventDefault();
     var selText = $(this).find(".select-seg-opt").text();
     $("#select-segment3 .select-segment-txt").val(selText);
 });

 $('.modal-toggle').click(function (e) {
   var plan_div = $(this).attr("href");
   // alert();
  $('.pricing').removeClass('pricing-active');
    $(plan_div).addClass('pricing-active');
                     });


});

function add_nominee_div (nom_flag){

  $(".bank-account-sec.add-account"+nom_flag).removeClass('add-account');
  $(".add-remove").removeClass('add-account');

  div_status--;
   if(nom_flag == 2){
    $(".add-an-nominee-rw").addClass('add-account');
   }
   
  $("#nominee-count").text(div_status);

}

function remove_nominee_div (nom_flag,div_id){

  if((nom_flag == 2) && (div_id == 1))
  {
    // alert("Name: "+$('#t_name').val() +" Relation: "+$('#t_name').val() +" DOB: "+$('#t_dob').val() +" Add: "+$('#t_dob').val());
   // if($(".bank-account-sec.add-account"+nom_flag).is(":visible") && (($('#t_name').val() != "") || ($('#t_relation').val() != "") || ($('#t_dob').val() != "") || ($('#t_address').val() != "")))
   if($(".bank-account-sec.add-account"+nom_flag).is(":visible"))
   {
      $('#s_name').val($('#t_name').val());
      $('#s_relation').val($('#t_relation').val());
      $('#s_dob').val($('#t_dob').val());
      $('#s_address').val($('#t_address').val());

      $('#t_name').val("");
      $('#t_relation').val("");
      $('#t_dob').val("");
      $('#t_address').val("");

      $(".bank-account-sec.add-account"+nom_flag).addClass('add-account');
      $(".add-remove-nom"+nom_flag).addClass('add-account');
   }
   mf_nominee_flag --;
   div_status++;
  }
  else
  {
    $(".bank-account-sec.add-account"+nom_flag).addClass('add-account');
    $(".add-remove-nom"+nom_flag).addClass('add-account');
    if(nom_flag == 2)
    {
      $('#t_name').val("");
      $('#t_relation').val("");
      $('#t_dob').val("");
      $('#t_address').val("");
    }
    else
    {
      $('#s_name').val("");
      $('#s_relation').val("");
      $('#s_dob').val("");
      $('#s_address').val("");
    }
     mf_nominee_flag --;
     div_status++;

  }
   
  


    if(nom_flag <= 2){
      $(".add-an-nominee-rw").removeClass('add-account');
      $("#nominee-count").text(div_status);
    }
  
   // alert(nominee_flag);
}
