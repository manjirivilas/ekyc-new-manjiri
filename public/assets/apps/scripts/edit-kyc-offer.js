function populateAddModal(obj) {
	var discount = $(obj).closest("tr").find('#discount').text();
	var coupon = $(obj).closest("tr").find('#coupon').text();
	$("#offer_name").val($(obj).closest("tr").find('#name').text());
	$("#date").val($(obj).closest("tr").find('#from_date').text());
	$("#date1").val($(obj).closest("tr").find('#to_date').text());
	$("#offer_id").val($(obj).closest("tr").find('#offerid').text());
	$("#min-2").val($(obj).closest("tr").find('#min').text());

	$('#coupon_type').val($(obj).closest("tr").find('#coupon').text());

	if(coupon)
	{
		$('#dis-1').hide();
		$('#dis').hide();
		$('#code-1').show();
		$('#code-2').show();
		$('#off-1').hide();
		$('#off-2').hide();
		$('#min-1').hide();
		$('#min-2').hide();
		$('#trade_count-1').show();
		$('#trade_count-2').show();
		$('#pricing_plan-1').show();
		$('#pricing_plan-2').show();
		$('#code-2').val($(obj).closest("tr").find('#code').text());
		$('#trade_count-2').val($(obj).closest("tr").find('#trade-count').text());
		$('#pricing_plan-2').val($(obj).closest("tr").find('#pricing_plan').text());
	}
	else if (discount == '') {
		$('#dis-1').hide();
		$('#dis').hide();
		$('#code-1').show();
		$('#code-2').show();
		$('#off-1').show();
		$('#off-2').show();
		$('#min-1').show();
		$('#min-2').show();
		$('#trade_count-1').hide();
		$('#trade_count-2').hide();
		$('#pricing_plan-1').hide();
		$('#pricing_plan-2').hide();
		$('#code-2').val($(obj).closest("tr").find('#code').text());
		$('#off-2').val($(obj).closest("tr").find('#off').text());
		$('#min-2').val($(obj).closest("tr").find('#min').text());

	} else {
		$('#code-1').hide();
		$('#code-2').hide();
		$('#off-1').hide();
		$('#off-2').hide();
		$('#min-1').hide();
		$('#min-2').hide();
		$('#dis-1').show();
		$('#dis').show();
		$('#trade_count-1').hide();
		$('#trade_count-2').hide();
		$('#pricing_plan-1').hide();
		$('#pricing_plan-2').hide();
		$("#dis").val($(obj).closest("tr").find('#discount').text());
	}

}
function updateOffer() {
	var coupon = $('#coupon_type').val();
	var data;

if(coupon)//coupon
{
	

	var flag = -1;
	data = {
		id : $('#offer_id').val(),
		name : $('#offer_name').val(),
		from_date : $('#date').val(),
		to_date : $("#date1").val(),
		code : $('#code-2').val(),
		trade_count : $('#trade_count-2').val(),
		pricing_plan : $('#pricing_plan-2').val(),
		flag : flag
	}
}
else if ($(dis).is(":visible")) {
		var flag = 0;
		data = {
			id : $('#offer_id').val(),
			name : $('#offer_name').val(),
			from_date : $('#date').val(),
			to_date : $("#date1").val(),
			discount : $("#dis").val(),
			flag : flag
		}

	} else

	{
		var flag = 1;

		data = {
			id : $('#offer_id').val(),
			name : $('#offer_name').val(),
			from_date : $('#date').val(),
			to_date : $("#date1").val(),
			code : $('#code-2').val(),
			offer : $('#off-2').val(),
			min_val : $('#min-2').val(),
			flag : flag
		}

	}

	$.ajax({
		type : "post",
		url : $('#url_path').val() + '/update-offer',
		data : {
			data : data
		},
		dataType : 'json',
		success : function(jsonResponse) {
			if (jsonResponse.msg == undefined || jsonResponse.msg == null
					|| jsonResponse.msg.length == 0) {
				$("#top_msg").removeClass("alert alert-info").addClass(
						"alert alert-danger").text(jsonResponse.msg);
				$("#top_msg").show();
				jQuery('.scroll-to-top').click();
			} else {
				$("#top_msg").removeClass("alert alert-danger").addClass(
						"alert alert-success").text(jsonResponse.msg);
				$("#top_msg").show();
				jQuery('.scroll-to-top').click();

			}

			setTimeout(function() {
				window.location.reload(1);
			}, 5000);

		}
	});
}

function deleteOffer() {

	id = $('#offer_id').val();
	$.ajax({
		type : "post",
		url : $('#url_path').val() + '/delete-offer',
		data : {
			id : id
		},
		dataType : 'json',
		success : function(jsonResponse) {
			if (jsonResponse.msg == undefined || jsonResponse.msg == null
					|| jsonResponse.msg.length == 0) {
				$("#top_msg").removeClass("alert alert-info").addClass(
						"alert alert-danger").text(jsonResponse.msg);
				$("#top_msg").show();
				jQuery('.scroll-to-top').click();
			} else {
				$("#top_msg").removeClass("alert alert-danger").addClass(
						"alert alert-success").text(jsonResponse.msg);
				$("#top_msg").show();
				jQuery('.scroll-to-top').click();

			}

			setTimeout(function() {
				window.location.reload(1);
			}, 5000);

		}
	});
}

function populateDeleteModal(obj) {
	$("#offer_id").val($(obj).closest("tr").find('#offerid').text());

}