$(document).ready(function () {

    $('.later-btn').on('click',function(){
      $(".proc-btn").removeClass('dis');
      $(".pay-nw-btn1").removeClass('dis');
      $(".pay-later-sec").removeClass('dis');
      $(".last-pay-sec").removeClass('br-btm');
      $(".later-btn").addClass('dis');
      $(".pay-nw-btn").addClass('dis');
      $(".btn-rw").addClass('padding-tp');
});





    $('input[type="radio"]').click(function() {
       if($(this).attr('id') == 'add-demat') {
            $('.exist-demat-ac').show();
            document.getElementById("demat-ac").readOnly = false;
            document.getElementById("dp-id").readOnly = false;
            document.getElementById("bo-id").readOnly = false;
       }

       else {
            $('.exist-demat-ac').hide();
            document.getElementById("demat-ac").readOnly = true;
            document.getElementById("dp-id").readOnly = true;
            document.getElementById("bo-id").readOnly = true;
       }
   });


// Show/Hide Address
  $("#same_address").change(function () {
    //alert($('#same_address').is(':checked'))
      $('.permanent-add').toggle();
  });


 /*edit sections*/
    $('#personal-details-edit').click(function() {
      document.getElementById("fathername").readOnly = false;
      document.getElementById("mothername").readOnly = false;
      document.getElementById("birthdate").readOnly = false;
      document.getElementById("male").disabled = false;
      document.getElementById("female").disabled = false;
      document.getElementById("email").readOnly = false;
      document.getElementById("mobile").readOnly = false;
      document.getElementById("married").disabled = false;
      document.getElementById("unmarried").disabled = false;
      $("#personal-details-save").css("display", "block");
      $("#personal-details-edit").css("display", "none");
   });


    $('#personal-details-save').click(function() {
      document.getElementById("fathername").readOnly = true;
      document.getElementById("mothername").readOnly = true;
      document.getElementById("birthdate").readOnly = true;
      document.getElementById("male").disabled = true;
      document.getElementById("female").disabled = true;
      document.getElementById("email").readOnly = true;
      document.getElementById("mobile").readOnly = true;
      document.getElementById("married").disabled = true;
      document.getElementById("unmarried").disabled = true;
      $("#personal-details-edit").css("display", "block");
      $("#personal-details-save").css("display", "none");
   });


        $('#corr-add-edit').click(function() {
      document.getElementById("address").readOnly = false;
      document.getElementById("address1").readOnly = false;
      document.getElementById("address2").readOnly = false;
      document.getElementById("city").disabled = false;
      document.getElementById("state").disabled = false;
      document.getElementById("pincode").readOnly = false;
      document.getElementById("address_proof").readOnly = false;
      $("#corr-add-save").css("display", "block");
      $("#corr-add-edit").css("display", "none");
   });


    $('#corr-add-save').click(function() {
      document.getElementById("address").readOnly = true;
      document.getElementById("address1").readOnly = true;
      document.getElementById("address2").readOnly = true;
      document.getElementById("city").disabled = true;
      document.getElementById("state").disabled = true;
      document.getElementById("pincode").readOnly = true;
      document.getElementById("address_proof").readOnly = true;
      $("#corr-add-save").css("display", "none");
      $("#corr-add-edit").css("display", "block");
   });


    $('#permanet-add-edit').click(function() {
      document.getElementById("permanent_address").readOnly = false;
      document.getElementById("permanent_address1").readOnly = false;
      document.getElementById("permanent_address2").readOnly = false;
      document.getElementById("permanent_city").disabled = false;
      document.getElementById("permanent_state").disabled = false;
      document.getElementById("permanent_pincode").readOnly = false;
      document.getElementById("permanent_address_proof").readOnly = false;
      $("#permanet-add-save").css("display", "block");
      $("#permanet-add-edit").css("display", "none");
   });


    $('#permanet-add-save').click(function() {
      document.getElementById("permanent_address").readOnly = true;
      document.getElementById("permanent_address1").readOnly = true;
      document.getElementById("permanent_address2").readOnly = true;
      document.getElementById("permanent_city").disabled = true;
      document.getElementById("permanent_state").disabled = true;
      document.getElementById("permanent_pincode").readOnly = true;
      document.getElementById("permanent_address_proof").readOnly = true;
      $("#permanet-add-save").css("display", "none");
      $("#permanet-add-edit").css("display", "block");
   });


        $('#other-details-edit').click(function() {
      document.getElementById("annual-income").readOnly = false;
      document.getElementById("net-worth").readOnly = false;
      document.getElementById("as-on").readOnly = false;
      document.getElementById("occuption").readOnly = false;
      $("#other-details-save").css("display", "block");
      $("#other-details-edit").css("display", "none");
   });


    $('#other-details-save').click(function() {
      document.getElementById("annual-income").readOnly = true;
      document.getElementById("net-worth").readOnly = true;
      document.getElementById("as-on").readOnly = true;
      document.getElementById("occuption").readOnly = true;
      $("#other-details-save").css("display", "none");
      $("#other-details-edit").css("display", "block");
   });

        $('#bank-details-edit').click(function() {
      document.getElementById("bank-name").readOnly = false;
      document.getElementById("branch-name").readOnly = false;
      document.getElementById("ifsc-code").readOnly = false;
      document.getElementById("br-ac-no").readOnly = false;
      document.getElementById("micr-no").readOnly = false;
      document.getElementById("saving").disabled = false;
      document.getElementById("Current").disabled = false;
      document.getElementById("branch-add").readOnly = false;
      $("#bank-details-save").css("display", "block");
      $("#bank-details-edit").css("display", "none");
   });

    $('#fatca-details-edit').click(function() {
      document.getElementById("fatca-yes").disabled = false;
      document.getElementById("fatca-no").disabled = false;
      $("#fatca-details-save").css("display", "block");
      $("#fatca-details-edit").css("display", "none");
   });


        $('#fatca-details-save').click(function() {
      document.getElementById("fatca-yes").disabled = true;
      document.getElementById("fatca-no").disabled = true;
      $("#fatca-details-save").css("display", "none");
      $("#fatca-details-edit").css("display", "block");
   });


    $('#demat-details-edit').click(function() {
      document.getElementById("vns-demat").disabled = false;
      document.getElementById("add-demat").disabled = false;
      document.getElementById("demat-ac").readOnly = false;
      document.getElementById("dp-id").readOnly = false;
      document.getElementById("bo-id").readOnly = false;
      $("#demat-details-save").css("display", "block");
      $("#demat-details-edit").css("display", "none");
   });


        $('#demat-details-save').click(function() {
      document.getElementById("vns-demat").disabled = true;
      document.getElementById("add-demat").disabled = true;
      document.getElementById("demat-ac").readOnly = true;
      document.getElementById("dp-id").readOnly = true;
      document.getElementById("bo-id").readOnly = true;
      $("#demat-details-save").css("display", "none");
      $("#demat-details-edit").css("display", "block");
   });


   $('#trading-details-edit').click(function() {
      document.getElementById("nominee-1").readOnly = false;
      document.getElementById("nominee-2").readOnly = false;
      document.getElementById("yes").disabled = false;
      document.getElementById("no").disabled = false;
      $("#trading-details-save").css("display", "block");
      $("#trading-details-edit").css("display", "none");
      $(".plan-sec-rw").css("display", "none");
      $(".edit-trading-sec").css("display", "block");
});


   $('#trading-details-save').click(function() {
      document.getElementById("nominee-1").readOnly = true;
      document.getElementById("nominee-2").readOnly = true;
      document.getElementById("yes").disabled = true;
      document.getElementById("no").disabled = true;
      $("#trading-details-save").css("display", "none");
      $("#trading-details-edit").css("display", "block");
      $(".plan-sec-rw").css("display", "block");
      $(".edit-trading-sec").css("display", "none");
   });



/*select segment options js*/
 $("#select-segment-list1 li").click(function(e){
     e.preventDefault();
     var selText = $(this).find(".select-seg-opt").text();
     $("#select-segment1 .select-segment-txt").val(selText);
 });

  $("#select-segment-list2 li").click(function(e){
     e.preventDefault();
     var selText = $(this).find(".select-seg-opt").text();
     $("#select-segment2 .select-segment-txt").val(selText);
 });

 $("#select-segment-list3 li").click(function(e){
     e.preventDefault();
     var selText = $(this).find(".select-seg-opt").text();
     $("#select-segment3 .select-segment-txt").val(selText);
 });


$("input[name='selector5']").on("click", function(){

  $(".country-add").toggle();

});

/*mobi tab js*/
 if ($(window).width() < 767) {
  $('div.bank-account-sec').each(function() {
    var $dropdown = $(this);

    $("span.edit-title-main", $dropdown).click(function(e) {
      e.preventDefault();
      $dropdown.toggleClass('mobi-add-bank-sec');
      $("div.bank-account-sec").not($dropdown).removeClass('mobi-add-bank-sec');
      $(".bank-account-sec .edit-btn").not(".mobi-add-bank-sec .edit-btn").css('display', 'none');
      $(".bank-account-sec .save-btn").not(".mobi-add-bank-sec .save-btn").css('display', 'none');
      $(".bank-account-sec.mobi-add-bank-sec .edit-btn").css('display', 'block');
      $div = $("div.bank-account-inner-sec", $dropdown);
      $div.toggle();
      $dropdown.find('.mobi-add .fa').toggleClass('fa-minus').toggleClass('fa-plus');
      $("div.bank-account-inner-sec").not($div).hide();
      $("div.bank-account-sec").not($dropdown).find('.mobi-add .fa.fa-minus').removeClass('fa-minus').addClass('fa-plus');
      return false;
    });

});
    
}
});






