$(document).ready(function () {


$("#selectPlanForm").validate({
    rules: {
      product_chk: {
        required: true
      }
      
    },

    messages: {
        product_chk : {
          required : "Please select atleast one product"
        }
    },
    errorPlacement: function(error, element) {
      
        error.insertAfter("#trade_div");
    },
    submitHandler: function(form) {
      form.submit();
    }

});

$("#selectSegmentForm").validate({
    rules: {
      select_segment: {
         required: true
      },
      equity:{
        required :function(element) { 
                                  if($('#segment-equity').is(':checked'))
                                    { 
                                      return true;
                                    }
                                  else
                                    {
                                      return false;
                                    };
        }
      },
      commodity:{
        required :function(element) { 
                                  if($('#segment-commodity').is(':checked'))
                                    { 
                                      return true;
                                    }
                                  else
                                    {
                                      return false;
                                    };
        }
      },
       currency:{
        required :function(element) { 
                                  if($('#segment-currency').is(':checked'))
                                    { 
                                      return true;
                                    }
                                  else
                                    {
                                      return false;
                                    };
        }
      }
    },

    messages: {
        select_segment : {
          required : "Please select atleast one Segment"
        },
        equity : {
          required : "Please select atleast one Equity Plan"
        },
        commodity : {
          required : "Please select atleast one Commodity Plan"
        },
        currency : {
          required : "Please select atleast one Currency Plan"
        }
    },
    errorPlacement: function(error, element) {
      
        error.insertAfter("#trade_div");
    },
    submitHandler: function(form) {
      form.submit();
    }

});


$("#selectProduct").validate({
    rules: {
      demat_name:{
        required :function(element) { 
                                  if($('#nondematchk').is(':checked'))
                                    { 
                                      return true;
                                    }
                                  else
                                    {
                                      return false;
                                    };
        }
      },
      dp_id:{
        required :function(element) { 
                                  if($('#nondematchk').is(':checked'))
                                    { 
                                      return true;
                                    }
                                  else
                                    {
                                      return false;
                                    };
        }
      },
       bo_id:{
        required :function(element) { 
                                  if($('#nondematchk').is(':checked'))
                                    { 
                                      return true;
                                    }
                                  else
                                    {
                                      return false;
                                    };
        }
      }
    },

    messages: {
        demat_name : {
          required : "Please enter Demat Account Name"
        },
        dp_id : {
          required : "Please enter DP ID"
        },
        bo_id : {
          required : "Please enter BO ID"
        }
    },
    // errorPlacement: function(error, element) {
      
    //     error.insertAfter("#trade_div");
    // },
    submitHandler: function(form) {
      form.submit();
    }

});
// alert($('.equity_chk').is(':checked'));


$("#selectSegmentForm").validate({
    rules: {
      select_segment: {
         required: true
      },
      equity:{
        required :function(element) { 
                                  if($('#segment-equity').is(':checked'))
                                    { 
                                      return true;
                                    }
                                  else
                                    {
                                      return false;
                                    };
        }
      },
      commodity:{
        required :function(element) { 
                                  if($('#segment-commodity').is(':checked'))
                                    { 
                                      return true;
                                    }
                                  else
                                    {
                                      return false;
                                    };
        }
      },
       currency:{
        required :function(element) { 
                                  if($('#segment-currency').is(':checked'))
                                    { 
                                      return true;
                                    }
                                  else
                                    {
                                      return false;
                                    };
        }
      }
    },

    messages: {
        select_segment : {
          required : "Please select atleast one Segment"
        },
        equity : {
          required : "Please select atleast one Equity Plan"
        },
        commodity : {
          required : "Please select atleast one Commodity Plan"
        },
        currency : {
          required : "Please select atleast one Currency Plan"
        }
    },
    errorPlacement: function(error, element) {
      
        error.insertAfter("#trade_div");
    },
    submitHandler: function(form) {
      form.submit();
    }

});

});




