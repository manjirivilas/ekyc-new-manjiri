/**
 * function to fetch all the dtails of client
 */


function showDetails(){	
	var password = $('#password').val();
	var new_password = $('#new_password').val();
	var confirm_password = $('#confirm_password').val();
	//var data_array = [password,new_password];
	if(password!="" && new_password!="" && confirm_password!=""){	
		if((new_password) != (confirm_password)){
			$( "#top_msg" ).removeClass( "alert alert-info" ).addClass( "alert alert-danger" ).text("Password mismatch !");
		    $( "#top_msg" ).show();	  
		    $( "#top_msg" ).fadeOut(3000);   
		}else{
			$.ajax({
				type: "post",
				url:$('#url_path').val()+'/reset-client-password',
				data:{data:new_password},
				dataType: 'json',
				headers: {
			        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
			    },
				success: function(jsonResponse){
					
					 if(jsonResponse.success == true){
						 $( "#top_msg" ).removeClass( "alert alert-danger" ).addClass( "alert alert-info" ).text("Password updated successfully");
						    $( "#top_msg" ).show();	  
						    $('.scroll-to-top').click();
						    $( "#top_msg" ).fadeOut(3000);
						
						}
					 setTimeout(function(){
						   window.location.reload(1);
						}, 4000);
				}
			});
		}
		
		
	}else{
		$( "#top_msg" ).removeClass( "alert alert-info" ).addClass( "alert alert-danger" ).text("Please fill all the mandatory fields");
	    $( "#top_msg" ).show();	  
	    $('.scroll-to-top').click();
	    $( "#top_msg" ).fadeOut(3000);
	}
	}



$("#password").blur(function(){
	var password = $('#password').val();
	if(password){
	$.ajax({
		type: "post",
		url:$('#url_path').val()+'/check-old-password',
		data:{data:password},
		dataType: 'json',
		headers: {
	        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
	    },
		success: function(jsonResponse){
			
			if(jsonResponse.success == false){
				$("#error").show();
				document.getElementById("btn1").disabled = true;
				return false;
				}else if(jsonResponse.success == true){
				$("#error").hide();
				document.getElementById("btn1").disabled = false;
				return true;
				}		
		}
	});
	}
	
});
