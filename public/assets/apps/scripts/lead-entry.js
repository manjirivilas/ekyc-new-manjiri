
function isNumber(evt) { // function to check and allow numbers only
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

function isAlfa(evt) {
  evt = (evt) ? evt : window.event;
  var charCode = (evt.which) ? evt.which : evt.keyCode;
  if (charCode > 32 && (charCode < 65 || charCode > 90) && (charCode < 97 || charCode > 122)) {
    return false;
  }
  return true;
}
function IsAlphaNumeric(e) {
            var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
			var key = keyCode;
            if((key == 8) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105)) 
             return true;
			else
			 return false;
        }
/**
 * function to fetch all the dtails of client
 */


/**
 * Phone no field validations
 */
	function phoneVal(){
		if($("#mobile_no").val().length != 10){
			
			 $("#mobile").html('Phone number should be 10 digit');
		}else{
			$("#mobile").html('');
		}
	}

	/**
	 * Introducer_mobile_no  field validations
	 */
		function phoneVal(){
			if($("#introducer_mobile_no").val().length != 10){

				 $("#introducer_mobile_no").html('Phone number should be 10 digit');
			}else{
				$("#introducer_mobile_no").html('');
			}
		}
	

	/**
	 * reset T-pin
	 */
	
	function showDetails(objForm){
		var email_id =document.getElementById('email_id').value;
		var lead_name = document.getElementById("lead_name").value;
		var mobile_no = document.getElementById("mobile_no").value;;
		var lead_source= document.getElementById("mySelect").value;
		var referred_by = document.getElementById("referred_by").value;
		var introducer_mobile_no = document.getElementById("introducer_mobile_no").value;
    var description = document.getElementById("description").value;
		arr_data = [email_id,lead_name,mobile_no,lead_source,referred_by,introducer_mobile_no,description];
		
		if (email_id.length==0 || lead_name.length==0 || mobile_no.length==0 || lead_source.length==0)
		{
			$( "#top_msg" ).removeClass( "alert alert-info" ).addClass( "alert alert-danger" ).text("Please Fill All Required Fields");	
				$( "#top_msg" ).show();	  
			    jQuery('.scroll-to-top').click();
			    $( "#top_msg" ).fadeOut(5000);
				
		}	
		else
		{
			$('#btn1').prop('disabled', true);
			$.ajax({
			type: "post",
			url:$('#url_path').val()+'/create-lead',
			data:{data:arr_data},
			dataType: 'json',
			success: function(jsonResponse){
				if(jsonResponse.msg == undefined || jsonResponse.msg == null || jsonResponse.msg.length == 0)
				{
				$( "#top_msg" ).removeClass( "alert alert-info" ).addClass( "alert alert-danger" ).text(jsonResponse.msg);	
				$( "#top_msg" ).show();	  
			    jQuery('.scroll-to-top').click();
			    $( "#top_msg" ).fadeOut(5000);
				}
				else
				{
					
					$( "#top_msg" ).removeClass( "alert alert-danger" ).removeClass( "alert alert-info" ).addClass( "alert alert-success" ).text(jsonResponse.msg).append(jsonResponse.name);
				    $( "#top_msg" ).show();	  
				    jQuery('.scroll-to-top').click();
				}
				setTimeout(function(){
			    	   window.location.reload(1);
			    	}, 5000);
			}
		    });
		}
	}
	
	
	
	
	
	
