
function populateAddModal(obj) {
	$('#note_id').val($(obj).closest("tr").find('#nid').text());
	$("#note_title").val($(obj).closest("tr").find('#title').text());
	$("#note_link").val($(obj).closest("tr").find('#link').text());
	$("#notification_2").val($(obj).closest("tr").find('#from_date').text());
	$("#notification_1").val($(obj).closest("tr").find('#to_date').text());
	$("#desc1").val($(obj).closest("tr").find('#desc').text());

}

function updateNotification() {
	var id = $('#note_id').val();
	var title = $("#note_title").val();
	var from_date = $("#notification_2").val();
	var to_date = $("#notification_1").val();
	var link = $("#note_link").val();
	var desc = $("#desc1").val();
	var d = new Date().toISOString().slice(0, 10);
	var data = [ $('#note_id').val(), $("#note_title").val(),
			$("#note_link").val(), $("#notification_2").val(),
			$("#notification_1").val(), $("#desc1").val() ];

	if (title == '' || from_date == '' || to_date == '' || desc == '') {
		$("#top_msg1").removeClass().addClass("alert alert-danger").text(
				"Fill all the required (*) fields");
		$("#top_msg1").show();
		jQuery('.scroll-to-top').click();
		return false;
	} else
		{
		if(link!==""){
			alert('xscsac');
		if (isUrlValid(link) == false) {
		$("#top_msg1").removeClass("alert alert-info").addClass(
				"alert alert-danger").text("Enter URL in proper format");
		$("#top_msg1").show();
		 jQuery('.scroll-to-top').click();
		 return false;
			} 
		}
		}
		$('#myModal').modal('hide');
		$.ajax({
			type : "post",
			url : $('#url_path').val() + '/update-box-notification',
			// url: '{{ URL::route("get-offer-details") }}',
			data : {
				notification : data
			},
			dataType : 'json',
			success : function(jsonResponse) {

				if (jsonResponse.msg == undefined || jsonResponse.msg == null
						|| jsonResponse.msg.length == 0) {
					$("#top_msg").removeClass("alert alert-info").addClass(
							"alert alert-danger").text(jsonResponse.msg);
					$("#top_msg").show();
					jQuery('.scroll-to-top').click();
				} else {
					$("#top_msg").removeClass("alert alert-danger").addClass(
							"alert alert-success").text(jsonResponse.msg);
					$("#top_msg").show();
					jQuery('.scroll-to-top').click();

				}

			}
		});
		setTimeout(function() {
			window.location.reload(1);
		}, 4000);

	
}

function populateDeleteModal(obj) {
	$('#note_id').val($(obj).closest("tr").find('#nid').text());

}

function deleteNotification() {
	var id = $('#note_id').val();
	$.ajax({
		type : "post",
		url : $('#url_path').val() + '/delete-box-notification',
		data : {
			id : id
		},
		dataType : 'json',
		success : function(jsonResponse) {

			if (jsonResponse.msg == undefined || jsonResponse.msg == null
					|| jsonResponse.msg.length == 0) {
				$("#top_msg").removeClass("alert alert-info").addClass(
						"alert alert-danger").text(jsonResponse.msg);
				$("#top_msg").show();
				jQuery('.scroll-to-top').click();
			} else {
				$("#top_msg").removeClass("alert alert-danger").addClass(
						"alert alert-success").text(jsonResponse.msg);
				$("#top_msg").show();
				jQuery('.scroll-to-top').click();

			}

			setTimeout(function() {
				window.location.reload(1);
			}, 4000);

		}
	});
}
function showErrormessages(){
	$("#top_msg1").show();
	 $('#update_notification').prop('disabled', true);
	
}
function hideErrormessages(){
	$("#top_msg1").hide();
	 $('#update_notification').prop('disabled', false);
}
function notificationFromDate() {
	var d = new Date().toISOString().slice(0, 10);
	hideErrormessages()
	if ($("#notification_2").val() < d ) {
		$("#top_msg1").removeClass().addClass("alert alert-danger").text(
				"From date should be current or future date");
		showErrormessages();
		jQuery('.scroll-to-top').click();
	}
	
	 if ( $("#notification_1").val()<$("#notification_2").val()) {
		$("#top_msg1").removeClass().addClass("alert alert-danger").text(
				"To date should be greater than from date");
		showErrormessages();
		jQuery('.scroll-to-top').click();
	}
	
	
}

function closeModal() {
	$("#top_msg1").hide();

}

function isUrlValid(url) {
	return /^(https?|s?ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i
			.test(url);
}
