/*
 * to disable future date
 */
function verifyDate()
	{
		var newdate=$('#datepicker-margin').val();
		if( newdate!=''){
			$('#submit').prop('disabled', false);
		}
	}
//Button Disable/Enable
$('#chkactall').click(function(e){
    var table= $(e.target).closest('table');
   var numOfVisibleRows = $('tr:visible');
   $('td input:checkbox',table).prop('checked',e.target.checked);
});


$('#chkactall').change(function(){
	var search = $('.input-small').val();
	if ($('#chkactall').is(':checked')) {
		var table=$('#sample_4').DataTable();
	    var cells = table.cells( ).nodes();
	  // $( cells ).find(':checkbox').prop('checked', $(this).is(':checked'));
	    $('#btn1').prop('disabled', false);

    } else {
    	 $('#btn1').prop('disabled', true);
    }
	
});

// Each button select enable button

function getClientid()
{
	var count=0;
	 $('.chk1').each(function() {
	       if ($(this).is(':checked')) {// check the checked property with .is
	             count++;
	        }
	      
	    });
	    if(count>0)
	    {
	    	$('#btn1').prop('disabled', false);
		 }
	    else{$('#btn1').prop('disabled', true);}
	
}

//send mail js



function validateform(objForm)/* datatable form validation with checkbox*/
{
    jQuery('.scroll-to-top').click();
	$( "#top_msg" ).addClass( "alert alert-info" ).text("Please wait email sending is in progress.......");
	  $( "#top_msg" ).show();	
	
	$('#btn1').prop('disabled', true);
	//$('#btn2').prop('disabled', true);
	var client_id_arr = [];
    var client_id = {};
    var key = [];
    $('.chk1').each(function() {
       if ($(this).is(':checked')) {// check the checked property with .is
               var current = $(this).val();
               var clientid = $(this).parents("tr").find(".client_id").text();
               var key = $(this).parents("tr").find("#key").text();//get the input textbox associated with it
              
               client_id[key]=clientid;
              
        }
        client_id_arr.push($(this).val());
    });
  
    
  
    
    $.ajax({
		type: "post",
//		url: '{{route("send-mail-margin-reporting")}}',
		url: $('#hndroute').val()+'/sendmail-margin-reporting',
		data: {client:client_id},
		dataType: 'json',
		success: function(jsonResponse){
			if(jsonResponse.msg == undefined || jsonResponse.msg == null || jsonResponse.msg.length == 0)
			{
			$( "#top_msg" ).removeClass( "alert alert-info" ).addClass( "alert alert-success" ).text("Email has been sent to ").append(jsonResponse.count).append(" clients ");
		    $( "#top_msg" ).show();	  
		    jQuery('.scroll-to-top').click();
			}
			else
			{
				$( "#top_msg" ).removeClass( "alert alert-info" ).addClass( "alert alert-danger" ).text(jsonResponse.msg).append(" Email Failed");
			    $( "#top_msg" ).show();	  
			    jQuery('.scroll-to-top').click();
				
			}
			
		    setTimeout(function(){
		    	   window.location.reload(1);
		    	}, 5000);	
	    
		}
	});



}

