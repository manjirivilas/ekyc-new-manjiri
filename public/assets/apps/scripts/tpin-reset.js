
$("#client_id").blur(function(){
	$("#table1").hide();
	var client_id = $('#client_id').val();
	if(client_id){
	$.ajax({
		type: "post",
		url:$('#url_path').val()+'/populate-mobile',
		data:{data:client_id},
		dataType: 'json',
		headers: {
	        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
	    },
		success: function(jsonResponse){
			
			if(jsonResponse.msg)
			{
			$('#mobile_no').val(jsonResponse.msg);
			document.getElementById("btn1").disabled = false;
			}
		else
			{
			$('#mobile_no').val(jsonResponse.msg);
			document.getElementById("btn1").disabled = true;
			$('#error').show();
			$('#error').fadeOut(3000);
			}			
		}
	});
	}
});

function isNumber(evt) { // function to check and allow numbers only
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
/**
 * function to fetch all the dtails of client
 */


function showDetails(){	
	var client_id = $('#client_id').val();
	var mobile = $('#mobile_no').val();
	
	if(client_id!="" && mobile!=""){
		var data_array = [client_id,mobile];
		$.ajax({
			type : "POST",
			url : $('#url_path').val() + '/show-details',
			data: {data:data_array},
			dataType : 'json',
			headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            },
			success : function(jsonResponse) {
				
				
						if(jsonResponse.flag==0){
				if (jsonResponse.msg == undefined || jsonResponse.msg == null
						|| jsonResponse.msg.length == 0) {
					$("#top_msg").removeClass("alert alert-info").addClass(
							"alert alert-danger").text(jsonResponse.msg);
					$("#top_msg").show();
					jQuery('.scroll-to-top').click();
				}if (jsonResponse.msg =="The phone number that you have entered does not match with any account" || jsonResponse.msg =="Client ID does not exists"){
					$("#top_msg").addClass(
					"alert alert-danger").text(jsonResponse.msg);
					$("#top_msg").show();
					$("#table1").hide();
					jQuery('.scroll-to-top').click();
					 $( "#top_msg" ).fadeOut(5000);
				} else {
					$("#top_msg").removeClass("alert alert-danger").addClass(
							"alert alert-success").text(jsonResponse.msg);
					$("#top_msg").show();
					jQuery('.scroll-to-top').click();
					setTimeout(function(){
				    	   window.location.reload(1);
				    	}, 5000);
				}
				
			}else{
				if(jsonResponse.flag==1 )
				{
					if(typeof JSON.stringify(jsonResponse.msg) == 'string') {
						 $("#table1").show();
					
						var arr_offer=[];
						 arr_offer.push('<tr><th>Client Id</th><th>Client Name</th><th>Mobile</th><th>Email Id</th><th>PAN No.</th></tr>');
							for (var i=0;i<jsonResponse.msg.length;i++) {
								
								  arr_offer.push('<tr> ');
								  arr_offer.push('<td id="client_id"> ' +jsonResponse.msg[i].Client_ID + ' </td>');
								  arr_offer.push('<td id="client_name"> ' +jsonResponse.msg[i].CLIENT_NAME + ' </td>');
								  arr_offer.push('<td> ' +jsonResponse.msg[i].mobile + ' </td>');
								  arr_offer.push('<td id="email_id"> ' +jsonResponse.msg[i].EMAIL_ID + ' </td>');
								  arr_offer.push('<td> ' +jsonResponse.msg[i].PAN_NO + ' </td>');
								  
arr_offer.push('</tr> ');
							  }
						 $("#excelDataTable").html( arr_offer );
				}
				}
			}
			}
		});
		
	}else{
		$( "#top_msg" ).removeClass( "alert alert-info" ).addClass( "alert alert-danger" ).text("Please fill all the mandatory fields");
	    $( "#top_msg" ).show();	  
	    $('.scroll-to-top').click();
	    $( "#top_msg" ).fadeOut(3000);
	}
}
/**
 * Phone no field validations
 */
	function phoneVal(){
		if($("#mobile_no").val().length != 10){
			
			 $("#mobile").html('Phone number should be 10 digit');
		}else{
			$("#mobile").html('');
		}
	}

	/**
	 * reset T-pin
	 */
	
	function resetTpin(){
		document.getElementById("btn2").disabled = true;
		 jQuery('.scroll-to-top').click();
			$( "#top_msg" ).removeClass( "alert alert-danger" ).addClass( "alert alert-info" ).text("Please wait email sending is in progress.......");
			  $( "#top_msg" ).show();
		//alert("hasdgfshd");
		var email = $('#email_id').text();
		var client_id = $('#client_id').val();
		var client_name = $('#client_name').text();
		
		arr_data = [email,client_id,client_name]
			$.ajax({
			type: "post",
			url:$('#url_path').val()+'/reset-pin',
			data:{data:arr_data},
			dataType: 'json',
			success: function(jsonResponse){
				
				//alert(JSON.stringify(jsonResponse.name));
				if(jsonResponse.msg == undefined || jsonResponse.msg == null || jsonResponse.msg.length == 0)
				{
				$( "#top_msg" ).removeClass( "alert alert-info" ).addClass( "alert alert-danger" ).text(jsonResponse.msg);	
				$( "#top_msg" ).show();	  
			    jQuery('.scroll-to-top').click();
			    $( "#top_msg" ).fadeOut(5000);
				}
				else
				{
					$( "#top_msg" ).removeClass( "alert alert-danger" ).removeClass( "alert alert-info" ).addClass( "alert alert-success" ).text(jsonResponse.msg).append(jsonResponse.name);
				    $( "#top_msg" ).show();	  
				    jQuery('.scroll-to-top').click();
				   // $( "#top_msg" ).fadeOut(5000);
					
				}
				setTimeout(function(){
			    	   window.location.reload(1);
			    	}, 5000);
			}
		});
	}
	
	
	
	
	
	
