$(document).ready(function () {


//Custom Allow Characters & Spaces Validation
jQuery.validator.addMethod("characterspaces", function(value, element) {
    return this.optional(element) || /^[a-z\s]+$/i.test(value);
}, "Only alphabetical characters"); 


//Custom Max Length Validation
jQuery.validator.addMethod("maxLen", function (value, element, param) {
    //console.log('element= ' + $(element).attr('name') + ' param= ' + param )
    if ($(element).val().length > param) {
        return false;
    } else {
        return true;
    }
}, "You have reached the maximum number of characters allowed for this field.");

//Custom Special Characters Validation
jQuery.validator.addMethod("specialChars", function( value, element ) {
        var regex = new RegExp("^[a-zA-Z0-9]+$");
        var key = value;

        if (!regex.test(key)) {
           return false;
        }
        return true;
    }, "Special Characters not allowed");

//Custom Mobile Starts with 7|8|9 Validation
    jQuery.validator.addMethod('mobilestart', function(value, element) {
    return value.match(/^[7-9][0-9]{9}$/);
    },'Mobile no. should start with either 7|8|9');

//Custom PAN Number Validation
jQuery.validator.addMethod("panvalidate", function(value, element) {
    // return value.match(/^([a-zA-Z]{5})(\d{4})([a-zA-Z]{1})$/);
   return this.optional(element) || /^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/.test(value);
   
}, "Please enter valid PAN."); 




$.validator.addMethod("check_chkboxes", function(value, elem, param) {
    if($(".selectsegment:checkbox:checked").length > 0){
       return true;
   }else {
       return false;
   }
},"You must select at least one!");

$.validator.addMethod("noSpecialChars", function(value, element) {
05
      return this.optional(element) || /^[a-z0-9]+$/i.test(value);
06
  }, "Must contain only letters, numbers.");


});





