




function createNewCareer(){
	
Department=$("#career_dept_name").val();
Designation=document.getElementById('create_dsgn').value;
Qualification=document.getElementById('create_qual').value;
No_of_Openings=document.getElementById('creat_opn').value;

//alert(No_of_Openings);
Minimum_Experience=parseFloat(document.getElementById('create_min_exp').value);
Maximum_Experience=parseFloat(document.getElementById('create_max_exp').value);

Min_exp=document.getElementById('create_min_exp').value;
Max_exp=document.getElementById('create_max_exp').value;



Primary_Skills=document.getElementById('create_pri_skill').value;
Secondary_Skills=document.getElementById('create_sec_skill').value;
Minimum_Salary=parseFloat(document.getElementById('create_min_sal').value);
Maximum_Salary=parseFloat(document.getElementById('create_max_sal').value);

Min_sal=document.getElementById('create_min_sal').value;
Max_sal=document.getElementById('create_max_sal').value;



Job_Location=document.getElementById('create_job_loc').value;
//Job_Description=document.getElementById('create_job_desc_man').innerHTML;
Job_Description=$("textarea[name=summernote_create]").val();
Note=document.getElementById('create_note').value;
Walkin=document.getElementById('create_walkin').value;


if(Walkin == 'Yes')
	{
	Walkin = 1;
	}
else{
	Walkin = 0;
}



var exp_sal_pat = /^[0-9]\d{0,1}(\.\d{1,2})?$/;
var Designation_pat=/^[A-Z][a-zA-Z./ _]+$/;
var Qualification_pat=/^[a-zA-Z0-9/. _]+$/;
var Qualification_pat_1=/^[0-9][a-zA-Z/.,]+$/;
var Qualification_pat_2=/^[/.,][a-zA-Z0-9]+$/;
//var numeric_char = /^[0-9_ ]+$/;
//var min_sal_pat = /^\d+\.?\d{0,2}$/;
var Job_Location_pat=/^[a-zA-Z _]+$/;
var opening_pat=/^[0-9]{1,3}$/;
//var exp_pat=/^[0-9]{1,2}$/;


if(Designation == '')
{
	document.getElementById('err_create_dsgn').innerHTML = 'Please Enter Designation';
	$('#new_user_id').animate({ 'scrollTop' : $("#scrl_dsgn").position().top });
	//$('#new_user_id').scrollTop(0);
	return false;
}
else if(Designation_pat.test(Designation) == false)  
{  
	document.getElementById('err_create_dsgn').innerHTML = 'Accepts only alphabates(first letter must be capital) and ". and /"';  
	$('#new_user_id').animate({ 'scrollTop' : $("#scrl_dsgn").position().top });

	return false;  
}
else if(Qualification == '')
{
		document.getElementById('err_create_qual').innerHTML = 'Please Enter Qualification';
		$('#new_user_id').animate({ 'scrollTop' : $("#scrl_qual").position().top });

		return false;
}
else if(Qualification_pat.test(Qualification) == false)  
{  
		document.getElementById('err_create_qual').innerHTML = 'Use only alphabates and "." and "/"';  
		$('#new_user_id').animate({ 'scrollTop' : $("#scrl_qual").position().top });		
		return false;  
}

else if(Qualification_pat.test(Qualification) == true && Qualification_pat_1.test(Qualification) == true){
	document.getElementById('err_create_qual').innerHTML = 'Alphabates must be follwed by numbers only';
	$('#new_user_id').animate({ 'scrollTop' : $("#scrl_qual").position().top });
	return false;
	
}
else if(Qualification_pat.test(Qualification) == true && Qualification_pat_2.test(Qualification) == true){
	document.getElementById('err_create_qual').innerHTML = 'Invalid Sentence';
	$('#new_user_id').animate({ 'scrollTop' : $("#scrl_qual").position().top });
	return false;
}


else if(No_of_Openings ==''){
		document.getElementById('err_create_no_of_opn').innerHTML = 'Please Enter no. of Openings.';
		$('#new_user_id').animate({ 'scrollTop' : $("#scrl_opngs").position().top });
		return false;
}
else if(opening_pat.test(No_of_Openings) == false)  
{  
 document.getElementById('err_create_no_of_opn').innerHTML = 'Only numbers are accepted (max 3 digits).';  
	$('#new_user_id').animate({ 'scrollTop' : $("#scrl_opngs").position().top });

	return false;  
}

else if(Min_exp == ''){
		document.getElementById('err_create_min_exp').innerHTML = 'Please Enter Minimum. Experience';
		$('#new_user_id').animate({ 'scrollTop' : $("#scrl_min_exp").position().top });

return false;
}
else if(exp_sal_pat.test(Min_exp) == false)  
{  
document.getElementById('err_create_min_exp').innerHTML = 'Only numbers/decimals are accepted (max 2 digits).';  
$('#new_user_id').animate({ 'scrollTop' : $("#scrl_min_exp").position().top });

return false;  
}
else if(Max_exp == ''){
		document.getElementById('err_create_max_exp').innerHTML = 'Please Enter Maximum. Experience';
		$('#new_user_id').animate({ 'scrollTop' : $("#scrl_max_exp").position().top });

return false;
}
else if(exp_sal_pat.test(Min_exp) == false)  
{  
document.getElementById('err_create_max_exp').innerHTML = 'Only numbers/decimals are accepted (max 2 digits).';  
$('#new_user_id').animate({ 'scrollTop' : $("#scrl_max_exp").position().top });

return false;  
}
else if(Minimum_Experience > Maximum_Experience){
	 document.getElementById('err_create_max_exp').innerHTML = 'Max Experience should be greater than Min Experience'; 
	 $('#new_user_id').animate({ 'scrollTop' : $("#scrl_max_exp").position().top });
   	return false;  
}

else if(Primary_Skills == ''){
		document.getElementById('err_create_pri_skill').innerHTML = 'Please Enter Primary Skill';
		$('#new_user_id').animate({ 'scrollTop' : $("#scrl_pri_skill").position().top });

return false;
}
else if(Min_sal == ''){
		document.getElementById('err_create_min_sal').innerHTML = 'Please Enter Min Salary';
		$('#new_user_id').animate({ 'scrollTop' : $("#scrl_min_sal").position().top });

return false;
}
else if(exp_sal_pat.test(Min_sal) == false)  
{  
	document.getElementById('err_create_min_sal').innerHTML = 'Only numbers or decimal numbers (upto two digits)are accepted';
	$('#new_user_id').animate({ 'scrollTop' : $("#scrl_min_sal").position().top });

return false;  
}
else if(Max_sal == ''){
		document.getElementById('err_create_max_sal').innerHTML = 'Please Enter Max Salary';
		$('#new_user_id').animate({ 'scrollTop' : $("#scrl_max_sal").position().top });

return false;
}
else if(exp_sal_pat.test(Max_sal) == false)  
{  
	document.getElementById('err_create_max_sal').innerHTML = 'Only numbers or decimal numbers (upto two digits)are accepted';
	$('#new_user_id').animate({ 'scrollTop' : $("#scrl_max_sal").position().top });

return false;  
}
else if(Minimum_Salary > Maximum_Salary){
	 document.getElementById('err_create_max_sal').innerHTML = 'Max Salary should be greater than Min Salary'; 
	 $('#new_user_id').animate({ 'scrollTop' : $("#scrl_max_sal").position().top });
  	return false;  
}
else if(Job_Location == ''){
		document.getElementById('err_create_job_loc').innerHTML = 'Please Enter Job Location';
		$('#new_user_id').animate({ 'scrollTop' : $("#scrl_job_loc").position().top });

return false;
}
else if(Job_Location_pat.test(Job_Location) == false)  
{  
document.getElementById('err_create_job_loc').innerHTML = 'Only alphabatess are accepted';  
$('#new_user_id').animate({ 'scrollTop' : $("#scrl_job_loc").position().top });

return false;  
}
else if(Job_Description == false)  
{  
document.getElementById('err_create_job_desc').innerHTML = 'Please enter Job Description';  
$('#new_user_id').animate({ 'scrollTop' : $("#scrl_desc").position().top });

return false;  
}
else{
	$('job_wait_msg').show();
	$("#create").attr("disabled", true);
	
$.ajax({
	type: "post",
	url: $('#url_path').val() + '/vns-career',

	data: {
		Department,
		Designation,
		Qualification,
		No_of_Openings,
		Minimum_Experience,
		Maximum_Experience,
		Primary_Skills,
		Secondary_Skills,
		Minimum_Salary,
		Maximum_Salary,
		Job_Location,
		Job_Description,
		Note,
		Walkin
		
		
	
		},
	
	
	dataType: 'json',
	success: function(jsonResponse){
		
		 if(jsonResponse.status == 'success')
			{
			 $('job_wait_msg').hide();
				
				$('#job_crt_msg').show();
				document.getElementById('job_crt_msg').innerHTML =jsonResponse.msg ;
				$('#new_user_id').animate({ 'scrollTop' : $("#job_crt_msg").position().top });
				setTimeout(function(){
	 				$('#updt_msg').hide();
	 				$('#new_user_id').modal('hide');
	 				$("#create").attr("disabled", false);
	 				location.reload();
	 			}, 3000);
			}
			else
			{
				 $('job_wait_msg').hide();
				$('#job_crt_err_msg').show();
				document.getElementById('job_crt_err_msg').innerHTML =jsonResponse.msg ;
				$('#new_user_id').animate({ 'scrollTop' : $("#job_crt_err_msg").position().top });

				setTimeout(function(){
	 				$('#updt_err_msg').hide();
	 				$('#new_user_id').modal('hide');
	 				$("#create").attr("disabled", false);
	 				location.reload();
	 			}, 3000);
			} 
    
	}
	});
return true;
}

}














function valDept(){
	Dept_Name=document.getElementById('add_dept').value;
	var num_pat=/^[a-zA-Z0-9\-\s]+$/;
		
	if(Dept_Name=='')
	{
		document.getElementById('err_dept_add').innerHTML='please write department';
		return false;
	}
	else if(num_pat.test(Dept_Name) == false){
		document.getElementById('err_dept_add').innerHTML='Accepts alphabates and numbers only';
			return false;
	}
	else{
		document.getElementById('err_dept_add').innerHTML='';
	}
}

function editDept(){
	Dept_Name=document.getElementById('d_name').value;
	var num_pat=/^[a-zA-Z0-9\-\s]+$/;
		
	if(Dept_Name=='')
	{
		document.getElementById('err_dept_edit').innerHTML='please write department';
		return false;
	}
	else if(num_pat.test(Dept_Name) == false){
		document.getElementById('err_dept_edit').innerHTML='Accepts alphabates and numbers only';
			return false;
	}
	else{
		document.getElementById('err_dept_edit').innerHTML='';
	}
}

$("#updateJob").click(function() {
	ID=document.getElementById('upt_id').value;
	
	Department=document.getElementById('upt_dept_name').value;
	Designation=document.getElementById('upt_dsgn').value;
	Qualification=document.getElementById('upt_qaul').value;
	No_of_Openings=document.getElementById('upt_no_of_opn').value;
	Minimum_Experience=parseFloat(document.getElementById('upt_min_exp').value);
	Maximum_Experience=parseFloat(document.getElementById('upt_max_exp').value);
	Min_exp=document.getElementById('upt_min_exp').value;
	Max_exp=document.getElementById('upt_max_exp').value;
	Primary_Skills=document.getElementById('upt_pri_skill').value;
	Secondary_Skills=document.getElementById('upt_sec_skill').value;
	Minimum_Salary=parseFloat(document.getElementById('upt_min_sal').value);
	Maximum_Salary=parseFloat(document.getElementById('upt_max_sal').value);
	Min_sal=document.getElementById('upt_min_sal').value;
	Max_sal=document.getElementById('upt_max_sal').value;
	Job_Location=document.getElementById('upt_job_loc').value;
	Job_Description=$('#career_editor_update').code(); 
	Note=document.getElementById('upt_note').value;
	Walkin=document.getElementById('upt_walkin').value;
	Status=document.getElementById('upt_status').value;

	var Designation_pat=/^[A-Z][a-zA-Z./ _]+$/;
	var Qualification_pat=/^[a-zA-Z0-9./\- _]+$/;
	var Qualification_pat_1=/^[0-9][a-zA-Z/.\-, _]+$/;
	var Qualification_pat_2=/^[/\-.,][a-zA-Z0-9]+$/;
	//var min_sal_pat = /^\d+\.?\d{0,2}$/;
	var Job_Location_pat=/^[a-zA-Z _]+$/;
	var opening_pat=/^[0-9]{1,3}$/;
	//var exp_pat=/^[0-9]{1,2}$/;
	
	var exp_sal_pat = /^[0-9]\d{0,1}(\.\d{1,2})?$/;

	if(Designation == '')
	{
		$('#update_model').animate({ 'scrollTop' : $("#scrl_crt_degn").position().top });
		document.getElementById('err_upt_dsgn').innerHTML = 'Please Enter Designation';
		return false;
		
	}
	else if(Designation_pat.test(Designation) == false)  
	{  
		$('#update_model').animate({ 'scrollTop' : $("#scrl_crt_degn").position().top });
		document.getElementById('err_upt_dsgn').innerHTML = 'Accepts only alphabates(first letter must be capital) and ". and /"';  
		return false;
	}
	else if(Qualification == '')
	{
		$('#update_model').animate({ 'scrollTop' : $("#scrl_crt_qual").position().top });
			document.getElementById('err_upt_qaul').innerHTML = 'Please Enter Qualification';
			return false;
	}
	else if(Qualification_pat.test(Qualification) == false)  
	{  
		$('#update_model').animate({ 'scrollTop' : $("#scrl_crt_qual").position().top });
		document.getElementById('err_upt_qaul').innerHTML = 'Use only alphabates and "." and "/" and "-"'; 
		return false;
	}
	if(Qualification_pat.test(Qualification) == true && Qualification_pat_1.test(Qualification) == true){
		$('#update_model').animate({ 'scrollTop' : $("#scrl_crt_qual").position().top });
		document.getElementById('err_upt_qaul').innerHTML = 'Alphabates must be follwed by numbers only';
		return false;
	}
	if(Qualification_pat.test(Qualification) == true && Qualification_pat_2.test(Qualification) == true){
		$('#update_model').animate({ 'scrollTop' : $("#scrl_crt_qual").position().top });
		document.getElementById('err_upt_qaul').innerHTML = 'Invalid Sentence';
		return false;
	}
	
	else if(No_of_Openings == '')
	{
		//alert(No_of_Openings);
		$('#update_model').animate({ 'scrollTop' : $("#scrl_crt_opn").position().top });
		document.getElementById('err_upt_no_of_opn').innerHTML = 'Please Enter no. of Openings.';
		return false;
	}
	else if(opening_pat.test(No_of_Openings) == false)  
	{  
	    $('#update_model').animate({ 'scrollTop' : $("#scrl_crt_opn").position().top });
	    document.getElementById('err_upt_no_of_opn').innerHTML = 'Only numbers are accepted (max 3 digits).'; 
	    return false;
	}

	else if(Min_exp == ''){
		$('#update_model').animate({ 'scrollTop' : $("#scrl_crt_min_exp").position().top });
			document.getElementById('err_upt_min_exp').innerHTML = 'Please Enter Minimum. Experience';
			return false;
	}
	else if(exp_sal_pat.test(Min_exp) == false)  
	{  
		$('#update_model').animate({ 'scrollTop' : $("#scrl_crt_min_exp").position().top });
		document.getElementById('err_upt_min_exp').innerHTML = 'Only numbers/decimals are accepted (max 2 digits).';  
		return false;
	}
	else if(Max_exp == ''){
		$('#update_model').animate({ 'scrollTop' : $("#scrl_crt_max_exp").position().top });

			document.getElementById('err_upt_max_exp').innerHTML = 'Please Enter Maximum. Experience';
			return false;
	}
	else if(exp_sal_pat.test(Max_exp) == false)  
	{  
		$('#update_model').animate({ 'scrollTop' : $("#scrl_crt_max_exp").position().top });

	document.getElementById('err_upt_max_exp').innerHTML = 'Only numbers/decimals are accepted (max 2 digits).';  
	return false;
	}
	else if(Minimum_Experience > Maximum_Experience){
		 document.getElementById('err_upt_max_exp').innerHTML = 'Max Experience should be greater than Min Experience'; 
		 $('#update_model').animate({ 'scrollTop' : $("#scrl_crt_max_exp").position().top });
	  	return false;  
	}
	else if(Primary_Skills == ''){
		$('#update_model').animate({ 'scrollTop' : $("#scrl_crt_pri_skill").position().top });

			document.getElementById('err_upt_pri_skill').innerHTML = 'Please Enter Primary Skill';
			return false;
	}
	else if(Min_sal == ''){
		$('#update_model').animate({ 'scrollTop' : $("#scrl_crt_min_sal").position().top });

			document.getElementById('err_upt_min_sal').innerHTML = 'Please Enter Min Salary';
			return false;
	}
	else if(exp_sal_pat.test(Min_sal) == false)  
	{  
		$('#update_model').animate({ 'scrollTop' : $("#scrl_crt_min_sal").position().top });

		document.getElementById('err_upt_min_sal').innerHTML = 'Only numbers or decimal numbers (upto two digits)are accepted';
		return false;
	}
	else if(Max_sal == ''){
		$('#update_model').animate({ 'scrollTop' : $("#scrl_crt_max_sal").position().top });

			document.getElementById('err_upt_max_sal').innerHTML = 'Please Enter Max Salary';
			return false;
	}
	else if(exp_sal_pat.test(Max_sal) == false)  
	{  
		$('#update_model').animate({ 'scrollTop' : $("#scrl_crt_max_sal").position().top });

		document.getElementById('err_upt_max_sal').innerHTML = 'Only numbers or decimal numbers (upto two digits)are accepted';
		return false;
	}
	else if(Minimum_Salary > Maximum_Salary)
	{
		 $('#update_model').animate({ 'scrollTop' : $("#scrl_crt_max_sal").position().top });
		 document.getElementById('err_upt_max_sal').innerHTML = 'Max Salary should be greater than Min Salary'; 
	  	return false;  
	}
	else if(Job_Location == ''){
		$('#update_model').animate({ 'scrollTop' : $("#scrl_crt_job_loc").position().top });

			document.getElementById('err_upt_job_loc').innerHTML = 'Please Enter Job Location';
			return false;
	}
	else if(Job_Location_pat.test(Job_Location) == false)  
	{  
		$('#update_model').animate({ 'scrollTop' : $("#scrl_crt_job_loc").position().top });

	document.getElementById('err_upt_job_loc').innerHTML = 'Only alphabatess are accepted';  
	return false;
	}
	else if(Job_Description == '')  
	{  
		$('#update_model').animate({ 'scrollTop' : $("#scrl_crt_desc").position().top });

	document.getElementById('err_career_editor_update').innerHTML = 'Please enter Job Description';  
	return false;
	}
	else
	{
		$('wait_msg_upt').show();
		$("#updateJob").attr("disabled", true);
	
	$.ajax({
		type: "post",
		url:$('#url_path').val()+'/update-jobs',
		data: {ID,
			Department,
			Designation,
			Qualification,
			No_of_Openings,
			Minimum_Experience,
			Maximum_Experience,
			Primary_Skills,
			Secondary_Skills,
			Minimum_Salary,
			Maximum_Salary,
			Job_Location,
			Job_Description,
			Note,
			Walkin,
			Status
			
		
			
		},
		dataType: 'json',
		success: function(jsonResponse){
		
		 if(jsonResponse.status == 'success')
		{
			 $('wait_msg_upt').hide();
			$('#updt_msg').show();
			document.getElementById('updt_msg').innerHTML =jsonResponse.msg ;
			$('#update_model').animate({ 'scrollTop' : $("#updt_msg").position().top });

			setTimeout(function(){
 				$('#updt_msg').hide();
 				$('#update_model').modal('hide');
 				$("#updateJob").attr("disabled", false);

 				location.reload();
 			}, 3000);
		}
		else
		{
			 $('wait_msg_upt').hide();
			$('#updt_err_msg').show();
			document.getElementById('updt_err_msg').innerHTML =jsonResponse.msg ;
			$('#update_model').animate({ 'scrollTop' : $("#updt_err_msg").position().top });

			setTimeout(function(){
 				$('#updt_err_msg').hide();
 				$('#update_model').modal('hide');
 				$("#updateJob").attr("disabled", false);

 				location.reload();
 			}, 3000);
		} 
			
			
		}
	});

	}
	
});


$(".update").click(function() {
	
	


	var $row = $(this).closest("tr");
	
    var ID=$row.find(".id").text();
    
    var Department=$row.find(".dept_name").text();
    var Designation=$row.find(".desgn").text();
    var Minimum_Experience=$row.find(".min_exp").text();
    var Maximum_Experience=$row.find(".max_exp").text();
    var Qualification=$row.find(".qualification").text();
    var Job_Location=$row.find(".job_loc").text();
    var No_of_Openings=$row.find(".no_of_opening").text();
    var Primary_Skills=$row.find(".primary_skill").text();
    var Secondary_Skills=$row.find(".secondary_skill").text();
    var Minimum_Salary=$row.find(".min_sal").text();
    var Maximum_Salary=$row.find(".max_sal").text();
    var Job_Description=$row.find(".description").text();
    var Note=$row.find(".note").text();
    var Walkin=$row.find(".walkin").text();
    var Status=$row.find(".status").text();
    if(Walkin == 1)
    	{
        $('#upt_walkin option[value=1]').attr("selected",true);

    	}
    else{
        $('#upt_walkin option[value=0]').attr("selected",true);

    }
    
    if(Status == 'Active'){
    	$('#upt_status option[value=1]').attr("selected",true);
    }
    else{
    	$('#upt_status option[value=0]').attr("selected",true);
    }
   
    
    
    document.getElementById('upt_id').value=ID;
    document.getElementById('upt_dept_name').value=Department;

    document.getElementById('upt_dsgn').value=Designation;
    document.getElementById('upt_qaul').value=Qualification;
    document.getElementById('upt_no_of_opn').value=No_of_Openings;
    document.getElementById('upt_min_exp').value=Minimum_Experience;
    document.getElementById('upt_max_exp').value=Maximum_Experience;
    document.getElementById('upt_pri_skill').value=Primary_Skills;
    document.getElementById('upt_sec_skill').value=Secondary_Skills;
    document.getElementById('upt_min_sal').value=Minimum_Salary;
    document.getElementById('upt_max_sal').value=Maximum_Salary;
    document.getElementById('upt_job_loc').value=Job_Location;
    $('#career_editor_update').code(Job_Description); 
    document.getElementById('upt_note').value=Note;
    
    $('#update_model').modal('show');
});


function do_not_delete_jobs(){
	$('#delete_modal').modal('hide');
}

function do_not_delete_dept(){
	$('#delete_dept_modal').modal('hide');
}


$(".delete").click(function() {
	
	$('#delete_modal').modal('show');
	var $row = $(this).closest("tr");
    var ID=$row.find(".id").text();
    
    document.getElementById('job_id').value=ID;
    
		
});


$("#change_job").click(function() {
	$('#change_job').hide();
	$('#upt_dept').hide();
	
	$('#dept_list').show();
});




function delete_jobs()
{
	$(".delete-job-body").hide();
	$(".delete-job-footer").hide();
	$('#delete_job_wait_msg').show();
	
	$("#delete-job-disable").attr("disabled", true);
	
	var ID = document.getElementById('job_id').value; 
	
		$.ajax({
			type: "post",
			url:$('#url_path').val()+'/delete-jobs',
			data: {id:ID },
			dataType: 'json',
			success: function(jsonResponse){
			
			if(jsonResponse.status == 'success')
			{
				
				$('#delete_job_wait_msg').hide();
				$('#delete_job_msg').show();
				document.getElementById('delete_job_msg').innerHTML =jsonResponse.msg ;
				setTimeout(function(){
	 				$('#delete_job_msg').hide();
	 				$("#delete-job-disable").attr("disabled", false);

	 				location.reload();
	 			}, 3000);
			}
			else
			{
				$('#delete_job_wait_msg').hide();
				$('#error_delete_job_msg').show();
				document.getElementById('error_delete_job_msg').innerHTML =jsonResponse.msg ;
				setTimeout(function(){
	 				$('#error_delete_job_msg').hide();
	 				$("#delete-job-disable").attr("disabled", false);
	 				location.reload();
	 			}, 3000);
			}
				
				
			}
		});
}


function valDesignation()
{
	var Designation_pat=/^[A-Z][a-zA-Z./ _]+$/;
	var Designation = document.getElementById('create_dsgn').value;
	if(Designation == ''){
	 		document.getElementById('err_create_dsgn').innerHTML = 'Please Enter Designation';
		return false;
	}
	else if(Designation_pat.test(Designation) == false)  
    {  
   	 document.getElementById('err_create_dsgn').innerHTML = 'Accepts only alphabates(first letter must be capital) and ". and /"';  
    	return false;  
    }
	else{
			document.getElementById('err_create_dsgn').innerHTML = '';
		return true;
	}
}

function valQualification()
{
	var Qualification_pat=/^[a-zA-Z0-9.,/\- _]+$/;
	var Qualification_pat_1=/^[0-9][a-zA-Z.,/\- _]+$/;
	var Qualification_pat_2=/^[/\-.,][a-zA-Z0-9 _]+$/;
	var Qualification = document.getElementById('create_qual').value;
	if(Qualification == ''){
	 		document.getElementById('err_create_qual').innerHTML = 'Please Enter Qualification.';
		return false;
	}
	else if(Qualification_pat.test(Qualification) == false)  
    {  
		document.getElementById('err_create_qual').innerHTML = 'Accepts only alphabates,numbers, "." , "/","-"'; 
		return false;  
    }	
	else{
		if(Qualification_pat_1.test(Qualification) == true){
			document.getElementById('err_create_qual').innerHTML = 'Alphabates must be follwed by numbers only';
			return false;
		}
		else if(Qualification_pat_2.test(Qualification) == true){
			document.getElementById('err_create_qual').innerHTML = 'Invalid Sentence';
			return false;
		}
		else{
			document.getElementById('err_create_qual').innerHTML = '';
			return true;
		}
			
	}
}


function valNo_of_Openings()
{
	var Openings = document.getElementById('creat_opn').value;
	var numeric_char = /^[0-9]{1,3}$/;
	
	if(Openings == ''){
	 		document.getElementById('err_create_no_of_opn').innerHTML = 'Please Enter no. of Openings';
		return false;
	}
	 else if(numeric_char.test(Openings) == false)  
     {  
    	 document.getElementById('err_create_no_of_opn').innerHTML = 'Only numbers are accepted (max 3 digits).';  
     	return false;  
     }
	
	else{
			document.getElementById('err_create_no_of_opn').innerHTML = '';
		return true;
	}
}

function valMin_Experience()
{
	var Experience = document.getElementById('create_min_exp').value;
	var numeric_char=/^[0-9]\d{0,1}(\.\d{1,2})?$/;

	if(Experience == ''){
	 		document.getElementById('err_create_min_exp').innerHTML = 'Please Enter Minimum. Experience';
		return false;
	}
	else if(numeric_char.test(Experience) == false)  
    {  
   	 document.getElementById('err_create_min_exp').innerHTML = 'Only numbers/decimals are accepted (max 2 digits).';  
    	return false;  
    }
	else{
			document.getElementById('err_create_min_exp').innerHTML = '';
		return true;
	}
}


function valMax_Experience()
{
	var Min_Experience =parseFloat( document.getElementById('create_min_exp').value);
	var Max_Experience = parseFloat(document.getElementById('create_max_exp').value);
	var Experience = document.getElementById('create_max_exp').value;

	var numeric_char=/^[0-9]\d{0,1}(\.\d{1,2})?$/;
	if(Experience == ''){
	 		document.getElementById('err_create_max_exp').innerHTML = 'Please Enter Maximum. Experience';
		return false;
	}
	else if(numeric_char.test(Experience) == false)  
    {  
   	 document.getElementById('err_create_max_exp').innerHTML = 'Only numbers/decimals are accepted (max 2 digits).';  
    	return false;  
    }
	else if(Min_Experience > Max_Experience){
		 document.getElementById('err_create_max_exp').innerHTML = 'Max Experience should be greater than Min Experience';  
	    	return false;  
	}
	else{
			document.getElementById('err_create_max_exp').innerHTML = '';
		return true;
	}
}




function valPrimary_Skill()
{
	var Primary_Skill = document.getElementById('create_pri_skill').value;
	if(Primary_Skill == ''){
	 		document.getElementById('err_create_pri_skill').innerHTML = 'Please Enter Primary Skill';
		return false;
	}else{
			document.getElementById('err_create_pri_skill').innerHTML = '';
		return true;
	}
}





function valMin_Salary()
{
	
	var min_sal = document.getElementById('create_min_sal').value;
		
	var min_sal_pat=/^[0-9]\d{0,1}(\.\d{1,2})?$/;
	
	if(min_sal == ''){
	 		document.getElementById('err_create_min_sal').innerHTML = 'Please Enter Min Salary';
		return false;
	}
	else if(min_sal_pat.test(min_sal) == false)  
    {  
 		document.getElementById('err_create_min_sal').innerHTML = 'Only numbers or decimal numbers (upto two digits)are accepted';
    	return false;  
    }
	else{
			document.getElementById('err_create_min_sal').innerHTML = '';
		return true;
	}
}

function valMax_Salary()
{
	var min_sal =parseFloat(  document.getElementById('create_min_sal').value);
	var max_sal =parseFloat(  document.getElementById('create_max_sal').value);
	var salary = document.getElementById('create_max_sal').value;
	var max_sal_pat=/^[0-9]\d{0,1}(\.\d{1,2})?$/;
	if(salary == ''){
	 		document.getElementById('err_create_max_sal').innerHTML = 'Please Enter Max Salary';
		return false;
	}
	else if(max_sal_pat.test(salary) == false)  
    {  
 		document.getElementById('err_create_max_sal').innerHTML = 'Only numbers or decimal numbers (upto two digits)are accepted';
    	return false;  
    }
	if(min_sal > max_sal){
		 document.getElementById('err_create_max_sal').innerHTML = 'Max Salary should be greater than Min Salary'; 
		 //$('#new_user_id').animate({ 'scrollTop' : $("#scrl_max_sal").position().top });
	  	return false;  
	}
	else{
			document.getElementById('err_create_max_sal').innerHTML = '';
		return true;
	}
}

function valJob_Location()
{
	var Job_Location_pat=/^[a-zA-Z _]+$/;
	var job_loc = document.getElementById('create_job_loc').value;
	if(job_loc == ''){
	 		document.getElementById('err_create_job_loc').innerHTML = 'Please Enter Job Location';
		return false;
	}
	else if(Job_Location_pat.test(job_loc) == false)  
    {  
   	 document.getElementById('err_create_job_loc').innerHTML = 'Only alphabatess are accepted';  
    	return false;  
    }
	else{
			document.getElementById('err_create_job_loc').innerHTML = '';
		return true;
	}
}


//Update Validation

function valUpdateDesignation()
{
	var Designation_pat=/^[A-Z][a-zA-Z./ _]+$/;
	var Designation = document.getElementById('upt_dsgn').value;
	if(Designation == ''){
	 		document.getElementById('err_upt_dsgn').innerHTML = 'Please Enter Designation';
		return false;
	}
	else if(Designation_pat.test(Designation) == false)  
    {  
   	 document.getElementById('err_upt_dsgn').innerHTML = 'Accepts only alphabates(first letter must be capital) and ". and /"';  
    	return false;  
    }
	else{
			document.getElementById('err_upt_dsgn').innerHTML = '';
		return true;
	}
}

function valUpdateQaulification()
{
	var Qualification_pat=/^[a-zA-Z0-9/\-.,]+$/;
	var Qualification_pat_1=/^[0-9][a-zA-Z/\-.,]+$/;
	var Qualification_pat_2=/^[/\-.,][a-zA-Z0-9]+$/;
	var Qualification = document.getElementById('upt_qaul').value;
	if(Qualification == ''){
	 		document.getElementById('err_upt_qaul').innerHTML = 'Please Enter Qualification';
		return false;
	}
	else if(Qualification_pat.test(Qualification) == false)  
    {  
   	 document.getElementById('err_upt_qaul').innerHTML = 'Accepts only alphabates,numbers, "." , "/","-"';  
    	return false;  
    }
	else{
		if(Qualification_pat_1.test(Qualification) == true){
			document.getElementById('err_upt_qaul').innerHTML = 'Alphabates must be follwed by numbers only';
			return false;
		}
		else if(Qualification_pat_2.test(Qualification) == true){
			document.getElementById('err_upt_qaul').innerHTML = 'Invalid Sentence';
			return false;
		}
		else{
			document.getElementById('err_upt_qaul').innerHTML = '';
			return true;
		}
	}
}


function valUpdateOpenings()
{
	var Openings = document.getElementById('upt_no_of_opn').value;
	var numeric_char = /^[0-9]{1,3}$/;
	
	if(Openings == ''){
	 		document.getElementById('err_upt_no_of_opn').innerHTML = 'Please Enter no. of Openings';
		return false;
	}
	 else if(numeric_char.test(Openings) == false)  
     {  
    	 document.getElementById('err_upt_no_of_opn').innerHTML = 'Only numbers are accepted (max 3 digits).';  
     	return false;  
     }
	
	else{
			document.getElementById('err_upt_no_of_opn').innerHTML = '';
		return true;
	}
}

function valUpdateMinExperience()
{
	var Experience = document.getElementById('upt_min_exp').value;
	var numeric_char=/^[0-9]\d{0,1}(\.\d{1,2})?$/;

	if(Experience == ''){
	 		document.getElementById('err_upt_min_exp').innerHTML = 'Please Enter Minimum. Experience';
		return false;
	}
	else if(numeric_char.test(Experience) == false)  
    {  
   	 document.getElementById('err_upt_min_exp').innerHTML = 'Only numbers/decimals are accepted (max 2 digits).';  
    	return false;  
    }
	else{
			document.getElementById('err_upt_min_exp').innerHTML = '';
		return true;
	}
}


function valUpdateMaxExperience()
{
	var Min_Experience = parseFloat(document.getElementById('upt_min_exp').value);
	var Max_Experience = parseFloat(document.getElementById('upt_max_exp').value);
	var Experience = document.getElementById('upt_max_exp').value;
	var numeric_char=/^[0-9]\d{0,1}(\.\d{1,2})?$/;

	if(Experience == ''){
	 		document.getElementById('err_upt_max_exp').innerHTML = 'Please Enter Maximum. Experience';
		return false;
	}
	else if(numeric_char.test(Experience) == false)  
    {  
   	 document.getElementById('err_upt_max_exp').innerHTML = 'Only numbers/decimals are accepted (max 2 digits).';  
    	return false;  
    }
	else if(Min_Experience > Max_Experience){
		 document.getElementById('err_upt_max_exp').innerHTML = 'Max Experience should be greater than Min Experience'; 
		// $('#new_user_id').animate({ 'scrollTop' : $("#scrl_max_sal").position().top });
	  	return false;  
	}
	else{
			document.getElementById('err_upt_max_exp').innerHTML = '';
		return true;
	}
}




function valUpdatePrimarySkills()
{
	var Primary_Skill = document.getElementById('upt_pri_skill').value;
	if(Primary_Skill == ''){
	 		document.getElementById('err_upt_pri_skill').innerHTML = 'Please Enter Primary Skill';
		return false;
	}else{
			document.getElementById('err_upt_pri_skill').innerHTML = '';
		return true;
	}
}





function valUpdateMinSalary()
{
	
	var min_sal = document.getElementById('upt_min_sal').value;
	var min_sal_pat=/^[0-9]\d{0,1}(\.\d{1,2})?$/;
	
	
	
	if(min_sal == ''){
	 		document.getElementById('err_upt_min_sal').innerHTML = 'Please Enter Min Salary';
		return false;
	}
	else if(min_sal_pat.test(min_sal) == false)  
    {  
 		document.getElementById('err_upt_min_sal').innerHTML = 'Only numbers or decimal numbers (upto two digits)are accepted';
    	return false;  
    }
	else{
			document.getElementById('err_upt_min_sal').innerHTML = '';
		return true;
	}
}

function valUpdateMaxSalary()
{
	
	var min_sal = parseFloat(document.getElementById('upt_min_sal').value);
	var max_sal =parseFloat( document.getElementById('upt_max_sal').value);
	var salary = document.getElementById('upt_max_sal').value;
	var max_sal_pat=/^[0-9]\d{0,1}(\.\d{1,2})?$/;
	if(salary == ''){
	 		document.getElementById('err_upt_max_sal').innerHTML = 'Please Enter Max Salary';
		return false;
	}
	else if(max_sal_pat.test(salary) == false)  
    {  
 		document.getElementById('err_upt_max_sal').innerHTML = 'Only numbers or decimal numbers (upto two digits)are accepted';
    	return false;  
    }
	else if(min_sal > max_sal){
		 document.getElementById('err_upt_max_sal').innerHTML = 'Max Salary should be greater than Min Salary'; 
		// $('#new_user_id').animate({ 'scrollTop' : $("#scrl_max_sal").position().top });
	  	return false;  
	}
	else{
			document.getElementById('err_upt_max_sal').innerHTML = '';
		return true;
	}
}

function valUpdateJobLoc()
{
	var Job_Location_pat=/^[a-zA-Z_ ]+$/;
	var job_loc = document.getElementById('upt_job_loc').value;
	if(job_loc == ''){
	 		document.getElementById('err_upt_job_loc').innerHTML = 'Please Enter Job Location';
		return false;
	}
	else if(Job_Location_pat.test(job_loc) == false)  
    {  
   	 document.getElementById('err_upt_job_loc').innerHTML = 'Only alphabatess are accepted';  
    	return false;  
    }
	else{
			document.getElementById('err_upt_job_loc').innerHTML = '';
		return true;
	}
}


function editbtndept(s,r){
	document.getElementById('err_dept_add').innerHTML='';
	document.getElementById('add_dept').value='';
	$('#add_department').animate({ 'scrollTop' : $(".deptmnt").position().top });
	
	$('#d_name').val(s);
	$('#d_id').val(r);
	$('.deptmnt').show();
	$('#addDepartment-btn').hide();
	$('#add_dept').hide();
			
		
		
}

$("#delete_dept").click(function() {
	
	Dept_ID=document.getElementById('delete_dept_id').value;
	
	$('.delete-dept-body').hide();
	$('.delete-dept-ftr').hide();
	$("#delete_dept_wait_msg").show();
	$("#delete_dept").attr("disabled", true);
	$.ajax({
		type: "post",
		url:$('#url_path').val()+'/delete-dept',
		data: {Dept_ID},
		dataType: 'json',
		success: function(jsonResponse){
		
		if(jsonResponse.status == 'success')
		{
			
			$("#delete_dept_wait_msg").hide();
			$('#delete_dept_msg').show();

			
			
			document.getElementById('delete_dept_msg').innerHTML =jsonResponse.msg ;
			setTimeout(function(){
				$('#delete_dept_modal').modal('hide');
 				$('#delete_dept_msg').hide();
 				$("#delete_dept").attr("disabled", false);

				document.getElementById('delete_dept_msg').innerHTML ='';
 				location.reload();
 			}, 3000);
		}
		else
		{
			$("#delete_dept_wait_msg").hide();
			$('#error_delete_dept_msg').show();
			$('.delete-dept-body').hide();
			$('.delete-dept-ftr').hide();
			document.getElementById('error_delete_dept_msg').innerHTML =jsonResponse.msg ;
			setTimeout(function(){
				$('#delete_dept_modal').modal('hide');
				document.getElementById('error_delete_dept_msg').innerHTML ='' ;
 				$("#delete_dept").attr("disabled", false);

 				$('#error_delete_dept_msg').hide();
 				location.reload();
 			}, 3000);
		}
			
			
		}
	});	
		
});




//delete department
function deletebtndept(Dept_ID){
	$('#delete_dept_modal').modal('show');
	 document.getElementById('delete_dept_id').value=Dept_ID;
	//alert(s);
	
		
}


$("#cancelEditDept").click(function() {
	
	$('.deptmnt').hide();
	$('#addDepartment-btn').show();
	$('#add_dept').show();
	
	
	
});
$("#department-edit-button").click(function() {
	
	$('#edit_details').hide();
	$('#d_name').show();
	$("#edit_dept").show();
	$('.deptmnt').hide();
	$('#addDepartment-btn').show();
	$('#add_dept').show();
	document.getElementById('err_dept_add').innerHTML='';
	document.getElementById('add_dept').value='';
	
});
$("#saveDept").click(function() {
	
	Dept_ID=document.getElementById('d_id').value;
	Dept_Name=document.getElementById('d_name').value;
	var num_pat=/^[a-zA-Z0-9\-\s]+$/;
	if(Dept_Name=='')
	{
		document.getElementById('err_dept_edit').innerHTML='please write department';
		return false;
	}
	else if(num_pat.test(Dept_Name) == false){
		document.getElementById('err_dept_edit').innerHTML='Accepts alphabates and numbers only';
			return false;
	}
	else{
		$('#wait_dept').show();
		
		document.getElementById('err_dept_edit').innerHTML='';
		$("#saveDept").attr("disabled", true);
	$.ajax({
		type: "post",
		url:$('#url_path').val()+'/update-dept',
		data: {Dept_ID, Dept_Name},
		dataType: 'json',
		success: function(jsonResponse){
		
		if(jsonResponse.status == 'success')
		{
			
			$('#wait_dept').hide();
			$('#updt_dept').show();
			document.getElementById('updt_dept').innerHTML =jsonResponse.msg ;
			setTimeout(function(){
 				$('#updt_dept').hide();
 				$("#saveDept").attr("disabled", false);

 				location.reload();
 			}, 3000);
		}
		else
		{
			$('#wait_dept').hide();
			$('#updt_err_dept').show();
			document.getElementById('updt_err_dept').innerHTML =jsonResponse.msg ;
			setTimeout(function(){
 				$('#updt_err_dept').hide();
 				$("#saveDept").attr("disabled", false);

 				location.reload();
 			}, 3000);
		}
			
			
		}
	});
	return true;
	}
});


$("#addDepartment-btn").click(function() 
{

Dept_Name=document.getElementById('add_dept').value;
var num_pat=/^[a-zA-Z0-9\-\s]+$/;
	
if(Dept_Name=='')
{
	document.getElementById('err_dept_add').innerHTML='please write department';
	return false;
}
else if(num_pat.test(Dept_Name) == false){
	document.getElementById('err_dept_add').innerHTML='Accepts alphabates and numbers only';
		return false;
}
else{
	$('#wait_dept').show();
	document.getElementById('err_dept_add').innerHTML='';
	$("#addDepartment-btn").attr("disabled", true);
	$.ajax({
		type: "post",
		url:$('#url_path').val()+'/add-dept',
		data: {Dept_Name},
		dataType: 'json',
		success: function(jsonResponse){
		
		if(jsonResponse.status == 'success')
		{
			$('#wait_dept').hide();
			$('#updt_dept').show();
			document.getElementById('updt_dept').innerHTML =jsonResponse.msg ;
			setTimeout(function(){
 				$('#updt_dept').hide();
 				$("#addDepartment-btn").attr("disabled", false);

 				location.reload();
 			}, 3000);
		}
		else
		{
			$('#wait_dept').hide();
			$('#updt_err_dept').show();
			document.getElementById('updt_err_dept').innerHTML =jsonResponse.msg ;
			setTimeout(function(){
 				$('#updt_err_dept').hide();
 				$("#addDepartment-btn").attr("disabled", false);

 				location.reload();
 			}, 3000);
		}
			
			
		}
	});
	return true;
}
	
	
});




