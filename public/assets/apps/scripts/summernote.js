/**
 * 
 */
window.onload = function() {
	if ($('#top_msg1').css('display') != 'none') {
	    $('#top_msg1').fadeOut(1000);
	    $('#myModal1').modal('hide');
	}
}

/*$('#summernote_1').summernote({
	height: 300,
	  toolbar: [
	    ['style', ['style']],
		['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
        ['fontname', ['fontname']],
        ['fontsize', ['fontsize']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['height', ['height']],
        ['table', ['table']],
        ]
	});*/
/**
 * ajax function call on submit button to inert the offer
 * @returns {Boolean}
 */
	function SubmitForm(){
		var d = new Date().toISOString().slice(0,10);
		var title = document.getElementById("title").value;
		var subtitle = document.getElementById("subtitle").value;
		var from_date = document.getElementById("datepicker").value;
		var to_date = document.getElementById("datepicker_1").value;
		var desc = document.getElementById("summernote_1").value;
		var html_chars=$('#summernote_1').val();
		var striped_val = html_chars.replace(/<\/?[^>]+(>|$)/g, "");
		
		if(title== "" || subtitle== "" || to_date == d || to_date == "" || from_date == "" || (from_date!= d && from_date <= d) || striped_val.length == 0){ 
				$( "#top_msg" ).removeClass( "alert alert-info" ).addClass( "alert alert-danger" ).text("Please Enter Valid Details !");
				$('#top_msg1').hide();
			    $( "#top_msg" ).show();
			    jQuery('.scroll-to-top').click();
				return false;
			}else if(to_date < from_date){
				$( "#top_msg" ).removeClass( "alert alert-info" ).addClass( "alert alert-danger" ).text("Please Enter Proper Dates !");
				$('#top_msg1').hide();
			    $( "#top_msg" ).show();	  
			    jQuery('.scroll-to-top').click();
					return false;
				}
				
		if(striped_val.length<=500 ){
	var offer_array=[title,subtitle,from_date,to_date,desc];
				$.ajax({
					type: "post",
					url:$('#url_path').val()+'/insert-offer-ekyc',
					data: {offer:offer_array},
					dataType: 'json',
					success: function(jsonResponse){
						if(jsonResponse.flag==1)
						{
						$( "#top_msg" ).removeClass(  ).addClass( "alert alert-success" ).text(jsonResponse.msg);	
						//$( "#top_msg" ).show();	  
					    jQuery('.scroll-to-top').click();
					    $("#top_msg").fadeTo(5000, 700).slideUp(700, function(){
	    	   				$('#title').val('');
	    	   				$('#subtitle').val('');
	    	   				$('#datepicker').val('');
	    	   				$('#datepicker_1').val('');
	    	   				$('#summernote_1').code('');
	    	   		   });
//					    setTimeout(function(){
//				    	   window.location.reload(1);
//				    	}, 5000);
						}
						else
						{
							if(jsonResponse.flag==0 )
								{
								if(typeof JSON.stringify(jsonResponse.msg) == 'string') {
									var arr_offer=[];
									 arr_offer.push('<tr><th>From Date</th><th>To Date</th><th>Title</th><th>sub_title</th></tr>');
										for (var i=0;i<jsonResponse.msg.length;i++) {
										  
											  $('#myModal1').modal('show');
											  
											  arr_offer.push('<tr> ');
											  arr_offer.push('<td> ' +jsonResponse.msg[i].from_date + '</td>');
											  arr_offer.push('<td> ' +jsonResponse.msg[i].to_date + '</td>');
											  arr_offer.push('<td> ' +jsonResponse.msg[i].title + '</td>');
											  arr_offer.push('<td> ' +jsonResponse.msg[i].sub_title + '</td>');
											  arr_offer.push('</tr> ');
										  }
									 $(".modal-body #excelDataTable").html( arr_offer );
														
									}
								else
									{
									$( "#top_msg" ).removeClass( "alert alert-danger" ).addClass( "alert alert-danger" ).text(jsonResponse.msg);
								    $( "#top_msg" ).show();	  
								    jQuery('.scroll-to-top').click();
									}
								}
						}
					}
				});
		}
		else
		{
			$( "#top_msg" ).removeClass( "alert alert-info" ).addClass( "alert alert-danger" ).text("offers description should be less than 500 characters!");
			$('#top_msg1').hide();
		    $( "#top_msg" ).show();	  
		    jQuery('.scroll-to-top').click();
		return false;
		}
	}
	/**
	 * function to show modal on row click
	 * @param row
	 */
	
	function selectRow(row){
		
		$("#formModal-"+row).modal("show"); 
	}
	/**
	 * function to show edit modal with values
	 * @param rowid
	 */
	function ShowModal(rowid){
//	 	$("#myModal-"+rowid).modal("show");
	 $("#offer_id").val($("#offer_id-"+rowid).val());
	$("#title").val($("#title-"+rowid).html());
	$("#subtitle").val($("#subtitles-"+rowid).val());
	$("#datepicker_2").val($("#fromdate-"+rowid).html());
	$("#datepicker_1").val($("#todate-"+rowid).html());
	$("#summernote_1").code($("#summernote_1-"+rowid).val());
	$("#myModal").modal("show");
	}
	/**
	 * for updating values in database
	 */
	function updateOffers(){
		var d = new Date().toISOString().slice(0, 10);
		var title = $('#title').val();
		var subtitle = $('#subtitle').val();
		var fromDate = $('#datepicker').val();
		var toDate = $('#datepicker_1').val();
		var desc = $('#summernote_1').code();
		var offer_array = [ title,subtitle,fromDate,toDate,desc ];
		updateAjax(offer_array);
	}
	
	function updateAjax(offers){
		$.ajax({
			
			type: "post",
			url:$('#url_path').val()+'/update-existing-offerofmonth',
			data: {offer:offers},
			dataType: 'json',
			success: function(jsonResponse){
				if(jsonResponse.msg == undefined || jsonResponse.msg == null || jsonResponse.msg.length == 0)
				{
				$( "#top_msg" ).removeClass( "alert alert-info" ).addClass( "alert alert-danger" ).text(jsonResponse.msg);	
				$( "#top_msg" ).show();	  
			    jQuery('.scroll-to-top').click();
				}
				else
				{
					$( "#top_msg" ).removeClass( "alert alert-danger" ).addClass( "alert alert-success" ).text(jsonResponse.msg);
				    $( "#top_msg" ).show();	  
				    jQuery('.scroll-to-top').click();
					
				}
				
				 $("#top_msg").fadeTo(5000, 700).slideUp(700, function(){
					 $('#title').val('');
 	   				$('#subtitle').val('');
 	   				$('#datepicker').val('');
 	   				$('#datepicker_1').val('');
 	   				$('#summernote_1').code('');
 	   		   });

			}
		});
	}
/**
 * delete the offer from database
 * @param id
 */
	
	function DeleteModal(id){
		 $('#myDelModal-'+id).modal('show');
	}
	
	function deleteOfferOfMonth(offer_id){
		var id = $('#offer_id-'+offer_id).val();
		$.ajax({
			type : "post",
			url : $('#url_path').val() + '/delete-offerofmonth',
			data : {
				id : id
			},
			dataType : 'json',
			success : function(jsonResponse) {
				if (jsonResponse.msg == undefined || jsonResponse.msg == null
						|| jsonResponse.msg.length == 0) {
					$("#top_msg").removeClass("alert alert-info").addClass(
							"alert alert-danger").text(jsonResponse.msg);
					$("#top_msg").show();
					jQuery('.scroll-to-top').click();
				} else {
					window.location.reload();
//					$("#top_msg").removeClass("alert alert-danger").addClass(
//							"alert alert-success").text(jsonResponse.msg);
//					$("#top_msg").show();
//					jQuery('.scroll-to-top').click();

				}
				
			/*	 $("#top_msg").fadeTo(5000, 700).slideUp(700, function(){
 	   		   });*/
				
				setTimeout(function() {
					window.location.reload(1);
				}, 5000);

			}
		});
	}
	
	
	
	/**
	 * Main update function
	 * @returns {Boolean}
	 */
	function validateform()
	{
		var d = new Date().toISOString().slice(0,10);
		var id =  document.getElementById('offer_id').value;
		var title = document.getElementById('title').value;
		var subtitle = document.getElementById('subtitle').value;
		var From_date = document.getElementById('datepicker_2').value;
		var to_date = document.getElementById('datepicker_1').value;
		var desc = $('#summernote_1').code();
		var striped_val = desc.replace(/<\/?[^>]+(>|$)/g, "");
		
		if(striped_val.length<=500 ){
		var offer=[id,title,subtitle,From_date,to_date,desc];
		$("#myModal").modal("hide");
	$.ajax({
			type: "post",
			url: $('#url_path').val() + '/update-offer-of-month',
			data: {offer:offer},
			dataType: 'json',
			success: function(jsonResponse){
				if(jsonResponse.flag==1)
				{
				$( "#top_msg" ).removeClass( "alert alert-info" ).addClass( "alert alert-success" ).text(jsonResponse.msg);	
				$( "#top_msg" ).show();	  
			    jQuery('.scroll-to-top').click();
			    
			    setTimeout(function(){
		    	   window.location.reload(1);
		    	}, 5000);
				}
				else
				{
					if(jsonResponse.flag==0 )
						{
						if(typeof JSON.stringify(jsonResponse.msg) == 'string') {
							var arr_offer=[];
							 arr_offer.push('<tr><th>From Date</th><th>To Date</th><th>Title</th><th>sub_title</th></tr>');
								for (var i=0;i<jsonResponse.msg.length;i++) {
								  
									  $('#updateModal').modal('show');
									  
									  $('#update-offer').val(jsonResponse.msg[i].id);
									  
									  arr_offer.push('<tr> ');
									  arr_offer.push('<td> ' +jsonResponse.msg[i].from_date + '</td>');
									  arr_offer.push('<td> ' +jsonResponse.msg[i].to_date + '</td>');
									  arr_offer.push('<td> ' +jsonResponse.msg[i].title + '</td>');
									  arr_offer.push('<td> ' +jsonResponse.msg[i].sub_title + '</td>');
									  arr_offer.push('</tr> ');
								  }
							 $(".modal-body #excelDataTableup").html( arr_offer );
												
							}
						else
							{
							$( "#top_msg" ).removeClass( "alert alert-danger" ).addClass( "alert alert-danger" ).text(jsonResponse.msg);
						    $( "#top_msg" ).show();	  
						    jQuery('.scroll-to-top').click();
							}
						}
				}
			}
		});
	}else
	{
		$("#myModal").modal("hide");
		$( "#top_msg" ).removeClass( "alert alert-info" ).addClass( "alert alert-danger" ).text("offers description should be less than 500 characters!");
		$('#top_msg1').hide();
	    $( "#top_msg" ).show();	  
	    jQuery('.scroll-to-top').click();
	return false;
	}
	}
	
	function updateOffersOfmonth(){
		var d = new Date().toISOString().slice(0, 10);
		var id = $("#update-offer").val();
		var title = $('#title').val();
		var subtitle = $('#subtitle').val();
		var fromDate = $('#datepicker_2').val();
		var toDate = $('#datepicker_1').val();
		var desc = $('#summernote_1').code();
		var offer_array = [ id,title,subtitle,fromDate,toDate,desc ];
		updateOffersOfmonthAjax(offer_array);
	}
	
	function updateOffersOfmonthAjax(offers){
		$.ajax({
			type: "post",
			url:$('#url_path').val()+'/update-offer-of-month',
			data: {offer:offers, flag:"update"},
			dataType: 'json',
			success: function(jsonResponse){
				if(jsonResponse.msg == undefined || jsonResponse.msg == null || jsonResponse.msg.length == 0)
				{
				$( "#top_msg" ).removeClass( "alert alert-info" ).addClass( "alert alert-danger" ).text(jsonResponse.msg);	
				$( "#top_msg" ).show();	  
			    jQuery('.scroll-to-top').click();
				}
				else if(jsonResponse.msg==1)
					{
					window.location.reload();
//					$( "#top_msg" ).removeClass( "alert alert-info" ).addClass( "alert alert-success" ).text('Offer Updated sucessfully');	
//					$( "#top_msg" ).show();	  
//				    jQuery('.scroll-to-top').click();
					}
				else
				{
					$( "#top_msg" ).removeClass( "alert alert-danger" ).addClass( "alert alert-success" ).text(jsonResponse.msg);
				    $( "#top_msg" ).show();	  
				    jQuery('.scroll-to-top').click();
					
				}
				
//			    setTimeout(function(){
//			    	   window.location.reload(1);
//			    	}, 5000);	
		    
			}
		});
	}
	
	
	
	/**
	 * to populate the values
	 */
	function populateAddModal(){
		$("#title1").html($('#title').val());
		$("#subtitle1").html($('#subtitle').val());
		$("#form_date").html($('#datepicker').val());
		$("#to_date").html($('#datepicker_1').val());
		$("#description").html($('#summernote_1').val());
		
		}