<style>
@media only screen and (max-width: 720px) {
	.responsive-size {
		width: 270px;
		height: 320px;
	}
}

@media ( min-width :720px) {
	.responsive-size {
		width: auto;
		height: auto;
	}
}
​
</style>
<div class="row">
	<h3 class="block text-center">
		<b><font color="#32c5d2">IPV - In Person Verification</font></b>
	</h3>
</div>


<div class="row" id="camera">
	<input type="hidden" name="client_code" id="client_code"
		value="{{$code}}">
	<div id="message"></div>
	<div id="left" class=" text-center col-md-6 col-sm-12 col-xs-12"
		style="background-color: white;">

		<div class="text-center" id="my_camera" style="margin: 0 auto;"></div>
		<div class="row" id="start_ipv"
			style="text-align: center; margin: 10%">
			<div class="col-md-12 col-xs-12">
				<button class="btn-block btn green" onClick="startipv()"
					style="max-width: 300px">Start IPV</button>
			</div>
			<div class="col-md-12 col-xs-12">
				<a href="{!!url('/download-form')!!}"
					class="btn-block btn btn-primary"
					style="margin-top: 30px; max-width: 300px">Do It Later </a>
			</div>
		</div>
		<div class="row text-center">
			<div class="col-md-2 col-md-offset-3 col-xs-2 col-xs-offset-1"
				id="retake_buttons" style="display: none">
				<a class="btn btn-circle btn-icon-only border-dark "
					onClick="cancel_preview()"
					style="margin-top: 10px; color: black; border-radius: 2px"> <i
					class="fa fa-reply-all"></i>
				</a><b>Retake</b>
			</div>
			<div class="col-md-2 col-md-offset-5 col-xs-3 col-xs-offset-4"
				id="pre_take_buttons" style="max-width: 300px; display: none;">
				<a class="btn btn-circle btn-icon-only border-dark"
					onClick="preview_snapshot()"
					style="margin-top: 10px; color: black;"> <i class="fa fa-camera"></i>
				</a><b>Click</b>
			</div>
			<div class="col-md-2 col-md-offset-2 col-xs-2 col-xs-offset-5 "
				id="post_take_buttons" style="display: none">
				<a class="btn btn-circle btn-icon-only border-dark"
					onClick="save_photo()" style="margin-top: 10px; color: black;"> <i
					class="fa fa-save"></i>
				</a><b>Save</b>
			</div>
		</div>
	</div>
	<div id="info" class="col-md-6 col-xs-12 col-sm-6">
		<div style="text-align: center">
			<p>
				<b>What is In-Person Verification (IPV) and why is it required?</b>
			</p>
			<p>In-Person Verification (IPV) is a regulatory requirement that
				needs to be completed before we can activate your Trading account.


			<p>


			<p>IPV is also an additional requirement by SEBI(Securities and
				Exchange Board of India) from January 1, 2012


			<p id='hideme'>Please see the sample image below for guidelines</p>

		</div>
		<div id="samples" class="row">
			<div class="col-md-2 col-xs-3 col-sm-3">
				<a data-target="#basic" data-toggle="modal"
					onclick="populateDeleteModal('<?php echo Request::root();?>/images/ipv-sample/sample.jpg')"
					class="btn green-jungle btn-outline sbold"><img
					!class="responsive-size"
					src="<?php echo Request::root();?>/images/ipv-sample/sample.jpg"
					width="30" height="30"> </a>
			</div>
			<div class="col-md-2 col-xs-3 col-sm-3">
				<a data-target="#basic" data-toggle="modal"
					onclick="populateDeleteModal('<?php echo Request::root();?>/images/ipv-sample/sample1.jpg')"
					class="btn red btn-outline sbold"><img
					src="<?php echo Request::root();?>/images/ipv-sample/sample1.jpg"
					width="30" height="30"> </a>
			</div>
			<div class="col-md-2 col-xs-3 col-sm-3">
				<a data-target="#basic" data-toggle="modal"
					onclick="populateDeleteModal('<?php echo Request::root();?>/images/ipv-sample/sample2.jpg')"
					class="btn red btn-outline"><img
					src="<?php echo Request::root();?>/images/ipv-sample/sample2.jpg"
					width="30" height="30"> </a>
			</div>
			<div class="col-md-2 col-xs-3 col-sm-3">
				<a data-target="#basic" data-toggle="modal"
					onclick="populateDeleteModal('<?php echo Request::root();?>/images/ipv-sample/sample3.jpg')"
					class="btn red btn-outline"><img
					src="<?php echo Request::root();?>/images/ipv-sample/sample3.jpg"
					width="30" height="30"> </a>
			</div>
			<div class="col-md-2 col-xs-3 col-sm-3">
				<button hidden id="showerror" data-target="#basic"
					data-toggle="modal" onclick="ErrorMessage()"
					class=" hidden btn red btn-outline">123</button>
			</div>
		</div>
	</div>

</div>


<script
	src="<?php echo Request::root();?>/assets/global/plugins/bootstrap/js/bootstrap.min.js"
	type="text/javascript"></script>
<div aria-hidden="true" role="basic" tabindex="-1" id="basic"
	class="modal fade" style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button aria-hidden="true" data-dismiss="modal" class="close"
					type="button"></button>
			</div>
			<div class="modal-body" style="text-align: center;">
				<div id="result"></div>
			</div>
			<div class="modal-footer">
				<button data-dismiss="modal" class="btn dark btn-outline"
					type="button">Okay I Got it</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>

<!-- Configure a few settings and attach camera -->
<script language="JavaScript">
function Errorhandeller(){
	$('#my_camera').hide();
	$("#start_ipv").show();
	document.getElementById('pre_take_buttons').style.display = 'none';
	jQuery('#showerror').click();
}
function populateDeleteModal(link){
	var image = '<img class = "responsive-size" src='+link+'\>';
	 $("#result").html( image );
}
function ErrorMessage(){
	var screen_width =($( window ).width());
	if(screen_width <= 500){
		var msg = "No Camera.Please Try Again";
		$("#result").html( '&nbsp;<span class="alert alert-danger text-center text-responsive" style="text-align:center">'+msg+'</span>' );
	}
	if(screen_width >501){
		var msg = "No Camera Device Detected.Please Try Again";
		$("#result").html( '&nbsp;<span class="alert alert-danger text-center text-responsive" style="text-align:center">'+msg+'</span>' );

}
}
function startipv(){
	$("#start_ipv").hide();


	document.getElementById('pre_take_buttons').style.display = '';
	var screen_width =($( window ).width());

	if(screen_width<=500){

		Webcam.set({
				// live preview size
				width: 320,
				height: 240,

				// device capture size
				dest_width: 640,
				dest_height: 480,

				// final cropped size
				crop_width: 480,
				crop_height: 480,

				// format and quality
				image_format: 'jpeg',
				jpeg_quality: 90,

				// flip horizontal (mirror mode)
				flip_horiz: true
			});


			Webcam.attach( '#my_camera' );
	}
	else{
		Webcam.set({
			// live preview size
			width: 480,
			height: 320,

			// device capture size
			dest_width: 640,
			dest_height: 480,

			// final cropped size
			crop_width: 480,
			crop_height: 480,

			// format and quality
			image_format: 'jpeg',
			jpeg_quality: 90,

			// flip horizontal (mirror mode)
			flip_horiz: false
		});


		Webcam.attach( '#my_camera' );
	}
}
Webcam.on( 'error', function(err) {
    Errorhandeller();
} );

		</script>

<!-- Code to handle taking the snapshot and displaying it locally -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script language="JavaScript">

		// preload shutter audio clip
		var shutter = new Audio();
		shutter.autoplay = false;
		shutter.src = navigator.userAgent.match(/Firefox/) ? 'shutter.ogg' : 'shutter.mp3';

		function preview_snapshot() {
			// play sound effect
			try { shutter.currentTime = 0; } catch(e) {;} // fails in IE
			shutter.play();

			// freeze camera so user can preview current frame
			Webcam.freeze();

			// swap button sets
			document.getElementById('pre_take_buttons').style.display = 'none';
			document.getElementById('post_take_buttons').style.display = '';
			document.getElementById('retake_buttons').style.display = '';

		}

		function cancel_preview() {
			// cancel preview freeze and return to live camera view
			Webcam.unfreeze();

			// swap buttons back to first set
			document.getElementById('pre_take_buttons').style.display = '';
			document.getElementById('post_take_buttons').style.display = 'none';
			document.getElementById('retake_buttons').style.display = 'none';

		}

		function save_photo() {
			// actually snap photo (from preview freeze) and display it
			Webcam.snap( function(data_uri) {
				  var raw_image_data = data_uri.replace(/^data\:image\/\w+\;base64\,/, '');
				  var client_code = $("#client_code").val();
		    	$.ajax({
		    		type: "post",
		    		data: {raw_image_data,client_code},
		    		dataType: 'json',
		    		url:'{!! URL::route("save-ipv")!!}',
		    		success: function(jsonResponse){
		    		if(jsonResponse.success == true ){
						document.getElementById('post_take_buttons').style.display = 'none';
						document.getElementById('left').className = "hidden";
						$('#info').removeClass('').addClass('text-center col-md-12 col-xs-12 col-sm-12');
						document.getElementById('retake_buttons').style.display = 'none';
		    			document.getElementById('top_msg').style.display = '';
		    			document.getElementById('top_msg').style="text-align:center";
		    			$("#top_msg").html('&nbsp;<span class="alert alert-success" style="text-align:center">Image saved successfully</span>');
		    			$("#hideme").hide();
		    			$("#samples").hide();
		    			setTimeout(function(){window.location.href='{!! URL::route("generate-forms")!!}'},2000);
		    		}
		    		if(jsonResponse.success == false){

		    		return false;
		    		}
		    		}
		    		});

				// shut down camera, stop capturing
				Webcam.reset();

			} );
		}




	</script>
