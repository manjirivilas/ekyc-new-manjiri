$(document).ready(function () {
  $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });

$( "#ifsc" ).change(function() {

  $ifsc = $( "#ifsc" ).val().trim();
  $ifsc_regex = /^[A-Za-z]{4}\d{7}$/;
  if($ifsc_regex.test($ifsc) == false)
  {
    $("#ifscError").text('Invalid IFSC Code');
    setTimeout(function(){
	 				$("#ifscError").text('');
	 			}, 5000);
    return false;
  }
  else
  {

    $("#ifscError").text('');
    $.ajax({
   					 type: "POST",
   					 url: $('#url_path').val()+'/get-bank-details',
   					 data: {ifsc:$ifsc},
             dataType: 'json',
   					 success: function(jsonResponse)
   					 {
               if(jsonResponse.status == 3)
               {

                  // $("#bank_and_branch").val(jsonResponse.data['BANK']+","+jsonResponse.data['BRANCH']);
                  $("#bankname").val(jsonResponse.data['BANK']);
                  $("#branch").val(jsonResponse.data['BRANCH']);
                  $("#micr").val(jsonResponse.data['MICR']);
                 return true;

               }
               else
               {
                 $("#bank_and_branch").val();
                 $("#micr").val();
                 $('.error-msg').show();
                 setTimeout(function(){
             	 				 $('.error-msg').hide();
             	 			}, 5000);
                 return false;
               }

             }
           });
  }

});


$( "#secifsc" ).change(function() {
  $ifsc = $( "#secifsc" ).val().trim();
  $ifsc_regex = /^[A-Za-z]{4}\d{7}$/;
  if($ifsc_regex.test($ifsc) == false)
  {
    $("#sec_ifscError").text('Invalid IFSC Code');
    setTimeout(function(){
	 				$("#sec_ifscError").text('');
	 			}, 5000);
    return false;
  }
  else
  {
    $("#sec_ifscError").text('');
    $.ajax({
   					 type: "POST",
   					 url: $('#url_path').val()+'/get-bank-details',
   					 data: {ifsc:$ifsc},
             dataType: 'json',
   					 success: function(jsonResponse)
   					 {
               if(jsonResponse.status == 3)
               {
                 $("#secbankname").val(jsonResponse.data['BANK']);
                 $("#secbranch").val(jsonResponse.data['BRANCH']);
                 $("#secmicr").val(jsonResponse.data['MICR']);
                 return true;
               }
               else
               {
                 $("#sec_bank_and_branch").val();
                 $("#secmicr").val();
                 $('.sec_error-msg').show();
                 setTimeout(function(){
             	 				 $('.sec_error-msg').hide();
             	 			}, 5000);
                 return false;
               }

             }
           });
  }

});



    /*bank details js start*/
       var flag=0;
       $("#flag").val(flag);
       $("body").on('click', ".add-det", function(){
       $(".bank-account-sec.add-account1").removeClass('add-account');
       $(".add-det").addClass('add-account');
       $(".add-remove").removeClass('add-account');
       flag=1;
       $("#flag").val(flag);

    });


    $("body").on('click', ".add-remove", function(){
       $(".bank-account-sec.add-account1").addClass('add-account');
       $(".add-det").removeClass('add-account');
       $(".add-remove").addClass('add-account');
       flag=0;
       $("#flag").val(flag);
    });
     /*bank details js end*/

});
