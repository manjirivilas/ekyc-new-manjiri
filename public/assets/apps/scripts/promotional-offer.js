

	function viewHistory()
	{

		if(document.getElementById('client_id').value === '' )
		{
			document.getElementById('err_msg').style.display = "block";
			document.getElementById('err_msg').innerText ="please enter client id";
			$('.portlet-body').hide();
			$('#page_status_view').dataTable().fnClearTable();
			setTimeout(function(){ document.getElementById('err_msg').style.display = "none";document.getElementById('err_msg').innerText ='';}, 5000);
			return;
		}
$('#wait_msg').show();
	$.ajax({
		type: "post",
		url:$('#url_path').val()+'/client-offer',
		data: {client_id:$("#client_id").val()},
		dataType: 'json',
		success: function(jsonResponse){
			if(jsonResponse.status == 'success')
			{
				$('.portlet-body').show();
				$("#submit").removeAttr("disabled");
				$('#wait_msg').hide();
				$('#err_msg').hide();
				$('#page_status_view').dataTable().fnDestroy();
				$('#page_status_view').dataTable( {
				 "data" : jsonResponse.msg,
				 "columns": [
										 { "data": "id" },
										 { "data": "name" },
										 { "data": "applied_date" },
										{ "data": "type" },
										{ "data": "expiry_date" },
										{ "data": "off_value" },
										],
					 } );
			}
			else if(jsonResponse.status == 'fail')
			{
							$('.portlet-body').hide();
						  $("#submit").removeAttr("disabled");
							$('#wait_msg').hide();
							$('#page_status_view').dataTable().fnClearTable();
							$('#err_msg').show();
							$('#err_msg').text(jsonResponse.msg);
						  setTimeout(function(){ $('#err_msg').slideUp(); $('#err_msg').text('');}, 5000);
			}
			else
			{
				$('#wait_msg').hide();
				$('.portlet-body').hide();
				$('#page_status_view').dataTable().fnClearTable();
				$('#err_msg').show();
				$('#err_msg').text(jsonResponse.msg);
				setTimeout(function(){ $('#err_msg').slideUp(); $('#err_msg').text('');}, 5000);
			}

		}
	});
	}

function insertCoupon()
{
	if(document.getElementById('client_id').value === '' )
	{
		document.getElementById('err_msg').style.display = "block";
		document.getElementById('err_msg').innerText ="please enter client id";
		$('.portlet-body').hide();
		$('#page_status_view').dataTable().fnClearTable();
		setTimeout(function(){ document.getElementById('err_msg').style.display = "none";document.getElementById('err_msg').innerText ='';}, 5000);
		return;
	}
	$('#wait_msg').show();
	$('#apply').prop('disabled', true);
	$.ajax({
		type: "post",
		url:$('#url_path').val()+'/assign-client-offer',
		data: {client_id:$("#client_id").val(),coupon_code:$(".offer").val()},
		dataType: 'json',
		success: function(jsonResponse){
			$('#apply').prop('disabled', false);
			$('#wait_msg').hide();
			if(jsonResponse.status == 'success')
			{
				//$('.portlet-body').show();
				$("#submit").removeAttr("disabled");

				$('#err_msg').hide();
				$('#top_msg').show();
				$('#top_msg').text(jsonResponse.msg);
				setTimeout(function(){ $('#top_msg').slideUp(); $('#top_msg').text('');}, 5000);
			}
			else
			{
							//$('.portlet-body').hide();
						  $("#submit").removeAttr("disabled");
							$('#top_msg').hide();
							$('#err_msg').show();
							$('#err_msg').text(jsonResponse.msg);
						  setTimeout(function(){ $('#err_msg').slideUp(); $('#err_msg').text('');}, 5000);
			}


		}
	});
}
$("#page_status_view").dataTable( {
    "sScrollX": '100%'
} );

//coupon jv file



function callCoupon()
{
//  alert($("#date").val());
if($("#date").val())
{
  $('#muliSelect6')
    .find('option')
    .remove()
    .end();
  $('#wait_msg').show();
  	$.ajax({
  		type:"post",
  		url:$('#url_path').val()+'/active-coupons',
  		data: {date:$("#date").val()},
  		dataType: 'json',
      success: function(data){
        $('#wait_msg').hide();

              var toAppend = '';
             $.each(data,function(i,o){
             toAppend += '<option  value="'+o.code+'">'+o.name+'</option>';
            });

           $('#muliSelect6').append(toAppend);
					 if($("#muliSelect6").val()== 'no_coupon')
					 {
						  $('.report').prop('disabled', true);
					 }
					 else
					 {
					 		$('.report').prop('disabled', false);
					 }
				 }

  	});


}

}
function DownloadReport()
{
	if(document.getElementById('date').value === '' )
	{
		document.getElementById('err_msg').style.display = "block";
		document.getElementById('err_msg').innerText ="please select the month";
		$('.portlet-body').hide();
		$('#page_status_view').dataTable().fnClearTable();
		setTimeout(function(){ document.getElementById('err_msg').style.display = "none";document.getElementById('err_msg').innerText ='';}, 5000);
		return;
	}

	$('#wait_msg').show();
	$('.report').prop('disabled', true);
	$.ajax({
	type:"post",
	url:$('#url_path').val()+'/generate-report',
	data: {date:$("#date").val(),coupon_code:$("#muliSelect6").val()},
	dataType: 'json',
	success: function(jsonResponse)
	{
		$('#wait_msg').hide();
		$('.report').prop('disabled', false);
		if(jsonResponse.status == 'success')
		{
			$('.portlet-body').show();
			$("#submit").removeAttr("disabled");
			$('#wait_msg').hide();
			$('#err_msg').hide();
			$('#page_status_view').dataTable().fnDestroy();
			document.getElementById('download_link').click();
		}
		else
		{
			$('#err_msg').show();
			$('#err_msg').text(jsonResponse.msg);
			setTimeout(function(){ $('#err_msg').slideUp(); $('#err_msg').text('');}, 5000);
		}
	}

});

}
function GenerateReport()
{
	if(document.getElementById('date').value === '' )
	{
		document.getElementById('err_msg').style.display = "block";
		document.getElementById('err_msg').innerText ="please select the month";
		$('.portlet-body').hide();
		$('#page_status_view').dataTable().fnClearTable();
		setTimeout(function(){ document.getElementById('err_msg').style.display = "none";document.getElementById('err_msg').innerText ='';}, 5000);
		return;
	}
	  $('#wait_msg').show();
		$('.report').prop('disabled', true);
	$.ajax({
		type:"post",
		url:$('#url_path').val()+'/generate-report',
		data: {date:$("#date").val(),coupon_code:$("#muliSelect6").val()},
		dataType: 'json',
		success: function(jsonResponse)
		{
			$('#wait_msg').hide();
			$('.report').prop('disabled', false);
			if(jsonResponse.status == 'success')
			{
				$('.portlet-body').show();
				$("#submit").removeAttr("disabled");
				$('#wait_msg').hide();
				$('#err_msg').hide();
				$('#page_status_view').dataTable().fnDestroy();
				$('#page_status_view').dataTable( {
				 "data" : jsonResponse.msg,
				 "columns": [
										 { "data": "client_id" },
										 { "data": "trade_count" },
										 { "data": "status" },
										],
					 } );
			}
			else
			{
				$('#err_msg').show();
				$('#err_msg').text(jsonResponse.msg);
				setTimeout(function(){ $('#err_msg').slideUp(); $('#err_msg').text('');}, 5000);
			}
		}

	});

}
