/*
 * to disable future date
 */
function verifyDate()
	{
		var newdate=$('#datepicker-margin').val();
		if( newdate!=''){
			$('#submit').prop('disabled', false);
		}
	}
//Button Disable/Enable
$('#chkactall').click(function(e){
    var table= $(e.target).closest('table');
   var numOfVisibleRows = $('tr:visible');
   $('td input:checkbox',table).prop('checked',e.target.checked);
});


$('#chkactall').change(function(){
	var search = $('.input-small').val();
	if ($('#chkactall').is(':checked')) {
		var table=$('#sample_4').DataTable();
	    var cells = table.cells( ).nodes();
	  // $( cells ).find(':checkbox').prop('checked', $(this).is(':checked'));
	    $('#btn1').prop('disabled', false);

    } else {
    	 $('#btn1').prop('disabled', true);
    }
	
});

// Each button select enable button

function getClientid()
{
	var count=0;
	 $('.chk1').each(function() {
	       if ($(this).is(':checked')) {// check the checked property with .is
	             count++;
	        }
	      
	    });
	    if(count>0)
	    {
	    	$('#btn1').prop('disabled', false);
		 }
	    else{$('#btn1').prop('disabled', true);}
	
}

//send mail js



function validateform(objForm)/* datatable form validation with checkbox*/
{
    jQuery('.scroll-to-top').click();
	$( "#top_msg" ).addClass( "alert alert-info" ).text("Please wait email sending is in progress.......");
	  $( "#top_msg" ).show();	
	
	$('#btn1').prop('disabled', true);
	//$('#btn2').prop('disabled', true);
	var client_id_arr = [];
    var client_id = {};
    var key = [];
    $('.chk1').each(function() {
       if ($(this).is(':checked')) {// check the checked property with .is
               var current = $(this).val();
			   var clientid = $(this).parents("tr").find(".client_id").text();
               var key = $(this).parents("tr").find("#key").text();//get the input textbox associated with it
			   client_id[key]=clientid;
              
        }
        client_id_arr.push($(this).val());
    });
	
//	  alert(JSON.stringify(client_id));

    $.ajax({
		type: "post",
//		url: '{{route("send-mail-margin-reporting")}}',
		url: $('#hndroute').val()+'/sendmail-margin-shortage',
		data: {client:client_id},
		dataType: 'json',
		success: function(jsonResponse){
			if(jsonResponse.msg == undefined || jsonResponse.msg == null || jsonResponse.msg.length == 0)
			{
			$( "#top_msg" ).removeClass( "alert alert-info" ).addClass( "alert alert-success" ).text("Email has been sent to ").append(jsonResponse.count).append(" clients ");
		    $( "#top_msg" ).show();	  
		    jQuery('.scroll-to-top').click();
			}
			else
			{
				$( "#top_msg" ).removeClass( "alert alert-info" ).addClass( "alert alert-danger" ).text(jsonResponse.msg).append(" Email Failed");
			    $( "#top_msg" ).show();	  
			    jQuery('.scroll-to-top').click();
				
			}
			
		    setTimeout(function(){
		    	   window.location.reload(1);
		    	}, 5000);	
	    
		}
	});



}

function validateformsms(objForm)/* datatable form validation with checkbox*/
{
	jQuery('.scroll-to-top').click();
	$( "#top_msg" ).addClass( "alert alert-info" ).text("Please wait sms sending is in progress.......");
	  $( "#top_msg" ).show();	
	
	$('#btn22').prop('disabled', true);
	//$('#btn2').prop('disabled', true);
	var client_id_arr1 = [];
    var client_id1 = {};
    var key1 = [];
    $('.chk12').each(function() {
       if ($(this).is(':checked')) {// check the checked property with .is
    	   var client_id1 = {};
               var current1 = $(this).val();
             //  alert(current1);
			   var clientid1 = $(this).parents("tr").find(".msg").text();
			   
               var key1 = $(this).parents("tr").find(".mobile_no").text();//get the input textbox associated with it
               			   client_id1[key1]=clientid1;
               	//		alert(clientid1);

        }
        client_id_arr1.push(client_id1);
        
        
       // alert(JSON.stringify(client_id_arr1));
    });
	
	
	
	  
    $.ajax({
		type: "post",
//		url: '{{route("send-sms-margin-reporting")}}',
		url: $('#hndroute').val()+'/sendsms-margin-shortage',
		data: {client:client_id_arr1},
		dataType: 'json',
		success: function(jsonResponse){
			if(jsonResponse.msg == undefined || jsonResponse.msg == null || jsonResponse.msg.length == 0)
			{
			$( "#top_msg" ).removeClass( "alert alert-info" ).addClass( "alert alert-success" ).text("SMS has been sent to ").append(jsonResponse.count).append(" clients ");
		    $( "#top_msg" ).show();	  
		    jQuery('.scroll-to-top').click();
			}
			else
			{
				$( "#top_msg" ).removeClass( "alert alert-info" ).addClass( "alert alert-danger" ).text(jsonResponse.msg).append(" SMS Failed");
			    $( "#top_msg" ).show();	  
			    jQuery('.scroll-to-top').click();
				
			}
			
		    setTimeout(function(){
		    	   window.location.reload(1);
		    	}, 5000);	
	    
		}
	});


}

