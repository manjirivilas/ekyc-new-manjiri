/*
 * hide top messages
 */
	  var msg = $("#top_msg1").text();
	  if(msg == "Select proper file"){
		  $("#top_msg1").fadeOut(5000);
	  }
/*
 * call onclick of UPLODE FILES
 */
function validateform(){
	
	var file1=$('#upload1').val();
	var file2=$('#upload2').val();
	var file3=$('#upload3').val();
	var file4=$('#upload4').val();

	if(file1 !=''|| file2!=''||file3 !=''||file4 !='')
	{
		var now = file1.indexOf("NOW") > -1;
		var nest = file2.indexOf("NEST") > -1;
		var comm = file3.indexOf("COMM") > -1;
		var eq = file4.indexOf("EQ") > -1;
		if(!now && file1 !=''){
			var wrongData = 'now';
		}else if(!nest && file2 !=''){
			var wrongData = 'nest';
		}else if(!comm && file3 !=''){
			var wrongData = 'commodity';
		}else if(!eq && file4 !=''){
			var wrongData = 'equity';
		}
		if( (!now && file1 !='') || (!nest && file2!='') || (!comm && file3!='') || (!eq && file4!='') ){
			 $( "#top_msg" ).removeClass().addClass( "alert alert-danger"  ).text("Wrong file uploaded. Please try again.").show();
		}else{
			   $("#btn1").prop('disabled', true);
		 $( "#top_msg" ).removeClass().addClass( "alert alert-info"  ).text("File uploading is in progress please wait...").show();
		$('#fileupload').submit();
		   }
	} else
	{
		 $( "#top_msg" ).removeClass( "alert alert-info" ).addClass( "alert alert-danger" ).text("Please select at least one file");
		    $( "#top_msg" ).show();	  
		    $('.scroll-to-top').click();
		    $( "#top_msg" ).fadeOut(3000);
	}
}

/*
 * Call Onclick of Generate File
 */
function generatePayout(){

		$.ajax({
			type : "POST",
			url : $('#url_path').val() + '/generate-payout-file',
			dataType : 'json',
			headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            },
			success : function(jsonResponse) {
				if (jsonResponse.msg == undefined || jsonResponse.msg == null
						|| jsonResponse.msg.length == 0) {
					$("#top_msg").removeClass("alert alert-info").addClass(
							"alert alert-danger").text(jsonResponse.msg);
					$("#top_msg").show();
					jQuery('.scroll-to-top').click();
				} else {
					$("#top_msg").removeClass("alert alert-danger").addClass(
							"alert alert-success").text(jsonResponse.msg);
					$("#top_msg").show();
					jQuery('.scroll-to-top').click();

				}
				setTimeout(function(){
			    	   window.location.reload(1);
			    	}, 5000);
			}
		});
		
}
/*
 * Calls Onclick Of Compare 
 */
function comparePayout(){
	$("#top_msg1").hide();
	$("#compare").prop('disabled', true);
	 $( "#top_msg" ).removeClass().addClass( "alert alert-info"  ).text("File comparison is in progress please wait...");
	$.ajax({
		type : "post",
		url : $('#url_path').val() + '/compare-payout-file',
		dataType : 'json',
		success : function(jsonResponse) {
			
			if (jsonResponse.msg == undefined || jsonResponse.msg == null
					|| jsonResponse.msg.length == 0) {
				$("#top_msg").removeClass("alert alert-info").addClass(
						"alert alert-danger").text(jsonResponse.msg);
				$("#top_msg").show();
			
				jQuery('.scroll-to-top').click();
				$("#top_msg").fadeOut(3000);
			} else {
				$("#top_msg").removeClass("alert alert-info").addClass(
						"alert alert-success").text(jsonResponse.msg);
				$("#top_msg").show();
				jQuery('.scroll-to-top').click();
				$("#top_msg").fadeOut(3000);
			}

		}
	});
	
}

