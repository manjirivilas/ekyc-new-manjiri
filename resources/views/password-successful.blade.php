
<input type = 'hidden' value = "<?php echo Request::root();?>" id = "url_path"/>



@include('includes.header')

        <div class="main-inner-sec full">
          <div class="main-tb-sec pass-successful-sec">
            <div class="form-col">

              @if($errors->has('server-error'))
              <div class="alert alert-danger msgError">
                  {{ $errors->first('server-error') }}
              </div>
              @endif

            @if(session()->has('message'))
                <div class="alert alert-success msgError">
                    {{ session()->get('message') }}
                </div>
            @endif
                <div class="form-col-inner full">
                    <h2 class="form-title">Password reset successful!</h2>
                    <img src="images/pass-reset-icon.png" alt=""/>
                    <p class="pass-para">Let’s get started and explore the world of <br/>Trading at your finger tips</p>
                    <a href="<?php echo Request::root();?>/login"><button type="submit" value="login" class="btn blue-btn">login</button></a>
                </div>
             </div>



              <!--<div class="document-col">
                  <div class="document-col-inner full">
                      <div class="req-doc-sec">
                          <div class="hi-box full">
                              <div class="hi-box-inner tooltip-sec" id="tool-tip">
                                  <h4>Few Tips!</h4>
                                  <p>Make sure you don’t share the password with anyone.</p>
                              </div>
                          </div>
                          <div class="req-img-sec">
                              <img src="images/women.png" alt="req-img" />
                          </div>
                      </div>
                  </div>
            </div>-->

            <div class="document-col">
                <div class="document-col-inner full">
                  <div class="mobi-document-col-inner">
                  <div class="req-doc-sec">
                    <div class="hi-box tooltip-bx">
                      <div class="hi-box-inner tooltip-sec  tooltip-bx-inner" id="tool-tip">
                          <h4>Few Tips!</h4>
                          <p>Make sure you don’t share the password with anyone.</p>
            </div>
            </div>
                    <div class="req-img-sec">
                      <img src="{{ Request::root() }}/images/women.png" alt="req-img" />
                    </div>
                <div class="">
                <div class="mobi-hi-sec-inner full">
                  <div class="profile-img-sec">
                        <div class="profile-img">
                            <img src="{{ Request::root() }}/images/profile-img.png" alt="profile-img"/>
                        </div>
                        <h4><span>Hey!</span> Aishwarya</h4>
                         <p>I am here to help!</p>
                   </div>
                   <div class="up-arrow-sec">
                      <img src="{{ Request::root() }}/images/popup-close-arw.png" alt="close-img"/>
                   </div>
                </div>
                  </div>
                </div>
                </div>
                </div>
             </div>


            </div>
             <div class="mobi-hi-sec">
                <div class="mobi-hi-sec-inner bt-mobi-inner full">
                  <div class="profile-img-sec">
                        <div class="profile-img">
                            <img src="<?php echo Request::root();?>/images/profile-img.png" alt="profile-img"/>
                        </div>
                        <h4><span>Hey!</span>  Aishwarya</h4>
                        <p>I am here to help!</p>
                   </div>
                   <div class="up-arrow-sec">
                      <img src="{{ Request::root() }}/images/up-arrow.png" alt="up-arrow"/>
                   </div>
                   </div>
                </div>
             </div>
         </div>
       </div>
    </div>
@include('includes.footer')
