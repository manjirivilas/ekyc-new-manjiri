{!! Form::open(array('route' => 'logout','method'=>'POST', 'class'=>'form-horizontal', 'id'=>'logout'))
!!}

@if(session()->has('error'))
    <div class="alert alert-success msgError">
        {{ session()->get('error') }}
    </div>
@endif
<div align="right" style="vertical-align: top;">
  <button id="logout" >Logout</button>
</div>
<h1>Welcome to Dashboard</h1>
<input type = 'hidden' value = "<?php echo Request::root();?>" id = "url_path"/>
<meta name="csrf-token" content="{{ csrf_token() }}">
