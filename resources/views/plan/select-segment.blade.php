<!-- Header -->
@include('includes.index-header')

<!-- Content Start -->

<div class="tab-content">
    <div class="tab-pane active">
        <h2 class="form-title">Select Segment</h2>
        <div class="form-para-sec">
            <p class="form-para">You can select multiple segments</p>
        </div>
        @if($errors->has('error'))
            <div class="alert alert-danger msgError">
                {{ $errors->first('error') }}
            </div>
        @endif


          <form method="post" id="selectSegmentForm" name="selectSegment"  action-xhr="<?php echo Request::root().'/select-segment';?>">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="adhaar-sec">
                <div class="form-rw full">
                    <div class="segment-div full">
                        <div class="form-group full">
                            <div class="segment-head"><img src="{{ Request::root() }}/images/segment/equity-icon.png"><span>Equity</span></div>
                            <div class="checkbox">
                                <label data-toggle="collapse" data-target="#collapse-equity" aria-expanded="false" aria-controls="collapse-equity">
                                    <span class="segmentchk"><input type="checkbox" name="equity_flag" id="segment-equity" value="0" {{ $data['equity_flag']?'checked':'' }}><label for="segment-equity"></label></span>
                                </label>
                            </div>
                        </div>
                        <div id="collapse-equity" aria-expanded="false" class="full collapse {{ $data['equity_flag']?'in':'' }}">
                            <div class="well segment-txt full">
                                <ul class="full seg-chk">
                                    <li><span class="segmentchk"><input type="checkbox" name="equity_seg[]"class="equity_chk" id="nse-cash" value="NSE_CASH" {{ strstr($data['equity_seg'], "NSE_CASH")?'checked':'' }} ><label for="nse-cash"></label></span>nse cash</li>
                                    <li><span class="segmentchk"><input type="checkbox" name="equity_seg[]" class="equity_chk" id="nse-fo" value="NSE_FNO" {{ strstr($data['equity_seg'], "NSE_FNO")?'checked':'' }} ><label for="nse-fo"></label></span>nse f & o</li>
                                    <li><span class="segmentchk"><input type="checkbox" name="equity_seg[]" class="equity_chk" id="bse-cash" value="BSE_CASH" {{ strstr($data['equity_seg'], "BSE_CASH")?'checked':'' }} ><label for="bse-cash"></label></span>bse cash</li>
                                    <li><span class="segmentchk"><input type="checkbox" name="equity_seg[]" class="equity_chk" id="bse-fo" value="BSE_FNO"  {{ strstr($data['equity_seg'], "BSE_FNO")?'checked':'' }} ><label for="bse-fo"></label></span>bse f & o</li>
                                </ul>
                                <p class="full segment-select">
                                    <div id="select-segment1" class="dropdown select-segment-sec">
                                        <input type="text" name="equity_plan" id="equity-txt" class="select-segment-txt dropdown-toggle" data-toggle="dropdown" id="select-segment-txt1"
                                        value="Select pricing plan for equity" readonly>
                                        <ul class="dropdown-menu select-segment-list" id="select-segment-list1">
                                            <li  ><a href="#" class="select-seg-opt">Only Rs. 15 per executed order</a></li>
                                            <li>
                                                <label class="select-seg-opt">Trade @ 7 Paise</label>
                                                <a href="#">Intraday & Futures @ 0.0007 %, Delivery @ 0.007 % , Other Options @ Rs 7/ Lot</a>
                                            </li>
                                            <li><a href="#"  class="select-seg-opt">Unlimited Trading for Rs 3999 per month</a></li>
                                        </ul>
                                    </div>
                                </p>

                            </div>
                        </div>
                    </div>
                    <div class="segment-div full">
                        <div class="form-group full">
                            <div class="segment-head"><img src="{{ Request::root() }}/images/segment/commodity-icon.png"><span>commodity</span></div>
                            <div class="checkbox">
                                <label data-toggle="collapse" data-target="#collapse-commodity" aria-expanded="false" aria-controls="collapse-commodity">
                                    <span class="segmentchk"><input type="checkbox" name="commodity_flag" id="segment-commodity" value="0" {{ $data['commodity_flag']?'checked':'' }}><label for="segment-commodity"></label></span>
                                </label>
                            </div>
                        </div>
                        <div id="collapse-commodity" aria-expanded="false" class="full collapse {{ $data['commodity_flag']?'in':'' }}">
                            <div class="well segment-txt full">
                                <ul class="full seg-chk">
                                    <li><span class="segmentchk"><input type="checkbox" name="commodity_seg[]" class="commodity-chk" id="nse" value="NSE" {{ strstr($data['commodity_seg'], "NSE")?'checked':'' }}><label for="nse"></label></span>nse</li>
                                    <li><span class="segmentchk"><input type="checkbox" name="commodity_seg[]" class="commodity-chk" id="ncdex" value="NCDEX" {{ strstr($data['commodity_seg'], "NCDEX")?'checked':'' }}><label for="ncdex"></label></span>ncdex</li>
                                </ul>
                                <p class="full segment-select">
                                    <div id="select-segment2" class="dropdown select-segment-sec">
                                        <input type="text" value="Select pricing plan for commodity" name="commodity_plan" id="select-segment-btn" class="select-segment-txt dropdown-toggle" data-toggle="dropdown" readonly>
                                        <ul class="dropdown-menu select-segment-list" id="select-segment-list2">
                                            <li><a href="#" class="select-seg-opt">Only Rs. 15 per executed order</a></li>
                                            <li>
                                                <label class="select-seg-opt">Trade @ 7 Paise</label>
                                                <a href="#">Intraday &amp; Futures @ 0.0007 %, Delivery @ 0.007 % , Other Options @ Rs 7/ Lot</a>
                                            </li>
                                            <li><a href="#"  class="select-seg-opt">Unlimited Trading for Rs 3999 per month</a></li>
                                        </ul>
                                    </div>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="segment-div full">
                        <div class="form-group full">
                            <div class="segment-head"><img src="{{ Request::root() }}/images/segment/currency-icon.png"><span>currency</span></div>
                            <div class="checkbox">
                                <label data-toggle="collapse" data-target="#collapse-currency" aria-expanded="false" aria-controls="collapse-currency">
                                    <span class="segmentchk"><input type="checkbox" name="currency_flag" id="segment-currency" value="0" {{ $data['currency_flag']?'checked':'' }}><label for="segment-currency"></label></span>
                                </label>
                            </div>
                        </div>
                        <div id="collapse-currency" aria-expanded="false" class="full collapse {{ $data['currency_flag']?'in':'' }}">
                            <div class="well segment-txt">
                                <ul  class="full seg-chk">
                                    <li><span class="segmentchk"><input type="checkbox" name="currency_seg[]" class="currency-chk" id="mcx" value="MCX" {{ strstr($data['currency_seg'], "MCX")?'checked':'' }}><label for="mcx"></label></span>nse</li>
                                    <li><span class="segmentchk"><input type="checkbox" name="currency_seg[]" class="currency-chk" id="ncdex-currency" value="NCDEX" {{ strstr($data['currency_seg'], "NCDEX")?'checked':'' }}><label for="ncdex-currency"></label></span>ncdex</li>
                                </ul>
                                <p class="full segment-select">
                                    <div id="select-segment3" class="dropdown select-segment-sec">
                                        <input type="text" value="Select pricing plan for currency" name="currency_plan" id="select-segment-btn" class="select-segment-txt dropdown-toggle" data-toggle="dropdown" readonly>
                                        <ul class="dropdown-menu select-segment-list"  id="select-segment-list3">
                                            <li><a href="#" class="select-seg-opt">Only Rs. 15 per executed order</a></li>
                                            <li>
                                                <label class="select-seg-opt">Trade @ 7 Paise</label>
                                                <a href="#">Intraday &amp; Futures @ 0.0007 %, Delivery @ 0.007 % , Other Options @ Rs 7/ Lot</a>
                                            </li>
                                            <li><a href="#"  class="select-seg-opt">Unlimited Trading for Rs 3999 per month</a></li>
                                        </ul>
                                    </div>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div id="trade_div"></div>
                </div>
                <button type="submit" value="submit" class="btn blue-btn" onclick="">Proceed to next step</button>
            </div>
        </form>
    </div>
    <div class="clearfix"></div>
</div>
<!-- Content end -->

<!-- Tooltip Content -->

<!-- Tooltip Content -->
@include('ui-inner.tooltip.select-segment')
<!-- includes scripts -->
@include('includes.index-footer')
<script type="text/javascript" src="{{ Request::root() }}/assets/apps/scripts/plan-validation.js"></script>
<script type="text/javascript" src="{{ Request::root() }}/assets/apps/scripts/plan.js"></script>

</body>

</html>
