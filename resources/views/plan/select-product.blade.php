<!-- Header -->
@include('includes.index-header')

<!-- Content Start -->

<div class="tab-content">
    <div class="tab-pane active">
        <h2 class="form-title">Please choose product/s</h2>
        @if($errors->has('error'))
            <div class="alert alert-danger msgError">
                {{ $errors->first('error') }}
            </div>
        @endif
          <form method="post" id="selectProduct" name="selectProduct"  action-xhr="<?php echo Request::root().'/select-product';?>">

                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="adhaar-sec  plan-sec">
                <div class="form-rw full">
                    <div class="form-group">
                        <div class="half-row">
                            <div class="half">
                                <div id="dmaholder" class="dmaholder">
                                    <img src="{{ Request::root() }}/images/plan-icon.jpg" class="center-img">
                                    <span class="center-span plan-txt">i wish to open<br>demat account with vns</span>
                                    <div id="mask" class="mask plan-chk prodctinput-container">
                                        <input type="checkbox" id="dematchk" name="demat_acc_flag" value="1" checked="checked"/>
                                        <label for="customCheckboxInput"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="half">
                                <div id="ndmaholder" class="dmaholder">
                                    <img src="{{ Request::root() }}/images/plan-icon.jpg" class="center-img">
                                    <span class="center-span plan-txt">add existing<br>demat account</span>
                                    <div id="mask" class="mask  plan-chk prodctinput2-container">
                                        <input type="checkbox" id="nondematchk"  checked="checked">
                                        <label for="customCheckboxInput"></label>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
                <div class="form-rw full add_nondemat_sec">
                    <div class="demat-limit add_nondemat" id="demat-limit">
                        <span class="txt-red">Caution!!! Limitations if you donot open a dematNc with us but links an outside demat account: </span>
                        <ul>
                            <li>You won't be able to sell shares in delivery.</li>
                            <li>You won’t be able to avail Margin Against Shares facility.</li>
                        </ul>

                    </div>
                    <div class="add_nondemat full">
                      <div class="form-rw full">
                        <label>Demat Account Name*</label>
                        <input type="text" name="demat_name" id="demat_name" class="form-control">
                        @if($errors->has('demat_name'))
                        <span class="msgError">{{ $errors->first('demat_name') }}</span>
                        @endif

                    </div>
                    <div class="bank-rw">
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>DP ID*</label>
                                  <input type="text" name="dp_id" id="dp_id" value=""  class="form-control">
                                  @if($errors->has('dp_id'))
                                  <span class="msgError">{{ $errors->first('dp_id') }}</span>
                                  @endif
                            </div>
                        </div>
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>BO ID*</label>
                                  <input type="text" name="bo_id" id="bo_id" value="" class="form-control">
                                  @if($errors->has('bo_id'))
                                  <span class="msgError" >{{ $errors->first('bo_id') }}</span>
                                  @endif
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="form-rw full">
            <div class="form-group">
                <label>Do you want Delivery Instruction slip?<span><img src="{{ Request::root() }}/images/icons/info-icon.png" class="dis-img"></span></label>
                <div class="radio-rw">
                    <ul class="radio-list">
                        <li>
                            <input type="radio" id="ds-yes-option" name="instruction_slip" value="1">
                            <label for="ds-yes-option">Yes</label>
                            <div class="check"></div>
                        </li>
                        <li>
                            <input type="radio" id="ds-no-option" name="instruction_slip" value="0" checked="true">
                            <label for="ds-no-option">No</label>
                            <div class="check">
                                <div class="inside"></div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
                <button type="submit" value="submit" class="btn blue-btn" onclick="">Proceed to next step</button>
            </div>
        </form>
    </div>
    <div class="clearfix"></div>
</div>
<!-- Content end -->

<!-- Tooltip Content -->

<!-- Tooltip Content -->
@include('ui-inner.tooltip.select-product')
<!-- includes scripts -->
@include('includes.index-footer')

 <script type="text/javascript" src="{{ Request::root() }}/assets/apps/scripts/plan-validation.js"></script>

    <script type="text/javascript" src="{{ Request::root() }}/assets/apps/scripts/plan.js"></script>

    </body>

</html>
