<!-- Header -->
@include('includes.index-header')

<!-- Content Start -->

<div class="tab-content">
    <div class="tab-pane active">

        <form method="post" id="planSummaryForm" name="planSummeryForm"  action-xhr="<?php echo Request::root().'/plan-summary';?>">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="adhaar-sec">
                <div class="bank-account-sec remove-line plan-summ-sec full">
                    <div class="edit-title-rw full">
                        <h2 class="bank-title form-title">Summary</h2>
                        <a class="edit-btn" id="trading-details-edit" href="{{ Request::root() }}/plan-selection/select-segment"><img src="{{ Request::root() }}/images/add-icon/edit-pencil.png" alt="edit-pen" />Edit</a>
                        <!-- <a class="save-btn" id="trading-details-save"><img src="{{ Request::root() }}/images/add-icon/edit-pencil.png" alt="edit-pen"/>Save</a> -->
                    </div>
                    <div class="plan-sec-rw">
                        <div class="plan-col">
                            <div class="plan-col-inner full">
                                <span class="circle-no">1</span>
                                <div class="plan-info">
                                    <h2 class="plan-title">EQUITY</h2>
                                    <p class="plan-status">Exchange</p>
                                    <p class="plan-para">{{ (isset($segment['equity_seg']) && $segment['equity_seg'])?$segment['equity_seg']:'None'}}</p>
                                </div>
                                <div class="plan-price-sec">
                                    <p class="plan-status">Pricing Plan</p>
                                  <p class="order-para">
                                      {{ isset($segment['equity_plan'])?$segment['equity_plan']:'None'}}
                                  </p>
                                    <!-- <span class="plan-price">Rs.15</span>
                                    <p class="order-para">Per Executed Order</p> -->
                                </div>
                            </div>
                        </div>
                        <div class="plan-col">
                            <div class="plan-col-inner full">
                                <span class="circle-no">2</span>
                                <div class="plan-info">
                                    <h2 class="plan-title">COMMODITY</h2>
                                    <p class="plan-status">Exchange</p>
                                    <p class="plan-para">{{ (isset($segment['commodity_seg']) && $segment['commodity_seg'])?$segment['commodity_seg']:'None'}}</p>
                                </div>
                                <div class="plan-price-sec">
                                    <p class="plan-status">Pricing Plan</p>
                                    <p class="order-para">
                                      {{ isset($segment['commodity_plan'])?$segment['commodity_plan']:'None'}}
                                    </p>
                                    <!-- <span class="plan-price">Rs.15</span>
                                    <p class="order-para">Per Executed Order</p> -->
                                </div>
                            </div>
                        </div>
                        <div class="plan-col">
                            <div class="plan-col-inner full">
                                <span class="circle-no">3</span>
                                <div class="plan-info">
                                    <h2 class="plan-title">CURRENCY</h2>
                                    <p class="plan-status">Exchange</p>
                                    <p class="plan-para">{{ (isset($segment['currency_seg']) && $segment['currency_seg'])?$segment['currency_seg']:'None'}}</p>
                                </div>
                                <div class="plan-price-sec">
                                    <p class="plan-status">Pricing Plan</p>
                                    <p class="order-para">
                                      {{ isset($segment['currency_plan'])?$segment['currency_plan']:'None'}}
                                  </p>
                                    <!-- <span class="plan-price">Rs.15</span>
                                    <p class="order-para">Per Executed Order</p> -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="add-an-nominee-rw full plan-nominee-rw" {{ $segment['mutual_fund']?'':'hidden'}}>
                        <span class="add-det">
<img src="{{ Request::root() }}/images/icons/nominee-icon.png" alt=""/>
<span class="summary-nom">Mutual Fund (Nominee Details)
                          <span class="add-in-nom-name" id="nominee-name">{{ $nominee[0]['name']}}{{ $nominee[1]['name']?'.'.$nominee[1]['name']:''}}{{ $nominee[2]['name']?'.'.$nominee[2]['name']:''}}</span>
				 </span>
                            <span class="add-det-btn-sec">
                          <a href="{{ Request::root() }}/plan-selection/nominee-details"  class="btn blue-btn">EDIt</a>
                        </span>
                        </span>
                    </div>
                    <div class="add-an-nominee-rw full  plan-nominee-rw demat-ac-rw">
                        <span class="add-det">
				<img src="{{ Request::root() }}/images/icons/demat-icon.png" alt=""/>
<span class="summary-nom">Demat Account Details<span class="add-in-demat-name" id="demat-name">I wish to open Demat Account with {{ $segment['demat_acc_flag']?'VNS Fin':'DP ID:'.$segment['dp_id'] }}</span>
 </span>
                            <span class="add-det-btn-sec">
                                <a href="{{ Request::root() }}/plan-selection/select-product" class="btn blue-btn" >EDIt</a>

 </span>
                        </span>
                    </div>
                </div>
                <button type="submit" value="submit" class="btn blue-btn" onclick="">Proceed to next step</button>
            </div>
        </form>
    </div>
    <div class="clearfix"></div>
</div>
<!-- Content end -->

<!-- Tooltip Content -->

<!-- Tooltip Content -->
@include('ui-inner.tooltip.plan-summary')
<!-- includes scripts -->
@include('includes.index-footer')

<script type="text/javascript" src="{{ Request::root() }}/assets/apps/scripts/plan.js"></script>

</body>

</html>
