<!-- Header -->
@include('includes.index-header')

<!-- Content Start -->
<style>
.active_box{
  border:1px solid #01b792 !important;
}

.inactive_box{
  border:1px solid #f4f5f6 !important;
}
</style>
<div class="tab-content">
    <div class="tab-pane active">

        <h2 class="form-title">Please choose product/s</h2>
        @if($errors->has('error'))
            <div class="alert alert-danger msgError">
                {{ $errors->first('error') }}
            </div>
        @endif
            <form method="post" id="selectPlanForm" name="selectPlanForm"  action-xhr="<?php echo Request::root().'/select-plan';?>">
            <div class="adhaar-sec plan-sec">
                <div class="form-rw full">
                    <div class="form-group"  id="trade_div">

                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="half-row">
                            <div class="half">
                                <div id="holder" class="holder "> <!--{{$trading? 'active_box':'inactive_box'}} -->

                                    <img src="{{ Request::root() }}/images/plan-icon.jpg" class="center-img">
                                    <span class="center-span plan-txt">Trading</span>
                                    <span class="center-span plan-txt2">You can trade Equity, Futures and Options, Currency and Commodities with this product</span>
                                    <div id="mask" class="mask  plan-chk input-container" >
                                        <input type="checkbox" value="1" id="tradechk"  name="trading" {{$trading  ? 'checked' : '' }}/>
                                        <label for="customCheckboxInput"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="half">
                                <div id="holdermf" class="holder inactive_box ">
                                    <img src="{{ Request::root() }}/images/plan-icon.jpg" class="center-img">
                                    <span class="center-span plan-txt">Mutual Fund</span>
                                    <span style="color:#696969;" class="center-span plan-txt2">Coming Soon !!!</span>
                                    <!-- <span class="center-span plan-txt2">You can invest in Mutual funds with Rs. 0 brokerage </span> -->
                                    <div id="mask" class="mask  plan-chk input-container" >
                                        <input type="checkbox" value="1" id="mfchk"  name="mutual_fund" disabled  />
                                        <label for="customCheckboxInput"></label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- <div class="full">
                            <span class="info-span gry-txt"><img src="{{ Request::root() }}/images/icons/info-icon.png">&nbsp;Please mention (For eg: 2 years and 7 months)</span>
                        </div> -->

                    </div>

                </div>
                <button type="submit" value="submit" class="btn blue-btn" onclick="">Proceed to next step</button>
            </div>
        </form>
    </div>
    <div class="clearfix"></div>
</div>
<!-- Content end -->

<!-- Tooltip Content -->

<!-- Tooltip Content -->
@include('ui-inner.tooltip.select-plan')
<!-- includes scripts -->
@include('includes.index-footer')

<script type="text/javascript" src="{{ Request::root() }}/assets/apps/scripts/plan-validation.js"></script>

<script type="text/javascript" src="{{ Request::root() }}/assets/apps/scripts/plan.js"></script>

    </body>

</html>
