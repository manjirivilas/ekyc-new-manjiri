<!-- Header -->
@include('includes.index-header')

<!-- Content Start -->

<div class="tab-content">
    <div class="tab-pane active">
        <h2 class="form-title">Please choose product/s</h2>
        <form id="frm-adhaar" method="get">
            <div class="adhaar-sec">
                <div class="form-rw full">
                    <div class="form-group">
                        <div class="half-rw">
                            <div class="half">
                                <div id="holder">
                                    <img src="images/plan-icon.jpg" class="center-img">
                                    <span class="center-span plan-txt">Trading</span>
                                    <span class="center-span plan-txt2">You can trade Equity, Derivatives, Futures, Options and Commodities with this product</span>
                                    <div id="mask"></div>
                                    <div class="center-span plan-chk input-container">
                                        <input type="checkbox" value="1" name="" />
                                        <label for="customCheckboxInput"></label>
                                  </div>
                                </div>
                            </div>
                            <div class="half">

                            </div>
                        </div>
                        <!-- <div class="full">
                            <span class="info-span gry-txt"><img src="images/icons/info-icon.png">&nbsp;Please mention (For eg: 2 years and 7 months)</span>
                        </div> -->

                    </div>
                </div>
                <button type="submit" value="submit" class="btn blue-btn" onclick="">Proceed</button>
            </div>
        </form>
        <div class="id-sec">Don’t have Aadhar Card? Continue with <a href="" onclick="return nextstep('identity/pan-details.blade.php','step1');">paper based ekyc</a></div>
    </div>
    <div class="clearfix"></div>
</div>
<!-- Content end -->

<!-- Tooltip Content -->

<!-- Tooltip Content -->
@include('ui-inner.tooltip.aadhar-details')
<!-- includes scripts -->
@include('includes.index-footer')
