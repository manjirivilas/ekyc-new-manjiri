<!-- Header -->
@include('includes.index-header')

<!-- Content Start -->

<div class="tab-content">
    <div class="tab-pane active">
        <h2 class="form-title">Nominee for Mutual Fund A/C</h2>
        <div class="form-para-sec">
            <p class="form-para">We are charging 0 brokerage for investing in Mutual Funds. So, just sit and enjoy your money grow!</p>
        </div>
        <!-- <form method="post" id="nomineeDetails"> -->
        <form method="post" id="addNomineeForm" name="addNomineeForm"  action-xhr="<?php echo Request::root().'/nominee-details';?>">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <input type="hidden" name="nominee_flag" id="nominee_flag" value=0 >
            <!-- <input type="hidden" name="nominee_flag" id="nominee_flag_1" value="{{isset($nominee)?$nominee[0]['total_nominee']:0}}"> -->
            <div class="adhaar-sec">
                <div class="bank-account-sec full addhar-br">
                    <h6 class="bank-title"><span class="bank-icon"><img src="{{ Request::root() }}/images/icons/nominee-user-icon.png" alt="bank-account-img"/></span> First nominee detail</h6>
                    <div class="bank-rw">
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>Nominee Name*</label>
                                <input type="text" name="f_name" id="nominee_name" value="{{ isset($nominee)?$nominee[0]['name']:old('f_name') }}" class="form-control">
                            </div>
                        </div>
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>Relation*</label>
                                <input type="text" name="f_relation" id="nominee_relation" value="{{ isset($nominee)?$nominee[0]['relation']:old('f_relation') }}" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="bank-rw">
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>Date of Birth*</label>
                                <div class='input-group date' id='nominee_datetime'>
                                    <input type='text'name="f_dob" value="{{ isset($nominee)?$nominee[0]['dob']:old('f_dob') }}" class="form-control removeborder" />
                                    <span class="input-group-addon removeback">
                                        <!-- <span class="fa fa-calendar">
                                        </span> -->
                                        <img src="{{ Request::root() }}/images/add-icon/cal-icon.png" alt="calendar">
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="frm-col"></div>
                    </div>

                    <div class="form-rw full">
                        <label>Nominee Adreess*</label>
                        <input type="text" name="f_address" id="nominee_address" value="{{ isset($nominee)?$nominee[0]['address']:old('f_address') }}" class="form-control">
                    </div>
                </div>

                <div class="bank-account-sec full add-account addhar-bt add-account1  ">
                    <h6 class="bank-title"><span class="bank-icon"><img src="{{ Request::root() }}/images/icons/nominee-user-icon.png" alt="bank-account-img"/></span> second nominee detail<span class="add-remove add-remove-nom1"><img src="{{ Request::root() }}/images/error-arw.png" alt=""/></span></h6>
                    <div class="bank-rw">
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>Nominee Name*</label>
                                <input type="text" name="s_name" id="s_name" value="{{ isset($nominee)?$nominee[1]['name']:old('s_name') }}" class="form-control">

                            </div>
                        </div>
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>Relation*</label>
                                <input type="text" name="s_relation" id="s_relation" value="{{ isset($nominee)?$nominee[1]['address']:old('s_relation') }}"  class="form-control">

                            </div>
                        </div>
                    </div>
                    <div class="bank-rw">
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>Date of Birth*</label>
                                <div class='input-group date'  id='nominee_datetime_1'>
                                    <input type='text' name="s_dob" id="s_dob" value="{{ isset($nominee)?$nominee[1]['dob']:old('s_dob') }}" class="form-control removeborder" />
                                    <span class="input-group-addon removeback">
                                        <!-- <span class="fa fa-calendar">
                                        </span> -->
                                        <img src="{{ Request::root() }}/images/add-icon/cal-icon.png" alt="calendar">
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="frm-col"></div>
                    </div>

                    <div class="form-rw full">
                        <label>Nominee Adreess*</label>
                        <textarea type="text" name="s_address" id="s_address" value="{{ isset($nominee)?$nominee[1]['address']:old('s_address') }}"  class="form-control"></textarea>
                    </div>
                </div>

                <div class="bank-account-sec full add-account addhar-bt add-account2  ">
                    <h6 class="bank-title"><span class="bank-icon"><img src="{{ Request::root() }}/images/icons/nominee-user-icon.png" alt="bank-account-img"/></span> third nominee detail<span class="add-remove add-remove-nom2"><img src="{{ Request::root() }}/images/error-arw.png" alt=""/></span></h6>
                    <div class="bank-rw">
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>Nominee Name*</label>
                                <input type="text" name="t_name" value="{{ isset($nominee)?$nominee[2]['name']:old('t_name') }}"  id="t_name" class="form-control">

                            </div>
                        </div>
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>Relation*</label>
                                <input type="text" name="t_relation" id="t_relation" value="{{ isset($nominee)?$nominee[2]['relation']:old('t_relation') }}"  class="form-control">

                            </div>
                        </div>
                    </div>
                    <div class="bank-rw">
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>Date of Birth*</label>
                                <div class='input-group date' id='nominee_datetime_2'>
                                    <input type='text' name="t_dob" id="t_dob" class="form-control removeborder" value="{{ isset($nominee)?$nominee[2]['dob']:old('t_dob') }}" />
                                    <span class="input-group-addon removeback">
                                        <!-- <span class="fa fa-calendar">
                                        </span> -->
                                        <img src="{{ Request::root() }}/images/add-icon/cal-icon.png" alt="calendar">
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="frm-col"></div>
                    </div>

                    <div class="form-rw full">
                        <label>Nominee Adreess*</label>
                        <textarea type="text" name="t_address" id="t_address" value="{{ isset($nominee)?$nominee[2]['address']:old('t_address') }}" class="form-control"></textarea>
                    </div>
                </div>
                <div class="add-an-nominee-rw full">
                    <span class="add-det"><img src="{{ Request::root() }}/images/icons/nominee-icon.png" alt=""/>
                      <span class="add-nom">Add Nominee<span class="add-in-nom">You can add upto <span id="nominee-count"></span> more nominee</span>
                    </span>
                    </span>
                </div>
                <button type="submit" value="submit" id="submit" class="btn blue-btn">Proceed to next step</button>
            </div>
        </form>
    </div>
    <div class="clearfix"></div>
</div>
<!-- Content end -->

<!-- Tooltip Content -->
@include('ui-inner.tooltip.nominee-details')

<!-- includes scripts -->
@include('includes.index-footer')

<script type="text/javascript" src="{{ Request::root() }}/assets/apps/scripts/plan.js"></script>

</body>

</html>
