<!-- Aadhar Card Modal -->
                    <div class="modal fade ekyc-modal" id="AadharModal" role="dialog">
                        <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"><span class="cross-sign">&times;</span> <span>Close</span></button>
                                </div>
                                <div class="modal-body">
                                    <img src="{{ Request::root() }}/images/cheque-img-popoup.png" alt="cheque-img"  class="img-responsive"/>
                                </div>
                            </div>

                        </div>
                    </div>


<!-- Pan Card Modal -->
                    <div class="modal fade ekyc-modal" id="PanModal" role="dialog">
                        <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"><span class="cross-sign">&times;</span> <span>Close</span></button>
                                </div>
                                <div class="modal-body">
                                    <img src="{{ Request::root() }}/images/cheque-img-popoup.png" alt="cheque-img"  class="img-responsive"/>
                                </div>
                            </div>

                        </div>
                    </div>



<!-- Cancelled Cheque Modal -->
                    <div class="modal fade ekyc-modal" id="ChequeModal" role="dialog">
                        <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"><span class="cross-sign">&times;</span> <span>Close</span></button>
                                </div>
                                <div class="modal-body">
                                    <img src="{{ Request::root() }}/images/cheque-img-popoup.png" alt="cheque-img"  class="img-responsive"/>
                                </div>
                            </div>

                        </div>
                    </div>

<!-- Bank Statement Modal -->
                    <div class="modal fade ekyc-modal" id="StatementModal" role="dialog">
                        <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"><span class="cross-sign">&times;</span> <span>Close</span></button>
                                </div>
                                <div class="modal-body">
                                    <img src="{{ Request::root() }}/images/cheque-img-popoup.png" alt="cheque-img"  class="img-responsive"/>
                                </div>
                            </div>

                        </div>
                    </div>

<!-- Physical Signature Modal -->
                    <div class="modal fade ekyc-modal" id="SignatureModal" role="dialog">
                        <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"><span class="cross-sign">&times;</span> <span>Close</span></button>
                                </div>
                                <div class="modal-body">
                                    <img src="{{ Request::root() }}/images/cheque-img-popoup.png" alt="cheque-img"  class="img-responsive"/>
                                </div>
                            </div>

                        </div>
                    </div>