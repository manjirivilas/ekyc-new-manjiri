<!-- Header -->
@include('includes.payment-header')

<!-- Content Start -->

	<div class="emil-send-sec full error">
        <img src="{{ Request::root() }}/images/404.png" class="center-img">
        <h3 class="form-title text-size">Looks like the page is missing! Don't worry.</h3>
        <p>
        	Meanwhile, why don't try again by going
        </p>
        <div>
        	 <button type="submit" value="submit" class="btn blue-btn" onclick="goBack()">Back</button>
        </div>
    </div>
    <div class="clearfix"></div>

<!-- Content end -->

<!-- includes scripts -->
<script>
function goBack() {
    window.history.back();
}
</script>
