</div>

</div>
</div>


<div class="document-col">
    <div class="document-col-inner full">
                  <div class="mobi-document-col-inner">
        <div class="req-doc-sec">
                    <div class="hi-box tooltip-bx">
                        <div class="hi-box-inner tooltip-sec  tooltip-bx-inner" id="tool-tip">
                    <h4><span>Hey!</span> {{ Auth::user()->name }}</h4>
                        <p class="remove-padding">You could see the details of our
                            <br>3 trading plans:</p>
                        <ul class="req-list">
                            <li><a href="#value" class="modal-toggle" data-toggle="modal" data-target="#myModal"><span class="text-frm">Value Plan</span></a></li>
                            <li><a href="#power" class="modal-toggle" data-toggle="modal" data-target="#myModal"><span class="text-frm">Power Plan</span></a></li>
                            </ul>

                </div>
            </div>
            <div class="req-img-sec">
                <img src="{{ Request::root() }}/images/women.png" alt="req-img" />
            </div>
                <div class="">
                <div class="mobi-hi-sec-inner full">
                  <div class="profile-img-sec">
                        <div class="profile-img">
                            <img src="{{ Request::root() }}/images/profile-img.png" alt="profile-img"/>
        </div>
                        <h4><span>Hey!</span> {{ Auth::user()->name }}</h4>
                        <p>I am here to help!</p>
                   </div>
                   <div class="up-arrow-sec">
                      <img src="{{ Request::root() }}/images/popup-close-arw.png" alt="close-img"/>
                   </div>
                </div>
                  </div>
                </div>
                </div>
                </div>
             </div>


             <div class="mobi-hi-sec">
                <div class="mobi-hi-sec-inner bt-mobi-inner full">
                  <div class="profile-img-sec">
                        <div class="profile-img">
                            <img src="{{ Request::root() }}/images/profile-img.png" alt="profile-img"/>
                        </div>
                        <h4><span>Hey!</span> {{ Auth::user()->name }}</h4>
                        <p>I am here to help!</p>
                   </div>
                   <div class="up-arrow-sec">
                      <img src="{{ Request::root() }}/images/up-arrow.png" alt="up-arrow"/>
                   </div>
                </div>
             </div>

      <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog custom-modal">

          
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times; <span>CLOSE</span></button>
            </div>
            <div class="container content">
              <div class="row">
                <!-- Pricing -->
                    <div class="col-md-2"></div>
                    <div class="col-md-4">
                        <div class="pricing hover-effect" id="value">
                    <div class="pricing-head plan-txt-color-1">
                      <h3>VALUE </h3>
                      <h4><span class="numbers circle-border-1">0.007<span class="icon-txt">%</span></span>
                      <span>
                      brokerage </span>
                      </h4>
                    </div>
                    <div class="btm-border"></div>
                    <ul class="pricing-content">
                      <li class="back-img-1">
                                    Brokerage: Intraday Cash, Futures, Currency, Commodities » 0.007%, Delivery » 0.07%, Options » Rs 7 per lot
                      </li>
                      <li class="back-img-1">
                        Ideal for traders with lesser volume
                      </li>
                      <li class="back-img-1">
                        Upto 30x exposure on Equities, 7x on F&O, 3x on Currency and 6x on Commodities
                      </li>
                      <li class="back-img-1">
                       Margin against shares
                      </li>
                      <li class="back-img-1">
                        No Minimum commitment
                      </li>
                    </ul>
                    <div class="pricing-footer">
                      
                    </div>
                  </div>
                </div>
                <div class="col-md-4">
                        <div class="pricing hover-effect" id="power">
                    <div class="pricing-head plan-txt-color-2">
                      <h3>POWER PLAN</h3>
                      <h4><span class="numbers circle-border-2"><i class="fa fa-inr isize" aria-hidden="true"></i>15</span>
                      <span class="plan-txt-color-2">
                      PER EXECUTED ORDER </span>
                      </h4>
                    </div>
                    <div class="btm-border"></div>
                    <ul class="pricing-content">
                      <li class="back-img-2">
                                    Rs. 15 per executed order, regardless of the size of the trade
                </li>
                <li class="back-img-2">
                  Ideal for traders with less frequency but bigger trade size
                </li>
                <li class="back-img-2">
                  Upto 30x exposure on Equities, 7x on F&O, 3x on Currency and 6x on Commodities
                </li>
                <li class="back-img-2">
                 Margin against shares
                </li>
                <li class="back-img-2">
                  No Minimum commitment
                </li>
              </ul>
              <div class="pricing-footer">
                
              </div>
            </div>
          </div>
                    <div class="col-md-2"></div>
                    <!-- <div class="col-md-4">
                        <div class="pricing hover-effect" id="infinity">
              <div class="pricing-head plan-txt-color-3">
                <h3>INFINITY</h3>
                                <h4>
                                  <span class="numbers circle-border-3">
                                    <i class="fa fa-inr isize2" aria-hidden="true">
                                    </i>1999
                                  </span>
                <span class="plan-txt-color-3">
                                    UNLIMITED TRADING / month Onwards
                                  </span>
                </h4>
              </div>
              <div class="btm-border"></div>
              <ul class="pricing-content">
                <li class="back-img-3">
                  Pay the flat monthly fee of Rs. 3999/- for Equity and Commodity each and 1999 for Currency only.
                </li>
                <li class="back-img-3">
                  Ideal for active traders having higher frequency and volume
                </li>
                <li class="back-img-3">
                 Upto 30x exposure on Equities, 7x on F&O, 3x on Currency and 6x on Commodities
                </li>
                <li class="back-img-3">
                 No limit on number of trades or quantities
                </li>
                <li class="back-img-3">
                  No need to pay any brokerage on turnover
                </li>
                <li class="back-img-3">
                  No use no charge policy
                </li>
              </ul>
              <div class="pricing-footer">
                
              </div>
            </div>
                    </div> -->
          <!--//End Pricing -->
        </div>
      </div>
      <!-- <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <p>Some text in the modal.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div> -->
    </div>


    </div>
</div>
