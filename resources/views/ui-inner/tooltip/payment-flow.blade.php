</div>

</div>
</div>


<div class="document-col">
    <div class="document-col-inner full">
        <div class="req-doc-sec">
            <div class="hi-box full">
                <div class="hi-box-inner tooltip-sec" id="tool-tip">
                    <h4><span>Hey!</span> {{ Auth::user()->name }}</h4>
                    <span class="tooltip-sub-title">Summary</span>
					<p>Your form is now ready to be digitally signed. Please review your details before you proceed ahead</p>
                </div>
            </div>
            <div class="req-img-sec">
                <img src="{{ Request::root() }}/images/women.png" alt="req-img" />
            </div>
        </div>
    </div>
</div>
