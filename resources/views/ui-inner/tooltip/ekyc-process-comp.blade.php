</div>

</div>
</div>


<div class="document-col">
    <div class="document-col-inner full">
                  <div class="mobi-document-col-inner">
        <div class="refl-code-sec-outer">
            <div class="refl-code-sec full">
                <div class="refl-code-box">
                    <p class="refl-title">Your Referal Code</p>
                      <div class="refl-code-box-inner">
                        <span class="refl-code-txt" id="refl-code">YDLA140</span>
                      </div>
                    <p>You can now refer us to your friends or relatives and share your unique referral code</p>
                </div>
                <ul class="social-list">
                  <li class="fb-lst"><a href=""><img src="{{ Request::root() }}/images/ekyc-completion/facebook-comp-icon.png" alt="fb-icon"/> Share on Facebook</a></li>
                  <li class="g-lst"><a href=""><img src="{{ Request::root() }}/images/ekyc-completion/google-comp-icon.png" alt="gb-icon"/> Share on Google +</a></li>
                </ul>
            </div>
        </div>


        <div class="req-doc-sec req-before-sec">
                    <div class="hi-box tooltip-bx">
                        <div class="hi-box-inner tooltip-sec  tooltip-bx-inner" id="tool-tip">
                    <h4><span>Hey!</span> {{ Auth::user()->name }}</h4>
					<p>For successfully opening your account with us, please sign and courier your EKYC form (send on your registered e-mail id/ LINK) along with self-attested copies of required documents.</p>
                    <p class="paperless-tooltip-para">To allow you trade in delivery, we would require you to download and sign the Power of Attorney (form link) and courier it to us.</p>
                </div>
            </div>
            <div class="req-img-sec">
                <img src="{{ Request::root() }}/images/women.png" alt="req-img" />
            </div>
                <div class="">
                <div class="mobi-hi-sec-inner full">
                  <div class="profile-img-sec">
                        <div class="profile-img">
                            <img src="<?php echo Request::root();?>/images/profile-img.png" alt="profile-img"/>
        </div>
                        <h4><span>Hey!</span> {{ Auth::user()->name }}</h4>
                        <p>I am here to help!</p>
    </div>
                   <div class="up-arrow-sec">
                      <img src="{{ Request::root() }}/images/popup-close-arw.png" alt="close-img"/>
</div>
                </div>
                  </div>
                </div>
                </div>
                </div>
             </div>


             <div class="mobi-hi-sec">
                <div class="mobi-hi-sec-inner bt-mobi-inner full">
                  <div class="profile-img-sec">
                        <div class="profile-img">
                            <img src="{{ Request::root() }}/images/profile-img.png" alt="profile-img"/>
                        </div>
                        <h4><span>Hey!</span> {{ Auth::user()->name }}</h4>
                        <p>I am here to help!</p>
                   </div>
                   <div class="up-arrow-sec">
                      <img src="{{ Request::root() }}/images/up-arrow.png" alt="up-arrow"/>
                   </div>
                </div>
             </div>

