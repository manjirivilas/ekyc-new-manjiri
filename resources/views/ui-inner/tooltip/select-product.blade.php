</div>

</div>
</div>


<div class="document-col">
    <div class="document-col-inner full">
                  <div class="mobi-document-col-inner">
        <div class="req-doc-sec">
                    <div class="hi-box tooltip-bx">
                        <div class="hi-box-inner tooltip-sec  tooltip-bx-inner" id="tool-tip">
                    <h4><span>Hey!</span> {{ Auth::user()->name }}</h4>
                    <p id="dematac">Choose a plan to know more about it.</p>
                    <div id="existac">
                        <p>By default we'd prefer the client
                            to open a demat account with us.
                        </p>
                        <p>
                           There are some limitations in case
                            if the client links an outside demat
                            account with his trading account.
                        </p>
                        <span>The limitations as  below:</span>
                            <ul>
                                <li>Client won't be able to sell the
                            shares in delivery or BTST
                            trades.</li>
                            </ul>

                    </div>
                </div>
            </div>
            <div class="req-img-sec">
                <img src="{{ Request::root() }}/images/women.png" alt="req-img" />
            </div>
                <div class="">
                <div class="mobi-hi-sec-inner full">
                  <div class="profile-img-sec">
                        <div class="profile-img">
                            <img src="{{ Request::root() }}/images/profile-img.png" alt="profile-img"/>
        </div>
                        <h4><span>Hey!</span> {{ Auth::user()->name }}</h4>
                        <p>I am here to help!</p>
    </div>
                   <div class="up-arrow-sec">
                      <img src="{{ Request::root() }}/images/popup-close-arw.png" alt="close-img"/>
</div>
                </div>
                  </div>
                </div>
                </div>
                </div>
             </div>


             <div class="mobi-hi-sec">
                <div class="mobi-hi-sec-inner bt-mobi-inner full">
                  <div class="profile-img-sec">
                        <div class="profile-img">
                            <img src="<?php echo Request::root();?>/images/profile-img.png" alt="profile-img"/>
                        </div>
                        <h4><span>Hey!</span> {{ Auth::user()->name }}</h4>
                        <p>I am here to help!</p>
                   </div>
                   <div class="up-arrow-sec">
                      <img src="<?php echo Request::root();?>/images/up-arrow.png" alt="up-arrow"/>
                   </div>
                </div>
             </div>

