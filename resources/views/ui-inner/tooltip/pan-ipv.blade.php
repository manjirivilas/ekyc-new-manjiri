</div>

                    </div>
                </div>


                <div class="document-col">
                    <div class="document-col-inner full">
                  <div class="mobi-document-col-inner">
                        <div class="req-doc-sec">
                    <div class="hi-box tooltip-bx">
                        <div class="hi-box-inner tooltip-sec  tooltip-bx-inner" id="tool-tip">
                                    <h4><span>Hey!</span> {{ Auth::user()->name }}</h4>
                            <span class="gry-txt" id="add-txt">Your front camera will start on clicking RETAKE.</span>
                                    <div>
                                        <p>
                                            IN PERSON VERIFICATION
                                            IPV is a regulatory requirement, as
                                            per SEBI(Security and Exchange
                                            board of India) with effect from
                                            January 12, 2012. It needs to be
                                            completed before we can activate
                                            your trading account.
                                        </p>
                                        <p class="gry-txt">
                                            Be ready with your PAN card.
                                        </p>
                                        <p><a class="watch-btn" data-toggle="modal" data-target="#pandemoModal">Please Click Here to Check Demo Pan Card Image</a></p>
                                    </div>
								</div>
                            </div>
                            <div class="req-img-sec">
                                <img src="{{ Request::root() }}/images/women.png" alt="req-img" />
                            </div>
                <div class="">
                <div class="mobi-hi-sec-inner full">
                  <div class="profile-img-sec">
                        <div class="profile-img">
                            <img src="{{ Request::root() }}/images/profile-img.png" alt="profile-img"/>
                        </div>
                        <h4><span>Hey!</span> {{ Auth::user()->name }}</h4>
                        <p>I am here to help!</p>
                   </div>
                   <div class="up-arrow-sec">
                      <img src="{{ Request::root() }}/images/popup-close-arw.png" alt="close-img"/>
                   </div>
                </div>
                  </div>
                </div>
                </div>
                </div>
             </div>


             <div class="mobi-hi-sec">
                <div class="mobi-hi-sec-inner bt-mobi-inner full">
                  <div class="profile-img-sec">
                        <div class="profile-img">
                            <img src="{{ Request::root() }}/images/profile-img.png" alt="profile-img"/>
                        </div>
                        <h4><span>Hey!</span> Aishwarya</h4>
                        <p>I am here to help!</p>
                   </div>
                   <div class="up-arrow-sec">
                      <img src="{{ Request::root() }}/images/up-arrow.png" alt="up-arrow"/>
                        </div>
                    </div>
                </div>



<!-- Cheque Modal -->
                    <div class="modal fade ekyc-modal" id="pandemoModal" role="dialog">
                        <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"><span class="cross-sign">&times;</span> <span>Close</span></button>
                                </div>
                                <div class="modal-body">
                                    <img src="{{ Request::root() }}/images/cheque-img-popoup.png" alt="cheque-img"  class="img-responsive"/>
                                </div>
                            </div>

                    </div>
                </div>
