

        @include('includes.header')
        <div class="main-inner-sec full sign-inner-sec">
          <div class="main-tb-sec forget-sec">
            <div class="form-col">
              @if($errors->has('error'))
                <span class="alert alert-danger msgError" style="color:red">{{ $errors->first('error') }}</span>
              @endif
                <div class="form-col-inner full">

                    <h2 class="form-title">Forgot Password?</h2>
                    <div class="form-para-sec">
                      <p class="form-para">Enter your e-mail below to receive your password reset instructions.</p>
                     </div>


                     <form method="post" id="forgotPassword" name="forgotPassword"   action-xhr="<?php echo Request::root().'/email-sent';?>">
                    <fieldset class="login-section">
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      <div class="form-rw">
                        <div class="form-group">
                          <input type = 'hidden' name="url_path" value = "<?php echo Request::root();?>" id = "url_path"/>
                          <label>Enter Email*</label>

                          <input type="email" id="email" name="email" value="{{ Cookie::get('email') }}"  required class="form-control">
                          @if($errors->has('email'))
                            <span class="msgError" style="color:red">{{ $errors->first('email') }}</span>
                          @endif

                        </div>
                      </div>
                      <button type="submit" value="submit" class="btn blue-btn">Submit</button>
                    </fieldset>
                  </form>
                  {{-- Form::close() --}}
                  <div class="go-back-sec full">
                    <p>Go Back to <a href="<?php echo Request::root().'/login';?>">Sign In</a></p>
                  </div>
                </div>
             </div>
            <div class="document-col">
                <div class="document-col-inner full">
                  <div class="mobi-document-col-inner">
                  <div class="go-paperless-sec full">
                    <div class="go-paperless-sec-inner full">
                      <div class="addhar-img-sec">
                        <img src="<?php echo Request::root();?>/images/aadhar.png" alt="aadhar-img"/>
                      </div>
                      <h4>Go ‘paperless’ with Aadhar.</h4>
                      <p>Now open an account with  us  instantly.</p>
                    </div>
                    <div class="mobi-join">
                      <img src="<?php echo Request::root();?>/images/join-img.png" alt="join-img"/>
                     </div>
                  </div>
                  <div class="req-doc-sec">
                    <div class="hi-box tooltip-bx">
                      <div class="hi-box-inner tooltip-bx-inner">
                        <h4><span>Hey!</span> Aishwarya</h4>
                        <p>Don’t warry. After submiting your email id please check you email account.</p>
                        <p>We’ll sent you a link to reset your password.</p>
                      </div>
                    </div>
                    <div class="req-img-sec">
                      <img src="<?php echo Request::root();?>/images/men.png" alt="req-img"/>
                    </div>
                <div class="">
                <div class="mobi-hi-sec-inner full">
                  <div class="profile-img-sec">
                        <div class="profile-img">
                            <img src="<?php echo Request::root();?>/images/profile-img.png" alt="profile-img"/>
                        </div>
                        <h4><span>Hey!</span> Aishwarya</h4>
                        <p>We’ll sent you a link to reset your password.</p>
                   </div>
                   <div class="up-arrow-sec">
                      <img src="<?php echo Request::root();?>/images/popup-close-arw.png" alt="close-img"/>
                   </div>
                </div>
                  </div>
                </div>
                </div>
                </div>
             </div>
            </div>
             <div class="mobi-hi-sec">
                <div class="mobi-hi-sec-inner bt-mobi-inner full">
                  <div class="profile-img-sec">
                        <div class="profile-img">
                            <img src="<?php echo Request::root();?>/images/profile-img.png" alt="profile-img"/>
                        </div>
                        <h4><span>Hey!</span> Aishwarya</h4>
                        <p>We’ll sent you a link to reset your password.</p>
                   </div>
                   <div class="up-arrow-sec">
                      <img src="<?php echo Request::root();?>/images/up-arrow.png" alt="up-arrow"/>
                   </div>
                </div>
             </div>
         </div>
       </div>
    </div>

@include('includes.footer')
