<!-- Header -->
@include('includes.change-password-header')
<!-- Content Start -->
<div class="tab-content">
    <div class="tab-pane active">
        <h2 class="form-title">Change Password</h2>
        @if($errors->has('error'))
        <div class="alert alert-danger msgError">
            {{ $errors->first('error') }}
        </div>

        @endif
        @if(session()->has('message'))
            <div class="alert alert-success">
              fg
                {{ session()->get('message') }}
            </div>
        @endif
        <div class="form-para-sec">
          <p class="form-para">Enter a password for your account privacy.</p>
        </div>
        <!-- <form id="frm-adhaar" method="get"> -->
        <form method="post" id="changePasswordForm"  action-xhr="<?php echo Request::root().'/change-password';?>">

            <div class="adhaar-sec">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-rw">
                  <div class="form-group">
                    <label>Enter Old Password*</label>
                    <input type="password" name="old_password" maxlength="65" class="form-control">
                    @if($errors->has('old_password'))
                    <span class="msgError" style="color:red">{{ $errors->first('old_password') }}</span>
                    @endif
                  </div>
                </div>
                <div class="form-rw">
                  <div class="form-group">
                    <label>Enter New Password*</label>
                    <input type="password" name="password" maxlength="65" class="form-control">
                    @if($errors->has('password'))
                    <span class="msgError" style="color:red">{{ $errors->first('password') }}</span>
                    @endif
                  </div>
                </div>
                <div class="form-rw">
                  <div class="form-group">
                    <label>Re-enter New Password*</label>
                    <input type="password" name="password_confirmation" maxlength="65" class="form-control">
                    @if($errors->has('password_confirmation'))
                    <span class="msgError" style="color:red">{{ $errors->first('password_confirmation') }}</span>
                    @endif
                  </div>
                </div>
                <button type="submit" value="submit" class="btn blue-btn" onclick="">Proceed</button>
            </div>
        </form>
    </div>
    <div class="clearfix"></div>
</div>
<!-- Content end -->

<!-- Tooltip Content -->

<!-- Tooltip Content -->
@include('ui-inner.tooltip.login-change-password')


<!-- includes scripts -->
@include('includes.index-footer')

<script src="{{ Request::root() }}/assets/apps/scripts/login.js"></script>
    </body>

</html>
