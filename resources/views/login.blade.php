
@include('includes.header')

        <div class="main-inner-sec full sign-inner-sec">
          <div class="main-tb-sec forget-sec">
            <div class="form-col">
              @if($errors->has('error'))
              <div class="alert alert-danger msgError">
                  {{ $errors->first('error') }}
              </div>
              @endif
              <!-- @if($errors->has('server-error'))
              <div class="alert alert-danger msgError">
                  {{ $errors->first('server-error') }}
              </div>
              @endif -->

            @if(session()->has('message'))
                <div class="alert alert-success msgError">
                    {{ session()->get('message') }}
                </div>
            @endif
                <div class="form-col-inner full">
                    <h2 class="form-title">Filling forms has never been simpler!</h2>
                    <div class="form-para-sec">
                      <p class="form-para">A 5 step process with relevant help sections to guide you through your eKYC journey. Having your documents ready will help speed up the process.</p>
                     </div>
                    <form method="post" id="loginForm" name="loginForm"  action-xhr="<?php echo Request::root().'/login';?>">

                    <fieldset class="login-section">
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      <div class="form-rw">
                        <div class="form-group">
                          <label>Email ID*</label>
                          <input type="email" name="email"  value="{{ old('email')}}" class="form-control">
                          @if($errors->has('email'))
                          <span class="msgError" style="color:red">{{ $errors->first('email') }}</span>
                          @endif

                        </div>
                      </div>
                      <div class="form-rw">
                        <div class="form-group">
                          <label>Password*</label>
                          <input type="password" name="password"  class="form-control"  maxlength="65">

                          @if($errors->has('password'))
                            <span class="msgError" style="color:red">{{ $errors->first('password') }}  </span>
                          @endif


                        </div>
                      </div>
                      <div class="forget-rw">
                        <div class="form-rw">
                          <div class="rem-sec"><span class="squaredThree"><input type="checkbox" name="rember-me" id="squaredThree">  <label for="squaredThree"></label></span>Remember me</div>
                          <div class="for-sec"><a class="frg-pass" href="<?php echo Request::root().'/forgot-password';?>">Forgot Password?</a></div>
                        </div>
                      </div>
                      <button type="submit" value="submit" id="login" class="btn blue-btn">Sign In</button>
                    </fieldset>

                  </form>
                </div>
             </div>
            <div class="document-col">
                <div class="document-col-inner full">
                  <div class="mobi-document-col-inner">
                  <div class="go-paperless-sec full">
                    <div class="go-paperless-sec-inner full">
                      <div class="addhar-img-sec">
                        <img src="<?php echo Request::root();?>/images/aadhar.png" alt="aadhar-img"/>
                      </div>
                      <h4>Go ‘paperless’ with Aadhar.</h4>
                      <p>Now open an account with  us  instantly.</p>
                    </div>
                    <div class="mobi-join">
                      <img src="<?php echo Request::root();?>/images/join-img.png" alt="join-img"/>
                     </div>
                  </div>
                  <div class="req-doc-sec">
                    <div class="hi-box on-req-bx-main">
                      <div class="hi-box-inner on-req-bx">
                        <h4>required document</h4>
                        <ul class="req-list">
                          <li><span><a data-toggle="modal" data-target="#AadharModal">Aadhar Card</a></span></li>
                          <li><span><a data-toggle="modal" data-target="#PanModal">Pan Card</a></span></li>
                          <li><span><a data-toggle="modal" data-target="#ChequeModal">Cancelled Cheque</a></span></li>
                          <li><span><a data-toggle="modal" data-target="#StatementModal">6 Months Bank Statement</a></span></li>
                          <li><span><a data-toggle="modal" data-target="#SignatureModal">Physical Signature</a></span></li>
                        </ul>
                      </div>
                    </div>
                    <div class="req-img-sec">
                      <img src="<?php echo Request::root();?>/images/men.png" alt="req-img"/>
                    </div>
                <div class="">
                <div class="mobi-hi-sec-inner full">
                  <div class="profile-img-sec">
                        <div class="profile-img">
                            <img src="<?php echo Request::root();?>/images/profile-img.png" alt="profile-img"/>
                        </div>
                        <h4><span>Hey!</span> Aishwarya</h4>
                        <p>you need these documents.</p>
                   </div>
                   <div class="up-arrow-sec">
                      <img src="<?php echo Request::root();?>/images/popup-close-arw.png" alt="close-img"/>
                   </div>
                </div>
                  </div>
                </div>
                </div>
                </div>
             </div>
            </div>

             <div class="mobi-hi-sec">
                <div class="mobi-hi-sec-inner bt-mobi-inner full">
                  <div class="profile-img-sec">
                        <div class="profile-img">
                            <img src="<?php echo Request::root();?>/images/profile-img.png" alt="profile-img"/>
                        </div>
                        <h4><span>Hey!</span> Aishwarya</h4>
                        <p>you need these documents.</p>
                   </div>
                   <div class="up-arrow-sec">
                      <img src="<?php echo Request::root();?>/images/up-arrow.png" alt="up-arrow"/>
                   </div>
                </div>
             </div>
          @include('req-modal')
         </div>
       </div>
    </div>
@include('includes.footer')
