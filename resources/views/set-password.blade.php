
@include('includes.header')


<div class="main-inner-sec full  sign-inner-sec">
          <div class="main-tb-sec">
            <div class="form-col">
              @if($errors->has('error'))
              <div class="alert alert-danger msgError">
                  {{ $errors->first('error') }}
              </div>
              @endif
                <div class="form-col-inner full">
                    <h2 class="form-title">We keep things super secured!</h2>
                    <div class="form-para-sec">
                      <p class="form-para">Enter a password for your account privacy.</p>
                     </div>
                    <form method="post" id="set_password" class="password-validate"  action-xhr="<?php echo Request::root().'/set_password';?>">

                    <fieldset class="login-section">
                      <input type="text" id="name" name="name" value="{{ Cookie::get('name') }}" hidden/>
                      <input type="text" id="email" name="email" value="{{ Cookie::get('email') }}" hidden />
                      <input type="text" id="mobile" name="mobile" value="{{ Cookie::get('mobile') }}"hidden />
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      <div class="form-rw">
                        <div class="form-group">
                          <label>Enter Password*</label>
                          <input type="password" name="password"  class="form-control" maxlength="65">
                          @if($errors->has('password'))
                            <span class="msgError" style="color:red">{{ $errors->first('password') }}  </span>
                          @endif
                            </div>
                      </div>
                      <div class="form-rw">
                        <div class="form-group">
                          <label>Confirm Password*</label>
                          <input type="password" name="password_confirmation" maxlength="65" class="form-control">
                          @if($errors->has('password_confirmation'))
                            <span class="msgError" style="color:red">{{ $errors->first('password_confirmation') }}  </span>
                          @endif
                          </div>
                      </div>

                    </fieldset>
                    <button type="submit" value="Login" class="btn blue-btn">Proceed</button>
                  </form>
                </div>
             </div>
            <div class="document-col">
                <div class="document-col-inner full">
                  <div class="mobi-document-col-inner">
                  <div class="go-paperless-sec full">
                    <div class="go-paperless-sec-inner full">
                      <div class="addhar-img-sec">
                        <img src="<?php echo Request::root();?>/images/aadhar.png" alt="aadhar-img"/>
                      </div>
                      <h4>Go ‘paperless’ with Aadhar.</h4>
                      <p>Now open an account with  us  instantly.</p>
                    </div>
                    <div class="mobi-join">
                      <img src="<?php echo Request::root();?>/images/join-img.png" alt="join-img"/>
                     </div>
                  </div>
                  <div class="req-doc-sec">
                    <div class="req-doc-box on-req-bx-main">
                      <div class="req-doc-box-inner on-req-bx">
                        <h4>required document</h4>
                        <ul class="req-list">
                          <li><span><a data-toggle="modal" data-target="#AadharModal">Aadhar Card</a></span></li>
                          <li><span><a data-toggle="modal" data-target="#PanModal">Pan Card</a></span></li>
                          <li><span><a data-toggle="modal" data-target="#ChequeModal">Cancelled Cheque</a></span></li>
                          <li><span><a data-toggle="modal" data-target="#StatementModal">6 Months Bank Statement</a></span></li>
                          <li><span><a data-toggle="modal" data-target="#SignatureModal">Physical Signature</a></span></li>
                        </ul>
                        <div class="circle-bx"></div>
                      </div>
                    </div>
                    <div class="hi-box full">
                      <div class="hi-box-inner">
                        <h4><span>Hey!</span> Aishwarya</h4>
                        <p>Choose a password that easy to remember.</p>
                      </div>
                    </div>
                    <div class="req-img-sec">
                      <img src="<?php echo Request::root();?>/images/men.png" alt="req-img"/>
                    </div>
                <div class="">
                <div class="mobi-hi-sec-inner full">
                  <div class="profile-img-sec">
                        <div class="profile-img">
                            <img src="<?php echo Request::root();?>/images/profile-img.png" alt="profile-img"/>
                        </div>
                        <h4><span>Hey!</span> Aishwarya</h4>
                        <p>you need these documents.</p>
                   </div>
                   <div class="up-arrow-sec">
                      <img src="<?php echo Request::root();?>/images/popup-close-arw.png" alt="close-img"/>
                   </div>
                </div>
                  </div>
                </div>
                </div>
                </div>
             </div>
           </div>

             <div class="mobi-hi-sec">
                <div class="mobi-hi-sec-inner bt-mobi-inner full">
                  <div class="profile-img-sec">
                        <div class="profile-img">
                            <img src="<?php echo Request::root();?>/images/profile-img.png" alt="profile-img"/>
                        </div>
                        <h4><span>Hey!</span> Aishwarya</h4>
                        <p>you need these documents.</p>
                   </div>
                   <div class="up-arrow-sec">
                      <img src="<?php echo Request::root();?>/images/up-arrow.png" alt="up-arrow"/>
                   </div>
                </div>
             </div>
             <div class="login-para-sec full">
                <p>Already have an EKYC Login? <a href="">Sign In</a> here.</p>
             </div>
         </div>
        @include('req-modal')
       </div>
    </div>
@include('includes.footer')
