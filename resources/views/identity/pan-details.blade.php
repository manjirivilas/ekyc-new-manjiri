<!-- Header -->
@include('includes.index-header')

<!-- Content Start -->
<div class="tab-content">
    <div class="tab-pane active">
        <h2 class="form-title">PAN Card Details</h2>
        <form method="post" id="panDetail" name="panDetail"  action-xhr="<?php echo Request::root().'/pan-details';?>">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="adhaar-sec">
        <div class="form-rw">
            <div class="form-group">
                <label>Enter your Permanent Account number*</label>
                <input type="text" name="pan_no" value="{{ $pan_details['pan_no']?$pan_details['pan_no']:old('pan_no')}}" class="form-control" maxlength="10">
                @if($errors->has('pan_no'))
                <span class="msgError" style="color:red">{{ $errors->first('pan_no') }}</span>
                @endif
            </div>
        </div>
        <div class="form-rw full">
            <div class="form-group">
                <label>Is your country of tax residency other than India?</label>
                <div class="radio-rw">
                    <ul class="radio-list">
                        <li>
                            <input type="radio" id="f-option" name="other_residency" value=1 {{ ($pan_details['other_residency'] || old('other_residency'))? 'checked' : '' }}>
                            <label for="f-option">Yes</label>
                            <div class="check"></div>
                        </li>
                        <li>
                            <input type="radio" id="s-option" name="other_residency" value=0  {{ ($pan_details['other_residency'] || old('other_residency'))  ? '' : 'checked' }}>
                            <label for="s-option">No</label>
                            <div class="check">
                                <div class="inside"></div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="form-rw full " id="countryform">
            <div class="form-group">
                <div class="table-responsive country-add" style="display:{{ ($pan_details['other_residency'] || old('other_residency')) ? 'block' : 'none' }}"> <!-- country-add -->
                    <table class="table tax_table">
                      <thead>
                        <tr>
                          <th>Country of tax residency</th>
                          <th>Tax payer identification no.</th>
                          <th>Identification type</th>
                         </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td><span class="numberdisplay align1">1</span><input type="text" name="identification_country_1" class="country_text" value="{{ $pan_details['fatca_details'][0]['country'] ? $pan_details['fatca_details'][0]['country'] : old('identification_country_1')}}"/>
                          @if($errors->has('identification_country_1'))
                          <span class="msgError">{{ $errors->first('identification_country_1') }}</span>
                          @endif
                          </td>
                          <td><input type="text" class="country_text" name="identification_number_1" value="{{ $pan_details['fatca_details'][0]['number'] ? $pan_details['fatca_details'][0]['number'] : old('identification_number_1')}}"/>
                          @if($errors->has('identification_number_1'))
                          <span class="msgError">{{ $errors->first('identification_number_1') }}</span>
                          @endif
                          </td>
                          <td><input type="text" class="country_text"name="identification_type_1"  value="{{ $pan_details['fatca_details'][0]['type'] ? $pan_details['fatca_details'][0]['type'] : old('identification_type_1')}}" />
                          @if($errors->has('identification_type_1'))
                          <span class="msgError">{{ $errors->first('identification_type_1') }}</span>
                          @endif
                          </td>
                        </tr>
                        <tr>
                          <td><span class="numberdisplay align2">2</span><input type="text" class="country_text" name="identification_country_2"  value="{{ $pan_details['fatca_details'][1]['country'] ? $pan_details['fatca_details'][1]['country'] : old('identification_country_2')}}"/>
                          @if($errors->has('identification_country_2'))
                          <span class="msgError">{{ $errors->first('identification_country_2') }}</span>
                          @endif
                          </td>
                          <td><input type="text" class="country_text"name="identification_number_2"  value="{{ $pan_details['fatca_details'][1]['number'] ? $pan_details['fatca_details'][1]['number'] : old('identification_number_2')}}"/>
                          @if($errors->has('identification_number_2'))
                          <span class="msgError">{{ $errors->first('identification_number_2') }}</span>
                          @endif
                          </td>
                          <td><input type="text" class="country_text" name="identification_type_2" value="{{ $pan_details['fatca_details'][1]['type'] ? $pan_details['fatca_details'][1]['type'] : old('identification_type_2')}}"/>
                          @if($errors->has('identification_type_2'))
                          <span class="msgError">{{ $errors->first('identification_type_2') }}</span>
                          @endif
                          </td>
                        </tr>
                        <tr>
                          <td><span class="numberdisplay align3">3</span><input type="text" class="country_text"name="identification_country_3"  value="{{ $pan_details['fatca_details'][2]['country'] ? $pan_details['fatca_details'][2]['country'] : old('identification_country_3') }}"/>
                          @if($errors->has('identification_country_3'))
                          <span class="msgError">{{ $errors->first('identification_country_3') }}</span>
                          @endif
                          </td>
                          <td><input type="text" class="country_text" name="identification_number_3" value="{{ $pan_details['fatca_details'][2]['number'] ? $pan_details['fatca_details'][2]['number'] : old('identification_number_3')}}"/>
                          @if($errors->has('identification_number_3'))
                          <span class="msgError">{{ $errors->first('identification_number_3') }}</span>
                          @endif
                          </td>
                          <td><input type="text" class="country_text" name="identification_type_3" value="{{ $pan_details['fatca_details'][2]['type'] ? $pan_details['fatca_details'][2]['type'] : old('identification_type_3')}}"/>
                          @if($errors->has('identification_type_3'))
                          <span class="msgError">{{ $errors->first('identification_type_3') }}</span>
                          @endif
                          </td>
                        </tr>
                     </tbody>

                    </table>
                </div>
            </div>
        </div>

        <button type="submit" value="submit" class="btn blue-btn">Proceed to next step</button>

    </div>
</form>
    </div>
    <div class="clearfix"></div>
</div>
<!-- Content end -->

<!-- Tooltip Content -->

<!-- Tooltip Content -->
@include('ui-inner.tooltip.pan-details')
<!-- includes scripts -->
@include('includes.index-footer')
<script type="text/javascript" src="{{ Request::root() }}/assets/apps/scripts/identity-validation.js"></script>
  <script type="text/javascript" src="{{ Request::root() }}/assets/apps/scripts/identity.js"></script>

    </body>

</html>
