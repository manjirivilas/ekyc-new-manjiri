<!-- Header -->
@include('includes.index-header')

<!-- Content Start -->

<div class="tab-content">
    <div class="tab-pane active">
      @if($errors->has('error'))
      <div class="alert alert-danger msgError">
          {{ $errors->first('error') }}
      </div>
      @endif
        <h2 class="form-title">OTP for Aadhar <span class="txt-color" name ="adharno" value ="{{old('adharno')}}" id="adhaar_no">{{ session('adhaar')?session('adhaar'):old('adhaar') }}</span></h2><!-- {{ session('aadhar') }} -->
<div class="form-para-sec">
<p class="form-para">To fetch your personal details from UIDAI server, we are sending you a OTP message on
your AADHAR registered mobile number and E-mail ID</p>
                    </div>
<!-- <form id="frm-adhaar" method="get"> -->
<form method="post" name="fetchOTP" id="fetchOTP" action-xhr="<?php echo Request::root().'/aadhar-otp';?>">

  <input type="hidden" name="_token" value="{{ csrf_token() }}">
  <input type="hidden" name="txn_id" value="{{ Cookie::get('txn_id') }}">
  <input type="hidden"  name="adhaar" value="{{ Cookie::get('adhaar') }}">

<div class="adhaar-sec">
   <div class="form-rw adhaarcss">
       <label>Enter One Time Password here</label>
       <input type="text" name="pin" class="form-control" maxlength="6">
       @if($errors->has('pin'))
       <span class="msgError">{{ $errors->first('pin') }}</span>
       @endif
       <span class="info-span"><img src="{{ Request::root() }}/images/icons/info-icon.png">&nbsp;Resend OTP option will be available after <span id="adhaar-resend-otp" class="resend-otp"></span> minutes</span>
   </div>
   <button type="submit" value="submit" class="btn blue-btn" >Submit</button>
       <button type="submit" value="resend" id="resend-btn" class="btn blue-btn btn-margin"  disabled="true">Resend</button>
 </div>
</form>
    </div>
    <div class="clearfix"></div>
</div>
<!-- Content end -->

<!-- Tooltip Content -->

<!-- Tooltip Content -->
@include('ui-inner.tooltip.aadhar-otp')
<!-- includes scripts -->
@include('includes.index-footer')
<script type="text/javascript" src="{{ Request::root() }}/assets/apps/scripts/identity-validation.js"></script>
   <script type="text/javascript" src="{{ Request::root() }}/assets/apps/scripts/identity.js"></script>

    </body>

</html>
