<!-- Header -->
@include('includes.index-header')

<!-- Content Start -->

<div class="tab-content">
    <div class="tab-pane active">
        <h2 class="form-title">Additional Details</h2>
        <form method="post" id="tradingDetailsForm" custom-validation-reporting="interact-and-submit" target="_self"
         action-xhr="<?php echo Request::root().'/additional-details';?>">
        <!-- <form id="frm-adhaar" method="get"> -->

        <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="adhaar-sec">
                <div class="form-rw full">
                    <div class="form-group">
                        <label>Do you have any experience in Investment/Trading?</label>
                        <div class="radio-rw">
                            <ul class="radio-list">
                                <li>
                                    <input type="radio" id="f-option" name="Trading_exp" value="1" {{ old('Trading_exp') ? 'checked' : ''}}>
                                    <label for="f-option">Yes</label>
                                    <div class="check"></div>
                                </li>
                                <li>
                                    <input type="radio" id="s-option" name="Trading_exp" value="0" {{ old('Trading_exp')== 1  ? '': 'checked' }}>
                                    <label for="s-option">No</label>
                                    <div class="check">
                                        <div class="inside"></div>
                                    </div>
                                </li>
                            </ul>

                        </div>
                    </div>
                </div>
                <div class="form-rw full">
                    <div class="form-group">
                        <div class="half-rw" >
                            <div class="half" >
                                <label>Year</label>
                                <select class="form-control change-arrow" id="year" name="year">
                                    <option class="optioncss">0</option>
                                    <option class="optioncss">1</option>
                                    <option class="optioncss">2</option>
                                    <option class="optioncss">3</option>
                                    <option class="optioncss">4</option>
                                </select>
                            </div>
                            <div class="half" >
                                <label>Month</label>
                                <select class="form-control change-arrow" id="month" name="month">
                                    <option class="optioncss">0</option>
                                    <option class="optioncss">1</option>
                                    <option class="optioncss">2</option>
                                    <option class="optioncss">3</option>
                                    <option class="optioncss">4</option>
                                    <option class="optioncss">5</option>
                                    <option class="optioncss">6</option>
                                    <option class="optioncss">7</option>
                                    <option class="optioncss">8</option>
                                    <option class="optioncss">9</option>
                                    <option class="optioncss">10</option>
                                    <option class="optioncss">11</option>
                                </select>
                            </div>
                            @if($errors->has('Trading_exp'))
                            <span class="msgError" style="color:red">{{ $errors->first('Trading_exp') }}</span>
                            @endif
                        </div>
                        <div class="full">
                            <span class="info-span gry-txt"><img src="images/icons/info-icon.png">&nbsp;Please mention (For eg: 2 years and 7 months)</span>
                        </div>

                    </div>
                </div>
                <button type="submit" value="submit" class="btn blue-btn next-step">Proceed to next step</button>

            </div>
        </form>
    </div>
    <div class="clearfix"></div>
</div>
<!-- Content end -->

<!-- Tooltip Content -->

<!-- Tooltip Content -->
@include('ui-inner.tooltip.additional-details-2')
<!-- includes scripts -->
@include('includes.index-footer')
<script type="text/javascript" src="{{ Request::root() }}/assets/apps/scripts/identity-validation.js"></script>
   <script type="text/javascript" src="{{ Request::root() }}/assets/apps/scripts/identity.js"></script>
   

    </body>

</html>
