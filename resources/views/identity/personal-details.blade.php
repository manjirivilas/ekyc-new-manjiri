<!-- Header -->
@include('includes.index-header')

<!-- Content Start -->
<!-- disable aadhar details -->

<!-- end -->
<div class="tab-content">
    <div class="tab-pane active">
        <h2 class="form-title">Additional Details</h2>

        @if($errors->has('error'))
        <div class="alert alert-danger msgError">
            {{ $errors->first('error') }}
        </div>
        @endif

        <form method="post" id="personalDetailsForm" action-xhr="<?php echo Request::root().'/personal-details';?>">
         <input type="hidden" name="_token" value="{{ csrf_token() }}">
         <input type="hidden" name="same_address_flag" id="same_address_flag">
            <div class="adhaar-sec">


                <div class="bank-account-sec full">
                    <div class="edit-title-rw full">
                        <span class="edit-title-main">
                        <h6 class="bank-title"><span class="bank-icon"><img src="{{ Request::root() }}/images/add-icon/personal-icon.png" alt="bank-account-img"/></span> Personal Details</h6>
                        <a class="mobi-add"><i class="fa fa-plus" aria-hidden="true"></i></a>
                        <!-- <a class="edit-btn"><img src="{{ Request::root() }}/images/add-icon/edit-pencil.png" alt="edit-pen"/>Edit</a> -->
                        </span>
                     </div>
                     <div class="bank-account-inner-sec full">
                    <div class="bank-rw">
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>Father’s Name*</label>
                                <input type="text" name="fathername" class="form-control" value="{{ ($data['father_name'])  ? ($data['father_name']) :  old('fathername') }}" >
                                @if($errors->has('fathername'))
                                <span class="msgError" style="color:red">{{ $errors->first('fathername') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>Mother’s Name*</label>
                                <input type="text" name="mothername" class="form-control" value="{{ ($data['mother_name'])  ? ($data['mother_name']) :  old('mothername') }}" >
                                @if($errors->has('mothername'))
                                <span class="msgError" style="color:red">{{ $errors->first('mothername') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="bank-rw">
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>Date of Birth*</label>
                                <div class='input-group date' id='personal-detail-datetime'>
                                    <input type='text' name="birthdate" class="form-control removeborder"  value="{{ ($data['date_of_birth'])  ? ($data['date_of_birth']) :  old('birthdate') }}" />
                                    @if($errors->has('birthdate'))
                                    <span class="msgError" style="color:red">{{ $errors->first('birthdate') }}</span>
                                    @endif
                                    <span class="input-group-addon removeback">
                                        <!-- <span class="fa fa-calendar">
                                        </span> -->
                                        <img src="{{ Request::root() }}/images/add-icon/cal-icon.png" alt="calendar">
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>Gender*</label>
                                <div class="radio-rw">
                                    <ul class="radio-list add-dt-lst">
                                        <li>
                                            <input type="radio" id="male" name="gender" value="M" {{ ($data['gender']== "M")  ? "checked" : "" }}>
                                            <label for="male">Male</label>
                                            <div class="check"></div>
                                        </li>
                                        <li>
                                            <input type="radio" id="female" name="gender"  value="F" {{ ($data['gender'] == "F" )  ? "checked" : "" }}>
                                            <label for="female">Female</label>
                                            <div class="check"></div>
                                        </li>
                                    </ul>
                                    @if($errors->has('gender'))
                                    <span class="msgError" style="color:red">{{ $errors->first('gender') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="bank-rw">
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>E-mail ID*</label>
                                <input type="email" name="email" class="form-control" value="{{ ($data['email'])  ? ($data['email']) :  old('email') }} ">
                                @if($errors->has('email'))
                                <span class="msgError" style="color:red">{{ $errors->first('email') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>Mobile Number*</label>
                                <input type="text" name="mobile" class="form-control" value="{{ ($data['mobile'])  ? ($data['mobile']) :  old('mobile') }} ">
                                @if($errors->has('mobile'))
                                <span class="msgError" style="color:red">{{ $errors->first('mobile') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>


                    <div class="form-rw full">
                        <label>Marrital Status*</label>
                                <div class="radio-rw">
                                    <ul class="radio-list add-dt-lst">
                                        <li>
                                            <input type="radio" id="married" name="marrital_status" value="married" {{ ($data['marrital_status']== 'married')  ? 'checked' :  '' }} >
                                            <label for="married">Married</label>
                                            <div class="check"></div>
                                        </li>
                                        <li>
                                            <input type="radio" id="unmarried" name="marrital_status" value="unmarried" {{ ($data['marrital_status']== 'unmarried')  ? 'checked' :  '' }} >
                                            <label for="unmarried">Unmarried</label>
                                            <div class="check"></div>
                                        </li>
                                    </ul>
                                    @if($errors->has('marrital_status'))
                                    <span class="msgError" style="color:red">{{ $errors->first('marrital_status') }}</span>
                                    @endif
                                </div>
                    </div>
                    </div>
                </div>

                <div class="bank-account-sec full">
                    <div class="edit-title-rw full">
                        <span class="edit-title-main">
                        <h6 class="bank-title"><span class="bank-icon"><img src="{{ Request::root() }}/images/add-icon/cr-add-icon.png" alt="bank-account-img"/></span> CORRESPONDENCE ADDRESS</h6>
                        <a class="mobi-add"><i class="fa fa-plus" aria-hidden="true"></i></a>
                        <!-- <a class="edit-btn"><img src="{{ Request::root() }}/images/add-icon/edit-pencil.png" alt="edit-pen"/>Edit</a> -->
                        </span>
                     </div>
                     <div class="bank-account-inner-sec full">
                    <div class="bank-rw">
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>Address*</label>
                                <input type="text" name="address" class="form-control" value="{{ ($data['address'])  ? ($data['address']) :  old('address') }}" maxlength="150">
                                @if($errors->has('address'))
                                <span class="msgError" style="color:red">{{ $errors->first('address') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>Address 1*</label>
                                <input type="text" name="address1" class="form-control" value="{{ ($data['address1'])  ? ($data['address1']) :  old('address1') }}" maxlength="150">
                                @if($errors->has('address1'))
                                <span class="msgError" style="color:red">{{ $errors->first('address1') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="bank-rw">
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>Address 2*</label>
                                <input type="text" name="address2" class="form-control" value="{{ ($data['address2'])  ? ($data['address2']) :  old('address2') }}" maxlength="150">
                                @if($errors->has('address2'))
                                <span class="msgError" style="color:red">{{ $errors->first('address2') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>City*</label>
                                <input type="text" name="city" class="form-control" value="{{ ($data['city'])  ? ($data['city']) :  old('city') }}" maxlength="30">
                                <!-- <select class="form-control change-arrow" id="city" name="city">
                                   <option class="optioncss">Bangalore</option>
                                   <option class="optioncss">Mumbai</option>
                                   <option class="optioncss">Pune</option>
                               </select> -->
                               @if($errors->has('city'))
                               <span class="msgError" style="color:red">{{ $errors->first('city') }}</span>
                               @endif
                            </div>
                        </div>
                    </div>

                    <div class="bank-rw">
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>State*</label>


                                <select class="form-control change-arrow" id="state" name="state" >
                                    <?php
                                    foreach($state as $state_data)
                                    {
                                      ?>
                                      <option class="optioncss" <?php if($data['state']==$state_data->state_name){ print_r("selected");} ?>><?php print_r($state_data->state_name); ?></option>
                                      <?php
                                    }
                                     ?>
                                </select>


                               @if($errors->has('state'))
                               <span class="msgError" style="color:red">{{ $errors->first('state') }}</span>
                               @endif
                            </div>
                        </div>
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>PIN Code*</label>
                                <input type="text" name="pincode" class="form-control" value="{{ ($data['pincode'])  ? ($data['pincode']) :  old('pincode') }} ">
                                @if($errors->has('pincode'))
                                <span class="msgError" style="color:red">{{ $errors->first('pincode') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="bank-rw">
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>Address Proof*</label>
                                <!-- <input type="text" name="address_proof" class="form-control" value="{{ ($data['address_proof'])  ? ($data['address_proof']) :  old('address_proof') }} "> -->
                                <select class="form-control change-arrow" id="address_proof" name="address_proof" >
                                    <option class="optioncss" {{ (Auth::user()->user_group==3)? 'selected' : ''}}>Aadhar</option>
                                   <option class="optioncss" >Pan</option>
                                   <option class="optioncss">Driving Lincense</option>
                                </select>
                                @if($errors->has('address_proof'))
                                <span class="msgError" style="color:red">{{ $errors->first('address_proof') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="view-pdf-rw">
                        <p><span class="squaredThree"><input type="checkbox" name="rember-me" id="same_address" {{ old('same_address_flag')== "yes" ? 'checked' : '' }}><label for="same_address"></label></span>My permanent address is the same as my correspondence address</p>
                    </div>
                    </div>
                </div>


                <div class="bank-account-sec full permanent-add">
                    <div class="edit-title-rw full">
                       <span class="edit-title-main">
                        <h6 class="bank-title"><span class="bank-icon"><img src="{{ Request::root() }}/images/add-icon/cr-add-icon.png" alt="bank-account-img"/></span> PERMANENT ADDRESS</h6>
                        <a class="mobi-add"><i class="fa fa-plus" aria-hidden="true"></i></a>
                        <!-- <a class="edit-btn"><img src="{{ Request::root() }}/images/add-icon/edit-pencil.png" alt="edit-pen"/>Edit</a> -->
                       </span>
                     </div>
                     <div class="bank-account-inner-sec full">
                    <div class="bank-rw">
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>Address*</label>
                                <input type="text" name="permanent_address"  class="form-control" value="{{ ($data['permanent_address'])  ? ($data['permanent_address']) :  old('permanent_address') }} " maxlength="150">
                                @if($errors->has('permanent_address'))
                                <span class="msgError" style="color:red">{{ $errors->first('permanent_address') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>Address 1*</label>
                                <input type="text" name="permanent_address1"  class="form-control" value="{{ ($data['permanent_address1'])  ? ($data['permanent_address1']) :  old('permanent_address1') }} " maxlength="150">
                                @if($errors->has('permanent_address1'))
                                <span class="msgError" style="color:red">{{ $errors->first('permanent_address1') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="bank-rw">
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>Address 2*</label>
                                <input type="text" name="permanent_address2"  class="form-control" value="{{ ($data['permanent_address2'])  ? ($data['permanent_address2']) :  old('permanent_address2') }} " maxlength="150">
                                @if($errors->has('permanent_address2'))
                                <span class="msgError" style="color:red">{{ $errors->first('permanent_address2') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>City*</label>
                                <input type="text" name="permanent_city"  class="form-control" value="{{ ($data['permanent_city'])  ? ($data['permanent_city']) :  old('permanent_city') }} " maxlength="30">

                               @if($errors->has('permanent_city'))
                               <span class="msgError" style="color:red">{{ $errors->first('permanent_city') }}</span>
                               @endif
                            </div>
                        </div>
                    </div>

                    <div class="bank-rw">
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>State*</label>
                                <select class="form-control change-arrow" id="permanent_state" name="permanent_state" >
                                    <?php
                                    foreach($state as $state_data)
                                    {
                                      ?>
                                      <option class="optioncss" <?php if($data['permanent_state']==$state_data->state_name){ print_r("selected");} ?> ><?php print_r($state_data->state_name); ?></option>
                                      <?php
                                    }
                                     ?>
                                </select>
                                <!-- <select class="form-control change-arrow" id="year" name="permanent_state">
                                   <option class="optioncss">Karnataka</option>
                                   <option class="optioncss">Maharastra</option>
                                   <option class="optioncss">Gujrat</option>
                               </select> -->
                               @if($errors->has('permanent_state'))
                               <span class="msgError" style="color:red">{{ $errors->first('permanent_state') }}</span>
                               @endif
                            </div>
                        </div>
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>PIN Code*</label>
                                <input type="text" name="permanent_pincode"  class="form-control" value="{{ ($data['permanent_pincode'])  ? ($data['permanent_pincode']) :  old('permanent_pincode') }}">
                                @if($errors->has('permanent_pincode'))
                                <span class="msgError" style="color:red">{{ $errors->first('permanent_pincode') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="bank-rw">
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>Address Proof*</label>
                                <!-- <input type="text" name="permanent_address_proof"  class="form-control" value="{{ ($data['permanent_address_proof'])  ? ($data['permanent_address_proof']) :  old('permanent_address_proof') }}"> -->
                                 <select class="form-control change-arrow" id="permanent_address_proof" name="permanent_address_proof" >
                                    <option class="optioncss">Aadhar</option>
                                   <option class="optioncss">Pan</option>
                                   <option class="optioncss">Driving Lincense</option>
                                </select>
                                @if($errors->has('permanent_address_proof'))
                                <span class="msgError" style="color:red">{{ $errors->first('permanent_address_proof') }}</span>
                                @endif
                            </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="bank-account-sec full">
                    <div class="edit-title-rw full">
                       <span class="edit-title-main">
                        <h6 class="bank-title"><span class="bank-icon"><img src="{{ Request::root() }}/images/add-icon/other-icon.png" alt="bank-account-img"/></span> Other Details</h6>
                        <a class="mobi-add"><i class="fa fa-plus" aria-hidden="true"></i></a>
                        <!-- <a class="edit-btn"><img src="{{ Request::root() }}/images/add-icon/edit-pencil.png" alt="edit-pen"/>Edit</a> -->
                       </span>
                     </div>
                     <div class="bank-account-inner-sec full">
                    <div class="bank-rw">
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>Annual Income*</label>
                                <!-- <input type="text" name="annual-income"  class="form-control" value="{{  old('annual-income') }}"> -->
                                <select class="form-control change-arrow" id="year" name="annual_income" value="{{ ($data['annual_income'])  ? ($data['annual_income']) :  old('annual_income') }}">
                                    <?php
                                    foreach($income as $income_data)
                                    {
                                      ?>
                                      <option class="optioncss"><?php print_r($income_data->description); ?></option>
                                      <?php
                                    }
                                     ?>
                                </select>

                                @if($errors->has('annual-income'))
                                <span class="msgError" >{{ $errors->first('annual-income') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>Net Worth*</label>
                                <input type="text" name="net_worth"  class="form-control" value="{{ ($data['net_worth'])  ? ($data['net_worth']) :  old('net_worth') }}">
                                @if($errors->has('net_worth'))
                                <span class="msgError">{{ $errors->first('net_worth') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="bank-rw">
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>As On*</label>
				 <div class='input-group date' id='as-on-datetime'>
                                <input type="text" name="as_on"  class="form-control  removeborder" value="{{ ($data['as_on'])  ? ($data['as_on']) :  old('as_on') }}">
                                @if($errors->has('as_on'))
                                <span class="msgError" style="color:red">{{ $errors->first('as_on') }}</span>
                                @endif
                                    <span class="input-group-addon removeback">
                                        <img src="{{ Request::root() }}/images/add-icon/cal-icon.png" alt="calendar">
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>Occupation*</label>
                                <!-- <input type="text" name="occuption"  class="form-control" value="{{ ($data['occupation'])  ? ($data['occupation']) :  old('occupation') }}"> -->
				<select class="form-control change-arrow" id="occupation" name="occupation" >
                                    <option class="optioncss">Devloper</option>
                                   <option class="optioncss">Public</option>
                                   <option class="optioncss">Private</option>
                                </select>
                                @if($errors->has('occupation'))
                                <span class="msgError" style="color:red">{{ $errors->first('occupation') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                </div>

                <div class="btn-rw full">
                    <button type="submit" value="submit" class="btn blue-btn">Proceed to next step</button>
                  </div>
            </div>
        </form>
    </div>
    <div class="clearfix"></div>
</div>
<!-- Content end -->


<!-- Tooltip Content -->
@include('ui-inner.tooltip.additional-details-1')

<!-- includes scripts -->
@include('includes.index-footer')
<script type="text/javascript" src="{{ Request::root() }}/assets/apps/scripts/identity-validation.js"></script>
   <script type="text/javascript" src="{{ Request::root() }}/assets/apps/scripts/identity.js"></script>

   <?php
   if(Auth::user()->user_group == 3)
   {
     ?>
     <script type="text/javascript">
     $('input[name="fathername"]').prop('disabled', true);
     $('input[name="birthdate"]').prop('disabled', true);
     $('input[name="address"]').prop('disabled', true);
     $('input[name="address1"]').prop('disabled', true);
     $('input[name="address2"]').prop('disabled', true);
     $('input[name="city"]').prop('disabled', true);
     $('select[name="state"]').prop('disabled', true);
     $('input[name="pincode"]').prop('disabled', true);
     $('select[name="address_proof"]').prop('disabled', true);
     </script>
     <?php
   }

    ?>

    </body>

</html>
