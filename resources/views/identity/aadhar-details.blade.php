<!-- Header -->
@include('includes.index-header')

<!-- Content Start -->

@if($errors->has('error'))
<div class="alert alert-danger msgError">
    {{ $errors->first('error') }}
</div>
@endif
<div class="tab-content">
    <div class="tab-pane active">
        <h2 class="form-title">Go ‘Paperless’ with Aadhar Card Details</h2>
        	<form method="post" id="aadharDetail" action-xhr="<?php echo Request::root().'/aadhar-details';?>">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="adhaar-sec">
                <div class="form-rw adhaarcss">
                    <label>Enter Your Aadhar Card Number*</label>
                    <input type="text" name="adhaar" value="{{ $adhaar }}" class="form-control" maxlength="12">
                    @if($errors->has('adhaar'))
                    <span class="msgError" style="color:red">{{ $errors->first('adhaar') }}</span>
                    @endif
                </div>
                <button type="submit" value="submit" class="btn blue-btn">Proceed</button>
            </div>
        </form>
        <div class="id-sec">Don’t have Aadhar Card? Continue with <a href="<?php echo Request::root();?>/identity/pan-details" onclick="return nextstep('identity/pan-details.blade.php','step1');">paper based ekyc</a></div>
    </div>
    <div class="clearfix"></div>
</div>
<!-- Content end -->

<!-- Tooltip Content -->

<!-- Tooltip Content -->
@include('ui-inner.tooltip.aadhar-details')
@include('includes.index-footer')
<script type="text/javascript" src="{{ Request::root() }}/assets/apps/scripts/identity-validation.js"></script>
  <script type="text/javascript" src="{{ Request::root() }}/assets/apps/scripts/identity.js"></script>


    </body>

</html>
