<!-- Header -->
@include('includes.index-header')

<!-- Content Start -->


<div class="tab-content">
    <div class="tab-pane active">
        <h2 class="form-title">Bank Account Details</h2>
        @if($errors->has('error'))
        <div class="alert alert-danger msgError">
            {{ $errors->first('error') }}
        </div>
        @endif
        <form method="post" id="bankDetails" name="bankDetails" action-xhr="<?php echo Request::root().'/bank-details';?>">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="flag" id="flag" value="">
        <!-- <form id="frm-adhaar" method="get"> -->
            <div class="adhaar-sec">
                <div class="bank-account-sec full addhar-br">
                    <h6 class="bank-title"><span class="bank-icon"><img src="{{ Request::root() }}/images/bank-icon.png" alt="bank-account-img"/></span> Primary bank account details</h6>
                    <div class="form-rw full">
                        <label>IFSC Code*</label>
                        <input type="text" name="ifsc" id="ifsc" value="{{ ($data->ifsc)  ? ($data->ifsc) :  old('ifsc') }} " class="form-control" maxlength="11">
                        <p class="bank-info-para">We’ll fetch your branch details through IFSC code</p>
                          <span id="ifscError" style="color:red"></span>
                          @if($errors->has('ifsc'))
                          <span class="msgError" style="color:red">{{ $errors->first('ifsc') }}</span>
                          @endif
                            <div class="error-msg">
                              <span class="cls-btn"><img src="{{ Request::root() }}/images/error-arw.png" alt="error-img"> Error!</span>
                              <p>We are unable to fetch your bank details. Please manually enter these fields.</p>
                            </div>
                    </div>
                    <div class="bank-rw">
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>Bank Name*</label>
                                  <input type="text" name="bankname" id="bankname" value="{{ ($data->bankname)  ? ($data->bankname) :  old('bankname') }}" class="form-control" maxlength="45">
                                    <span id="bank_branchError" style="color:red"></span>

                            </div>
                        </div>
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>Branch Name*</label>
                                  <input type="text" name="branch" id="branch" value="{{ ($data->branch)  ? ($data->branch) :  old('branch') }}" required class="form-control" maxlength="45">
                                    <span id="bank_branchError" style="color:red"></span>

                            </div>
                        </div>
                    </div>
                    <div class="bank-rw">
                        <div class="frm-col">
                            <div class="form-rw full">
                              <label>MICR Number*</label>
                              <input type="text" name="micr" id="micr" value="{{ ($data->micr)  ? ($data->micr) :  old('micr') }}" required class="form-control" maxlength="9">
                              <span id="micr_noError" style="color:red"></span>
                              @if($errors->has('micr'))
                              <span class="msgError" style="color:red">{{ $errors->first('micr') }}</span>
                              @endif
                            </div>
                        </div>
                        <div class="frm-col">
                    <div class="form-rw full">
                        <label>Select Account type</label>
                        <div class="radio-rw">
                                <ul class="radio-list">
                                	   			<li>
                                            <input type="radio" id="f-option" value="Savings" name="acc_type"  {{ (($data->acc_type)== 'Savings'  || old('acc_type') == 'Savings' )  ? 'checked' :  '' }}>
                                    				<label for="f-option">Saving</label>
                                    				<div class="check"></div>
                                				</li>
                                	   			<li>
                                    				<input type="radio" id="s-option" value="Current" name="acc_type" {{ (($data->acc_type)== 'Current'  || old('acc_type') == 'Current' )  ? 'checked' :  '' }} >
                                    				<label for="s-option">Current</label>
                                    				<div class="check"><div class="inside"></div></div>
                                	   			</li>
                                </ul>
                        </div>
                        <span id="account_typeError"></span>
                        @if($errors->has('acc_type'))
                        <span class="msgError">{{ $errors->first('acc_type') }}</span>
                        @endif
                    </div>
                        </div>
                    </div>

                    <div class="bank-rw">
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>Account Number*</label>
                                  <input type="text" name="accno" id="accno" value="{{ ($data->accno)  ? ($data->accno) :  old('accno') }}" class="form-control" maxlength="16">
                                  <span id="acc_noError" style="color:red"></span>
                                  @if($errors->has('accno'))
                                  <span class="msgError" style="color:red">{{ $errors->first('accno') }}</span>
                                  @endif
                            </div>
                        </div>
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>Re-type Account Number*</label>
                                  <input type="text" name="accno_confirmation" value="{{ ($data->accno)  ? ($data->accno) :  old('accno_confirmation') }}" id="accno_confirmation" class="form-control" maxlength="16">
                                  <span id="confirm_acc_noError" style="color:red"></span>
                                  @if($errors->has('accno_confirmation'))
                                  <span class="msgError" style="color:red">{{ $errors->first('accno_confirmation') }}</span>
                                  @endif
                            </div>
                        </div>
                    </div>
                </div>

                <div class="bank-account-sec full  addhar-bt add-account1 {{ old('flag') ? '' : 'add-account'}}">
                    <h6 class="bank-title"><span class="bank-icon"><img src="{{ Request::root() }}/images/bank-icon.png" alt="bank-account-img"/></span>Secondary bank account details  <span class="add-remove"><img src="{{ Request::root() }}/images/error-arw.png" alt=""/></span></h6>
                    <div class="form-rw full">
                        <label>IFSC Code*</label>
                        <input type="text" name="secifsc" id="secifsc" value="{{ ($data->secifsc)  ? ($data->secifsc) :  old('secifsc') }}"  class="form-control" maxlength="11">
                        <p class="bank-info-para">We’ll fetch your branch details through IFSC code</p>
                        <span id="sec_ifscError" style="color:red"></span>
                        @if($errors->has('secifsc'))
                        <span class="msgError" style="color:red">{{ $errors->first('secifsc') }}</span>
                        @endif
                        <div class="error-msg sec_error-msg">
                          <span class="cls-btn"><img src="{{ Request::root() }}/images/error-arw.png" alt="error-img"> Error!</span>
                          <p>We are unable to fetch your bank details. Please manually enter these fields.</p>
                        </div>
                    </div>
                    <div class="bank-rw">
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>Bank Name*</label>
                                <input type="text" name="secbankname" value="{{ ($data->secbankname)  ? ($data->secbankname) :  old('secbankname') }}" id="secbankname"  class="form-control" maxlength="45">
                                <span id="sec_bank_branchError" style="color:red"></span>
                                @if($errors->has('secbankname'))
                                <span class="msgError" style="color:red">{{ $errors->first('secbankname') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>Branch Name*</label>
                                <input type="text" name="secbranch" value="{{ ($data->secbranch)  ? ($data->secbranch) :  old('secbranch') }}" id="secbranch"  class="form-control" maxlength="45">
                                <span id="sec_bank_branchError" style="color:red"></span>
                                @if($errors->has('secbranch'))
                                <span class="msgError" style="color:red">{{ $errors->first('secbranch') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>


                    <div class="bank-rw">
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>MICR Number*</label>
                                <input type="text" name="secmicr" id="secmicr" value="{{ ($data->secmicr)  ? ($data->secmicr) :  old('secmicr') }}" class="form-control">
                                <span id="sec_micr_noError" style="color:red"></span>
                                @if($errors->has('secmicr'))
                                <span class="msgError" style="color:red">{{ $errors->first('secmicr') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="frm-col">
                    <div class="form-rw full">
                        <label>Select Account type</label>
                        <div class="radio-rw">
                                <ul class="radio-list">
                                	   			<li>
                                	   				<input type="radio" id="f-option2" value="Savings" name="secacc_type"  {{ (($data->secacc_type) == 'Savings' || old('secacc_type') == 'Savings')  ? 'checked' : '' }}>
                                    				<label for="f-option2">Saving</label>
                                    				<div class="check"></div>
                                				</li>
                                	   			<li>
                                                            <input type="radio" id="s-option2" value="Current" name="secacc_type" {{ (($data->secacc_type)== 'Current' || old('secacc_type') == 'Current') ? 'checked' : '' }}>
                                    				<label for="s-option2">Current</label>
                                    				<div class="check"><div class="inside"></div></div>
                                	   			</li>
                                </ul>
                        </div>
                        <span id="sec_account_typeError" style="color:red"></span>
                        @if($errors->has('secacc_type'))
                        <span class="msgError" style="color:red">{{ $errors->first('secacc_type') }}</span>
                        @endif
                    </div>
                        </div>
                    </div>

                    <div class="bank-rw">
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>Account Number*</label>
                                <input type="text" name="secaccno" id="secaccno" value="{{ ($data->secaccno)  ? ($data->secaccno) :  old('secaccno') }}" class="form-control">
                                <span id="sec_acc_noError" style="color:red"></span>
                                @if($errors->has('secaccno'))
                                <span class="msgError" style="color:red">{{ $errors->first('secaccno') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>Re-type Account Number*</label>
                                <input type="text" name="secaccno_confirmation" value="{{ ($data->secaccno)  ? ($data->secaccno) :  old('secaccno_confirmation') }}"  id="secaccno_confirmation"  class="form-control">
                                <span id="sec_confirm_acc_noError" style="color:red"></span>
                                @if($errors->has('secaccno_confirmation'))
                                <span class="msgError" style="color:red">{{ $errors->first('secaccno_confirmation') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

                <div class="add-an-det-rw full">
                    <span class="add-det {{ old('flag') ? 'add-account':''}}"><img src="{{ Request::root() }}/images/add-details.png" alt=""/><span>Add another bank account details</span></span>
                </div>
                <button type="submit" value="submit" id="submit" class="btn blue-btn">Submit</button>
            </div>
        </form>
    </div>
    <div class="clearfix"></div>
</div>
<!-- Content end -->


<!-- Tooltip Content -->
@include('ui-inner.tooltip.bank-details')

<!-- includes scripts -->
@include('includes.index-footer')

   <script type="text/javascript" src="{{ Request::root() }}/assets/apps/scripts/bank-validation.js"></script>
   <script type="text/javascript" src="{{ Request::root() }}/assets/apps/scripts/bank-details.js"></script>

    </body>

</html>
