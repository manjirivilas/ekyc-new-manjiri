
@include('includes.header')

<div class="main-inner-sec full  sign-inner-sec">
         <div class="main-tb-sec forget-sec rest-pass-sec">
           <div class="form-col">
             <span class="msgError" style="color:red">
             @if($errors->has('server-error'))
                 {{ $errors->first('server-error') }}
             @endif
           </span>
               <div class="form-col-inner full">
                    <img src="<?php echo Request::root();?>/images/ch-pass-icon.png" alt="req-img"/>
                   <h2 class="form-title">Change Password</h2>
                   <div class="form-para-sec">
                     <p class="form-para">Enter a new password for your account privacy.</p>
                    </div>

                    @if($errors->has('error_msg'))
                      <span class="alert alert-danger msgError" style="color:red">{{ $errors->first('error_msg') }}</span>
                    @endif

                   <form method="post" id="reset-password" custom-validation-reporting="interact-and-submit" target="_self"
                     action-xhr="<?php echo Request::root().'/reset-password';?>" >

                     <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <!-- code here-->
                      <input type="text" name="token" id="token" value="{{ Cookie::get('token') }} " hidden/>
                      <input type="text" name="email" id="email" value="{{ Cookie::get('email') }} " hidden/>

                   <fieldset class="login-section">
                    <div class="change-pass-rw">
                     <div class="form-rw">
                       <div class="form-group">
                         <label>Enter Password</label>
                         <input type="password" name="password"  class="form-control">
                         <span class="msgError" style="color:red">
                         @if($errors->has('password'))
                             {{ $errors->first('password') }}
                         @endif
                       </span>
                       </div>
                       <span class="correct-arw"><img src="<?php echo Request::root();?>/images/correct-arw.png" alt=""/></span>
                     </div>
                     <div class="form-rw">
                       <div class="form-group">
                         <label>Re-enter Password</label>
                         <input type="password" name="password_confirmation"  class="form-control">
                         <span class="msgError" style="color:red">
                         @if($errors->has('password_confirmation'))
                             {{ $errors->first('password_confirmation') }}
                         @endif
                       </span>

                       </div>
                       <span class="correct-arw"><img src="images/correct-arw.png" alt=""/></span>
                     </div>
                     <div class="rest-btn-rw">
                     <button type="submit" value="submit" class="btn blue-btn">Submit </button>
                     </div>
                   </div>
                   </fieldset>
                 </form>

                  </div>
                  </div>
               </div>
            </div>
        </div>
      </div>
   </div>
@include('includes.footer')
