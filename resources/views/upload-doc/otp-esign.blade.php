<!-- Header -->
@include('includes.index-header')

<!-- Content Start -->

<div class="tab-content">
    <div class="tab-pane active">
        <h2 class="form-title">OTP for E-Sign</h2>
<div class="form-para-sec">
  <p class="form-para">To digitally sign your documents, please enter the OTP sent on your mobile/E-mail registered with Aadhaar</p>
 </div>
<form id="frm-adhaar" method="get">
<div class="adhaar-sec">
   <div class="form-rw adhaarcss">
       <label>Enter One Time Password here</label>
       <input type="text" name="adhaarnumber" required class="form-control">
       <span class="info-span"><img src="{{ Request::root() }}/images/icons/info-icon.png">&nbsp;Resend OTP option will be available after <span id="resend-otp-esign" class="resend-otp"></span> minutes</span>
   </div>
   <button type="submit" value="submit" class="btn blue-btn" onclick="nextstep('identity/additional-details-1.blade.php','step1');">Submit</button> 

    <button type="submit" value="resend" id="resend-btn" class="btn blue-btn btn-margin"  disabled="true">Resend</button>
 </div>
</form>
    </div>
    <div class="clearfix"></div>
</div>
<!-- Content end -->

<!-- Tooltip Content -->

<!-- Tooltip Content -->
@include('ui-inner.tooltip.otp-esign')
<!-- includes scripts -->
@include('includes.index-footer')
 <script src="{{ Request::root() }}/assets/apps/scripts/jquery.countdownTimer.js"></script>
   <script type="text/javascript" src="{{ Request::root() }}/assets/apps/scripts/upload.js"></script>

    </body>

</html>
