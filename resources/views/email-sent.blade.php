
        @include('includes.header')
        <div class="main-inner-sec full">
            <div class="emil-send-sec full">
              <div class="email-send-sec-inner">
                <div class="email-send-img-sec">
                  <amp-img src="<?php echo Request::root();?>/images/mail-sent.png" width="167" height="79"></amp-img>
                  <span class="user-name">Hello! <span>Aishwarya</span></span>
                  <p class="email-para">We just emailed you with the instructions  <br/>to change your password</p>
                  @if($errors->has('success_msg'))
                    <span class="alert alert-success msgError" style="color:green">{{ $errors->first('success_msg') }}</span>
                  @elseif($errors->has('error_msg'))
                    <span class="alert alert-danger msgError" style="color:red">{{ $errors->first('error_msg') }}</span>
                  @endif
                </div>
                  <div class="resend-sec full">
                    <span class="mail-msg full mail_msg">Didn’t receive our mail?</span>

                    <form method="post" id="email-sent" custom-validation-reporting="interact-and-submit" target="_self"
                      action-xhr="<?php echo Request::root().'/email-sent';?>" >
                      <input type = 'hidden' name="url_path" value = "<?php echo Request::root();?>" id = "url_path"/>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      <input hidden type="email" id="email" name="email" value="{{ Cookie::get('email') }}"
                       required class="form-control">

                    <!-- <a href="" class="btn resend-btn">resend</a> -->
                     <button class="btn resend-btn" type="submit" value="submit" >resend </button>
                  </form>
                  </div>
                </div>
            </div>
         </div>
       </div>
    </div>
    @include('includes.footer')
