 </div>
            </div>
          <div class="backdrop"></div>
        </main>

<script src="{{ Request::root() }}/assets/apps/scripts/app.js" async></script>
<script src="{{ Request::root() }}/assets/apps/scripts/jquery.min.js"></script>

  <script src="{{ Request::root() }}/assets/apps/scripts/jquery.validate.min.js"></script>
<script src="{{ Request::root() }}/assets/apps/scripts/bootstrap.min.js"></script>
<script src="{{ Request::root() }}/assets/apps/scripts/moment.js"></script>
<script src="{{ Request::root() }}/assets/apps/scripts/bootstrap-datetimepicker.min.js"></script>

<script src="{{ Request::root() }}/assets/apps/scripts/jquery.countdownTimer.js"></script>
<script type="{{ Request::root() }}/text/javascript" src="{{ Request::root() }}/assets/apps/scripts/custom.js"></script>

<script src="{{ Request::root() }}/assets/apps/scripts/common-validation-method.js"></script>

<script type="text/javascript">
var logindrop = false;
$(document).ready(function() {
  
   $('input[type="radio"]').click(function() {
       if($(this).attr('id') == 'add-demat') {
            $('.exist-demat-ac').show();
       }

       else {
            $('.exist-demat-ac').hide();
       }
   });


if ($(window).width() < 767){
   $('.dropdown-toggle.account-href').click(function() {
      $(".backdrop").toggleClass("in");
      $(".login-drop").toggleClass("overlay-logout");
      //alert($('.backdrop').is(':visible'));
      /*if($('.backdrop').is(':visible')){
        logindrop=true;
      }*/
   });


      $(".backdrop").click(function () {
        if ($('.backdrop').is(':visible')) {
          $(".backdrop").removeClass("in");
          $(".login-drop").removeClass("overlay-logout");
          //alert("test");
           logindrop=false;
        }
    });
}



});
  $(function(){
        $(".up-arrow-sec").click(function(){
          $(".document-col-inner").toggleClass("dc-block");
          $(".mobi-hi-sec-inner").toggleClass("mobi-open");
          $(".up-arrow-sec .fa").toggleClass("fa-times");
          $(".up-arrow-sec .fa").toggleClass("fa-chevron-up");
      });
    });
      $(function(){
        $(".navbar-toggle").click(function(){
          $(".navbar-collapse").addClass("in");
          $(".navbar-collapse").css("width", "100%");
      });
        $(".mobi-close-btn").click(function(){
          $(".navbar-collapse").removeClass("in");
          //$(".navbar-collapse").css("width", "100%");
      });
    });

</script>





<?php
$module_status = Module();
// print_r(Auth::user()->page_status);exit;
foreach($module_status as $data)
{

	if(Auth::user()->page_status >= $data['min'] && Auth::user()->page_status <= $data['max'] )
	{
		?>
    <script type="text/javascript">
		$("#<?php print_r($data['module']); ?>").removeClass("success-tab");
		$("#<?php print_r($data['module']); ?>").removeClass("disabled");
		$("#<?php print_r($data['module']); ?>").addClass("active");
    $(".<?php print_r($data['module']); ?>").attr("href", "{{ Request::root().'/'.$data['module'].'/'.$data['url'] }}");
		</script>
		<?php
	}
	else
	{
    if(Auth::user()->page_status < $data['min'])
    {
      ?>
  		<script type="text/javascript">
      $("#<?php print_r($data['module']); ?>").removeClass("success-tab");
      $("#<?php print_r($data['module']); ?>").removeClass("active");
  		$("#<?php print_r($data['module']); ?>").addClass("disabled");
  		</script>
  		<?php
    }
    else
    {
      ?>
  		<script type="text/javascript">
      $("#<?php print_r($data['module']); ?>").removeClass("active");
      $("#<?php print_r($data['module']); ?>").removeClass("disabled");
  		$("#<?php print_r($data['module']); ?>").addClass("success-tab");
      $(".<?php print_r($data['module']); ?>").attr("href", "{{ Request::root().'/'.$data['module'].'/'.$data['url'] }}");
  		</script>
  		<?php
    }

	}
}
?>
