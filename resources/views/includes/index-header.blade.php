<!DOCTYPE html>
<html>
	<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <link rel="canonical" href="https://weather-pwa-sample.firebaseapp.com/final/">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="csrf-token" content="{{ csrf_token() }}" />
  <title>EKYC</title>
  <link href="{{ Request::root() }}/assets/apps/css/bootstrap.min.css" rel="stylesheet">
  <link href="{{ Request::root() }}/assets/apps/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ Request::root() }}/assets/apps/css/custom.css">
    <link rel="stylesheet" href="{{ Request::root() }}/assets/apps/css/responsive.css">
     <link rel="stylesheet" href="{{ Request::root() }}/assets/apps/css/fonts.css">
     <link rel="stylesheet" href="{{ Request::root() }}/assets/apps/css/font-awesome.min.css">

     <link rel="stylesheet" href="{{ Request::root() }}/assets/apps/css/jquery.countdownTimer.css">
</head>

    <body>

		<!-- Logo & username -->
         <div class="main-sec">
      <div class="container">
        <nav class="navbar navbar-inverse main-navbar inner-navbar">
            <div >
              <div class="navbar-header">
                <a class="navbar-brand" href="<?php echo Request::root(); ?>">
                  <img src="../../images/tso-new-logo.png" class="dekstop-logo">
                  <img src="../../images/tso-new-logo-1.png" class="mobi-logo">
                </a>
              </div>
              <ul class="nav navbar-nav navbar-right">
                <li class="dropdown main-li">
                  <a class="dropdown-toggle account-href" data-toggle="dropdown" href="#">
                    <img src="../../images/icons/user-icon.png" class="inner-user-icon">
                    <img src="../../images/icons/mobi-user-icon.png" class="mobi-user-icon">
                    <span class="hi-txt">HI!</span>
                    <span class="user-name"><?php echo Auth::user()->name; ?></span>
                    <span class="glyphicon glyphicon-menu-down"></span>
                  </a>
									<ul class="dropdown-menu login-drop">
                    <li><a href="{{ Request::root() }}/change-password">Change password</a></li>
                    <li><a href="{{ Request::root() }}/logout">Logout</a></li>
                  </ul>
                </li>
              </ul>

            </div>
          </nav>
      </div>
    </div>


        <main class="main">
            <div class="container">
                <div class="main-inner full">
                    <!-- Form Content -->
                    <div class="form-col">
                        <div class="form-col-tab-inner full">
                            <div class="wizard">

                                <!-- progress bar -->
        						<div class="wizard-inner">
    <!-- <div class="connecting-line"></div> -->
    <ul class="nav nav-tabs identity-tab" role="tablist">

        <li role="presentation" id="identity" >
            <a class="identity">
                <img src="{{ Request::root() }}/images/icons/identity-icon.png" class="icon-responsive">
                <div class="text-center text-capital">
                  <img src="{{ Request::root() }}/images/success-arw.png" alt="success-icon" class="success-icon"/>
                  <img src="{{ Request::root() }}/images/mobi-success-arw.png" alt="success-icon" class="mobi-success-icon"/>
                  <span class="mobi-circle-no">1</span>
                  <span class="tab-text">Identity</span>
                </div>
            </a>
        </li>

        <li role="presentation" id="plan-selection">
            <a class="plan-selection">
                <img src="{{ Request::root() }}/images/icons/plan-icon.png" class="icon-responsive">
                <div class="text-center text-capital">
                  <img src="{{ Request::root() }}/images/success-arw.png" alt="success-icon" class="success-icon"/>
                  <img src="{{ Request::root() }}/images/mobi-success-arw.png" alt="success-icon" class="mobi-success-icon"/>
                  <span class="mobi-circle-no">2</span>
                  <span class="tab-text">Plan selection</span></div>
            </a>
        </li>
        <li role="presentation" id="bank-details">
            <a class="bank-details">
                <img src="{{ Request::root() }}/images/icons/bank-icon.png" class="icon-responsive">
                <div class="text-center text-capital">
                  <img src="{{ Request::root() }}/images/success-arw.png" alt="success-icon" class="success-icon"/>
                  <img src="{{ Request::root() }}/images/mobi-success-arw.png" alt="success-icon" class="mobi-success-icon"/>
                  <span class="mobi-circle-no">3</span>
                  <span class="tab-text">Bank Details</span>
                </div>
            </a>
        </li>

        <li role="presentation"  id="upload-document">
            <a class="upload-document">
                <img src="{{ Request::root() }}/images/icons/upload-icon.png" class="icon-responsive">
                <div class="text-center text-capital">
                  <img src="{{ Request::root() }}/images/success-arw.png" alt="success-icon" class="success-icon"/>
                  <img src="{{ Request::root() }}/images/mobi-success-arw.png" alt="success-icon" class="mobi-success-icon"/>
                  <span class="mobi-circle-no">4</span>
                  <span class="tab-text">Upload documents</span>
                </div>
            </a>
        </li>

        <li role="presentation" id="payment">
            <a class="payment">
                <img src="{{ Request::root() }}/images/icons/payment-icon.png" class="icon-responsive">
                <div class="text-center text-capital">
                  <img src="{{ Request::root() }}/images/success-arw.png" alt="success-icon" class="success-icon"/>
                  <img src="{{ Request::root() }}/images/mobi-success-arw.png" alt="success-icon" class="mobi-success-icon"/>
                  <span class="mobi-circle-no">5</span>
                  <span class="tab-text">Payment</span></div>
            </a>
        </li>

    </ul>
</div>
	<input type = 'hidden' value = "{{ Request::root() }}" id = "url_path"/>
