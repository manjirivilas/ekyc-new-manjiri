
<!doctype html>
<html amp lang="en">
  <head>
    <meta charset="utf-8">
    <!--<script async custom-element="amp-list" src="https://cdn.ampproject.org/v0/amp-list-0.1.js"></script>-->
    <script async src="https://cdn.ampproject.org/v0.js"></script>
   <!--<script async custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"></script>-->
    <title>EKYC</title>
    <link rel="canonical" href="http://example.ampproject.org/article-metadata.html">
    <meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400i,700" rel="stylesheet">
    <link rel="stylesheet" href="assets/apps/css/fonts.css">
    <link rel="stylesheet" href="assets/apps/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/apps/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/apps/css/custom.css">
    <link rel="stylesheet" href="assets/apps/css/responsive.css">

    <script type="application/ld+json">
      {
        "@context": "http://schema.org",
        "@type": "NewsArticle",
        "headline": "Open-source framework for publishing content",
        "datePublished": "2015-10-07T12:02:41Z",
        "image": [
          "logo.jpg"
        ]
      }
    </script>
    <style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
  </head>

    <body>

      <div class="main-sec">
        <div class="container">

          <nav class="navbar navbar-inverse main-navbar">
                      <div class="">
                        <div class="navbar-header">
                          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                            <span class="mobi-logo"><img src="/images/menu-icon.png"></span>
                          </button>
                          <a class="navbar-brand" href="<?php echo Request::root(); ?>">

                            <span class="dekstop-logo"><img src="/images/tso-new-logo.png"></span>
                            <span class="mobi-logo"><img src="/images/tso-new-logo-1.png"></span>
                          </a>
                          <a href="" class="sign-mobi-btn">Sign In</a>
                        </div>
                        <div class="collapse navbar-collapse" id="myNavbar">
                          <ul class="nav navbar-nav navbar-right">
                            <!--<li><a href="#">Why us</a></li>
                            <li><a href="#">products</a></li>
                            <li><a href="#">trading</a></li>
                            <li><a href="#">plans</a></li>
                            <li><a href="#">support</a></li>-->

                            <?php
                            if(Request::root().$_SERVER['REQUEST_URI'] == Request::root()."/login")
                            {
                              ?>
                              <li id="signUp" ><a href="<?php echo Request::root();?>/"><button type="button" class="btn btn-primary login-btn">Sign up</button></a></li>
                              <?php
                            }
                            else
                            {
                              ?>
                              <li id="signIn"><a href="<?php echo Request::root();?>/login"><button type="button" class="btn btn-primary login-btn">Login</button></a></li>
                              <?php
                            }
                             ?>

                          </ul>
                          <span class="mobi-close-btn"><i class="fa fa-times" aria-hidden="true"></i></span>
                        </div>
                      </div>
                    </nav>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      <input type = 'hidden' value = "<?php echo Request::root();?>" id = "url_path"/>
