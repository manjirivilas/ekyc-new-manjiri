<!DOCTYPE html>
<html>
  <head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel="canonical" href="https://weather-pwa-sample.firebaseapp.com/final/">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>EKYC</title>
    <link href="assets/apps/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/apps/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
     <link rel="stylesheet" href="assets/apps/css/fonts.css">
     <link rel="stylesheet" href="assets/apps/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/apps/css/custom.css">
    <link rel="stylesheet" href="assets/apps/css/responsive.css">
</head>

    <body>

    <!-- Logo & username -->
         <div class="main-sec">
      <div class="container">
        <nav class="navbar navbar-inverse main-navbar inner-navbar">
            <div >
              <div class="navbar-header">
                <a class="navbar-brand" href="#">
                  <img src="../../images/tso-new-logo.png" class="dekstop-logo">
                  <img src="../../images/tso-new-logo-1.png" class="mobi-logo">
                </a>
              </div>
              <ul class="nav navbar-nav navbar-right">
                <li class="dropdown main-li">
                  <a class="dropdown-toggle account-href" data-toggle="dropdown" href="#"><img src="../../images/icons/user-icon.png"> <span class="hi-txt">HI!</span> <span class="user-name"><?php echo Auth::user()->name; ?></span> <span class="glyphicon glyphicon-menu-down"></span></a>
                  <ul class="dropdown-menu login-drop">
                    <li><a href="<?php echo Request::root();?>/change-password">Change Password</a></li>
                    <li><a href="<?php echo Request::root();?>/logout">Log Out</a></li>
                  </ul>
                </li>
              </ul>

            </div>
          </nav>
      </div>
    </div>


        <main class="main">
            <div class="container">
                <div class="main-inner full">
                    <!-- Form Content -->
                    <div class="form-col">
                        <div class="form-col-tab-inner full">
                            <div class="wizard">

