	<script src="assets/apps/scripts/jquery.min.js"></script>
  <script src="assets/apps/scripts/jquery.validate.min.js"></script>
	<script src="assets/apps/scripts/bootstrap.min.js"></script>
    <script src="assets/apps/scripts/common-validation-method.js"></script>
  <script src="assets/apps/scripts/login.js"></script>
  <script type="text/javascript">
      $(function(){
        $(".up-arrow-sec").click(function(){
          $(".document-col-inner").toggleClass("dc-block");
          $(".mobi-hi-sec-inner").toggleClass("mobi-open");
          $(".up-arrow-sec .fa").toggleClass("fa-times");
          $(".up-arrow-sec .fa").toggleClass("fa-chevron-up");
      });
    });
      $(function(){
        $(".navbar-toggle").click(function(){
          $(".navbar-collapse").addClass("in");
          $(".navbar-collapse").css("width", "100%");
      });
        $(".mobi-close-btn").click(function(){
          $(".navbar-collapse").removeClass("in");
          //$(".navbar-collapse").css("width", "100%");
      });
    });
    $( document ).ready(function() {
          setTimeout(function(){
              $('.msgError').fadeOut()
          },5000);
    });


  </script>

    </body>
</html>
