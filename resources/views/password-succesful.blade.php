
@include('includes.header')

        <div class="main-inner-sec full">
          <div class="main-tb-sec forget-sec">
            <div class="form-col">

              @if($errors->has('server-error'))
              <div class="alert alert-danger msgError">
                  {{ $errors->first('server-error') }}
              </div>
              @endif

            @if(session()->has('message'))
                <div class="alert alert-success msgError">
                    {{ session()->get('message') }}
                </div>
            @endif
                <div class="form-col-inner full">
                    <h2 class="form-title">Password reset successful!</h2>
                    <img src="images/pass-reset-icon.png" alt=""/>
                </div>
             </div>
            <div class="document-col">
                <div class="document-col-inner full">
                  <div class="mobi-document-col-inner">
                  <div class="go-paperless-sec full">
                    <div class="go-paperless-sec-inner full">
                      <div class="addhar-img-sec">
                        <img src="<?php echo Request::root();?>/images/aadhar.png" alt="aadhar-img"/>
                      </div>
                      <h4>Go ‘paperless’ with Aadhar.</h4>
                      <p>Now open an account with  us  instantly.</p>
                    </div>
                    <div class="mobi-join">
                      <img src="<?php echo Request::root();?>/images/join-img.png" alt="join-img"/>
                     </div>
                  </div>
                  <div class="req-doc-sec">
                    <div class="hi-box full">
                      <div class="hi-box-inner on-req-bx">
                        <h4>required document</h4>
                        <ul class="req-list">
                          <li><span>Pan Card</span></li>
                          <li><span>Cancelled Cheque</span></li>
                          <li><span>6 Months Bank Statement</span></li>
                          <li><span>Physical Signature</span></li>
                        </ul>
                      </div>
                    </div>
                    <div class="req-img-sec">
                      <img src="<?php echo Request::root();?>/images/men.png" alt="req-img"/>
                    </div>
                <div class="">
                <div class="mobi-hi-sec-inner full">
                  <div class="profile-img-sec">
                        <div class="profile-img">
                            <img src="<?php echo Request::root();?>/images/profile-img.png" alt="profile-img"/>
                        </div>
                        <h4><span>Hey!</span> Aishwarya</h4>
                        <p>Choose a password that easy to remember.</p>
                   </div>
                   <div class="up-arrow-sec">
                      <i class="fa fa-chevron-up" aria-hidden="true"></i>
                   </div>
                </div>
                  </div>
                </div>
                </div>
                </div>
             </div>
            </div>
             <div class="mobi-hi-sec">
                <div class="mobi-hi-sec-inner bt-mobi-inner full">
                  <div class="profile-img-sec">
                        <div class="profile-img">
                            <img src="<?php echo Request::root();?>/images/profile-img.png" alt="profile-img"/>
                        </div>

                   </div>
                   <div class="up-arrow-sec">
                      <i class="fa fa-chevron-up" aria-hidden="true"></i>
                   </div>
                </div>
             </div>
         </div>
       </div>
    </div>
@include('includes.footer')
