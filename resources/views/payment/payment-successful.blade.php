<!-- Header -->
@include('includes.payment-header')

<!-- Content Start -->
            <div class="emil-send-sec full">
                <img src="../images/pay-success-icon.png"></img>
                <p class="pay-successful-para">Your Payment was successful!</p>
                <p class="pay-success-txt">Your Transaction ID, Payment ID, Product details and Order amount has been sent to your email address</p>
                <div class="ahead-sec full">
                  <p class="tra-msg full">Your Transaction ID:  <span class="tra-id">ALKH12HAKJ87</span></p>
                  <a href="" class="btn resend-btn">Proceed Ahead</a>
                </div>
            </div>
         </div>
       </div>
<!-- Content end -->


<!-- includes scripts -->
@include('includes.index-footer')

   <script type="text/javascript" src="assets/apps/scripts/payment.js"></script>

    </body>

</html>