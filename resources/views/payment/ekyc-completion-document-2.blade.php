            <div class="ekyc-process-paper-sec">
        		<h2 class="form-title">How to send us your documents</h2>
        		<div class="send-document-sec full">
        			<div class="ekyc-timeline-rw"></div>
        			<div class="send-document-col">
        				<div class="send-document-inner">
        					<img src="images/ekyc-completion/ekyc-email-icon.png" alt="send-doc-img"/>
        					<span class="send-doc-title">Download the eKYC form sent on your email id</span>
        				</div>
        			</div>
        			<div class="send-document-col">
        				<div class="send-document-inner">
        					<img src="images/ekyc-completion/ekyc-pen-icon.png" alt="send-doc-img"/>
        					<span class="send-doc-title">Print all the pages</span>
        				</div>
        			</div>
        			<div class="send-document-col">
        				<div class="send-document-inner">
        					<img src="images//ekyc-completion/ekyc-pinter-icon.png" alt="send-doc-img"/>
        					<span class="send-doc-title">Sign your form and self-attest required documents</span>
        				</div>
        			</div>
        			<div class="send-document-col">
        				<div class="send-document-inner">
        					<img src="images/ekyc-completion/ekyc-truck-icon.png" alt="send-doc-img"/>
        					<span class="send-doc-title">Courier the documents to Trade Smart Online</span>
        				</div>
        			</div>
        		</div>
            </div>