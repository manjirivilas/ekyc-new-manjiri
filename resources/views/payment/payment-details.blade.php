<!-- Header -->
@include('includes.index-header')

<!-- Content Start -->

<div class="tab-content">
    <div class="tab-pane active">
        <h2 class="form-title">Payment Details</h2>
        <div class="pay-summary-main full">
            <div class="pay-sum-sec full">
                <div class="pay-sum-rw full">
                    <span class="pay-label">Equity and Currency Account</span>
                    <span class="pay-amt">Rs.200</span>
                </div>
                <div class="pay-sum-rw full">
                    <span class="pay-label">Commodity Account</span>
                    <span class="pay-amt">-</span>
                </div>
                <div class="pay-sum-rw full">
                    <span class="pay-label">Account Processing Charges</span>
                    <span class="pay-amt">-</span>
                </div>
            </div>
            <div class="pay-sum-sec full">
                <div class="pay-sum-rw full">
                    <span class="pay-label">Mutual Fund Account</span>
                    <span class="pay-amt">Rs.0</span>
                </div>
                <div class="pay-sum-rw full">
                    <span class="pay-label">Processing Charges</span>
                    <span class="pay-amt">Rs.0</span>
                </div>
            </div>
            <div class="pay-sum-sec full br-btm last-pay-sec">
                <div class="pay-sum-rw full">
                    <span class="pay-label">Amount</span>
                    <span class="pay-amt">Rs.200</span>
                </div>
                <div class="pay-sum-rw full">
                    <span class="pay-label">Discount</span>
                    <span class="pay-amt">-</span>
                </div>
                <div class="pay-sum-rw full">
                    <span class="cp-label">Have a coupon code?</span>
                    <div class="cp-box">
                        <span class="cp-code">GD7589H</span>
                        <a href="" class="apply-btn">Apply</a>
                    </div>
                </div>
                <div class="pay-sum-rw full">
                    <span class="total-label">Total Payable Amount</span>
                    <span class="total-price">Rs. 200</span>
                </div>

                <div class="pay-later-sec full dis">
                    <div class="pay-cheque-col">
                        <div class="full">
                            <h4 class="pay-later-title">Pay via Cheque</h4>
                            <div class="pay-main-sec">
                                <h6>Send in the name of:</h6>
                                <p>Account Name:</p>
                                <span class="account-name">‘VNS Finance and Capital Services Ltd.’</span>
                                <div class="check-rw">
                                    <span class="check-label">Branch:</span>
                                    <span class="check-data">Fort , Mumbai-40000</span>
                                </div>
                                <div class="check-rw">
                                    <span class="check-label">Account Number:</span>
                                    <span class="check-data">00602740000680</span>
                                </div>
                                <div class="check-rw">
                                    <span class="check-label">Account Type:</span>
                                    <span class="check-data">Current A/c</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="btn-rw full pay-btn-rw">
                <button type="submit" value="pay-now" class="btn blue-btn pay-nw-btn">Pay Now</button>
                <button type="submit" value="proceed" class="btn blue-btn dis proc-btn">Proceed</button>
                <button type="submit" value="pay-now" class="btn blue-btn dis pay-nw-btn1">Pay Now</button>
                <button type="button" class="btn later-btn">Pay Later</button>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<!-- Content end -->


<!-- Tooltip Content -->
@include('ui-inner.tooltip.payment-summary')

<!-- includes scripts -->
@include('includes.index-footer')

   <script type="text/javascript" src="{{ Request::root() }}/assets/apps/scripts/payment.js"></script>

    </body>

</html>