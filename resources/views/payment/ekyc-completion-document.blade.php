             <div class="ekyc-process-paperless-sec">
                <h2 class="form-title">How to send us your documents</h2>
                <div class="send-document-sec full">
                    <div class="ekyc-timeline-rw"></div>
                    <div class="send-document-col">
                        <div class="send-document-inner">
                            <img src="images/ekyc-completion/ekyc-email-icon.png" alt="send-doc-img"/>
                            <span class="send-doc-title">Download &amp; Print POA (Power of Atony) sent on your Email id</span>
                        </div>
                    </div>
                    <div class="send-document-col">
                        <div class="send-document-inner">
                            <img src="images/ekyc-completion/ekyc-pen-icon.png" alt="send-doc-img"/>
                            <span class="send-doc-title">Sign wherever your <br/>signature is required</span>
                        </div>
                    </div>
                    <div class="send-document-col">
                        <div class="send-document-inner">
                            <img src="images/ekyc-completion/ekyc-truck-icon.png" alt="send-doc-img"/>
                            <span class="send-doc-title">Courier the documents to Trade Smart Online</span>
                        </div>
                    </div>
                </div>
            </div>