<!-- Header -->
@include('includes.index-header')

<!-- Content Start -->

<div class="tab-content">
    <div class="tab-pane active ekyc-process-tab">
        <h2 class="form-title">Congratulations!</h2>
        <div class="ekyc-process-comp-sec full ekyc-process-comp-1">
	        <img src="{{ Request::root() }}/images/ekyc-process-comp-icon.png" alt="ekyc-process-comp">
	        <p>You have successfully completed the eKYC process. We will take <span>1-2 working days</span> to verify your documents.</p>
	        <p>We have mailed your eKYC form to:<a href="mailto:aishwarya@gmail.com" class="email-id">aishwarya@gmail.com</a></p>
    	</div>

    	<div class="ekyc-process-comp-sec full ekyc-process-padd">

            @include('payment.ekyc-completion-document-2')


    		<div class="ekyc-address-sec">
    			<p class="ekyc-add-title">Trade Smart Online Office Address</p>
    			<span>A-401, Mangalya, Marol, Near Marigold Residency, Gamdevi,</span>
				<span>Marol, Andheri East, Mumbai, Maharashtra 400059</span>
    		</div>

    		<ul class="contact-list">
    			<li class="phone-lst-sec"><a href="tel:+02261208000">022 61208000</a> <a href="tel:+02242878000">022 42878000</a></li>
    			<li class="email-lst-sec"><a href="mailto:contactus@vnsfin.com">contactus@vnsfin.com</a></li>
    		</ul>

                <div class="refl-code-sec-outer mobi-refl-code-sec">
                    <div class="refl-code-sec full">
                        <div class="refl-code-box">
                            <p class="refl-title">Your Referal Code</p>
                              <div class="refl-code-box-inner">
                                <span class="refl-code-txt" id="refl-code">YDLA140</span>
                              </div>
                            <p>You can now refer us to your friends or relatives and share your unique referral code</p>
                        </div>
                        <ul class="social-list">
                          <li class="fb-lst"><a href=""><img src="{{ Request::root() }}/images/ekyc-completion/facebook-comp-icon.png" alt="fb-icon"/> Share on Facebook</a></li>
                          <li class="g-lst"><a href=""><img src="{{ Request::root() }}/images/ekyc-completion/google-comp-icon.png" alt="gb-icon"/> Share on Google +</a></li>
                        </ul>
                    </div>
                </div>
    	</div>
    </div>
    <div class="clearfix"></div>
</div>
<!-- Content end -->


<!-- Tooltip Content -->
@include('ui-inner.tooltip.ekyc-process-comp')

<!-- includes scripts -->
@include('includes.index-footer')

   <script type="text/javascript" src="{{ Request::root() }}/assets/apps/scripts/payment.js"></script>

    </body>

</html>
