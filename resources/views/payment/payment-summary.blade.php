<!-- Header -->
@include('includes.index-header')

<!-- Content Start -->

<div class="tab-content">
    <div class="tab-pane active">
        <h2 class="form-title">Summary</h2>
            <div class="adhaar-sec">
                <div class="bank-account-sec full">
                    <div class="edit-title-rw full">
                       <span class="edit-title-main">
                        <h6 class="bank-title"><span class="bank-icon"><img src="{{ Request::root() }}/images/add-icon/pan-icon.png" alt="bank-account-img"/></span> Pan Card Details</h6>
                        <a class="mobi-add"><i class="fa fa-plus" aria-hidden="true"></i></a>
                       </span>
                    </div>
                     <div class="bank-account-inner-sec full">
                        <div class="bank-rw">
                            <div class="frm-col">
                                <div class="form-rw full">
                                    <label>Your Name</label>
                                    <input type="text" name="yourname" required class="form-control" value="{{ Auth::user()->name }}" disabled>
                                </div>
                            </div>
                            <div class="frm-col">
                                <div class="form-rw full">
                                    <label>PAN Number</label>
                                    <input type="text" name="pannumber" required class="form-control" value="{{ $userDetails['pan_no'] }}"  disabled>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            <form id="frm-personal-details" method="get">
                <div class="bank-account-sec full">
                    <div class="edit-title-rw full">
                        <span class="edit-title-main">
                            <h6 class="bank-title"><span class="bank-icon"><img src="{{ Request::root() }}/images/add-icon/personal-icon.png" alt="bank-account-img"/></span> Personal Details</h6>
                            <a class="mobi-add"><i class="fa fa-plus" aria-hidden="true"></i></a>
                        </span>
                        <a class="edit-btn" id="personal-details-edit"><img src="{{ Request::root() }}/images/add-icon/edit-pencil.png" alt="edit-pen"/>Edit</a>
                        <a class="save-btn" id="personal-details-save"><img src="{{ Request::root() }}/images/add-icon/edit-pencil.png" alt="edit-pen"/>Save</a>
                     </div>
                    <div class="bank-account-inner-sec full">
                        <div class="bank-rw">
                            <div class="frm-col">
                                <div class="form-rw full">
                                    <label>Father’s Name*</label>
                                    <input type="text" name="yourname" required class="form-control" value="{{ $userDetails['father_name'] }}" id="fathername" readonly>
                                </div>
                            </div>
                            <div class="frm-col">
                                <div class="form-rw full">
                                    <label>Mother’s Name*</label>
                                    <input type="text" name="pannumber" required class="form-control" value="{{ $userDetails['mother_name'] }}" id="mothername" readonly>
                                </div>
                            </div>
                        </div>

                        <div class="bank-rw">
                            <div class="frm-col">
                                <div class="form-rw full">
                                    <label>Date of Birth*</label>
                                    <input type="text" name="yourname" required class="form-control" value="{{ $userDetails['date_of_birth'] }}"  id="birthdate" readonly>
                                </div>
                            </div>
                            <div class="frm-col">
                                <div class="form-rw full">
                                    <label>Gender*</label>
                                    <div class="radio-rw">
                                        <ul class="radio-list add-dt-lst">
                                            <li>
                                                <input type="radio" id="male" name="selector" {{ ($userDetails['gender']=='M')? 'checked' :'' }}  disabled>
                                                <label for="male">Male</label>
                                                <div class="check"></div>
                                            </li>
                                            <li>
                                                <input type="radio" id="female" name="selector" {{ ($userDetails['gender']=='M')?'' : 'checked' }} disabled>
                                                <label for="female">Female</label>
                                                <div class="check">
                                                    <div class="inside"></div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="bank-rw">
                            <div class="frm-col">
                                <div class="form-rw full">
                                    <label>E-mail ID*</label>
                                    <input type="text" name="yourname" required class="form-control" value="{{ $userDetails['email'] }}" id="email" readonly>
                                </div>
                            </div>
                            <div class="frm-col">
                                <div class="form-rw full">
                                    <label>Mobile Number*</label>
                                    <input type="text" name="pannumber" required class="form-control" value="{{ $userDetails['mobile'] }}"  id="mobile" readonly>
                                </div>
                            </div>
                        </div>


                        <div class="form-rw full">
                            <label>Marrital Status*</label>
                                    <div class="radio-rw">
                                        <ul class="radio-list add-dt-lst">
                                            <li>
                                                <input type="radio" id="married" name="selector2" {{ ($userDetails['marrital_status']== 'married')? 'checked' :'' }}  disabled>
                                                <label for="married">Married</label>
                                                <div class="check"></div>
                                            </li>
                                            <li>
                                                <input type="radio" id="unmarried" name="selector2" {{ ($userDetails['marrital_status']== 'married')? '' : 'checked'  }} disabled>
                                                <label for="unmarried">Unmarried</label>
                                                <div class="check">
                                                    <div class="inside"></div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                        </div>
                    </div>
                </div>
            </form>


            <form id="frm-correspondance-address" method="get">
                <div class="bank-account-sec full">
                    <div class="edit-title-rw full">
                        <span class="edit-title-main">
                            <h6 class="bank-title"><span class="bank-icon"><img src="{{ Request::root() }}/images/add-icon/cr-add-icon.png" alt="bank-account-img"/></span> CORRESPONDENCE ADDRESS</h6>
                            <a class="mobi-add"><i class="fa fa-plus" aria-hidden="true"></i></a>
                        </span>
                        <a class="edit-btn"  id="corr-add-edit"><img src="{{ Request::root() }}/images/add-icon/edit-pencil.png" alt="edit-pen"/>Edit</a>
                        <a class="save-btn"  id="corr-add-save"><img src="{{ Request::root() }}/images/add-icon/edit-pencil.png" alt="edit-pen"/>Save</a>
                     </div>
                    <div class="bank-account-inner-sec full">
                    <div class="bank-rw">
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>Address*</label>
                                <input type="text" name="address" required class="form-control" value="{{ $userDetails['address'] }}" id="address" readonly>
                            </div>
                        </div>
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>Address 1*</label>
                                <input type="text" name="address1" required class="form-control" value="{{ $userDetails['address1'] }}"  id="address1" readonly>
                            </div>
                        </div>
                    </div>

                    <div class="bank-rw">
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>Address 2*</label>
                                <input type="text" name="address2" required class="form-control" value="{{ $userDetails['address2'] }}"  id="address2" readonly>
                            </div>
                        </div>
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>City*</label>
                                <select class="form-control change-arrow" id="city" disabled>
                                   <option class="optioncss">Bangalore</option>
                                   <option class="optioncss">Mumbai</option>
                                   <option class="optioncss">Pune</option>
                               </select>
                            </div>
                        </div>
                    </div>

                    <div class="bank-rw">
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>State*</label>
                                <select class="form-control change-arrow" id="state" disabled>
                                   <option class="optioncss">Karnataka</option>
                                   <option class="optioncss">Maharastra</option>
                                   <option class="optioncss">Gujrat</option>
                               </select>
                            </div>
                        </div>
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>PIN Code*</label>
                                <input type="text" name="pin-numbare" required class="form-control" value="{{ $userDetails['pincode'] }}" id="pincode" readonly>
                            </div>
                        </div>
                    </div>

                    <div class="bank-rw">
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>Address Proof*</label>
                                <input type="text" name="add-prof" required class="form-control" value="{{ $userDetails['address_proof'] }}" id="address_proof" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="view-pdf-rw">
                        <p><span class="squaredThree"><input type="checkbox" name="permant-address-checkbox" id="same_address" checked="checked"><label for="same_address"></label></span>My permanent address is the same as my correspondence address</p>
                    </div>
                </div>
                </div>
            </form>

            <form id="frm-permanent-address" method="get">
                <div class="bank-account-sec full permanent-add">
                    <div class="edit-title-rw full">
                        <span class="edit-title-main">
                            <h6 class="bank-title"><span class="bank-icon"><img src="{{ Request::root() }}/images/add-icon/cr-add-icon.png" alt="bank-account-img"/></span> PERMANENT ADDRESS</h6>
                            <a class="mobi-add"><i class="fa fa-plus" aria-hidden="true"></i></a>
                        </span>
                        <a class="edit-btn"  id="permanet-add-edit"><img src="{{ Request::root() }}/images/add-icon/edit-pencil.png" alt="edit-pen"/>Edit</a>
                        <a class="save-btn"  id="permanet-add-save"><img src="{{ Request::root() }}/images/add-icon/edit-pencil.png" alt="edit-pen"/>Save</a>
                     </div>
                <div class="bank-account-inner-sec full">
                    <div class="bank-rw">
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>Address*</label>
                                <input type="text" name="address" required class="form-control" value="{{ $userDetails['permanent_address'] }}" id="permanent_address" readonly>
                            </div>
                        </div>
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>Address 1*</label>
                                <input type="text" name="address1" required class="form-control" value="{{ $userDetails['permanent_address1'] }}" id="permanent_address1" readonly>
                            </div>
                        </div>
                    </div>

                    <div class="bank-rw">
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>Address 2*</label>
                                <input type="text" name="address2" required class="form-control" value="{{ $userDetails['permanent_address2'] }}"  id="permanent_address2" readonly>
                            </div>
                        </div>
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>City*</label>
                                <select class="form-control change-arrow" id="permanent_city" disabled>
                                   <option class="optioncss">Bangalore</option>
                                   <option class="optioncss">Mumbai</option>
                                   <option class="optioncss">Pune</option>
                               </select>
                            </div>
                        </div>
                    </div>

                    <div class="bank-rw">
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>State*</label>
                                <select class="form-control change-arrow" id="permanent_state" disabled>
                                   <option class="optioncss">Karnataka</option>
                                   <option class="optioncss">Maharastra</option>
                                   <option class="optioncss">Gujrat</option>
                               </select>
                            </div>
                        </div>
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>PIN Code*</label>
                                <input type="text" name="pin-numbare" required class="form-control" value="{{ $userDetails['permanent_pincode'] }}" id="permanent_pincode" readonly>
                            </div>
                        </div>
                    </div>

                    <div class="bank-rw">
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>Address Proof*</label>
                                <input type="text" name="add-prof" required class="form-control" value="{{ $userDetails['permanent_address_proof'] }}" id="permanent_address_proof" readonly>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </form>

            <form id="frm-other-details" method="get">
                <div class="bank-account-sec full">
                    <div class="edit-title-rw full">
                        <span class="edit-title-main">
                            <h6 class="bank-title"><span class="bank-icon"><img src="{{ Request::root() }}/images/add-icon/other-icon.png" alt="bank-account-img"/></span> Other Details</h6>
                            <a class="mobi-add"><i class="fa fa-plus" aria-hidden="true"></i></a>
                        </span>
                        <a class="edit-btn" id="other-details-edit"><img src="{{ Request::root() }}/images/add-icon/edit-pencil.png" alt="edit-pen"/>Edit</a>
                        <a class="save-btn"  id="other-details-save"><img src="{{ Request::root() }}/images/add-icon/edit-pencil.png" alt="edit-pen"/>Save</a>
                     </div>
                <div class="bank-account-inner-sec full">
                    <div class="bank-rw">
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>Annual Income*</label>
                                <input type="text" name="annual-income"  class="form-control" value="{{ $userDetails['annual_income'] }}" id="annual-income" readonly>
                            </div>
                        </div>
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>Net Worth*</label>
                                <input type="text" name="net-worth"  class="form-control" value="{{ $userDetails['net_worth'] }}" id="net-worth" readonly>
                            </div>
                        </div>
                    </div>

                    <div class="bank-rw">
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>As On*</label>
                                <input type="text" name="as-date"  class="form-control" value="{{ $userDetails['as_on'] }}"  id="as-on" readonly>
                            </div>
                        </div>
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>Occupation*</label>
                                <input type="text" name="occuption"  class="form-control" value="{{ $userDetails['occupation'] }}" id="occuption" readonly>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </form>

            <form id="frm-bank-deatils" method="get">
                <div class="bank-account-sec full">
                    <div class="edit-title-rw full">
                        <span class="edit-title-main">
                            <h6 class="bank-title"><span class="bank-icon"><img src="{{ Request::root() }}/images/bank-icon.png" alt="bank-account-img"/></span> Bank Account Details</h6>
                            <a class="mobi-add"><i class="fa fa-plus" aria-hidden="true"></i></a>
                        </span>
                        <a class="edit-btn" id="bank-details-edit"><img src="{{ Request::root() }}/images/add-icon/edit-pencil.png" alt="edit-pen"/>Edit</a>
                        <a class="save-btn" id="bank-details-save"><img src="{{ Request::root() }}/images/add-icon/edit-pencil.png" alt="edit-pen"/>Save</a>
                     </div>
                    <div class="bank-account-inner-sec full">
                    <div class="bank-rw">
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>Bank Name*</label>
                                <input type="text" name="bank-name" required class="form-control" value="{{ $bankDetails['bankname'] }}"  id="bank-name" readonly>
                            </div>
                        </div>
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>Branch Name*</label>
                                <input type="text" name="branch-name" required class="form-control" value="{{ $bankDetails['branch'] }}"  id="branch-name" readonly>
                            </div>
                        </div>
                    </div>

                    <div class="bank-rw">
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>IFSC Code*</label>
                                <input type="text" name="ifsc-code" required class="form-control" value="{{ $bankDetails['IFSC'] }}" id="ifsc-code" readonly>
                            </div>
                        </div>
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>Branch Account Number*</label>
                                <input type="text" name="br-ac-no" required class="form-control" value="{{ $bankDetails['acc_no'] }}" id="br-ac-no" readonly>
                            </div>
                        </div>
                    </div>

                    <div class="bank-rw">
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>MICR Number*</label>
                                <input type="text" name="micr-no" required class="form-control" value="{{ $bankDetails['MICR'] }}" id="micr-no" readonly>
                            </div>
                        </div>
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>Account Type*</label>
                                <div class="radio-rw">
                                    <ul class="radio-list add-dt-lst">
                                        <li>
                                            <input type="radio" id="saving" name="selector3" {{ ($bankDetails['acc_type']=='Savings')?'checked':'' }}  disabled>
                                            <label for="saving">Savings</label>
                                            <div class="check"></div>
                                        </li>
                                        <li>
                                            <input type="radio" id="Current" name="selector3" {{ ($bankDetails['acc_type']=='Current')?'checked':'' }} disabled>
                                            <label for="Current">Current</label>
                                            <div class="check"><div class="inside"></div></div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-rw full">
                        <label>Branch Address*</label>
                        <input type="text" name="branch-add" required class="form-control" value="" id="branch-add" readonly>
                    </div>
                </div>
                </div>
            </form>

            <form id="frm-trading-details" method="get">
                <div class="bank-account-sec full">
                    <div class="edit-title-rw full">
                        <span class="edit-title-main">
                            <h6 class="bank-title"><span class="bank-icon"><img src="{{ Request::root() }}/images/add-icon/trade-icon.png" alt="bank-account-img"/></span> Trading Plan Details</h6>
                            <a class="mobi-add"><i class="fa fa-plus" aria-hidden="true"></i></a>
                         </span>
                        <a class="edit-btn" id="trading-details-edit"><img src="{{ Request::root() }}/images/add-icon/edit-pencil.png" alt="edit-pen"/>Edit</a>
                        <a class="save-btn" id="trading-details-save"><img src="{{ Request::root() }}/images/add-icon/edit-pencil.png" alt="edit-pen"/>Save</a>
                     </div>
                <div class="bank-account-inner-sec full">
                     <div class="plan-sec-rw">
                        <div class="plan-col">
                            <div class="plan-col-inner full">
                                <span class="circle-no">1</span>
                                <div class="plan-info">
                                    <h2 class="plan-title">EQUITY</h2>
                                    <p class="plan-status">Exchange</p>
                                    <p class="plan-para">{{ $tradingDetails['equity_seg']?$tradingDetails['equity_seg']:'None'}}</p>
                                </div>
                                <div class="plan-price-sec">
                                    <p class="plan-status">Pricing Plan</p>
                                    <!-- <span class="plan-price">Rs.15</span> -->
                                    <p class="order-para">{{ $tradingDetails['equity_plan']?$tradingDetails['equity_plan']:'None'}}</p>
                                </div>
                            </div>
                        </div>
                        <div class="plan-col">
                            <div class="plan-col-inner full">
                                <span class="circle-no">2</span>
                                <div class="plan-info">
                                    <h2 class="plan-title">COMMODITY</h2>
                                    <p class="plan-status">Exchange</p>
                                    <p class="plan-para">{{$tradingDetails['commodity_seg']?$tradingDetails['commodity_seg']:'None'}}</p>
                                </div>
                                <div class="plan-price-sec">
                                    <p class="plan-status">Pricing Plan</p>
                                    <!-- <span class="plan-price">Rs.15</span> -->
                                    <p class="order-para">{{$tradingDetails['commodity_plan']?$tradingDetails['commodity_plan']:'None'}}</p>
                                </div>
                            </div>
                        </div>
                        <div class="plan-col">
                            <div class="plan-col-inner full">
                                <span class="circle-no">3</span>
                                <div class="plan-info">
                                    <h2 class="plan-title">CURRENCY</h2>
                                    <p class="plan-status">Exchange</p>
                                    <p class="plan-para">{{$tradingDetails['currency_seg']?$tradingDetails['currency_seg']:'None'}}</p>
                                </div>
                                <div class="plan-price-sec">
                                    <p class="plan-status">Pricing Plan</p>
                                    <!-- <span class="plan-price">Rs.15</span> -->
                                    <p class="order-para">{{ $tradingDetails['currency_plan']?$tradingDetails['currency_plan']:'None'}}
</p>
                                </div>
                            </div>
                        </div>
                     </div>


                     <!--trading hide section-->
                     <div class="edit-trading-sec">
                    <div class="segment-div full">
                        <div class="form-group full">
                            <div class="segment-head"><img src="{{ Request::root() }}/images/segment/equity-icon.png"><span>Equity</span></div>
                            <div class="checkbox">
                                <label data-toggle="collapse" data-target="#collapse-equity" aria-expanded="false" aria-controls="collapse-equity">
                                    <span class="segmentchk">
                                      <input type="checkbox" name="segment-equity" id="segment-equity" value="0" {{ $tradingDetails['equity_flag']? 'checked' : '' }}>
                                      <label for="segment-equity"></label></span>
                                </label>
                            </div>
                        </div>
                        <div id="collapse-equity" aria-expanded="false" class="full collapse {{ $tradingDetails['equity_flag']?'in':'' }}">
                            <div class="well segment-txt full">
                                <ul class="full seg-chk">
                                    <li><span class="segmentchk"><input type="checkbox" name="equity" id="nse-cash" value="NSE_CASH" {{ strstr($tradingDetails['equity_seg'], "NSE_CASH")?'checked':'' }}><label for="nse-cash"></label></span>nse cash</li>
                                    <li><span class="segmentchk"><input type="checkbox" name="equity" id="nse-fo" value="NSE_FNO" {{ strstr($tradingDetails['equity_seg'], "NSE_FNO")?'checked':'' }}><label for="nse-fo"></label></span>nse f & o</li>
                                    <li><span class="segmentchk"><input type="checkbox" name="equity" id="bse-cash" value="BSE_CASH" {{ strstr($tradingDetails['equity_seg'], "BSE_CASH")?'checked':'' }}><label for="bse-cash"></label></span>bse cash</li>
                                    <li><span class="segmentchk"><input type="checkbox" name="equity" id="bse-fo" value="BSE_FNO"  {{ strstr($tradingDetails['equity_seg'], "BSE_FNO")?'checked':'' }}><label for="bse-fo"></label></span>bse f & o</li>
                                </ul>
                                <p class="full segment-select">
                                    <div id="select-segment1" class="dropdown select-segment-sec">
                                        <input type="text" id="equity-txt"  value="" class="select-segment-txt dropdown-toggle" data-toggle="dropdown" id="select-segment-txt1" readonly>
                                        <ul class="dropdown-menu select-segment-list" id="select-segment-list1">
                                            <li  ><a href="#" class="select-seg-opt">Only Rs. 15 per executed order</a></li>
                                            <li>
                                                <label class="select-seg-opt">Trade @ 7 Paise</label>
                                                <a href="#">Intraday & Futures @ 0.0007 %, Delivery @ 0.007 % , Other Options @ Rs 7/ Lot</a>
                                            </li>
                                            <li><a href="#"  class="select-seg-opt">Unlimited Trading for Rs 3999 per month</a></li>
                                        </ul>
                                    </div>
                                </p>

                            </div>
                        </div>
                    </div>
                    <div class="segment-div full">
                        <div class="form-group full">
                            <div class="segment-head"><img src="{{ Request::root() }}/images/segment/commodity-icon.png"><span>commodity</span></div>
                            <div class="checkbox">
                                <label data-toggle="collapse" data-target="#collapse-commodity" aria-expanded="false" aria-controls="collapse-commodity">
                                    <span class="segmentchk"><input type="checkbox" name="rember-me" id="segment-commodity" value="0" {{ $tradingDetails['commodity_flag']?'checked':'' }}><label for="segment-commodity"></label></span>
                                </label>
                            </div>
                        </div>
                        <div id="collapse-commodity" aria-expanded="false" class="full collapse {{ $tradingDetails['commodity_flag']?'in':'' }}">
                            <div class="well segment-txt full">
                                <ul class="full seg-chk">
                                    <li><span class="segmentchk"><input type="checkbox" name="commodity" id="nse" value="NSE" {{ strstr($tradingDetails['commodity_seg'], "NSE")?'checked':'' }}><label for="nse"></label></span>nse</li>
                                    <li><span class="segmentchk"><input type="checkbox" name="commodity" id="ncdex" value="NCDEX" {{ strstr($tradingDetails['commodity_seg'], "NCDEX")?'checked':'' }}><label for="nse-fo"></label></span>ncdex</li>
                                </ul>
                                <p class="full segment-select">
                                    <div id="select-segment2" class="dropdown select-segment-sec">
                                        <input type="text" value=""  id="select-segment-btn" class="select-segment-txt dropdown-toggle" data-toggle="dropdown" readonly>
                                        <ul class="dropdown-menu select-segment-list" id="select-segment-list2">
                                            <li><a href="#" class="select-seg-opt">Only Rs. 15 per executed order</a></li>
                                            <li>
                                                <label class="select-seg-opt">Trade @ 7 Paise</label>
                                                <a href="#">Intraday &amp; Futures @ 0.0007 %, Delivery @ 0.007 % , Other Options @ Rs 7/ Lot</a>
                                            </li>
                                            <li><a href="#"  class="select-seg-opt">Unlimited Trading for Rs 3999 per month</a></li>
                                        </ul>
                                    </div>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="segment-div full">
                        <div class="form-group full">
                            <div class="segment-head"><img src="{{ Request::root() }}/images/segment/currency-icon.png"><span>currency</span></div>
                            <div class="checkbox">
                                <label data-toggle="collapse" data-target="#collapse-currency" aria-expanded="false" aria-controls="collapse-currency">
                                    <span class="segmentchk"><input type="checkbox" name="rember-me" id="segment-currency" value="0" {{ $tradingDetails['currency_flag']?'checked':'' }} ><label for="segment-currency"></label></span>
                                </label>
                            </div>
                        </div>
                        <div id="collapse-currency" aria-expanded="false" class="full collapse {{ $tradingDetails['currency_flag']?'in':'' }} ">
                            <div class="well segment-txt">
                                <ul class="full seg-chk">
                                    <li><span class="segmentchk"><input type="checkbox" name="currency" id="mcx" value="MCX" {{ strstr($tradingDetails['currency_seg'], "MCX")?'checked':'' }} ><label for="mcx"></label></span>nse</li>
                                    <li><span class="segmentchk"><input type="checkbox" name="currency" id="ncdex-currency" value="NCDEX" {{ strstr($tradingDetails['currency_seg'], "NCDEX")?'checked':'' }}><label for="ncdex-currency"></label></span>ncdex</li>
                                </ul>
                                <p class="full segment-select">
                                    <div id="select-segment3" class="dropdown select-segment-sec">
                                        <input type="text" value=""  id="select-segment-btn" class="select-segment-txt dropdown-toggle" data-toggle="dropdown" readonly>
                                        <ul class="dropdown-menu select-segment-list"  id="select-segment-list3">
                                            <li><a href="#" class="select-seg-opt">Only Rs. 15 per executed order</a></li>
                                            <li>
                                                <label class="select-seg-opt">Trade @ 7 Paise</label>
                                                <a href="#">Intraday &amp; Futures @ 0.0007 %, Delivery @ 0.007 % , Other Options @ Rs 7/ Lot</a>
                                            </li>
                                            <li><a href="#"  class="select-seg-opt">Unlimited Trading for Rs 3999 per month</a></li>
                                        </ul>
                                    </div>
                                </p>
                            </div>
                        </div>
                    </div>
                     </div>

                    <div class="bank-rw">
                        <p class="frm-subtitle">Mutual Fund Account Details</p>
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>Nominee 1*</label>
                                <input type="text" name="nominee-1" required class="form-control" value="" id="nominee-1" readonly>
                            </div>
                        </div>
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>Nominee 2*</label>
                                <input type="text" name="nominee-2" required class="form-control" value="" id="nominee-2" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="form-rw full">
                        <p class="frm-subtitle">Do you want Delivery Instruction Slip?</p>
                                <div class="radio-rw">
                                    <ul class="radio-list add-dt-lst">
                                        <li>
                                            <input type="radio" id="yes" name="selector4" {{ $tradingDetails['currency_flag']?'in':'' }} disabled>
                                            <label for="yes">Yes</label>
                                            <div class="check"></div>
                                        </li>
                                        <li>
                                            <input type="radio" id="no" name="selector4" disabled>
                                            <label for="no">No</label>
                                            <div class="check"><div class="inside"></div></div>
                                        </li>
                                    </ul>
                                </div>
                          <p class="note-para"><span>NOTE:</span> You may always switch plans later on as a customer with just an email to our support team.</p>
                    </div>
                </div>
                </div>
            </form>

            <form id="frm-fatca-details" method="get">
                  <div class="bank-account-sec full">
                    <div class="edit-title-rw full">
                        <span class="edit-title-main">
                            <h6 class="bank-title"><span class="bank-icon"><img src="{{ Request::root() }}/images/add-icon/trade-icon.png" alt="bank-account-img"/></span> FATCA details</h6>
                            <a class="mobi-add"><i class="fa fa-plus" aria-hidden="true"></i></a>
                         </span>
                        <a class="edit-btn"  id="fatca-details-edit"><img src="{{ Request::root() }}/images/add-icon/edit-pencil.png" alt="edit-pen"/>Edit</a>
                        <a class="save-btn" id="fatca-details-save"><img src="{{ Request::root() }}/images/add-icon/edit-pencil.png" alt="edit-pen"/>Save</a>
                     </div>
                    <div class="bank-account-inner-sec full">
                    <div class="form-rw full">
                        <p class="frm-subtitle">Is your country of tax residency other than India?</p>
                                <div class="radio-rw">
                                    <ul class="radio-list add-dt-lst" id="fatca-flag">
                                        <li>
                                            <input type="radio" id="fatca-yes" name="selector5"  disabled>
                                            <label for="fatca-yes">Yes</label>
                                            <div class="check"></div>
                                        </li>
                                        <li>
                                            <input type="radio" id="fatca-no" name="selector5" checked disabled>
                                            <label for="fatca-no">No</label>
                                            <div class="check"><div class="inside"></div></div>
                                        </li>
                                    </ul>
                                </div>
                    </div>
                    <div class="form-rw full country-add" id="countryform">
                        <div class="form-group">
                            <div class="table-responsive ">
                                <table class="table tax_table">
                                  <thead>
                                    <tr>
                                      <th>Country of tax residency</th>
                                      <th>Tax payer identification no.</th>
                                      <th>Identification type</th>
                                     </tr>
                                  </thead>
                                  <tbody>
                                    <tr>
                                      <td><span class="numberdisplay align1">1</span><input type="text" class="country_text" /></td>
                                      <td><input type="text" class="country_text" /></td>
                                      <td><input type="text" class="country_text" /></td>
                                    </tr>
                                    <tr>
                                      <td><span class="numberdisplay align2">2</span><input type="text" class="country_text" /></td>
                                      <td><input type="text" class="country_text" /></td>
                                      <td><input type="text" class="country_text" /></td>
                                    </tr>
                                    <tr>
                                      <td><span class="numberdisplay align3">3</span><input type="text" class="country_text" /></td>
                                      <td><input type="text" class="country_text" /></td>
                                      <td><input type="text" class="country_text" /></td>
                                    </tr>
                                 </tbody>

                                </table>
                    </div>
                </div>
                    </div>
                    </div>
                </div>
            </form>

            <form id="frm-demat-details" method="get">
                <!--Demat Details-->
                  <div class="bank-account-sec full">
                    <div class="edit-title-rw full">
                        <span class="edit-title-main">
                            <h6 class="bank-title"><span class="bank-icon"><img src="{{ Request::root() }}/images/add-icon/trade-icon.png" alt="bank-account-img"/></span> Demat Details</h6>
                            <a class="mobi-add"><i class="fa fa-plus" aria-hidden="true"></i></a>
                        </span>
                        <a class="edit-btn" id="demat-details-edit"><img src="{{ Request::root() }}/images/add-icon/edit-pencil.png" alt="edit-pen"/>Edit</a>
                        <a class="save-btn" id="demat-details-save"><img src="{{ Request::root() }}/images/add-icon/edit-pencil.png" alt="edit-pen"/>Save</a>
                     </div>
                    <div class="bank-account-inner-sec full">
                    <div class="form-rw full">

                                <div class="radio-rw">
                                    <ul class="radio-list add-dt-lst radio-full-lst negavtivemargin">
                                        <li>
                                            <input type="radio" id="vns-demat" name="selector6" checked disabled>
                                            <label for="vns-demat">I wish to open a demat A/c with VNS</label>
                                            <div class="check"></div>
                                        </li>
                                        <li>
                                            <input type="radio" id="add-demat" name="selector6"  disabled>
                                            <label for="add-demat">Add Existing Demat A/c</label>
                                            <div class="check"><div class="inside"></div></div>
                                        </li>
                                    </ul>
                                </div>
                    </div>

                    <!-open on existing demat--->
                    <div class="exist-demat-ac">
                        <div class="form-rw full">
                            <label>Demat Account Name*</label>
                            <input type="text" name="demat-ac" required class="form-control" value="" id="demat-ac" readonly>
                        </div>
                        <div class="bank-rw">
                            <div class="frm-col">
                                <div class="form-rw full">
                                    <label>DP ID*</label>
                                    <input type="text" name="dp-id" required class="form-control" value=""  id="dp-id" readonly>
                                </div>
                            </div>
                            <div class="frm-col">
                                <div class="form-rw full">
                                    <label>BO ID*</label>
                                    <input type="text" name="bo-id" required class="form-control" value="" id="bo-id" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </form>

            <form id="frm-enquiry-details" method="get">
                <!--Enquiry Details-->
                <div class="bank-account-sec full">
                   <div class="bank-account-inner-sec full hw-hear-about-sec">
                       <div class="form-rw full hear-rw">
                           <p class="frm-subtitle">How did you hear about us?</p>
                           <select class="form-control change-arrow" id="social-icon">
                               <option class="optioncss">Friends/Family</option>
                               <option class="optioncss">TV</option>
                               <option class="optioncss">Online</option>
                               <option class="optioncss">Social Media</option>
                               <option class="optioncss">Other</option>
                           </select>
                       </div>
                    </div>
               </div>
            </form>
                <div class="view-pdf-rw">
                    <p><span class="squaredThree"><input type="checkbox" name="rember-me" id="squaredThree1"><label for="squaredThree1"></label></span>I am ready to digitally sign my account opening form. <a class="view-btn-pdf">View pdf form</a></p>
                </div>
                <div class="btn-rw full">
                    <button type="submit" value="submit" class="btn blue-btn">Proceed</button>

                </div>
            </div>

    </div>
    <div class="clearfix"></div>
</div>
<!-- Content end -->


<!-- Tooltip Content -->
@include('ui-inner.tooltip.payment-flow')

<!-- includes scripts -->
@include('includes.index-footer')

   <script type="text/javascript" src="{{ Request::root() }}/assets/apps/scripts/payment.js"></script>

    </body>

</html>
