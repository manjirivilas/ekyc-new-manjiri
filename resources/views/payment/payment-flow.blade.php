<!-- Header -->
@include('includes.mainhead')

<!-- Content Start -->

<div class="tab-content">
    <div class="tab-pane active">
        <h2 class="form-title">Additional Details</h2>
        <form id="frm-additional-details" method="get">
            <div class="adhaar-sec">
                <div class="bank-account-sec full">
                    <h6 class="bank-title"><span class="bank-icon"><img src="images/add-icon/pan-icon.png" alt="bank-account-img"/></span> Pan Card Details</h6>
                    <div class="bank-rw">
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>Your Name</label>
                                <input type="textbox" name="yourname" required class="form-control" value="Amit Jain" disabled>
                            </div>
                        </div>
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>PAN Number</label>
                                <input type="textbox" name="pannumber" required class="form-control" value="BHJF58374"  disabled>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="bank-account-sec full">
                    <div class="edit-title-rw full">
                        <h6 class="bank-title"><span class="bank-icon"><img src="images/add-icon/personal-icon.png" alt="bank-account-img"/></span> Personal Details</h6>
                        <a href="" class="edit-btn"><img src="images/add-icon/edit-pencil.png" alt="edit-pen"/>Edit</a>
                     </div>
                    <div class="bank-rw">
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>Father’s Name*</label>
                                <input type="textbox" name="yourname" required class="form-control" value="Suresh Jain">
                            </div>
                        </div>
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>Mother’s Name*</label>
                                <input type="textbox" name="pannumber" required class="form-control" value="Veena Jain">
                            </div>
                        </div>
                    </div>

                    <div class="bank-rw">
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>Date of Birth*</label>
                                <input type="textbox" name="yourname" required class="form-control" value="22.01.1988">
                            </div>
                        </div>
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>Gender*</label>
                                <div class="radio-rw">
                                    <ul class="radio-list add-dt-lst">
                                        <li>
                                            <input type="radio" id="male" name="selector" checked>
                                            <label for="male">Male</label>
                                            <div class="check"></div>
                                        </li>
                                        <li>
                                            <input type="radio" id="female" name="selector">
                                            <label for="female">Female</label>
                                            <div class="check">
                                                <div class="inside"></div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="bank-rw">
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>E-mail ID*</label>
                                <input type="textbox" name="yourname" required class="form-control" value="amit123@gmail.com">
                            </div>
                        </div>
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>Mobile Number*</label>
                                <input type="textbox" name="pannumber" required class="form-control" value="9885467465">
                            </div>
                        </div>
                    </div>


                    <div class="form-rw full">
                        <label>Marrital Status*</label>
                                <div class="radio-rw">
                                    <ul class="radio-list add-dt-lst">
                                        <li>
                                            <input type="radio" id="married" name="selector2" checked>
                                            <label for="married">Married</label>
                                            <div class="check"></div>
                                        </li>
                                        <li>
                                            <input type="radio" id="unmarried" name="selector2">
                                            <label for="unmarried">Unmarried</label>
                                            <div class="check">
                                                <div class="inside"></div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                    </div>
                </div>

                <div class="bank-account-sec full">
                    <div class="edit-title-rw full">
                        <h6 class="bank-title"><span class="bank-icon"><img src="images/add-icon/cr-add-icon.png" alt="bank-account-img"/></span> CORRESPONDENCE ADDRESS</h6>
                        <a href="" class="edit-btn"><img src="images/add-icon/edit-pencil.png" alt="edit-pen"/>Edit</a>
                     </div>
                    <div class="bank-rw">
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>Address*</label>
                                <input type="textbox" name="address" required class="form-control" value="#132, Mantri Sarovar">
                            </div>
                        </div>
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>Address 1*</label>
                                <input type="textbox" name="address1" required class="form-control" value="27th Main">
                            </div>
                        </div>
                    </div>

                    <div class="bank-rw">
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>Address 2*</label>
                                <input type="textbox" name="address2" required class="form-control" value="22.01.1988">
                            </div>
                        </div>
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>City*</label>
                                <select class="form-control change-arrow" id="city">
                                   <option class="optioncss">Bangalore</option>
                                   <option class="optioncss">Mumbai</option>
                                   <option class="optioncss">Pune</option>
                               </select>
                            </div>
                        </div>
                    </div>

                    <div class="bank-rw">
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>State*</label>
                                <select class="form-control change-arrow" id="year">
                                   <option class="optioncss">Karnataka</option>
                                   <option class="optioncss">Maharastra</option>
                                   <option class="optioncss">Gujrat</option>
                               </select>
                            </div>
                        </div>
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>PIN Code*</label>
                                <input type="textbox" name="pin-numbare" required class="form-control" value="560102">
                            </div>
                        </div>
                    </div>

                    <div class="bank-rw">
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>Address Proof*</label>
                                <input type="textbox" name="add-prof" required class="form-control" value="Aadhar Card">
                            </div>
                        </div>
                    </div>
                    <div class="view-pdf-rw">
                        <p><span class="squaredThree"><input type="checkbox" name="rember-me" id="squaredThree2"><label for="squaredThree2"></label></span>My permanent address is the same as my correspondence address</p>
                    </div>
                </div>


                <div class="bank-account-sec full">
                    <div class="edit-title-rw full">
                        <h6 class="bank-title"><span class="bank-icon"><img src="images/add-icon/cr-add-icon.png" alt="bank-account-img"/></span> CORRESPONDENCE ADDRESS</h6>
                        <a href="" class="edit-btn"><img src="images/add-icon/edit-pencil.png" alt="edit-pen"/>Edit</a>
                     </div>
                    <div class="bank-rw">
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>Address*</label>
                                <input type="textbox" name="address" required class="form-control" value="#132, Mantri Sarovar">
                            </div>
                        </div>
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>Address 1*</label>
                                <input type="textbox" name="address1" required class="form-control" value="27th Main">
                            </div>
                        </div>
                    </div>

                    <div class="bank-rw">
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>Address 2*</label>
                                <input type="textbox" name="address2" required class="form-control" value="22.01.1988">
                            </div>
                        </div>
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>City*</label>
                                <select class="form-control change-arrow" id="city">
                                   <option class="optioncss">Bangalore</option>
                                   <option class="optioncss">Mumbai</option>
                                   <option class="optioncss">Pune</option>
                               </select>
                            </div>
                        </div>
                    </div>

                    <div class="bank-rw">
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>State*</label>
                                <select class="form-control change-arrow" id="year">
                                   <option class="optioncss">Karnataka</option>
                                   <option class="optioncss">Maharastra</option>
                                   <option class="optioncss">Gujrat</option>
                               </select>
                            </div>
                        </div>
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>PIN Code*</label>
                                <input type="textbox" name="pin-numbare" required class="form-control" value="560102">
                            </div>
                        </div>
                    </div>

                    <div class="bank-rw">
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>Address Proof*</label>
                                <input type="textbox" name="add-prof" required class="form-control" value="Aadhar Card">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="bank-account-sec full">
                    <div class="edit-title-rw full">
                        <h6 class="bank-title"><span class="bank-icon"><img src="images/add-icon/other-icon.png" alt="bank-account-img"/></span> Other Details</h6>
                        <a href="" class="edit-btn"><img src="images/add-icon/edit-pencil.png" alt="edit-pen"/>Edit</a>
                     </div>
                    <div class="bank-rw">
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>Annual Income*</label>
                                <input type="textbox" name="annual-income" required class="form-control" value="1-5 Lac">
                            </div>
                        </div>
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>Net Worth*</label>
                                <input type="textbox" name="net-worth" required class="form-control" value="0">
                            </div>
                        </div>
                    </div>

                    <div class="bank-rw">
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>As On*</label>
                                <input type="textbox" name="as-date" required class="form-control" value="01.05.2017">
                            </div>
                        </div>
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>Occupation*</label>
                                <input type="textbox" name="occuption" required class="form-control" value="Private Sector Service">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="bank-account-sec full">
                    <div class="edit-title-rw full">
                        <h6 class="bank-title"><span class="bank-icon"><img src="images/bank-icon.png" alt="bank-account-img"/></span> Bank Account Details</h6>
                        <a href="" class="edit-btn"><img src="images/add-icon/edit-pencil.png" alt="edit-pen"/>Edit</a>
                     </div>
                    <div class="bank-rw">
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>Bank Name*</label>
                                <input type="textbox" name="bank-name" required class="form-control" value="Axis Bank">
                            </div>
                        </div>
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>Branch Name*</label>
                                <input type="textbox" name="net-worth" required class="form-control" value="HSR Layout">
                            </div>
                        </div>
                    </div>

                    <div class="bank-rw">
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>IFSC Code*</label>
                                <input type="textbox" name="ifsc-code" required class="form-control" value="UTIB000057364">
                            </div>
                        </div>
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>Branch Account Number*</label>
                                <input type="textbox" name="br-ac-no" required class="form-control" value="90385737598545">
                            </div>
                        </div>
                    </div>

                    <div class="bank-rw">
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>MICR Number*</label>
                                <input type="textbox" name="micr-no" required class="form-control" value="695747586">
                            </div>
                        </div>
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>Account Type*</label>
                                <div class="radio-rw">
                                    <ul class="radio-list add-dt-lst">
                                        <li>
                                            <input type="radio" id="saving" name="selector3" checked>
                                            <label for="saving">Savings</label>
                                            <div class="check"></div>
                                        </li>
                                        <li>
                                            <input type="radio" id="Current" name="selector3">
                                            <label for="Current">Current</label>
                                            <div class="check"><div class="inside"></div></div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-rw full">
                        <label>Branch Address*</label>
                        <input type="textbox" name="branch-add" required class="form-control" value="HN Towers, Sector 2, HSR Layout, Bengaluru, Karnataka, 560 002">
                    </div>
                </div>

                <div class="bank-account-sec full">
                    <div class="edit-title-rw full">
                        <h6 class="bank-title"><span class="bank-icon"><img src="images/add-icon/trade-icon.png" alt="bank-account-img"/></span> Trading Plan Details</h6>
                        <a href="" class="edit-btn"><img src="images/add-icon/edit-pencil.png" alt="edit-pen"/>Edit</a>
                     </div>
                     <div class="plan-sec-rw">
                        <div class="plan-col">
                            <div class="plan-col-inner full">
                                <span class="circle-no">1</span>
                                <div class="plan-info">
                                    <h2 class="plan-title">EQUITY</h2>
                                    <p class="plan-status">Exchange</p>
                                    <p class="plan-para">NSE Cash, BSE Cash</p>
                                    <p class="plan-para">NSE F&amp;O, BSE F&amp;O</p>
                                </div>
                                <div class="plan-price-sec">
                                    <p class="plan-status">Pricing Plan</p>
                                    <span class="plan-price">Rs.15</span>
                                    <p class="order-para">Per Executed Order</p>
                                </div>
                            </div>
                        </div>
                        <div class="plan-col">
                            <div class="plan-col-inner full">
                                <span class="circle-no">2</span>
                                <div class="plan-info">
                                    <h2 class="plan-title">COMMODITY</h2>
                                    <p class="plan-status">Exchange</p>
                                    <p class="plan-para">MCX</p>
                                    <p class="plan-para">NCDEX</p>
                                </div>
                                <div class="plan-price-sec">
                                    <p class="plan-status">Pricing Plan</p>
                                    <span class="plan-price">Rs.15</span>
                                    <p class="order-para">Per Executed Order</p>
                                </div>
                            </div>
                        </div>
                        <div class="plan-col">
                            <div class="plan-col-inner full">
                                <span class="circle-no">3</span>
                                <div class="plan-info">
                                    <h2 class="plan-title">CURRENCY</h2>
                                    <p class="plan-status">Exchange</p>
                                    <p class="plan-para">NSE</p>
                                    <p class="plan-para">NCDEX</p>
                                </div>
                                <div class="plan-price-sec">
                                    <p class="plan-status">Pricing Plan</p>
                                    <span class="plan-price">Rs.15</span>
                                    <p class="order-para">Per Executed Order</p>
                                </div>
                            </div>
                        </div>
                     </div>
                    <div class="bank-rw">
                        <p class="frm-subtitle">Mutual Fund Account Details</p>
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>Nominee 1*</label>
                                <input type="textbox" name="nominee-1" required class="form-control" value="Nominee Name">
                            </div>
                        </div>
                        <div class="frm-col">
                            <div class="form-rw full">
                                <label>Nominee 2*</label>
                                <input type="textbox" name="nominee-2" required class="form-control" value="Nominee Name">
                            </div>
                        </div>
                    </div>
                    <div class="form-rw full">
                        <p class="frm-subtitle">Do you want Delivery Instruction Slip?</p>
                                <div class="radio-rw">
                                    <ul class="radio-list add-dt-lst">
                                        <li>
                                            <input type="radio" id="yes" name="selector4" checked>
                                            <label for="yes">Yes</label>
                                            <div class="check"></div>
                                        </li>
                                        <li>
                                            <input type="radio" id="no" name="selector4">
                                            <label for="no">No</label>
                                            <div class="check"><div class="inside"></div></div>
                                        </li>
                                    </ul>
                                </div>
                          <p class="note-para"><span>NOTE:</span> You may always switch plans later on as a customer with just an email to our support team.</p>
                    </div>

                </div>

                  <div class="bank-account-sec full">
                    <div class="edit-title-rw full">
                        <h6 class="bank-title"><span class="bank-icon"><img src="images/add-icon/trade-icon.png" alt="bank-account-img"/></span> FATCA details</h6>
                        <a href="" class="edit-btn"><img src="images/add-icon/edit-pencil.png" alt="edit-pen"/>Edit</a>
                     </div>
                    <div class="form-rw full">
                        <p class="frm-subtitle">Is your country of tax residency other than India?</p>
                                <div class="radio-rw">
                                    <ul class="radio-list add-dt-lst">
                                        <li>
                                            <input type="radio" id="yes1" name="selector5" checked>
                                            <label for="yes1">Yes</label>
                                            <div class="check"></div>
                                        </li>
                                        <li>
                                            <input type="radio" id="no1" name="selector5">
                                            <label for="no1">No</label>
                                            <div class="check"><div class="inside"></div></div>
                                        </li>
                                    </ul>
                                </div>
                    </div>

                </div>


                <!--Demat Details-->
                  <div class="bank-account-sec full">
                    <div class="edit-title-rw full">
                        <h6 class="bank-title"><span class="bank-icon"><img src="images/add-icon/trade-icon.png" alt="bank-account-img"/></span> Demat Details</h6>
                        <a href="" class="edit-btn"><img src="images/add-icon/edit-pencil.png" alt="edit-pen"/>Edit</a>
                     </div>
                    <div class="form-rw full">
                        <p class="frm-subtitle">Is your country of tax residency other than India?</p>
                                <div class="radio-rw">
                                    <ul class="radio-list add-dt-lst radio-full-lst">
                                        <li>
                                            <input type="radio" id="vns-demat" name="selector6" checked>
                                            <label for="vns-demat">I wish to open a demat A/c with VNS</label>
                                            <div class="check"></div>
                                        </li>
                                        <li>
                                            <input type="radio" id="add-demat" name="selector6">
                                            <label for="add-demat">Add Existing Demat A/c</label>
                                            <div class="check"><div class="inside"></div></div>
                                        </li>
                                    </ul>
                                </div>
                    </div>

                    <!-open on existing demat--->
                    <div class="exist-demat-ac">
                        <div class="form-rw full">
                            <label>Demat Account Name*</label>
                            <input type="textbox" name="branch-add" required class="form-control" value="">
                        </div>
                        <div class="bank-rw">
                            <div class="frm-col">
                                <div class="form-rw full">
                                    <label>DP ID*</label>
                                    <input type="textbox" name="bank-name" required class="form-control" value="">
                                </div>
                            </div>
                            <div class="frm-col">
                                <div class="form-rw full">
                                    <label>BO ID*</label>
                                    <input type="textbox" name="net-worth" required class="form-control" value="">
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="view-pdf-rw">
                    <p><span class="squaredThree"><input type="checkbox" name="rember-me" id="squaredThree1"><label for="squaredThree1"></label></span>I am ready to digitally sign my account opening form. <a href="" class="view-btn-pdf">View pdf form</a></p>
                </div>
                <div class="btn-rw full">
                    <button type="submit" value="submit" class="btn blue-btn">Proceed</button>

                </div>
            </div>
        </form>
    </div>
    <div class="clearfix"></div>
</div>
<!-- Content end -->


<!-- Tooltip Content -->
@include('ui-inner.tooltip.payment-flow')

<!-- includes scripts -->
@include('includes.innerfooter')