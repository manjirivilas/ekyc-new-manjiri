<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['middleware' => 'guest'], function ()
{
      //ekyc-signup
      Route::get('/', array('as' => 'sign-up', 'uses' => 'LoginController@getDetails'));

      Route::post('/', array('as' => 'confirm-details', 'uses' => 'LoginController@postClientData'));


      Route::get('/set-password', array('as' => 'set-password', 'uses' => 'LoginController@viewSetPassword'));

      Route::post('/set-password', array('as' => 'set-password', 'uses' =>   'LoginController@createUser'));

      //ekyc-login
      Route::get('/login', array('as' => 'login', 'uses' =>   'LoginController@viewLogin'));
      Route::post('/login', array('as' => 'login', 'uses' =>   'LoginController@verifyLogin'));

      //ekyc- set password
      Route::get('/forgot-password', array('as' => 'forgot-password', 'uses' =>   'LoginController@getEmail'));
      Route::post('/forgot-password', array('as' => 'forgot-password', 'uses' =>   'LoginController@confirmEmail'));

      // reset password
      Route::get('/user-reset-password', array('as' => 'user-reset-password', 'uses' =>   'LoginController@getPassword'));
      Route::get('/reset-password', array('as' => 'reset-password', 'uses' =>   'LoginController@validatePassword'));
      Route::post('/reset-password', array('as' => 'reset-password', 'uses' =>   'LoginController@resetPassword'));

      //forgot passwprd
      Route::get('/email-sent', array('as' => 'forgot-password', 'uses' =>   'LoginController@viewEmailSent'));
      Route::post('/email-sent', array('as' => 'email-sent', 'uses' =>   'LoginController@sendResetPasswordLink'));

      Route::get('/password-successful', array('as' => 'password-successful', 'uses' =>   'LoginController@viewPasswordSuccessful'));

      //404 page-not-found
      Route::get('/page-not-found', array('as' => 'page-not-found', 'uses' => 'ErrorController@viewPageNotFound'));


});

Route::group(['middleware' => 'auth'], function ()
{
      Route::get('/change-password', array('as' => 'change-password', 'uses' =>   'LoginController@viewChangePassword'));
      Route::post('/change-password', array('as' => 'change-password', 'uses' =>   'LoginController@getChangePassword'));
      Route::get('/dashboard', array('as' => 'dashboard', 'uses' =>   'LoginController@viewDashboard'));
      Route::get('/logout', array('as' => 'logout', 'uses' =>   'LoginController@logoutUser'));

      //IPV
      Route::post('/save-ipv', array('as' => 'save-ipv', 'uses' => 'PanDetailsController@saveIPV'));
      //fetch details by Ifsc
      Route::post('/get-bank-details', array('as' => 'get-bank-details', 'uses' =>   'BankDetailsController@getBankDetails'));

      //404 page-not-found
      Route::get('/page-not-found', array('as' => 'page-not-found', 'uses' => 'ErrorController@viewPageNotFound'));
      Route::get('/session', array('as' => 'error', 'uses' => 'SessionController@viewSession'));

      //Module start
      Route::group(['prefix' => 'identity'], function ()
      {
        //aadhar
        Route::get('/aadhar-details', array('as' => 'aadhar-details', 'uses' =>   'AadharDetailsController@viewAadharDetails'));
        Route::post('/aadhar-details', array('as' => 'aadhar-details', 'uses' =>   'AadharDetailsController@getAadharDetails'));
        Route::get('/aadhar-otp', array('as' => 'aadhar-otp', 'uses' =>   'AadharDetailsController@viewAadharOtp'));
        Route::post('/aadhar-otp', array('as' => 'aadhar-otp', 'uses' =>  'AadharDetailsController@getAadharOtp'));

        //pan
        Route::get('/pan-details', array('as' => 'pan-details', 'uses' =>   'PanDetailsController@viewPanDetails'));
        Route::post('/pan-details', array('as' => 'pan-details', 'uses' =>   'PanDetailsController@getPanDetails'));
        Route::get('/pan-ipv', array('as' => 'pan-ipv', 'uses' =>   'PanDetailsController@viewPanIPV'));


        //personal details
        Route::get('/personal-details', array('as' => 'personal-details', 'uses' =>   'AdditionalDetailsController@viewPersonalDetails'));
        Route::post('/personal-details', array('as' => 'personal-details', 'uses' =>   'AdditionalDetailsController@getPersonalDetails'));
        Route::get('/additional-details', array('as' => 'additional-details', 'uses' =>   'AdditionalDetailsController@viewTradingDetails'));
        Route::post('/additional-details', array('as' => 'additional-details', 'uses' =>   'AdditionalDetailsController@getTradingDetails'));
      });

      Route::group(['prefix' => 'plan-selection'], function ()
      {
        Route::get('/select-plan', array('as' => 'select-plan', 'uses' =>   'SelectPlanController@viewSelectPlan'));
        Route::post('/select-plan', array('as' => 'select-plan', 'uses' =>   'SelectPlanController@getSelectPlan'));

        Route::get('/select-segment', array('as' => 'select-segment', 'uses' =>   'SelectPlanController@viewSelectSegment'));
        Route::post('/select-segment', array('as' => 'select-segment', 'uses' =>   'SelectPlanController@getSegment'));

        Route::get('/select-product', array('as' => 'select-product', 'uses' =>   'SelectPlanController@viewSelectProduct'));
        Route::post('/select-product', array('as' => 'select-product', 'uses' =>   'SelectPlanController@getProduct'));

        Route::get('/nominee-details', array('as' => 'nominee-details', 'uses' =>   'SelectPlanController@viewNomineeDetails'));
        Route::post('/nominee-details', array('as' => 'nominee-details', 'uses' =>   'SelectPlanController@getNomineeDetails'));

        Route::get('/plan-summary', array('as' => 'plan-summary', 'uses' =>   'SelectPlanController@viewPlanSummary'));
        Route::post('/plan-summary', array('as' => 'plan-summary', 'uses' =>   'SelectPlanController@getPlanSummary'));

      });

      Route::group(['prefix' => 'bank-details'], function ()
      {
        //bank-details
        Route::get('/bank-details', array('as' => 'bank-details', 'uses' =>   'BankDetailsController@viewBankDetails'));
        Route::post('/bank-details', array('as' => 'bank-details', 'uses' =>   'BankDetailsController@updateBankDetails'));
      });

      Route::group(['prefix' => 'payment'], function ()
      {
        Route::get('/payment-summary', array('as' => 'payment-summary', 'uses' =>   'PaymentSummaryController@viewPaymentSummary'));
        Route::get('/payment-details', array('as' => 'payment-details', 'uses' =>   'PaymentDetailsController@viewPaymentDetails'));

        Route::get('/otp-esign', array('as' => 'otp-esign', 'uses' =>   'OtpEsignController@viewOtpEsign'));
        Route::get('/payment-successful', array('as' => 'payment-successful', 'uses' =>   'PaymentSuccessfulController@viewPaymentSuccessful'));
      });

      Route::group(['prefix' => 'upload-document'], function ()
      {
        Route::get('/pan-ipv', array('as' => 'pan-ipv', 'uses' =>   'PanDetailsController@viewPanIPV'));
        //manji ipv
        Route::get('/manji-ipv', array('as' => 'manji-ipv', 'uses' =>   'PanDetailsController@viewManjiPanIPV'));
          Route::get('/upload-documents', array('as' => 'upload-documents', 'uses' =>   'UploadDocumentsController@viewUploadDocuments'));
      });

      Route::get('/ekyc-process-completion', array('as' => 'ekyc-process', 'uses' =>   'EkycProcessCompletionController@viewEkycProcess'));
});
