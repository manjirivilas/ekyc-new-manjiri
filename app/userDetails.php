<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserDetails extends Model
{
  protected $table = 'user_details';
  protected $connection = 'mysql';

  protected $fillable = [ 'user_id'];

  public function user()
  {
      return $this->belongsTo('App\User');
  }
}
