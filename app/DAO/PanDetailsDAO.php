<?php
namespace App\DAO;
use Input;
use View;
use Log;
use Session;
use Auth;
use Redirect;
use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;
use Exception;
use App\User;
use App\BankDetails;
use App\userDetails;
use App\PageMaster;
use App\TradingPlan;
use App\FatcaDetails;
use Request;


require_once app_path () . '/helpers/Constants.php';
class PanDetailsDAO extends Model
{

  /**
  *
  * Description:save nominee data
  * 13-oct-2017
  * 04:05:29 pm
  * @author Manjiri Parab
  */
  public function removeFatcaDetails($pan_no,$other_residency)
  {
    try
    {
      Log::debug ( ' [PanDetailsDAO] ' . ' [removeFatcaDetails] ' . 'Entered in PanDetailsDAO of removeFatcaDetails function' );

      //update user group
      $user = User::where('id', Auth::user()->id)->first();
      $user->user_group = 2;
      $user->save();

      //update pan no
      $user_details = UserDetails::where('user_id', Auth::user()->id)->first();
      $user_details->pan_no = $pan_no;
      $user_details->other_residency = $other_residency;
      $user_details->save();

      //soft delete old fatca details
      $nominee = FatcaDetails::where('user_id', Auth::user()->id)->first();
      if($nominee)
      {
        return FatcaDetails::where('user_id',Auth::user()->id)->update(['flag' => 0]);
      }
      return 1;
    }
    catch ( Exception $exception )
    {
        Log::error ( ' [PanDetailsDAO] ' . ' [removeFatcaDetails] ' . $exception->getMessage () );
    }

  }
  /**
  *
  * Description: create user in User table,also create null entry in bank details,trading plan and in more table.
  * 22-Aug-2017
  * 05:57:29 pm
  * @author Manjiri Parab
  */
  public function updatePanDetails($country,$number,$type)
  {
    try
    {
      Log::debug ( ' [PanDetailsDAO] ' . ' [updatePanDetails] ' . 'Entered in PanDetailsDAO of updatePanDetails function' );
      return FatcaDetails::insert([
            'user_id' => Auth::user()->id,
            'country'=>$country,
            'number'=>$number,
            'type'=>$type,
            'flag'=>1
          ]);
    }
    catch ( Exception $exception )
    {
        Log::error ( ' [PanDetailsDAO] ' . ' [updatePanDetails] ' . $exception->getMessage () );
    }

  }

  /**
  *
  * Description: pan details
  * 22-Aug-2017
  * 05:57:29 pm
  * @author Manjiri Parab
  */
  public function getPanDetails()
  {
    try
    {
      $fatca_details = null;
      Log::debug ( ' [PanDetailsDAO] ' . ' [getPanDetails] ' . 'Entered in PanDetailsDAO of getPanDetails function' );
      $pan_no =  UserDetails::where('user_id', Auth::user()->id)->first();

      if($pan_no->other_residency)
      {
        $fatca_details = FatcaDetails::where('user_id', Auth::user()->id)->get();
        $fatca_details = $fatca_details->toArray ();
      }
      return array('pan_no'=>$pan_no->pan_no,'other_residency'=>$pan_no->other_residency,'fatca_details'=>$fatca_details);
    }
    catch ( Exception $exception )
    {
        Log::error ( ' [PanDetailsDAO] ' . ' [getPanDetails] ' . $exception->getMessage () );
    }

  }

  /**
	 *
	 * Description: update the ipv inage file path in db
	 *
	 * @param unknown $file_name
	 * @throws Exception 01-Sep-2016
	 *         12:04:11 pm
	 * @author suraj.b
	 */
	public function updateLink($file_name) {
		Log::debug (' [UserDAO] ' . ' [save0rupdate] ' . 'Entered in save0rupdate function .' );
		try {
			$userDetails = UserDetails::where ( 'user_id', '=', Auth::user ()->id )->first ();
			$userDetails->ipv_image = $file_name;
			$userDetails->ipv_status=1;
			$userDetails->save ();
			return true;
		} catch ( Exception $exception ) {
			throw $exception;
		}
	}

}
