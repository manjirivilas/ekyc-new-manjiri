<?php
namespace App\DAO;
use Input;
use View;
use Log;
use Session;
use Auth;
use Redirect;
use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;
use Exception;
use App\User;
use App\TradingPlan;
use App\NomineeDetails;
use Request;


require_once app_path () . '/helpers/Constants.php';
class SelectPlanDAO extends Model
{

  /**
  *
  * Description:store trading and mutual plan flag
  * 10-oct-2017
  * 07:05:29 pm
  * @author Manjiri Parab
  */
  public function saveTradingPlan($user_data)
  {
    try
    {
      $user = TradingPlan::where('user_id', Auth::user()->id)->first();
      $user->trading = $user_data['trading'];
      $user->mutual_fund = $user_data['mutual_fund'];
      $user->save();
      return $user;
    }
    catch ( Exception $exception )
    {
        Log::error ( ' [LoginDAO] ' . ' [createUserEntry] ' . $exception->getMessage () );
    }

  }
  /**
  *
  * Description:store trading and mutual plan flag
  * 10-oct-2017
  * 07:05:29 pm
  * @author Manjiri Parab
  */
  public function checkTradingFlag()
  {
    try
    {
      $user = TradingPlan::where('user_id', Auth::user()->id)->first();
      return array('trading'=>$user->trading,
                   'mutual_fund'=>$user->mutual_fund,
                   'equity_flag'=>$user->equity_flag,
                   'currency_flag'=>$user->currency_flag,
                   'commodity_flag'=>$user->commodity_flag,
                   'equity_seg'=>$user->equity_seg,
                   'currency_seg'=>$user->currency_seg,
                   'commodity_seg'=>$user->commodity_seg,
                   'equity_plan'=>$user->equity_plan,
                   'currency_plan'=>$user->currency_plan,
                   'commodity_plan'=>$user->commodity_plan,
                   'demat_acc_flag'=>$user->demat_acc_flag,
                   'dp_id'=>$user->dp_id,
                   'instruction_slip'=>$user->instruction_slip,
                  );
    }
    catch ( Exception $exception )
    {
        Log::error ( ' [LoginDAO] ' . ' [createUserEntry] ' . $exception->getMessage () );
    }

  }
  /**
  *
  * Description:store segment data
  * 10-oct-2017
  * 07:05:29 pm
  * @author Manjiri Parab
  */
  public function saveSegment($user_plan)
  {
    try
    {
      return TradingPlan::where('user_id',Auth::user()->id)->update([
               'equity_flag'=> trim($user_plan['equity_flag']),
               'commodity_flag' =>  trim($user_plan['commodity_flag']),
               'currency_flag' => trim($user_plan['currency_flag']),
               'equity_plan' =>trim($user_plan['equity_plan']),
               'commodity_plan' =>trim($user_plan['commodity_plan']),
               'currency_plan' =>trim($user_plan['currency_plan']),
               'equity_seg' =>trim($user_plan['equity_seg']),
               'commodity_seg' =>trim($user_plan['commodity_seg']),
               'currency_seg' =>trim($user_plan['currency_seg'])
             ]);
    }
    catch ( Exception $exception )
    {
        Log::error ( ' [LoginDAO] ' . ' [createUserEntry] ' . $exception->getMessage () );
    }

  }

  /**
  *
  * Description:store segment data
  * 10-oct-2017
  * 07:05:29 pm
  * @author Manjiri Parab
  */
  public function saveDematDetails($user_plan)
  {
    try
    {
      Log::debug ( ' [SelectProductDAO] ' . ' [saveDematDetails] ' . 'Entered in SelectProductDAO of saveDematDetails function' );

      return TradingPlan::where('user_id',Auth::user()->id)->update([
               'demat_acc_flag'=> trim($user_plan['demat_acc_flag']),
               'demat_name' =>  trim($user_plan['demat_name']),
               'dp_id' => trim($user_plan['dp_id']),
               'bo_id' =>trim($user_plan['bo_id']),
               'instruction_slip' =>trim($user_plan['instruction_slip'])
             ]);
    }
    catch ( Exception $exception )
    {
        Log::error ( ' [SelectProductDAO] ' . ' [saveDematDetails] ' . $exception->getMessage () );
    }

  }
  /**
  *
  * Description:save nominee data
  * 13-oct-2017
  * 04:05:29 pm
  * @author Manjiri Parab
  */
  public function removeNominee()
  {
    try
    {
      Log::debug ( ' [SelectProductDAO] ' . ' [removeNominee] ' . 'Entered in SelectProductDAO of removeNominee function' );
      $nominee = NomineeDetails::where('user_id', Auth::user()->id)->first();
      if($nominee)
      {
        return NomineeDetails::where('user_id',Auth::user()->id)->update(['flag' => 0]);
      }
      return;
    }
    catch ( Exception $exception )
    {
        Log::error ( ' [SelectProductDAO] ' . ' [saveDematDetails] ' . $exception->getMessage () );
    }

  }
  /**
  *
  * Description:save nominee data
  * 13-oct-2017
  * 04:05:29 pm
  * @author Manjiri Parab
  */
  public function saveNominee($name,$relation,$dob,$address,$nominee_flag)
  {
    try
    {
      Log::debug ( ' [SelectProductDAO] ' . ' [saveNominee] ' . 'Entered in SelectProductDAO of saveNominee function' );
      TradingPlan::where('user_id',Auth::user()->id)->update([ 'nominee_flag' => 1 ]); //set nominee flag 1

      return NomineeDetails::insert([
            'user_id' => Auth::user()->id,
            'name'=>$name,
            'relation'=>$relation,
            'dob'=>$dob,
            'address'=>$address,
            'flag'=>1,
            'total_nominee'=>$nominee_flag
          ]);
    }
    catch ( Exception $exception )
    {
        Log::error ( ' [SelectProductDAO] ' . ' [saveDematDetails] ' . $exception->getMessage () );
    }

  }

  /**
  *
  * Description:save nominee data
  * 13-oct-2017
  * 04:05:29 pm
  * @author Manjiri Parab
  */
  public function getNomineeDetails()
  {
    try
    {
      Log::debug ( ' [SelectProductDAO] ' . ' [getNomineeDetails] ' . 'Entered in SelectProductDAO of getNomineeDetails function' );
      return NomineeDetails::where('user_id', Auth::user()->id)->where('flag',1)->get();
    }
    catch ( Exception $exception )
    {
        Log::error ( ' [SelectProductDAO] ' . ' [getNomineeDetails] ' . $exception->getMessage () );
    }
  }


}
