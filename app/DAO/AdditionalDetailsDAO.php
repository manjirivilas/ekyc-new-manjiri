<?php
namespace App\DAO;
use Input;
use View;
use Log;
use Session;
use Auth;
use Redirect;
use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;
use Exception;
use App\User;
use App\userDetails;


require_once app_path () . '/helpers/Constants.php';
class AdditionalDetailsDAO extends Model
{
  public function updatePersonalDetails($user_details)
  {
      try
      {
          Log::debug ( ' [AdditionalDetailsDAO] ' . ' [updatePersonalDetails] ' . 'Entered in AdditionalDetailsDAO of updatePersonalDetails function' );
        return UserDetails::where('user_id',Auth::user()->id)->update([
                 'father_name'=> trim($user_details['fathername']),
                 'mother_name' =>  trim($user_details['mothername']),
                 'date_of_birth' => trim($user_details['birthdate']),
                 'gender' =>trim($user_details['gender']),
                 'email' =>trim($user_details['email']),
                 'mobile' =>trim($user_details['mobile']),
                 'marrital_status' =>trim($user_details['marrital_status']),
                 'address' =>trim($user_details['address']),
                 'address1' =>trim($user_details['address1']),
                 'address2' =>trim($user_details['address2']),
                 'city' =>trim($user_details['city']),
                 'state' =>trim($user_details['state']),
                 'pincode' =>trim($user_details['pincode']),
                 'address_proof' =>trim($user_details['address_proof']),
                 'permanent_address' =>trim($user_details['permanent_address']),
                 'permanent_address1' =>trim($user_details['permanent_address1']),
                 'permanent_address2' =>trim($user_details['permanent_address2']),
                 'permanent_city' =>trim($user_details['permanent_city']),
                 'permanent_state' =>trim($user_details['permanent_state']),
                 'permanent_pincode' =>trim($user_details['permanent_pincode']),
                 'permanent_address_proof' =>trim($user_details['permanent_address_proof']),
                 'annual_income' =>trim($user_details['annual-income']),
                 'net_worth' =>trim($user_details['net-worth']),
                 'as_on' =>trim($user_details['as-on']),
                 'occupation' =>trim($user_details['occuption']),
               ]);

      }
      catch ( Exception $exception )
      {
            throw $exception;
      }
  }



  public function getAnnualIncomeList()
  {
      try
      {
        Log::debug ( ' [AdditionalDetailsDAO] ' . ' [getAnnualIncomeList] ' . 'Entered in AdditionalDetailsDAO of getAnnualIncomeList function' );
        return DB::connection ( 'ekycserver' )->table('annual_income') ->groupBy('description')->get();
      }
      catch ( Exception $exception )
      {
            throw $exception;
      }
  }

  public function getAadharNo()
  {
      try
      {
        Log::debug ( ' [AdditionalDetailsDAO] ' . ' [getAadharNo] ' . 'Entered in AdditionalDetailsDAO of getAadharNo function' );
        $adhar_no = UserDetails::where('user_id',Auth::user()->id)->first(); //'forgot_password_flag'
        return $adhar_no->aadhar_no;
      }
      catch ( Exception $exception )
      {
            throw $exception;
      }
  }
  public function getStateList()
  {
      try
      {
        Log::debug ( ' [AdditionalDetailsDAO] ' . ' [getStateList] ' . 'Entered in AdditionalDetailsDAO of getStateList function' );
        return DB::connection ( 'ekycserver' )->table('state_master') ->groupBy('state_name')->get();
      }
      catch ( Exception $exception )
      {
          throw $exception;
      }
  }

  public function updateTradingDetails($user_details)
  {
      try
      {
        Log::debug ( ' [AdditionalDetailsDAO] ' . ' [updateTradingDetails] ' . 'Entered in AdditionalDetailsDAO of updateTradingDetails function' );
        return UserDetails::where('user_id',Auth::user()->id)->update([
                 'Investment/Trading'=> trim($user_details['Trading_exp']),
                 'Investment/Trading_exp' =>  trim($user_details['year'])." year ".trim($user_details['month'])." month"
               ]);
      }
      catch ( Exception $exception )
      {
          throw $exception;
      }
  }

  public function updateAdhaarDetails($adhaar_no,$users_details)
  {
    try
    {
        Log::debug ( ' [AdditionalDetailsDAO] ' . ' [updateAdhaarDetails] ' . 'Entered in AdditionalDetailsDAO of updateAdhaarDetails function' );

        $user = User::where('id', Auth::user()->id)->first();
        $user->user_group = 3;
        $user->save();

        return UserDetails::where('user_id',Auth::user()->id)->update([
               'father_name'=> trim($users_details->CO),
               'date_of_birth' => trim($users_details->DOB),
               'aadhar_no'=>$adhaar_no,
               'gender' =>trim($users_details->GENDER),
               'address' =>trim($users_details->HOUSE),
               'address1' =>trim($users_details->STREET),
               'address2' =>trim($users_details->PO),
               'city' =>trim($users_details->VTC),
               'state' =>trim($users_details->STATE),
               'pincode' =>trim($users_details->PC),
               'address_proof' =>'Aadhaar'
             ]);

    }
    catch ( Exception $exception )
    {
        throw $exception;
    }
  }

  public function getAdhaarDetails()
  {
    try
    {
        Log::debug ( ' [AdditionalDetailsDAO] ' . ' [getAdhaarDetails] ' . 'Entered in AdditionalDetailsDAO of getAdhaarDetails function' );
        return UserDetails::where('user_id', Auth::user()->id)->first();
    }
    catch ( Exception $exception )
    {
        throw $exception;
    }
  }



  public function insertAdhaarNo($adhaar_no)
  {
    try
    {
        Log::debug ( ' [AdditionalDetailsDAO] ' . ' [insertAdhaarNo] ' . 'Entered in AdditionalDetailsDAO of insertAdhaarNo function' );
        return UserDetails::where('user_id',Auth::user()->id)->update([
               'aadhar_no'=>$adhaar_no,
             ]);
    }
    catch ( Exception $exception )
    {
        throw $exception;
    }
  }

}
