<?php
namespace App\DAO;
use Input;
use View;
use Log;
use Session;
use Auth;
use Redirect;
use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;
use Exception;
use App\User;
use App\BankDetails;
use App\userDetails;
use App\PageMaster;
use App\TradingPlan;
use Request;


require_once app_path () . '/helpers/Constants.php';
class LoginDAO extends Model{

  /**
  *
  * Description: create user in User table,also create null entry in bank details,trading plan and in more table.
  * 22-Aug-2017
  * 05:57:29 pm
  * @author Manjiri Parab
  */
  public function createUser($user_data,$token)
  {
    try
    {
    $create_user = User::create([
             'name' => trim($user_data['name']),
             'phone'=>$user_data['mobile'],
             'email' => trim($user_data['email']),
             'password' => \Hash::make($user_data['password']),
             'api_token' => $token,
             'user_group' => 1
           ]);
    $this->createUserEntry($create_user->id,$user_data);
    return $create_user;
  }
  catch ( Exception $exception )
  {
      Log::error ( ' [LoginDAO] ' . ' [createUserEntry] ' . $exception->getMessage () );
  }

  }
  /**
  *
  * Description: fetches data to create session.
  * 05-sep-2017
  * 07:30:29 pm
  * @author Manjiri Parab
  */
  public function getUserData($email)
  {
    try
    {
      return User::where('email',$email)->get();
    }
    catch ( Exception $exception )
    {
      Log::error ( ' [LoginDAO] ' . ' [createUserEntry] ' . $exception->getMessage () );
    }
  }
  /**
  *
  * Description: Update api token when user logged in and makes null at the time of logged out.
  * 06-sep-2017
  * 07:04:29 pm
  * @author Manjiri Parab
  */
  public function updateToken($email,$token)
  {
    try
    {
      $user = User::where('email', $email)->first();
      $user->api_token = $token;
      $user->save();
    }
    catch ( Exception $exception )
    {
        Log::error ( ' [LoginDAO] ' . ' [createUserEntry] ' . $exception->getMessage () );
    }
  }
  /**
  *
  * Description: create null entry of user in all bankdetails trading plan etc tables at the time sign up.
  * 18-sep-2017
  * 12:27:29 pm
  * @author Manjiri Parab
  */
  private function createUserEntry($user_id,$user_data)
  {
    try
    {

        UserDetails::create(['user_id'=>$user_id]);
        BankDetails::create(['user_id'=>$user_id]);
        TradingPlan::create(['user_id'=>$user_id]);
        $user = UserDetails::where('user_id', $user_id)->first();
        $user->email = trim($user_data['email']);
        $user->mobile = trim($user_data['mobile']);
        $user->save();

        return;
    }
    catch ( Exception $exception )
    {
        Log::error ( ' [LoginDAO] ' . ' [createUserEntry] ' . $exception->getMessage () );
    }
  }

  /**
  *
  * Description: set forgot password flag.
  * 18-sep-2017
  * 12:27:29 pm
  * @author Manjiri Parab
  */
  public function getUserEmail($user_email,$flag)
  {
    try
    {
      $user = User::where('email', $user_email)->first();
      $user->forgot_password_flag = $flag;
      $user->save();
      return;
    }
    catch ( Exception $exception )
    {
        Log::error ( ' [LoginDAO] ' . ' [getUserEmail] ' . $exception->getMessage () );
    }
  }

  /**
  *
  * Description: check forgot password flag
  * 05-oct-2017
  * 01:25:29 pm
  * @author Manjiri Parab
  */
  public function checkResetPasswordFlag($email)
  {
    try
    {

        $email = User::where('email',$email)->first(); //'forgot_password_flag'
        return $email->forgot_password_flag;
    }
    catch ( Exception $exception )
    {
        Log::error ( ' [LoginDAO] ' . ' [createUserEntry] ' . $exception->getMessage () );
    }
  }
  /**
  *
  * Description: fetches data to create session.
  * 05-sep-2017
  * 07:30:29 pm
  * @author Manjiri Parab
  */
  public function getCurrentPage()
  {
    try
    {
      $url_and_module =  PageMaster::where('status',Auth::user()->page_status)->first();
      return $url_and_module->module_name."/".$url_and_module->url;
    }
    catch ( Exception $exception )
    {
      Log::error ( ' [LoginDAO] ' . ' [createUserEntry] ' . $exception->getMessage () );
    }
  }
  /**
  *
  * Description: update page status of user after successful submission.
  * 09-oct-2017
  * 02:23:29 pm
  * @author Manjiri Parab
  */
  public function updatePageStatus()
  {
    try
    {
      $status =  PageMaster::where('url',Request::segment(2))->first();
      $status = $status->status;
      if(Auth::user()->page_status <= $status)
      {
        $user = User::where('id', Auth::user()->id)->first();
        //return
         $this->incrementPageStatus($user->page_status + 1);
      }

      $url_and_module =  PageMaster::where('status',$status+1)->first();
      return $url_and_module->module_name."/".$url_and_module->url;

      // return $this->incrementPageStatus($user->page_status + 1);
      //else
      //{
      //  return 1;
      //}
    }
    catch ( Exception $exception )
    {
      Log::error ( ' [LoginDAO] ' . ' [createUserEntry] ' . $exception->getMessage () );
    }
  }

  /**
  *
  * Description: increment page status of user after successful submission.
  * 09-oct-2017
  * 02:47:29 pm
  * @author Manjiri Parab
  */
  public function incrementPageStatus($page_no)
  {
    try
    {
      $user = User::where('id', Auth::user()->id)->first();

      if(Auth::user()->page_status <= $page_no)
      {
        $user->page_status = $page_no;
        $user->save();

      }

      $url_and_module =  PageMaster::where('status',$page_no)->first();
      return $url_and_module->module_name."/".$url_and_module->url;


      // return $user;
    }
    catch ( Exception $exception )
    {
      Log::error ( ' [LoginDAO] ' . ' [createUserEntry] ' . $exception->getMessage () );
    }
  }

}
