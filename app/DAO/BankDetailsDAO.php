<?php
namespace App\DAO;
use Input;
use View;
use Log;
use Session;
use Auth;
use Redirect;
use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;
use Exception;
use App\User;
use App\BankDetails;


require_once app_path () . '/helpers/Constants.php';
class BankDetailsDAO extends Model{


  public function updateBankDetails($user_bank_details)
  {
    try
    {
        Log::debug ( ' [AdditionalDetailsDAO] ' . ' [updateBankDetails] ' . 'Entered in AdditionalDetailsDAO of updateBankDetails function' );
      foreach (POST_ALL_BANKDETAILS as $key)
      {
        if (array_key_exists($key, $user_bank_details))
        {
          $user_bank_details[$key] = $user_bank_details[$key];
        }
        else
        {
          $user_bank_details[$key] = null;
        }
      }

        return BankDetails::where('user_id',Auth::user()->id)->update([
                 'bankname'=> trim($user_bank_details['bankname']),
                 'branch' =>  trim($user_bank_details['branch']),
                 'IFSC' => trim($user_bank_details['ifsc']),
                 'MICR' =>trim($user_bank_details['micr']),
                 'acc_no' =>trim($user_bank_details['accno']),
                 'acc_type' =>trim($user_bank_details['acc_type']),
                 'sec_bankname' =>trim($user_bank_details['secbankname']),
                 'sec_branch' =>trim($user_bank_details['secbranch']),
                 'sec_IFSC' =>trim($user_bank_details['secifsc']),
                 'sec_acc_no' =>trim($user_bank_details['secaccno']),
                 'sec_acc_type' =>trim($user_bank_details['secacc_type']),
                 'sec_MICR' =>trim($user_bank_details['secmicr'])
               ]);
  }
  catch ( Exception $exception )
  {
        throw $exception;
  }

  }

  public function fetchBankDetails()
  {
    try
    {
        Log::debug ( ' [BankDetailsDAO] ' . ' [fetchBankDetails] ' . 'Entered in BankDetailsDAO of fetchBankDetails function' );
        return BankDetails::where('user_id', Auth::user()->id)->first();
    }
    catch ( Exception $exception )
    {
            throw $exception;
    }
  }
}
