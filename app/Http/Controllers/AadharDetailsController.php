<?php

namespace App\Http\Controllers;

use App\BO\AadharDetailsBO;
use App\BO\LoginBO;
use Log;
use Input;
use View;
use Session;
use Auth;
use Illuminate\Http\Request;
use Redirect;


use App\BO\EkycApiCaller;

class AadharDetailsController extends Controller
{
  protected $aadhar_details_bo;
	public function __construct(AadharDetailsBO $aadhar_details_bo)
  {
        $this->login_bo = new LoginBO();
        $this->aadhar_details_bo = new AadharDetailsBO();
        $this->common_curl_function = new EkycApiCaller();
	}
  /**
 *
* Description: ekyc-Aadhar details
* 11-Sept-2017
* 06:11:29 pm
* @author Manjiri Parab
 */

public function viewAadharDetails()
{
        Log::debug ( ' [AadharDetailsController] ' . ' [viewAadharDetails] ' . 'Entered in AadharDetailsController of viewAadharDetails function' );
        try
        {
          $user_adhar_no=$this->aadhar_details_bo->getAadharNo();
          return View::make ( 'identity.aadhar-details')->with('adhaar',$user_adhar_no);
        }
        catch ( Exception $exception )
        {
          Log::error ( ' [LoginController] ' . ' [getDetails] ' . $exception->getMessage () );
        }
}
/**
*
* Description: ekyc-Aadhar details
* 11-Sept-2017
* 06:11:29 pm
* @author Manjiri Parab
*/

public function getAadharDetails(Request $request)
{
      Log::debug ( ' [AadharDetailsController] ' . ' [getAadharDetails] ' . 'Entered in AadharDetailsController of getAadharDetails function' );
      try
      {

          $aadhar_no =	$request->all();
          $validate_aadhar=$this->aadhar_details_bo->validateAadhar($aadhar_no);
          // $get_users_current_page=$this->login_bo->getCurrentPage();
          if($validate_aadhar['status'] == SUCCESS)
          {
              return redirect($validate_aadhar['url'])->withCookie(cookie('adhaar', $validate_aadhar['msg']->adhaar, 3600))
                                          ->withCookie(cookie('txn_id', $validate_aadhar['msg']->txn_id, 3600));
          }
          else
          {
              return Redirect::back()->withInput($aadhar_no)->withErrors($validate_aadhar['msg']);
          }
      }
      catch ( Exception $exception )
      {
        Log::error ( ' [LoginController] ' . ' [getDetails] ' . $exception->getMessage () );
      }
}
/**
*
* Description: ekyc-Aadhar Otp
* 11-Sept-2017
* 06:11:29 pm
* @author Manjiri Parab
*/

public function viewAadharOtp()
{
      Log::debug ( ' [AadharOtpController] ' . ' [viewAadharOtp] ' . 'Entered in AadharOtpController of viewAadharOtp function' );
      try
      {
        return View::make ( 'identity.aadhar-otp');
      }
      catch ( Exception $exception )
      {
        Log::error ( ' [LoginController] ' . ' [getOtp] ' . $exception->getMessage () );
      }
}
/**
*
* Description: get otp from user and validate
* 11-Sept-2017
* 06:11:29 pm
* @author Manjiri Parab
*/

public function getAadharOtp(Request $request)
{
      Log::debug ( ' [AadharOtpController] ' . ' [getAadharOtp] ' . 'Entered in AadharOtpController of getAadharOtp function' );
      try
      {
          $otp_details =	$request->all();
          $validate_otp=$this->aadhar_details_bo->validateOTP($otp_details);
          if($validate_otp['status'] == SUCCESS)
          {
              // $get_users_current_page=$this->login_bo->getCurrentPage();
              \Cookie::queue(\Cookie::forget('adhaar'));
              \Cookie::queue(\Cookie::forget('txn_id'));
              return redirect($validate_otp['msg']);
          }
          else
          {
              return Redirect::back()->withInput()->withErrors($validate_otp['msg']);
          }
      }
      catch ( Exception $exception )
      {
        Log::error ( ' [AadharOtpController] ' . ' [getAadharOtp] ' . $exception->getMessage () );
      }
}


}
