<?php

namespace App\Http\Controllers;

use App\BO\LoginBO;
use Log;
use Input;
use View;
use Session;
use Auth;
use Illuminate\Http\Request;
use Redirect;


use App\BO\EkycApiCaller;

require_once app_path().'/helpers/Constants.php';

class LoginController extends Controller
{
  protected $login_bo;
	public function __construct(LoginBO $login_bo)
  {
        $this->login_bo = new LoginBO();
        $this->common_curl_function = new EkycApiCaller();
	}
    /**
   *
  * Description: ekyc-signup page
  * 22-Aug-2017
  * 12:02:29 pm
  * @author Manjiri Parab
   */

public function getDetails()
{
  Log::debug ( ' [LoginController] ' . ' [getDetails] ' . 'Entered in LoginController of getDetails function' );
  try{
  return View::make ( 'sign-up');
  } catch ( Exception $exception ) {
    Log::error ( ' [LoginController] ' . ' [getDetails] ' . $exception->getMessage () );
  }
}
/**
*
* Description: ekyc-signup page=> post data to set-password page
* 22-Aug-2017
* 12:19:29 pm
* @author Manjiri Parab
*/

public function postClientData(Request $request)
{
      $user_data =	$request->all();
      Log::debug ( ' [LoginController] ' . ' [postClientData] ' . 'Entered in LoginController of postClientData function' );
      try
      {
          $error_msg = array ();
          $data=$this->login_bo->validateData($user_data);






          if($data['status'] == SUCCESS)
          {
            // setcookie($user_data['name'], $user_data['email'], time() + (86400 * 30), "/");
            return redirect('set-password') ->withCookie(cookie('name', $user_data['name'], 900))
                                            ->withCookie(cookie('email', $user_data['email'], 900))
                                            ->withCookie(cookie('mobile', $user_data['mobile'], 900));
          }
          else
          {
            return Redirect::back()->withCookie(cookie('name', $user_data['name'], 900))
                                   ->withCookie(cookie('email', $user_data['email'], 900))
                                   ->withCookie(cookie('mobile', $user_data['mobile'], 900))
                                   ->withInput($user_data)->withErrors($data['msg']);
          }
      }
      catch ( Exception $exception )
      {
          Log::error ( ' [LoginController] ' . ' [getDetails] ' . $exception->getMessage () );
      }
}

/**
*
* Description: ekyc-signup view set password
* 13-sept-2017
* 12:59:29 pm
* @author Manjiri Parab
*/
public function viewSetPassword(Request $request)
{
  try
  {
      return view::make('set-password');
  }
  catch ( Exception $exception )
  {
      Log::error ( ' [LoginController] ' . ' [getDetails] ' . $exception->getMessage () );
  }
}
/**
*
* Description: ekyc-signup page post data to password set page
* 22-Aug-2017
* 07:05:29 pm
* @author Manjiri Parab
*/

public function createUser(Request $request)
{
  try
  {

    $user_data =	$request->all();
    $data=$this->login_bo->validatePassword($user_data);
    if($data['status']==SUCCESS)
    {
      $get_users_current_page=$this->login_bo->getCurrentPage();
      return redirect($get_users_current_page);
      // return redirect('aadhar-details');
    }
    else if($data['status']==FAIL)
    {
      return redirect('set-password')->withInput()->withErrors($data['msg']);
    }
    else
    {
      return redirect('')->withInput($user_data)->withErrors($data['msg']);
    }
  } catch (Exception $exception) {

  }

}
/**
*
* Description:display dashboard
* 22-Aug-2017
* 12:19:29 pm
* @author Manjiri Parab
*/

public function viewDashboard(Request $request)
{
      $user_data =	$request->all();
      Log::debug ( ' [LoginController] ' . ' [viewDashboard] ' . 'Entered in LoginController of viewDashboard function' );
      try
      {
            return View::make ( 'dashboard');
      }
      catch ( Exception $exception )
      {
          Log::error ( ' [LoginController] ' . ' [getDetails] ' . $exception->getMessage () );
      }
}
/**
*
* Description:display login
* 04-sept-2017
* 12:19:29 pm
* @author Manjiri Parab
*/

public function viewLogin(Request $request)
{
      if (Auth::check ())
      {
        return redirect ()->route ( 'aadhar-details' );
      }


      $user_data =	$request->all();
      Log::debug ( ' [LoginController] ' . ' [viewLogin] ' . 'Entered in LoginController of viewLogin function' );
      try
      {
            return View::make ( 'login');
      }
      catch ( Exception $exception )
      {
          Log::error ( ' [LoginController] ' . ' [viewLogin] ' . $exception->getMessage () );
      }
}
/**
*
* Description:check login details
* 04-sept-2017
* 04:44:29 pm
* @author Manjiri Parab
*/

public function verifyLogin(Request $request)
{

      $user_data =	$request->all();
      Log::debug ( ' [LoginController] ' . ' [viewLogin] ' . 'Entered in LoginController of viewLogin function' );
      try
      {
            $verify_login=$this->login_bo->verifyLogin($user_data);


            if($verify_login['status'] == SUCCESS)
            {
                  $user_details=$this->login_bo->getUserData($user_data['email']);
                  Auth::loginUsingId($user_details[0]->id);
                  Session::put('api-token',Auth::user()->api_token);
                  $get_users_current_page=$this->login_bo->getCurrentPage();
                  return redirect($get_users_current_page);
            }
            else
            {
                  return Redirect::back()->withInput($user_data)->withErrors($verify_login['msg']);
            }
      }
      catch ( Exception $exception )
      {
          Log::error ( ' [LoginController] ' . ' [viewLogin] ' . $exception->getMessage () );
      }
}

/**
*
* Description:display forgot password page
* 05-sept-2017
* 02:52:29 pm
* @author Manjiri Parab
*/

public function getEmail()
{
      Log::debug ( ' [LoginController] ' . ' [viewLogin] ' . 'Entered in LoginController of viewLogin function' );
      try
      {
            return View::make ( 'forgot-password');
      }
      catch ( Exception $exception )
      {
          Log::error ( ' [LoginController] ' . ' [viewLogin] ' . $exception->getMessage () );
      }
}
/**
*
* Description:display set password page
* 07-sept-2017
* 03:31:29 pm
* @author Manjiri Parab
*/

public function getPassword(Request $request)
{
      Log::debug ( ' [LoginController] ' . ' [viewLogin] ' . 'Entered in LoginController of viewLogin function' );
      try
      {
              $data =	$request->all();

              $check_mail = $this->login_bo->checkResetPasswordFlag(base64_decode($data['data']));

              if($check_mail)
              {
                return redirect( 'reset-password')->withCookie(cookie('token', $data['token'], 3600))
                                                  ->withCookie(cookie('email', base64_decode($data['data']), 3600));
              }
              else
              {
                return redirect( 'forgot-password')->withCookie(cookie('email', base64_decode($data['data']), 3600))
                                                   ->withInput()->withErrors(array('error'=>LINK_EXPIRED));

              }
      }
      catch ( Exception $exception )
      {
          Log::error ( ' [LoginController] ' . ' [viewLogin] ' . $exception->getMessage () );
      }
}
/**
*
* Description:display set password page
* 07-sept-2017
* 03:31:29 pm
* @author Manjiri Parab
*/

public function validatePassword(Request $request)
{
      Log::debug ( ' [LoginController] ' . ' [viewLogin] ' . 'Entered in LoginController of viewLogin function' );
      try
      {
            return View::make ( 'reset-password');
      }
      catch ( Exception $exception )
      {
          Log::error ( ' [LoginController] ' . ' [viewLogin] ' . $exception->getMessage () );
      }
}
/**
*
* Description:display set password page
* 07-sept-2017
* 03:31:29 pm
* @author Manjiri Parab
*/

public function resetPassword(Request $request)
{
      Log::debug ( ' [LoginController] ' . ' [viewLogin] ' . 'Entered in LoginController of viewLogin function' );
      try
      {
            $data =	$request->all();
            $resetPassword=$this->login_bo->resetPassword($data);
            if($resetPassword['status']==SUCCESS)
            {
              \Cookie::queue(\Cookie::forget('email'));
              \Cookie::queue(\Cookie::forget('token'));
              return redirect('password-successful');
            }
            else
            {
              return redirect('reset-password')->withInput()->withErrors(array('error_msg'=>$resetPassword['msg']));
            }

      }
      catch ( Exception $exception )
      {
          Log::error ( ' [LoginController] ' . ' [viewLogin] ' . $exception->getMessage () );
      }
}
/**
*
* Description:set password for user in forgot password page
* 07-sept-2017
* 12:22:29 pm
* @author Manjiri Parab
*/

public function confirmEmail(Request $request)
{
      $email =	$request->all();
      Log::debug ( ' [LoginController] ' . ' [viewLogin] ' . 'Entered in LoginController of viewLogin function' );
      try
      {
              $setPassword=$this->login_bo->setPassword($email);
              if($setPassword['status'] == SUCCESS)
              {
                return redirect( 'email-sent')->withCookie(cookie('email', $email['email'], 3600));  //->with('email',$email['email']);
              }
              else
              {
                return redirect('forgot-password')->withInput($email)->withErrors($setPassword['msg']);
              }

      }
      catch ( Exception $exception )
      {
          Log::error ( ' [LoginController] ' . ' [viewLogin] ' . $exception->getMessage () );
      }
}
/**
*
* Description:send reset password link for resend button
* 05-oct-2017
* 04:27:29 pm
* @author Manjiri Parab
*/

public function sendResetPasswordLink(Request $request)
{
      $email =	$request->all();
      Log::debug ( ' [LoginController] ' . ' [viewLogin] ' . 'Entered in LoginController of viewLogin function' );
      try
      {
        $setPassword=$this->login_bo->setPassword($email);
        if($setPassword['status'] == SUCCESS)
        {
          return redirect( 'email-sent')->withCookie(cookie('email', $email['email'], 3600))->withInput()->withErrors(array('success_msg'=>LINK_SENT));
        }
        else
        {
          return redirect('email-sent')->withCookie(cookie('email', $email['email'], 3600))->withInput()->withErrors(array('error_msg'=>LINK_SENT_FAIL));
        }
      }
      catch ( Exception $exception )
      {
          Log::error ( ' [LoginController] ' . ' [viewLogin] ' . $exception->getMessage () );
      }
}
/**
*
* Description:display forgot password page
* 05-sept-2017
* 06:22:29 pm
* @author Manjiri Parab
*/

public function logoutUser(Request $request)
{
      $user_data =	$request->all();
      Log::debug ( ' [LoginController] ' . ' [viewLogin] ' . 'Entered in LoginController of viewLogin function' );
      try
      {

        $post_array = [
                        'api_token'=>Auth::user()->api_token,
                        ];
          $data=$this->common_curl_function->callCurl('logout',$post_array);

          $update_token=$this->login_bo->updateToken(Auth::user()->email,null);
          Session::flush ();
      		Auth::logout ();
          return redirect('login');

      }
      catch ( Exception $exception )
      {
          Log::error ( ' [LoginController] ' . ' [viewLogin] ' . $exception->getMessage () );
      }
}
/**
*
* Description:display email sent message
* 09-sept-2017
* 01:31:29 pm
* @author Manjiri Parab
*/

public function viewEmailSent(Request $request)
{
      Log::debug ( ' [LoginController] ' . ' [viewLogin] ' . 'Entered in LoginController of viewLogin function' );
      try
      {
            if($request->cookie('email'))
            {
              return View::make ( 'email-sent');
            }
            else
            {
              return redirect('forgot-password');
            }

      }
      catch ( Exception $exception )
      {
          Log::error ( ' [LoginController] ' . ' [viewLogin] ' . $exception->getMessage () );
      }
}

/**
*
* Description:display email sent message
* 09-sept-2017
* 01:31:29 pm
* @author Manjiri Parab
*/

public function viewPasswordSuccessful(Request $request)
{
      Log::debug ( ' [LoginController] ' . ' [viewLogin] ' . 'Entered in LoginController of viewLogin function' );
      try
      {
            return View::make ( 'password-successful');
      }
      catch ( Exception $exception )
      {
          Log::error ( ' [LoginController] ' . ' [viewLogin] ' . $exception->getMessage () );
      }
}

/**
*
* Description: ekyc-Pan details
* 02-Oct-2017
* 12:47:29 pm
* @author Manjiri Parab
*/

public function viewChangePassword()
{
      Log::debug ( ' [LoginChangePasswordController] ' . ' [viewLoginChangePasswordController] ' . 'Entered in LoginChangePasswordController of viewLoginChangePassword function' );
      try
      {
        return View::make ( 'change-password.login-change-password');
      }
      catch ( Exception $exception )
      {
        Log::error ( ' [LoginController] ' . ' [getDetails] ' . $exception->getMessage () );
      }
}

/**
*
* Description: get change password details
* 02-Oct-2017
* 04:33:29 pm
* @author Manjiri Parab
*/

public function getChangePassword(Request $request  )
{
      Log::debug ( ' [LoginChangePasswordController] ' . ' [viewLoginChangePasswordController] ' . 'Entered in LoginChangePasswordController of viewLoginChangePassword function' );
      try
      {
          $user_password =	$request->all();
          $validate_password=$this->login_bo->changePassword($user_password);
          // print_r($validate_password);exit;
          if($validate_password['status'] == SUCCESS )
          {
            return Redirect::back()->with('message',SUCCESS_PWD_CHANGE) ;;
          }
          else
          {
            return Redirect::back()->withInput($user_password)->withErrors($validate_password['msg']);
          }
      }
      catch ( Exception $exception )
      {
        Log::error ( ' [LoginController] ' . ' [getDetails] ' . $exception->getMessage () );
      }
}


}
?>
