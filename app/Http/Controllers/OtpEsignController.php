<?php

namespace App\Http\Controllers;

// use App\BO\LoginBO;
use Log;
use Input;
use View;
use Session;
use Auth;
use Illuminate\Http\Request;
use Redirect;


use App\BO\EkycApiCaller;

class OtpEsignController extends Controller
{
  // protected $login_bo;
	// public function __construct(LoginBO $login_bo)
  // {
  //       $this->login_bo = new LoginBO();
  //       $this->common_curl_function = new EkycApiCaller();
	// }
  /**
 *
* Description: ekyc-Aadhar Otp
* 11-Sept-2017
* 06:11:29 pm
* @author Manjiri Parab
 */

public function viewOtpEsign()
{
        Log::debug ( ' [OtpEsignController] ' . ' [viewOtpEsign] ' . 'Entered in OtpEsignController of viewOtpEsign function' );
        try
        {
          return View::make ( 'upload-doc.otp-esign');
        }
        catch ( Exception $exception )
        {
          Log::error ( ' [LoginController] ' . ' [getOtp] ' . $exception->getMessage () );
        }
}


}
