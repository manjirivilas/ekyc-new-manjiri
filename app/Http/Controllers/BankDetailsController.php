<?php

namespace App\Http\Controllers;
use App\BO\LoginBO;
use App\BO\BankDetailsBO;
use Log;
use Input;
use View;
use Session;
use Auth;
use Illuminate\Http\Request;
use Redirect;


use App\BO\EkycApiCaller;

class BankDetailsController extends Controller
{
  protected $bank_details_bo;
	public function __construct(BankDetailsBO $bank_details_bo)
  {
        $this->login_bo = new LoginBO();
        $this->bank_details_bo = new BankDetailsBO();
        $this->common_curl_function = new EkycApiCaller();
	}
  /**
 *
* Description: ekyc-bank details
* 11-Sept-2017
* 06:11:29 pm
* @author Manjiri Parab
 */

public function viewBankDetails()
{
        Log::debug ( ' [BankDetailsController] ' . ' [viewBankDetails] ' . 'Entered in BankDetailsController of viewBankDetails function' );
        try
        {
          $view_bank_details=$this->bank_details_bo->viewBankDetails();
          if($view_bank_details['status'] == SUCCESS)
          {
            return View::make ( 'bank.bank-details')->with('data', $view_bank_details['msg']);
          }
          else
          {
            return View::make ( 'bank.bank-details')->withInput()->withErrors(BANK_API_ERROR_MSG);
          }
        }
        catch ( Exception $exception )
        {
          Log::error ( ' [LoginController] ' . ' [getDetails] ' . $exception->getMessage () );
        }
}

/**
*
* Description: get bank details via ifsc code
* 14-Sept-2017
* 12:38:29 pm
* @author Manjiri Parab
*/

public function getBankDetails(Request $request)
{
      $ifsc_code =	$request->all();
      Log::debug ( ' [BankDetailsController] ' . ' [getBankDetails] ' . 'Entered in BankDetailsController of getBankDetails function' );
        try
        {
        $bank_details=$this->bank_details_bo->getBankDetails($ifsc_code);
        return array (
								'status'=>$bank_details->status,
								'data' =>$bank_details->data
						);
        }
        catch ( Exception $exception )
        {
          Log::error ( ' [LoginController] ' . ' [getDetails] ' . $exception->getMessage () );
        }
}
/**
*
* Description: validate and insert bank details
* 15-Sept-2017
* 02:53:29 pm
* @author Manjiri Parab
*/

public function updateBankDetails(Request $request)
{
      $bank_details =	$request->all();
      Log::debug ( ' [BankDetailsController] ' . ' [getBankDetails] ' . 'Entered in BankDetailsController of getBankDetails function' );
        try
        {
            $update_details=$this->bank_details_bo->updateBankDetails($bank_details);

            if($update_details['status'] == SUCCESS)
            {
              // $get_users_current_page=$this->login_bo->getCurrentPage();
              return redirect($update_details['msg']);
            }
            else
            {
                return Redirect::back()->withInput($bank_details)->withErrors($update_details['msg']);
            }

        }
        catch ( Exception $exception )
        {
            Log::error ( ' [LoginController] ' . ' [getDetails] ' . $exception->getMessage () );
        }
}


}
