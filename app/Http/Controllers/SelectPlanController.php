<?php

namespace App\Http\Controllers;
use App\BO\LoginBO;
use App\BO\SelectPlanBO;
use Log;
use Input;
use View;
use Session;
use Auth;
use Illuminate\Http\Request;
use Redirect;


use App\BO\EkycApiCaller;

class SelectPlanController extends Controller
{
  // protected $login_bo;
	public function __construct()
  {
        $this->login_bo = new LoginBO();
        $this->select_plan_bo = new SelectPlanBO();
        $this->common_curl_function = new EkycApiCaller();
	}
  /**
 *
* Description: ekyc-Aadhar details
* 11-Sept-2017
* 06:11:29 pm
* @author Manjiri Parab
 */

public function viewSelectPlan()
{
        Log::debug ( ' [SelectPlanController] ' . ' [viewSelectPlan] ' . 'Entered in SelectPlanController of viewSelectPlan function' );
        try
        {
					$product = $this->select_plan_bo->checkTradingFlag();
          return View::make ( 'plan.select-plan')->with('trading',$product['trading'])->with('mutual_fund',$product['mutual_fund']);
        }
        catch ( Exception $exception )
        {
          Log::error ( ' [LoginController] ' . ' [viewSelectPlan] ' . $exception->getMessage () );
        }
}
/**
*
* Description: ekyc-Aadhar details
* 11-Sept-2017
* 06:11:29 pm
* @author Manjiri Parab
*/

public function getSelectPlan(Request $request)
{
      $product =	$request->all();
      Log::debug ( ' [SelectPlanController] ' . ' [getSelectPlan] ' . 'Entered in SelectPlanController of getSelectPlan function' );
      try
      {
          $product = $this->select_plan_bo->getSelectPlan($product);
					Log::debug ( $product );
          if($product['status'] == SUCCESS)
          {
            // $get_users_current_page=$this->login_bo->getCurrentPage();
						// Log::debug ( $get_users_current_page );
            return redirect($product['msg']);
          }
          else
          {
            return Redirect::back()->withInput()->withErrors(array('error'=>$product['msg']));
          }
      }
      catch ( Exception $exception )
      {
        Log::error ( ' [LoginController] ' . ' [getSelectPlan] ' . $exception->getMessage () );
      }
}
/**
*
* Description: ekyc-Aadhar details
* 11-Sept-2017
* 06:11:29 pm
* @author Manjiri Parab
*/

public function viewSelectSegment()
{
      Log::debug ( ' [SelectSegmentController] ' . ' [viewSelectSegment] ' . 'Entered in SelectSegmentController of viewSelectSegment function' );
      try
      {
        $product = $this->select_plan_bo->checkTradingFlag();
        if($product['trading'])
        {
          return View::make ( 'plan.select-segment')->with('data',$product);
        }
        else
        {
          return redirect('dashboard');
        }
      }
      catch ( Exception $exception )
      {
        Log::error ( ' [LoginController] ' . ' [viewSelectSegment] ' . $exception->getMessage () );
      }
}
/**
*
* Description: get segments
* 11-Oct-2017
* 03:16:29 pm
* @author Manjiri Parab
*/

public function getSegment(Request $request)
{

      Log::debug ( ' [SelectSegmentController] ' . ' [getSegment] ' . 'Entered in SelectSegmentController of getSegment function' );
      try
      {
				$segment =	$request->all();
        $check_segment = $this->select_plan_bo->getSegment($segment);

        if($check_segment['status'] == SUCCESS)
        {
          // $get_users_current_page=$this->login_bo->getCurrentPage();
          return redirect($check_segment['msg']);
        }
        else
        {
            return Redirect::back()->withInput()->withErrors($check_segment['msg']);
        }
      }
      catch ( Exception $exception )
      {
        Log::error ( ' [LoginController] ' . ' [getSegment] ' . $exception->getMessage () );
      }
}

/**
*
* Description: ekyc-Aadhar details
* 11-Sept-2017
* 06:11:29 pm
* @author Manjiri Parab
*/

public function viewSelectProduct()
{
      Log::debug ( ' [SelectProductController] ' . ' [viewSelectProduct] ' . 'Entered in SelectProductController of viewSelectProduct function' );
      try
      {
				$plan = $this->select_plan_bo->checkTradingFlag();
				$check_segment = strpos($plan['equity_seg'], "CASH");
				if($check_segment)
				{
					return View::make ( 'plan.select-product');
				}
				else
				{
					return Redirect('dashboard');
				}
      }
      catch ( Exception $exception )
      {
        Log::error ( ' [LoginController] ' . ' [viewSelectProduct] ' . $exception->getMessage () );
      }
}
/**
*
* Description: ekyc-Aadhar details
* 11-Sept-2017
* 06:11:29 pm
* @author Manjiri Parab
*/

public function getProduct(Request $request)
{
      Log::debug ( ' [SelectProductController] ' . ' [getProduct] ' . 'Entered in SelectProductController of getProduct function' );
      try
      {
				$product =	$request->all();
				$check_product = $this->select_plan_bo->getProduct($product);
				if($check_product['status'] == SUCCESS)
				{
					// $get_users_current_page=$this->login_bo->getCurrentPage();
          return redirect($check_product['msg']);
					// return View::make ( 'plan.select-product');
				}
				else
				{
					return Redirect::back()->withInput()->withErrors($check_product['msg']);
				}
      }
      catch ( Exception $exception )
      {
        Log::error ( ' [LoginController] ' . ' [getProduct] ' . $exception->getMessage () );
      }
}
/**
*
* Description: ekyc-Aadhar details
* 11-Sept-2017
* 06:11:29 pm
* @author Manjiri Parab
*/

public function viewNomineeDetails()
{
      Log::debug ( ' [NomineeDetailsController] ' . ' [viewNomineeDetails] ' . 'Entered in NomineeDetailsController of viewNomineeDetails function' );
      try
      {
        // return View::make ( 'plan.nominee-details');
				$product = $this->select_plan_bo->checkTradingFlag();
        if($product['mutual_fund'])
        {
						$nominee_details = $this->select_plan_bo->getNomineeDetails();
						if($nominee_details->toArray ())
						{
							return View::make ( 'plan.nominee-details')->with('nominee',$nominee_details->toArray ());
						}
						else
						{
							return View::make ( 'plan.nominee-details')->with('nominee',null);
						}

        }
        else
        {
          return redirect('dashboard');
        }
      }
      catch ( Exception $exception )
      {
        Log::error ( ' [LoginController] ' . ' [viewNomineeDetails] ' . $exception->getMessage () );
      }
}

/**
*
* Description: get user nominee details for mutual fund
* 13-Oct-2017
* 12:13:29 pm
* @author Manjiri Parab
*/

public function getNomineeDetails(Request $request)
{
      Log::debug ( ' [NomineeDetailsController] ' . ' [getNomineeDetails] ' . 'Entered in NomineeDetailsController of getNomineeDetails function' );
      try
      {
				$nominee_details =	$request->all();
				$save_nominee = $this->select_plan_bo->saveNomineeDetails($nominee_details);
				if($save_nominee['status'] == SUCCESS)
				{
					// $get_users_current_page=$this->login_bo->getCurrentPage();
          return redirect($save_nominee['msg']);
				}
				else
				{
					return Redirect::back()->withInput($nominee_details)->withErrors($save_nominee['msg']);
				}
      }
      catch ( Exception $exception )
      {
        Log::error ( ' [LoginController] ' . ' [getNomineeDetails] ' . $exception->getMessage () );
      }
}

/**
*
* Description: ekyc-Aadhar details
* 11-Sept-2017
* 06:11:29 pm
* @author Manjiri Parab
*/

public function viewPlanSummary()
{
      Log::debug ( ' [NomineeDetailsController] ' . ' [viewPlanSummary] ' . 'Entered in NomineeDetailsController of viewPlanSummary function' );
      try
      {
				$plan_summary = $this->select_plan_bo->checkTradingFlag();
				if($plan_summary['mutual_fund'] || $plan_summary['trading'])
				{
					$nominee_summary = $this->select_plan_bo->getNomineeDetails();
	        return View::make ( 'plan.plan-summary')->with('segment',$plan_summary)->with('nominee',$nominee_summary->toArray ());
				}
				else
				{
					return redirect('dashboard');
				}
      }
      catch ( Exception $exception )
      {
        Log::error ( ' [LoginController] ' . ' [viewPlanSummary] ' . $exception->getMessage () );
      }
}

/**
*
* Description: get plan summary
* 16-oct-2017
* 06:11:29 pm
* @author Manjiri Parab
*/

public function getPlanSummary()
{
      Log::debug ( ' [NomineeDetailsController] ' . ' [getPlanSummary] ' . 'Entered in NomineeDetailsController of getPlanSummary function' );
      try
      {
				$update_status = $this->login_bo->updatePageStatus();
				// $get_users_current_page=$this->login_bo->getCurrentPage();
				return redirect($update_status);
      }
      catch ( Exception $exception )
      {
        Log::error ( ' [LoginController] ' . ' [getPlanSummary] ' . $exception->getMessage () );
      }
}

}
