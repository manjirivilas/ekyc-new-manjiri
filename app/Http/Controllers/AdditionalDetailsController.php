<?php

namespace App\Http\Controllers;
use App\BO\LoginBO;
use App\BO\AdditionalDetailsBO;
use Log;
use Input;
use View;
use Session;
use Auth;
use Illuminate\Http\Request;
use Redirect;


use App\BO\EkycApiCaller;

class AdditionalDetailsController extends Controller
{
  protected $additional_details_bo;
	public function __construct(AdditionalDetailsBO $additional_details_bo)
  {
        $this->login_bo = new LoginBO();
        $this->additional_details_bo = new AdditionalDetailsBO();
        $this->common_curl_function = new EkycApiCaller();
	}
  /**
 *
* Description: view personal details
* 11-Sept-2017
* 06:11:29 pm
* @author Manjiri Parab
 */

public function viewPersonalDetails()
{
        Log::debug ( ' [AdditionalDetails1Controller] ' . ' [viewPersonalDetails] ' . 'Entered in AdditionalDetails1Controller of viewPersonalDetails function' );
        try
        {
          $annual_income_list=$this->additional_details_bo->getAnnualIncomeList();
          $state_list = $this->additional_details_bo->getStateList();
          $adhaar_details = $this->additional_details_bo->getAdhaarDetails()->toArray ();

          if($annual_income_list && $state_list && $adhaar_details)
          {
            return View::make ( 'identity.personal-details')->with('income',$annual_income_list)
                                                            ->with('state',$state_list)
                                                            ->with('data',$adhaar_details);
          }
          else
          {
            return View::make ( 'identity.personal-details')->withInput()->withErrors(ERROR_MSG);
          }
        }
        catch ( Exception $exception )
        {
          Log::error ( ' [LoginController] ' . ' [getDetails] ' . $exception->getMessage () );
        }
}
/**
*
* Description: getuser's personal details
* 23-Sept-2017
* 12:37:29 pm
* @author Manjiri Parab
*/

public function getPersonalDetails(Request $request)
{
      Log::debug ( ' [AdditionalDetailsController] ' . ' [getPersonalDetails] ' . 'Entered in AdditionalDetailsController of getPersonalDetails function' );
      try
      {
        $user_details =	$request->all();
        $validate_user_details = $this->additional_details_bo->getPersonalDetails($user_details);
        if($validate_user_details['status'] == SUCCESS)
        {
          // $get_users_current_page=$this->login_bo->getCurrentPage();
          return redirect($validate_user_details['msg']);
        }
        else
        {
          return Redirect::back()->withInput($user_details)->withErrors($validate_user_details['msg']);
        }
      }
      catch ( Exception $exception )
      {
        Log::error ( ' [LoginController] ' . ' [getDetails] ' . $exception->getMessage () );
      }
}
/**
*
* Description: ekyc-Aadhar details
* 11-Sept-2017
* 06:11:29 pm
* @author Manjiri Parab
*/

public function viewTradingDetails()
{
      Log::debug ( ' [AdditionalDetailsController] ' . ' [viewAdditionalDetails2] ' . 'Entered in AdditionalDetails2Controller of viewAdditionalDetails2 function' );
      try
      {
        return View::make ( 'identity.additional-details');
      }
      catch ( Exception $exception )
      {
        Log::error ( ' [LoginController] ' . ' [getDetails] ' . $exception->getMessage () );
      }
}

/**
*
* Description: get trading details
* 25-Sept-2017
* 06:25:29 pm
* @author Manjiri Parab
*/

public function getTradingDetails(Request $request)
{
      $user_trading_details =	$request->all();
      Log::debug ( ' [AdditionalDetailsController] ' . ' [getTradingDetails] ' . 'Entered in AdditionalDetails2Controller of getTradingDetails function' );
      try
      {
          $trading_details = $this->additional_details_bo->getTradingDetails($user_trading_details);

          if($trading_details['status'] == SUCCESS)
          {
              // $get_users_current_page=$this->login_bo->getCurrentPage();
              return redirect($trading_details['msg']);
              //return redirect('dashboard');
          }
          else
          {
              return Redirect::back()->withInput($user_trading_details)->withErrors($trading_details['msg']);
          }
      }
      catch ( Exception $exception )
      {
        Log::error ( ' [LoginController] ' . ' [getDetails] ' . $exception->getMessage () );
      }
}

}
