<?php

namespace App\Http\Controllers;

use App\BO\AdditionalDetailsBO;
use App\BO\LoginBO;
use App\BO\BankDetailsBO;
use App\BO\SelectPlanBO;
use Log;
use Input;
use View;
use Session;
use Auth;
use Illuminate\Http\Request;
use Redirect;


use App\BO\EkycApiCaller;

class PaymentSummaryController extends Controller
{
	public function __construct()
  {
        $this->additional_details_bo = new AdditionalDetailsBO();
				$this->bank_details_bo = new BankDetailsBO();
        $this->common_curl_function = new EkycApiCaller();
				$this->select_plan_bo = new SelectPlanBO();
	}
  /**
 *
* Description: ekyc-Pan details
* 11-Sept-2017
* 06:11:29 pm
* @author Manjiri Parab
 */

public function viewPaymentSummary()
{
        Log::debug ( ' [PaymentSummaryController] ' . ' [viewPaymentSummaryController] ' . 'Entered in PaymentSummaryController of viewPaymentSummary function' );
        try
        {
          $user_details = $this->additional_details_bo->getAdhaarDetails()->toArray (); //user details
					$bank_details = $this->bank_details_bo->fetchBankDetails()->toArray (); //user details
					$trading_details = $this->select_plan_bo->checkTradingFlag();//trading details
          // echo "<pre>";print_r($trading_details);exit;
          return View::make ( 'payment.payment-summary')->with('userDetails',$user_details)
																												->with('bankDetails',$bank_details)
																												->with('tradingDetails',$trading_details);
        }
        catch ( Exception $exception )
        {
          Log::error ( ' [LoginController] ' . ' [getDetails] ' . $exception->getMessage () );
        }
}


}
