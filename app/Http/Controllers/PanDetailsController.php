<?php

namespace App\Http\Controllers;
use App\BO\LoginBO;
use App\BO\PanDetailsBO;
use Log;
use Input;
use View;
use Session;
use Auth;
use Illuminate\Http\Request;
use Redirect;


use App\BO\EkycApiCaller;

class PanDetailsController extends Controller
{
  protected $pan_details_bo;
	public function __construct(PanDetailsBO $pan_details_bo)
  {
        $this->login_bo = new LoginBO();
        $this->pan_details_bo = new PanDetailsBO();
        $this->common_curl_function = new EkycApiCaller();
	}
  /**
 *
* Description: ekyc-view Pan details
* 11-Sept-2017
* 06:11:29 pm
* @author Manjiri Parab
 */

public function viewPanDetails()
{
        Log::debug ( ' [PanDetailsController] ' . ' [viewPanDetails] ' . 'Entered in PanDetailsController of viewPanDetails function' );
        try
        {
          $get_pan_details = $this->pan_details_bo->getPanDetails();
          // echo "<pre>";print_r($get_pan_details);exit;
          return View::make ( 'identity.pan-details')->with('pan_details',$get_pan_details);
        }
        catch ( Exception $exception )
        {
          Log::error ( ' [LoginController] ' . ' [getDetails] ' . $exception->getMessage () );
        }
}
/**
*
* Description: ekyc-get Pan details
* 20-Sept-2017
* 02:51:29 pm
* @author Manjiri Parab
*/

public function getPanDetails(Request $request)
{
      Log::debug ( ' [PanDetailsController] ' . ' [getPanDetails] ' . 'Entered in PanDetailsController of getPanDetails function' );
      try
      {
        $pan_no =	$request->all();
        $validate_pan = $this->pan_details_bo->validatePan($pan_no);
        // echo "<pre>";print_r($validate_pan['msg']);exit;
        if($validate_pan['status'] == SUCCESS)
        {
            // $get_users_current_page=$this->login_bo->getCurrentPage();
            return redirect($validate_pan['msg']);
        }
        else
        {
            return Redirect::back()->withInput($pan_no)->withErrors($validate_pan['msg']);
        }
      }
      catch ( Exception $exception )
      {
        Log::error ( ' [LoginController] ' . ' [getDetails] ' . $exception->getMessage () );
      }
}
/**
*
* Description: ekyc-Pan details
* 11-Sept-2017
* 06:11:29 pm
* @author Manjiri Parab
*/

public function viewPanIPV()
{
      Log::debug ( ' [PanIPVController] ' . ' [viewPanIPV] ' . 'Entered in PanIPVController of viewPanIPV function' );
      try
      {

        return View::make ( 'identity.pan-ipv');
      }
      catch ( Exception $exception )
      {
        Log::error ( ' [LoginController] ' . ' [getDetails] ' . $exception->getMessage () );
      }
}

/**
*
* Description: ekyc-Pan details
* 11-Sept-2017
* 06:11:29 pm
* @author Manjiri Parab
*/

public function viewManjiPanIPV()
{
      Log::debug ( ' [PanIPVController] ' . ' [viewPanIPV] ' . 'Entered in PanIPVController of viewPanIPV function' );
      try
      {
        return View::make ( 'manji-ipv');
      }
      catch ( Exception $exception )
      {
        Log::error ( ' [LoginController] ' . ' [getDetails] ' . $exception->getMessage () );
      }
}
/**
	 *
	* Description: Save IPV Image
	* @param Request $request
	* @return \Illuminate\Http\JsonResponse
	* \Illuminate\Http\JsonResponse
	* 25-oct-2017
	* 12:14:51 pm
	* @author manjiri
	 */
	public function saveIPV(Request $request) {
    try
    {
		Log::debug (  ' [PanIPVController] ' . ' [getIPV] ' . 'Entered in saveIPV function. ' );

		$encoded_data = $request->get ( 'raw_image_data' );
		$client_code = $request->get ( 'client_code' );
		$file = base64_decode ( $encoded_data );
		$save = $this->pan_details_bo->saveIPV ( $file, $client_code );
    $update_status = $this->login_bo->updatePageStatus();
		return response ()->json ( [
				'success' => true,
				'url' => $save
		] );
  }
  catch ( Exception $exception )
  {
    Log::error ( ' [LoginController] ' . ' [getDetails] ' . $exception->getMessage () );
  }

	}
}
