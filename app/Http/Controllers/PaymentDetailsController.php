<?php

namespace App\Http\Controllers;

// use App\BO\LoginBO;
use Log;
use Input;
use View;
use Session;
use Auth;
use Illuminate\Http\Request;
use Redirect;


use App\BO\EkycApiCaller;

class PaymentDetailsController extends Controller
{
  // protected $login_bo;
	// public function __construct(LoginBO $login_bo)
  // {
  //       $this->login_bo = new LoginBO();
  //       $this->common_curl_function = new EkycApiCaller();
	// }
  /**
 *
* Description: ekyc-Pan details
* 11-Sept-2017
* 06:11:29 pm
* @author Manjiri Parab
 */

public function viewPaymentDetails()
{
        Log::debug ( ' [PaymentDetailsController] ' . ' [viewPaymentDetailsController] ' . 'Entered in PaymentDetailsController of viewPaymentDetails function' );
        try
        {
          return View::make ( 'payment.payment-details');
        }
        catch ( Exception $exception )
        {
          Log::error ( ' [LoginController] ' . ' [getDetails] ' . $exception->getMessage () );
        }
}


}
