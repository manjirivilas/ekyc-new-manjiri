<?php

namespace App\Http\Controllers;

// use App\BO\LoginBO;
use Log;
use Input;
use View;
use Session;
use Auth;
use Illuminate\Http\Request;
use Redirect;


use App\BO\EkycApiCaller;

class EkycProcessCompletionController extends Controller
{
  // protected $login_bo;
	// public function __construct(LoginBO $login_bo)
  // {
  //       $this->login_bo = new LoginBO();
  //       $this->common_curl_function = new EkycApiCaller();
	// }
  /**
 *
* Description: ekyc-Pan details
* 11-Sept-2017
* 06:11:29 pm
* @author Manjiri Parab
 */

public function viewEkycProcess()
{
        Log::debug ( ' [EkycProcessController] ' . ' [viewEkycProcessController] ' . 'Entered in EkycProcessController of viewEkycProcess function' );
        try
        {
          return View::make ( 'payment.ekyc-completion');
        }
        catch ( Exception $exception )
        {
          Log::error ( ' [LoginController] ' . ' [getDetails] ' . $exception->getMessage () );
        }
}


}
