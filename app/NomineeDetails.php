<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NomineeDetails extends Model
{
  protected $table = 'nominee_details';
  protected $connection = 'mysql';

  protected $fillable = [ 'user_id','flag'];

  public function user()
  {
      return $this->belongsTo('App\User');
  }
}
