<?php

namespace App\BO;
use App\DAO\LoginDAO;
use App\DAO\BankDetailsDAO;
use Auth;
use Config;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Log;
use Request;
use Response;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Validator;
use App\BO\EkycApiCaller;

require_once app_path().'/helpers/Constants.php';
require_once app_path().'/helpers/validation.php';
require_once app_path().'/helpers/sanitizer.php';


class BankDetailsBO extends Model {

  public function __construct() {
      $this->login_dao = new LoginDAO ();
  		$this->bank_details_dao = new BankDetailsDAO ();
      $this->common_curl_function = new EkycApiCaller();
  	}
    /**
    *
    * Description: view bank details.
    * 21-Sept-2017
    * 02:24:29 pm
    * @author Manjiri Parab
    */
    public function viewBankDetails() {
      try
      {
        Log::debug ( ' [BankDetailsBO] ' . ' [viewBankDetails] ' . 'Entered in BankDetailsBO of viewBankDetails function' );
        $post_array = ['api_token'=>Auth::user()->api_token];
        $user_bank_details= $this->common_curl_function->callCurl('get-bank-details',$post_array);
        if(empty($user_bank_details))
        {
          return array (
              'msg' =>API_ERROR_MSG,
              'status' => FAIL
          );
        }

        if($user_bank_details->status == SUCCESS)
        {
          return array (
              'msg' =>$user_bank_details->data,
              'status' => SUCCESS
          );
        }
        else
        {
          return array (
              'msg' =>$user_bank_details->msg,
              'status' => FAIL
          );
        }
      }
      catch( Exception $exception ) {
        throw $exception;
      }
    }
    /**
    *
    * Description: validates ifsc code and fetches bank details.
    * 14-Sept-2017
    * 12:56:29 pm
    * @author Manjiri Parab
    */
    public function getBankDetails($ifsc_code) {
      try
      {
        Log::debug ( ' [BankDetailsBO] ' . ' [getBankDetails] ' . 'Entered in BankDetailsBO of getBankDetails function' );
        $post_array = [
                      'ifsc'=>$ifsc_code['ifsc'],
                      ];

        return $this->common_curl_function->callCurl('bank-details-ifsc',$post_array);
      }
      catch( Exception $exception ) {
        throw $exception;
      }
    }
    /**
    *
    * Description: update user's bank details.
    * 15-Sept-2017
    * 05:45:29 pm
    * @author Manjiri Parab
    */
    public function updateBankDetails($bank_details)
    {
      try
      {
        Log::debug ( ' [BankDetailsBO] ' . ' [updateBankDetails] ' . 'Entered in BankDetailsBO of updateBankDetails function' );
        $bank_details['api_token'] = Auth::user()->api_token;
        if($bank_details['flag'])
        {
              $post_data = POST_ALL_BANKDETAILS ;
              $post_data_rule = V_PRI_SEC_BANK_DETAILS ;
        }
        else
        {
              $post_data = POST_BANKDETAILS ;
              $post_data_rule = V_BANK_DETAILS ;
        }

        $check_mandatory=mandatory($post_data,$bank_details);
        if($check_mandatory['status']=='success')
        {
                $fetch_required = required($post_data,$bank_details);
                $return_error = array();
                $validator = Validator::make ($fetch_required,$post_data_rule,V_BANK_DETAILS_MSG);
                if ($validator-> fails())
                {

                    return array (
                        'msg' =>$validator->messages ()->toArray (),
                        'status' => 'fail'
                    );
                }
                else
                {

                    //update in database
                    $update_bank_details=$this->bank_details_dao->updateBankDetails($fetch_required);
                    if($update_bank_details)
                    {
                      //update in api
                      $update_api_details=$this->common_curl_function->callCurl('update-bank-details',$fetch_required);
                      if(empty($update_api_details))
                      {
                        return array ('msg' => API_ERROR_MSG,'status' => FAIL);
                      }
                      if($update_api_details->status != SUCCESS)
                      {
                        return array ('msg' => $update_api_details->msg,'status' => FAIL);
                      }
                      else
                      {
                        $update_status = $this->login_dao->updatePageStatus(); // to update user's page status
                        return array ('msg' => $update_status,'status' => SUCCESS);
                      }
                    }
                    else
                    {
                      return array (
                          'msg' => ERROR_MSG,
                          'status' => FAIL
                      );
                    }
                }
        }
        else
        {
          return array (
              'msg' =>$check_mandatory['data'],
              'status' => FAIL
          );

        }

      }
      catch( Exception $exception ) {
        throw $exception;
      }
    }
    /**
    *
    * Description: fetch user's bank details froem database.
    * 15-Sept-2017
    * 05:45:29 pm
    * @author Manjiri Parab
    */
    public function fetchBankDetails()
    {
      try
      {
        Log::debug ( ' [BankDetailsBO] ' . ' [fetchBankDetails] ' . 'Entered in BankDetailsBO of fetchBankDetails function' );
        return $this->bank_details_dao->fetchBankDetails();
      }
      catch( Exception $exception )
      {
        throw $exception;
      }
    }
  }
?>
