<?php
namespace App\BO;
use Log;

class EkycApiCaller {

	public static function  getURL($url,$timeout = 2){

		$arrURL = array(
					$url
				// 'http://bodb1.vnsfin.com:8084/client-code/index.php',
				// 'http://bodb2.vnsfin.com:8084/client-code/index.php',
				// 'http://bodb3.vnsfin.com:8084/client-code/index.php'
		);
		$status='';
			$i = 0;
			while ( $i < count ( $arrURL ) ) {

				$curl_request = curl_init ();
				curl_setopt ( $curl_request, CURLOPT_URL, $arrURL[$i] );
				curl_setopt ( $curl_request, CURLOPT_TIMEOUT, $timeout );
				curl_setopt ( $curl_request, CURLOPT_HEADER, 0 );
				curl_setopt ( $curl_request, CURLOPT_SSL_VERIFYPEER, 0 );
				curl_setopt ( $curl_request, CURLOPT_RETURNTRANSFER, 1 );
				curl_setopt ( $curl_request, CURLOPT_FOLLOWLOCATION, 0 );
				$result = curl_exec ( $curl_request );
				$status = curl_getinfo ( $curl_request, CURLINFO_HTTP_CODE );
				print_r($arrURL);exit;
				print_r($status);exit;
				if ($status == 200){

					return $arrURL[$i];
				}
				//var_dump($status);
				curl_close ( $curl_request );
				$i++;

			}

	  }
	 public static function callToCurl($url,$data) {

			// $url = self::getURL($url);
			$fields = '';
			foreach ( $data as $key => $value ) {
				$fields .= $key . '=' . $value . '&';
			}
			rtrim ( $fields, '&' );

				$curl_request = curl_init ();
				curl_setopt ( $curl_request, CURLOPT_URL, $url );
				curl_setopt ( $curl_request, CURLOPT_POST, count ( $data ) );
				curl_setopt ( $curl_request, CURLOPT_POSTFIELDS, $fields );
				curl_setopt ( $curl_request, CURLOPT_HEADER, 0 );
				curl_setopt ( $curl_request, CURLOPT_SSL_VERIFYPEER, 0 );
				curl_setopt ( $curl_request, CURLOPT_RETURNTRANSFER, 1 );
				curl_setopt ( $curl_request, CURLOPT_FOLLOWLOCATION, 0 );
				$result = curl_exec ( $curl_request );

				curl_close ( $curl_request );
			return json_decode ( $result );
		}



	/**
	 * Remove HTML tags, including invisible text such as style and
	 * script code, and embedded objects.  Add line breaks around
	 * block-level tags to prevent word joining after tag removal.
	 */
	public static function  strip_html_tags( $text )
	{
		$text = preg_replace(
				array(
						// Remove invisible content
						'@<head[^>]*?>.*?</head>@siu',
						'@<style[^>]*?>.*?</style>@siu',
						'@<script[^>]*?.*?</script>@siu',
						'@<object[^>]*?.*?</object>@siu',
						'@<embed[^>]*?.*?</embed>@siu',
						'@<applet[^>]*?.*?</applet>@siu',
						'@<noframes[^>]*?.*?</noframes>@siu',
						'@<noscript[^>]*?.*?</noscript>@siu',
						'@<noembed[^>]*?.*?</noembed>@siu',
						// Add line breaks before and after blocks
						'@</?((address)|(blockquote)|(center)|(del))@iu',
						'@</?((div)|(h[1-9])|(ins)|(isindex)|(p)|(pre))@iu',
						'@</?((dir)|(dl)|(dt)|(dd)|(li)|(menu)|(ol)|(ul))@iu',
						'@</?((table)|(th)|(td)|(caption))@iu',
						'@</?((form)|(button)|(fieldset)|(legend)|(input))@iu',
						'@</?((label)|(select)|(optgroup)|(option)|(textarea))@iu',
						'@</?((frameset)|(frame)|(iframe))@iu',
				),
				array(
						' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
						"\n\$0", "\n\$0", "\n\$0", "\n\$0", "\n\$0", "\n\$0",
						"\n\$0", "\n\$0",
				),
				$text );
		return strip_tags( $text );
	}


	public static function curl($url, $post = "")
	{
		$curl = curl_init();
		$userAgent = 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; .NET CLR 1.1.4322)';
		curl_setopt($curl, CURLOPT_URL, $url);
		//The URL to fetch. This can also be set when initializing a session with curl_init().
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
		//TRUE to return the transfer as a string of the return value of curl_exec() instead of outputting it out directly.
		curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 5);
		//The number of seconds to wait while trying to connect.
		if ($post != "") {
			curl_setopt($curl, CURLOPT_POST, 5);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $post);
		}
		curl_setopt($curl, CURLOPT_USERAGENT, $userAgent);
		//The contents of the "User-Agent: " header to be used in a HTTP request.
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE);
		//To follow any "Location: " header that the server sends as part of the HTTP header.
		curl_setopt($curl, CURLOPT_AUTOREFERER, TRUE);
		//To automatically set the Referer: field in requests where it follows a Location: redirect.
		curl_setopt($curl, CURLOPT_TIMEOUT, 10);
		//The maximum number of seconds to allow cURL functions to execute.
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
		//To stop cURL from verifying the peer's certificate.
		$contents = curl_exec($curl);
		curl_close($curl);
		return $contents;
	}

	/**
	*
	* Description:curl function
	* 06-sep-2017
	* 11:03:29 am
	* @author Manjiri Parab
	*/
	public function callCurl($url_name,$post_array)
	{
	      Log::debug ( ' [LoginController] ' . ' [viewDashboard] ' . 'Entered in LoginController of viewDashboard function' );
	      try
	      {
	          $url = "http://192.168.27.43:8000/api/v1/".$url_name;
	          $execute_curl_result =$this->callToCurl($url,$post_array);
						return $execute_curl_result;
	      }
	      catch ( Exception $exception )
	      {
	          Log::error ( ' [LoginController] ' . ' [getDetails] ' . $exception->getMessage () );
	      }
	}
}

?>
