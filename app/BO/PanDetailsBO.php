<?php

namespace App\BO;

use App\DAO\LoginDAO;
use App\DAO\PanDetailsDAO;
use Auth;
use Config;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Log;
use Request;
use Response;

require_once app_path().'/helpers/Constants.php';
require_once app_path().'/helpers/validation.php';
require_once app_path().'/helpers/sanitizer.php';

use Illuminate\Foundation\Validation\ValidatesRequests;
use Validator;
use App\BO\EkycApiCaller;


class PanDetailsBO extends Model {

  public function __construct() {

      $this->pan_details_dao = new PanDetailsDAO ();
  		$this->login_dao = new LoginDAO ();
      $this->common_curl_function = new EkycApiCaller();

  	}
    // /**
    // *
    // * Description: validate user pan
    // * 21-Sept-2017
    // * 10:44:29 am
    // * @author Manjiri Parab
    // */
    // public function validatePan($pan_details)
    // {
    //     try
    //     {
    //     }
    //     catch( Exception $exception )
    //     {
    //       throw $exception;
    //     }
    // }




    /**
    *
    * Description: validate user pan
    * 21-Sept-2017
    * 10:44:29 am
    * @author Manjiri Parab
    */
    public function validatePan($pan_details)
    {
        try
        {
          Log::debug ( ' [PanDetailsBO] ' . ' [validatePan] ' . 'Entered in PanDetailsBO of validatePan function' );
            $fetch_required = required(POST_PAN,$pan_details);
            $validator = Validator::make ($fetch_required,V_PAN,V_PAN_MSG);
            if ($validator->fails ())
            {
              return array (
                  'msg' =>$validator->messages ()->toArray (),
                  'status' => FAIL
              );
            }
            else
            {

              $update_status = $this->pan_details_dao->removeFatcaDetails($fetch_required['pan_no'],$fetch_required['other_residency']);
              $this->pan_details_dao->updatePanDetails($fetch_required['identification_country_1'],$fetch_required['identification_number_1'],$fetch_required['identification_type_1']);
              $this->pan_details_dao->updatePanDetails($fetch_required['identification_country_2'],$fetch_required['identification_number_2'],$fetch_required['identification_type_2']);
              $this->pan_details_dao->updatePanDetails($fetch_required['identification_country_3'],$fetch_required['identification_number_3'],$fetch_required['identification_type_3']);

              $update_status = $this->login_dao->updatePageStatus(); // to update user's page status
              return array (
                  'msg' =>$update_status,
                  'status' => SUCCESS
              );
            }
        }
        catch( Exception $exception )
        {
    			throw $exception;
    		}
    }

    /**
    *
    * Description: get pan details
    * 21-Sept-2017
    * 10:44:29 am
    * @author Manjiri Parab
    */
    public function getPanDetails()
    {
        try
        {
          return $this->pan_details_dao->getPanDetails();
        }
        catch( Exception $exception )
        {
          throw $exception;
        }
    }
    /**
	 *
	 * Description: save user image and update link in database
	 *
	 * @param file $file
	 * @throws Exception return_type boolean
	 *         31-oct-2017
	 *         1:05:04 pm
	 * @author manjiri
	 */
	public function saveIPV($file, $client_code) {
		Log::debug ( ' [UserBo] ' . ' [saveIPV] ' . 'Entered in saveIPV function .' );
		try {
			$image_folder = "/image/" . date ( "Y-m" );
			$month_folder = public_path () . $image_folder;
			if (! file_exists ( $month_folder )) {
				mkdir ( "$month_folder", 0777, true );
				chmod($month_folder,0777);

				return $this->saveFile ( $file, $month_folder, $image_folder, $client_code );
			} else {
				return $this->saveFile ( $file, $month_folder, $image_folder, $client_code );
			}
		} catch ( Exception $exception ) {
			throw $exception;
		}
	}

  /**
 *
 * Description: save user image and update link in database
 *
 * @param file $file
 * @throws Exception return_type boolean
 *         31-oct-2017
  *         1:05:04 pm
 * @author manjiri
 */
  private function saveFile($file, $month_folder, $image_folder, $client_code)
  {
  try {
    $name = $client_code;
    $file_name = $month_folder . '/' . $name . '.jpg'; // $month_folder . '/' . $name . '.jpg';
    file_put_contents ( $file_name, $file );
    $month_folder= $_SERVER['SERVER_NAME'].$image_folder;
    $file_name = $month_folder . '/' . $name . '.jpg'; // $month_folder . '/' . $name . '.jpg';
    $db_link = $this->pan_details_dao->updateLink ( $file_name );
    $file_name = Request::root () . $image_folder . '/' . $name . '.jpg';
    return $file_name;
  } catch ( Exception $exception ) {
    throw $exception;
  }
}

}
