<?php

namespace App\BO;

use App\DAO\AdditionalDetailsDAO;
use App\DAO\LoginDAO;
use Auth;
use Config;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Log;
use Request;
use Response;

require_once app_path().'/helpers/Constants.php';
require_once app_path().'/helpers/validation.php';
require_once app_path().'/helpers/sanitizer.php';

use Illuminate\Foundation\Validation\ValidatesRequests;
use Validator;
use App\BO\EkycApiCaller;


class AadharDetailsBO extends Model {

  public function __construct() {
  		$this->login_dao = new LoginDAO ();
      $this->additional_details_dao = new AdditionalDetailsDAO ();
      $this->common_curl_function = new EkycApiCaller();

  	}

    /**
    *
    * Description: get aadhar no  from user and validate
    * 11-Sept-2017
    * 06:11:29 pm
    * @author Manjiri Parab
    */

    public function validateAadhar($aadhar)
    {
      try
      {
        Log::debug ( ' [AdditionalDetailsBO] ' . ' [validateAadhar] ' . 'Entered in AdditionalDetailsBO of validateAadhar function' );
        $aadhar['api_token'] = Auth::user()->api_token;
        $check_mandatory=mandatory(POST_AADHAR,$aadhar);
        if($check_mandatory['status']=='success')
        {
            $fetch_required = required(POST_AADHAR,$aadhar);
            $validator = Validator::make ($fetch_required,V_AADHAR,V_AADHAR_MSG);
            if ($validator->fails ())
            {
              return array (
                  'msg' =>$validator->messages ()->toArray (),
                  'status' => FAIL
              );
            }
            else
            {
              $post_array = [
                            'api_token'=>$fetch_required['api_token'],
                            'adhaar'=>$fetch_required['adhaar'],
                            ];

              $get_otp=$this->common_curl_function->callCurl('get-otp-adhaar',$post_array);

              if(empty($get_otp))
              {
                  return array ('msg' => API_ERROR_MSG,'status' => FAIL);
              }
              if($get_otp->status == SUCCESS)
              {
                $insert_aadhar_no = $this->additional_details_dao->insertAdhaarNo($fetch_required['adhaar']);
                if($insert_aadhar_no)
                {
                  $update_status = $this->login_dao->updatePageStatus(); // to update user's page status
                  return array (
                      'url'=> $update_status,
                      'msg' =>$get_otp->data,
                      'status' => SUCCESS
                  );
                }
                else
                {
                  return array (
                      'msg' =>ERROR_MSG,
                      'status' => FAIL
                  );
                }

              }
              else
              {
                return array (
                    'msg' =>array('error'=>$get_otp->msg),
                    'status' => FAIL
                );
              }
            }
        }
        else
        {
          return array(  'msg' =>$check_mandatory['data'],'status' => FAIL);
        }
      }
      catch( Exception $exception ) {
        throw $exception;
      }

    }
    /**
    *
    * Description: fetch aadhar no
    * 06-oct-2017
    * 04:24:29 pm
    * @author Manjiri Parab
    */
    public function getAadharNo()
    {
      try
      {
        Log::debug ( ' [AdditionalDetailsBO] ' . ' [getAadharNo] ' . 'Entered in AdditionalDetailsBO of getAadharNo function' );
        return $this->additional_details_dao->getAadharNo();
      }
      catch( Exception $exception )
      {
        throw $exception;
      }
    }

    /**
    *
    * Description: get otp  from user and validate
    * 11-Sept-2017
    * 06:11:29 pm
    * @author Manjiri Parab
    */

    public function validateOTP($otp_details)
    {
      try
      {
          Log::debug ( ' [AdditionalDetailsDAO] ' . ' [validateOTP] ' . 'Entered in AdditionalDetailsDAO of validateOTP function' );
          $otp_details['api_token'] = Auth::user()->api_token;
          $check_mandatory=mandatory(POST_OTP,$otp_details);
          if($check_mandatory['status']=='success')
          {
                $fetch_required = required(POST_OTP,$otp_details);
                $validator = Validator::make ($fetch_required,V_OTP,V_OTP_MSG);
                if ($validator->fails ())
                {
                  return array (
                      'msg' =>$validator->messages ()->toArray (),
                      'status' => FAIL
                  );
                }
                else
                {
                  $post_array = [
                                'api_token'=>$fetch_required['api_token'],
                                'adhaar'=>$fetch_required['adhaar'],
                                'txn_id'=>$fetch_required['txn_id'],
                                'pin'=>$fetch_required['pin']
                                ];

                  $get_otp=$this->common_curl_function->callCurl('post-otp-adhaar',$post_array);
                  if(empty($get_otp))
                  {
                    return array (
                        'msg' =>API_ERROR_MSG,
                        'status' => FAIL
                    );
                  }

                  if($get_otp->status == SUCCESS)
                  {
                    $insert_personal_data = $this->additional_details_dao->updateAdhaarDetails($get_otp->data->adhaar,$get_otp->data->adhaar_details);

                    if($insert_personal_data)
                    {
                      $update_status = $this->login_dao->updatePageStatus(); // to update user's page status
                      return array (
                          'msg' =>$update_status,
                          'status' => SUCCESS
                      );
                    }
                    else
                    {
                      return array (
                          'msg' =>ERROR_MSG,
                          'status' => FAIL
                      );
                    }

                  }
                  else
                  {
                    return array (
                        'msg' =>array('error'=>$get_otp->msg),
                        'status' => FAIL
                    );
                  }
                }
          }
          else
          {
            return array(  'msg' =>$check_mandatory['data'],'status' => FAIL);
          }
      }
      catch( Exception $exception ) {
        throw $exception;
      }

    }

  }
