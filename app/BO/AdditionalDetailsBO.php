<?php

namespace App\BO;
use App\DAO\LoginDAO;
use App\DAO\AdditionalDetailsDAO;
use Auth;
use Config;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Log;
use Request;
use Response;

require_once app_path().'/helpers/Constants.php';
require_once app_path().'/helpers/validation.php';
require_once app_path().'/helpers/sanitizer.php';

use Illuminate\Foundation\Validation\ValidatesRequests;
use Validator;
use App\BO\EkycApiCaller;


class AdditionalDetailsBO extends Model {

  public function __construct() {
      $this->login_dao = new LoginDAO ();
  		$this->additional_details_dao = new AdditionalDetailsDAO ();
      $this->common_curl_function = new EkycApiCaller();

  	}

    /**
    *
    * Description: fetch list of annual income range
    * 25-sept-2017
    * 04:24:29 pm
    * @author Manjiri Parab
    */
    public function getAnnualIncomeList()
    {
      try
      {
        Log::debug ( ' [AdditionalDetailsBO] ' . ' [getAnnualIncomeList] ' . 'Entered in AdditionalDetailsBO of getAnnualIncomeList function' );
        return $this->additional_details_dao->getAnnualIncomeList();

      }
      catch( Exception $exception )
      {
  			throw $exception;
  		}
    }

    /**
    *
    * Description: fetch list of annual income range
    * 25-sept-2017
    * 04:24:29 pm
    * @author Manjiri Parab
    */
    public function getStateList()
    {
      try
      {
        Log::debug ( ' [AdditionalDetailsBO] ' . ' [getStateList] ' . 'Entered in AdditionalDetailsBO of getStateList function' );
        return $this->additional_details_dao->getStateList();
      }
      catch( Exception $exception )
      {
  			throw $exception;
  		}
    }

    /**
    *
    * Description: get details based on aadhar no
    * 29-sept-2017
    * 03:37:29 pm
    * @author Manjiri Parab
    */
    public function getAdhaarDetails()
    {
      try
      {
        Log::debug ( ' [AdditionalDetailsBO] ' . ' [getAdhaarDetails] ' . 'Entered in AdditionalDetailsBO of getAdhaarDetails function' );
        return $this->additional_details_dao->getAdhaarDetails();
      }
      catch( Exception $exception )
      {
  			throw $exception;
  		}
    }
  /**
  *
  * Description: validate user's personal details
  * 23-sept-2017
  * 02:06:29 pm
  * @author Manjiri Parab
  */
  public function getPersonalDetails($user_details)
  {
    try
    {
      Log::debug ( ' [AdditionalDetailsBO] ' . ' [getPersonalDetails] ' . 'Entered in AdditionalDetailsBO of getPersonalDetails function' );
      $user_details['api_token'] = Auth::user()->api_token;
      if($user_details['same_address_flag'] == 'yes')
      {
          $user_details['permanent_address']  =  $user_details['address'];
          $user_details['permanent_address1'] = $user_details['address1'];
          $user_details['permanent_address2'] = $user_details['address2'];
          $user_details['permanent_city'] = $user_details['city'];
          $user_details['permanent_address_proof'] = $user_details['address_proof'];
          $user_details['permanent_pincode'] = $user_details['pincode'];
      }

      $check_mandatory=mandatory(POST_USER_DATA,$user_details);
      if($check_mandatory['status']=='success')
      {
        $fetch_required = required(POST_USER_DATA,$user_details);

        if(Auth::user()->user_group == 3)
        {
            if($user_details['same_address_flag'] == 'yes')
            {
                $validator = Validator::make ($fetch_required,V_ADHAAR_USER_SAME_ADDRS_DATA,V_USER_DATA_MSG);
            }
            else
            {
                $validator = Validator::make ($fetch_required,V_ADHAAR_USER_DIFF_ADDRS_DATA,V_USER_DATA_MSG);
            }
        }
        else
        {
          $validator = Validator::make ($fetch_required,V_USER_DATA,V_USER_DATA_MSG);
        }


        if ($validator->fails ())
        {
          return array (
              'msg' =>$validator->messages ()->toArray (),
              'status' => FAIL
          );
        }
        else
        {
           $insert_personal_data = $this->additional_details_dao->updatePersonalDetails($fetch_required);
          //  print_r($insert_personal_data);exit;
           if($insert_personal_data)
           {
             $update_status = $this->login_dao->updatePageStatus(); // to update user's page status
               return array (
                   'msg' =>$update_status,
                   'status' => SUCCESS
               );
           }
           else
           {
               return array (
                   'msg' =>ERROR_MSG,
                   'status' => FAIL
               );
           }
        }

      }
      else
      {
        return array ('msg' => $check_mandatory['data'],'status' => FAIL);
      }
    }
    catch( Exception $exception )
    {
			throw $exception;
		}
  }

  /**
  *
  * Description: get trading details
  * 23-sept-2017
  * 06:32:29 pm
  * @author Manjiri Parab
  */
  public function getTradingDetails($user_details)
  {
    try
    {
      Log::debug ( ' [AdditionalDetailsBO] ' . ' [getTradingDetails] ' . 'Entered in AdditionalDetailsBO of getTradingDetails function' );
      $user_details['api_token'] = Auth::user()->api_token;
      $fetch_required = required(POST_USER_TRADING_DETIALS,$user_details);
        if($fetch_required['Trading_exp'])
        {
            if($fetch_required['year'] == 0 && $fetch_required['month'] == 0)
            {
               return array('msg' => MENTION_EXP,'status' => FAIL);
            }
        }
        else
        {
          $fetch_required['year'] = $fetch_required['month']= 0;
        }
        $update_trading_data = $this->additional_details_dao->updateTradingDetails($fetch_required);
        if($update_trading_data)
        {
          $update_status = $this->login_dao->updatePageStatus(); // to update user's page status
          return array('msg' => $update_status,'status' => SUCCESS);
        }
        else
        {
          return array('msg' => ERROR_MSG,'status' => FAIL);
        }

    }
    catch( Exception $exception )
    {
      throw $exception;
    }
  }

}
