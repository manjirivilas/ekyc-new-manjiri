<?php

namespace App\BO;

use App\DAO\LoginDAO;
use Auth;
use Config;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Log;
use Request;
use Response;

use Session;

require_once app_path().'/helpers/Constants.php';
require_once app_path().'/helpers/validation.php';
require_once app_path().'/helpers/sanitizer.php';

use Illuminate\Foundation\Validation\ValidatesRequests;
use Validator;
use App\BO\EkycApiCaller;


class LoginBO extends Model {

  public function __construct() {
  		$this->login_dao = new LoginDAO ();
      $this->common_curl_function = new EkycApiCaller();

  	}
  /**
  *
  * Description: ekyc-signup page post data to password set page
  * 22-Aug-2017
  * 05:57:29 pm
  * @author Manjiri Parab
  */
  public function validateData($data) {
    try{
      Log::debug ( ' [LoginBO] ' . ' [validateData] ' . 'Entered in LoginBO of validateData function' );
      $check_mandatory=mandatory(USERDATA,$data);
      if($check_mandatory['status']=='success')
      {

            //fetch required fields and perform validations
            $fetch_required = required(USERDATA,$data);

            $validator = Validator::make ($data,V_REG_USER_DATA,V_REG_USER_DATA_MSG);
       			if ($validator->fails ())
       			{
       				return array ('msg' => $validator->messages ()->toArray (),'status' => FAIL);
       			}
            else
            {
                $post_array = ['email'=>$data['email']];
                $email_existence=$this->common_curl_function->callCurl('email-avail',$post_array);
                // print_r($email_existence);exit;
                if(empty($email_existence))
                {
                  return array ('msg' => API_ERROR_MSG,'status' => FAIL);
                }

                if($email_existence->status == SUCCESS )
                {

                  return array ('msg' => null,'status' => SUCCESS);
                }
                else
                {
                  return array ('msg' => $email_existence->data,'status' =>FAIL);
                }

           }
   }
   else
   {

     return array (
         'msg' =>$check_mandatory['data'],
         'status' => FAIL
     );

   }

    }
    catch( Exception $exception ) {
			throw $exception;
		}
  }

    /**
    *
    * Description: ekyc-signup page -validate password
    * 25-Aug-2017
    * 11:47:29 am
    * @author Manjiri Parab
    */
    public function validatePassword($data) {
      try{
        Log::debug ( ' [LoginBO] ' . ' [validatePassword] ' . 'Entered in LoginBO of validatePassword function' );
        //check email,mobile and name
        $checkData = $this->validateData($data);
        if($checkData['status']==FAIL)
        {

          return array (
   						'msg' => $checkData['msg']/*->messages ()->toArray ()*/,
   						'status' => DATA_FAIL//'user_details_fail'
   				);
        }

        //check mandatory fields
        $check_mandatory=mandatory(POSTDATA,$data);
        if($check_mandatory['status']=='success')
        {


              $fetch_required = required(POSTDATA,$data);
              $validator = Validator::make ($fetch_required,V_REG_USER_PASSWORD,V_REG_USER_PASSWORD_MSG);

         			if ($validator->fails ())
         			{

         				return array (
         						'msg' =>$validator->messages ()->toArray (),
         						'status' => FAIL
         				);
         			}
              else
              {
                $post_array = [
                                  'name'=>$data['name'],
                                  'email'=>$data['email'],
                                  'phone'=>$data['mobile'],
                                  'password'=>$data['password'],
                                  'password_confirmation'=>$data['password_confirmation']
                                  ];
                $call_api=$this->common_curl_function->callCurl('register',$post_array);

                if(empty($call_api))
                {
                  return array (
                      'msg' =>API_ERROR_MSG,
                      'status' => FAIL
                  );
                }
                if($call_api->status == SUCCESS)
                {
                      $create_user =  $this->login_dao->createUser($data,$call_api->data->api_token);

                      //flush cookies
                      \Cookie::queue(\Cookie::forget('name'));
                      \Cookie::queue(\Cookie::forget('email'));
                      \Cookie::queue(\Cookie::forget('mobile'));

                      if($create_user)
                      {
                        Auth::loginUsingId($create_user->id);
                        Session::put('api-token',Auth::user()->api_token);
                        return array (
                 						'msg' =>null,
                 						'status' => SUCCESS
                 				);
                      }
                      else
                      {
                        return array (
                 						'msg' =>ERROR_MSG,
                 						'status' => FAIL
                 				);
                      }
                }
                else
                {
                  foreach($call_api->data as $data)
                  {
                    $error_msg = $data;
                  }
                  return array (
                      'msg' =>$error_msg,
                      'status' => FAIL
                  );
                }

              }
        }
        else
        {

          return array (
              'msg' =>$check_mandatory['data'],
              'status' => FAIL
          );

        }


      }
      catch( Exception $exception ) {
  			throw $exception;
  		}

}

/**
*
* Description: ekyc-check login details.
* 04-Sept-2017
* 05:40:29 pm
* @author Manjiri Parab
*/
public function verifyLogin($data) {
  try
  {
    Log::debug ( ' [LoginBO] ' . ' [verifyLogin] ' . 'Entered in LoginBO of verifyLogin function' );
    $check_mandatory=mandatory(POST_LOGIN_DATA,$data);
    if($check_mandatory['status']=='success')
    {
          $fetch_required = required(POST_LOGIN_DATA,$data);
          $validator = Validator::make ($fetch_required,V_LOGIN_USER,V_LOGIN_USER_MSG);
          if ($validator->fails ())
          {

            return array (
                'msg' =>$validator->messages ()->toArray (),
                'status' => FAIL
            );
          }
          else
          {
                $post_array = [
                              'email'=>$data['email'],
                              ];

                $email_existence=$this->common_curl_function->callCurl('check-email',$post_array);
                if(empty($email_existence))
                {
                  return array (
                      'msg' => API_ERROR_MSG,
                      'status' => FAIL
                  );
                }

                if($email_existence->status != SUCCESS) //not registered
                {
                      return array (
                          'msg' => array('email'=> NOT_USER),
                          'status' => FAIL
                      );
                }
                else
                {
                      $post_array = [
                                'email'=>$data['email'],
                                'password'=>$data['password']
                                ];
                      $check_login=$this->common_curl_function->callCurl('login',$post_array);

                      if(empty($check_login))
                      {
                          return array (
                              'msg' => API_ERROR_MSG,
                              'status' => FAIL
                          );
                      }

                      if($check_login->status == SUCCESS )
                      {

                          $update_token=$this->login_dao->updateToken($check_login->data->email,$check_login->data->api_token);
                          return array (
                              'msg' => $check_login->data,
                              'status' => SUCCESS
                          );
                      }
                      else
                      {

                        return array (
                            'msg' => array('password'=> MISMATCH_EMAIL_PASSWORD),
                            'status' => FAIL
                        );

                      }
                }
          }
    }
    else
    {


      return array (
          'msg' =>$check_mandatory['data'],
          'status' => FAIL
      );

    }
    // return $this->login_dao->createUser($data);
  }
  catch( Exception $exception ) {
    throw $exception;
  }
}

/**
*
* Description:display dashboard
* 05-sep-2017
* 07:30:29 pm
* @author Manjiri Parab
*/
public function getUserData($email)
{
      Log::debug ( ' [LoginBO] ' . ' [getUserData] ' . 'Entered in LoginBO of getUserData function' );
      try
      {
          return $this->login_dao->getUserData($email);
      }
      catch ( Exception $exception )
      {
          Log::error ( ' [LoginBO] ' . ' [getDetails] ' . $exception->getMessage () );
      }
}
/**
*
* Description:logout null token
* 06-sep-2017
* 07:04:29 pm
* @author Manjiri Parab
*/
public function updateToken($email,$token)
{
      Log::debug ( ' [LoginBO] ' . ' [updateToken] ' . 'Entered in LoginBO of updateToken function' );
      try
      {
          return $this->login_dao->updateToken($email,$token);
      }
      catch ( Exception $exception )
      {
          Log::error ( ' [LoginBO] ' . ' [getDetails] ' . $exception->getMessage () );
      }
}

/**
*
* Description:check mail,callcurl to set password.
* 07-sep-2017
* 12:23:56 pm
* @author Manjiri Parab
*/
public function setPassword($data)
{
      Log::debug ( ' [LoginBO] ' . ' [setPassword] ' . 'Entered in LoginBO of setPassword function' );
      try
      {
        // $data['url_path'].RESET_PASSWORD_LINK
        $check_mandatory=mandatory(POST_EMAIL_URL_DATA,$data);
        if($check_mandatory['status']=='success')
        {


              $fetch_required = required(POST_EMAIL_URL_DATA,$data);
              $validator = Validator::make ($fetch_required,V_REG_USER_EMAIL,V_REG_USER_EMAIL_MSG);
              if ($validator->fails ())
              {
                    return array ('msg' =>$validator->messages ()->toArray ()['email'][0],'status' => 'fail');
              }
              else
              {
                    $post_array = [
                                  'email'=>$data['email'],
                                  ];
                    $email_existence=$this->common_curl_function->callCurl('check-email',$post_array);
                    if(empty($email_existence))
                    {
                      return array ('msg' => API_ERROR_MSG,'status' => FAIL);
                    }
                    if($email_existence->status != SUCCESS)
                    {
                      return array ('msg' =>array('email'=>NOT_USER),'status' => FAIL);
                    }
                    else
                    {
                      $post_array = [
                                    'email'=>$data['email'],
                                    'url'=>$data['url_path'].RESET_PASSWORD_LINK
                                    ];

                      $forgot_password=$this->common_curl_function->callCurl('forgot-password',$post_array);
                      if(empty($email_existence))
                      {
                        return array ('msg' => API_ERROR_MSG,'status' => FAIL);
                      }

                      if($forgot_password->status == SUCCESS)
                      {
                        $user_email=$this->login_dao->getUserEmail($data['email'],1);
                        return array ('msg' => PSD_MAIL_SENT_MSG,'status' => SUCCESS);
                      }
                      else
                      {
                        return array ('msg' => ERROR_MSG,'status' => FAIL);
                      }

                    }
              }
        }
        else
        {
          return array ('msg' => BLANK_EMAIL,'status' => FAIL);
        }

      }
      catch ( Exception $exception )
      {
          Log::error ( ' [LoginBO] ' . ' [getDetails] ' . $exception->getMessage () );
      }
}

/**
*
* Description:reset password
* 07-sep-2017
* 05:54:29 pm
* @author Manjiri Parab
*/
public function resetPassword($data)
{
      Log::debug ( ' [LoginBO] ' . ' [resetPassword] ' . 'Entered in LoginBO of resetPassword function' );
      try
      {
        $check_mandatory=mandatory(POST_PASSWORD_DATA,$data);
        if($check_mandatory['status']=='success')
        {
              //fetch required fields and perform validations
              $fetch_required = required(POST_PASSWORD_DATA,$data);
              $validator = Validator::make ($fetch_required,V_REG_USER_PASSWORD,V_REG_USER_PASSWORD_MSG);

              if ($validator->fails ())
              {

                return array (
                    'msg' =>$validator->messages ()->toArray (),
                    'status' => FAIL
                );
              }
              else
              {
                $post_array = [
                              'password'=>$data['password'],
                              'password_confirmation'=>$data['password_confirmation'],
                              'token'=>$data['token']
                              ];
                $reset_password=$this->common_curl_function->callCurl('reset-password',$post_array);
                if(empty($reset_password))
                {
                  return array ('msg' => API_ERROR_MSG,'status' => FAIL);
                }
                if($reset_password->status != SUCCESS)
                {
                  return array ('msg' => $reset_password->msg,'status' => FAIL);
                }
                else
                {
                  \Cookie::queue(\Cookie::forget('email'));
                  \Cookie::queue(\Cookie::forget('token'));
                  $user_email=$this->login_dao->getUserEmail($data['email'],0);
                  return array ('msg' => null,'status' => SUCCESS);
                }

              }
            }
            else
            {
              return array ('msg' => $check_mandatory['data'],'status' => FAIL);
            }

      }
      catch ( Exception $exception )
      {
          Log::error ( ' [LoginBO] ' . ' [getDetails] ' . $exception->getMessage () );
      }
}
/**
*
* Description:reset password
* 07-sep-2017
* 05:54:29 pm
* @author Manjiri Parab
*/
public function changePassword($data)
{
      Log::debug ( ' [LoginBO] ' . ' [changePassword] ' . 'Entered in LoginBO of changePassword function' );
      try
      {
        $data['api_token'] = Auth::user()->api_token;
        $check_mandatory=mandatory(POST_CHANGE_PASSWORD,$data);

        if($check_mandatory['status']=='success')
        {

              //fetch required fields and perform validations
              $fetch_required = required(POST_CHANGE_PASSWORD,$data);
              $validator = Validator::make ($fetch_required,V_CHANGEPASSWORD,V_CHANGEPASSWORD_MSG);

         			if ($validator->fails ())
         			{
         				return array (
         						'msg' =>$validator->messages ()->toArray (),
         						'status' => FAIL
         				);
         			}
              else
              {
                $post_array = [
                              'api_token'=>$data['api_token'],
                              'old_password'=>$data['old_password'],
                              'password'=>$data['password'],
                              'password_confirmation'=>$data['password_confirmation'],
                              ];
                $change_password=$this->common_curl_function->callCurl('change-password',$post_array);
                if(empty($change_password))
                {
                  return array (
                      'msg' => API_ERROR_MSG,
                      'status' => FAIL
                  );
                }
                if($change_password->status == SUCCESS)
                {
                  return array (
                      'msg' => null,
                      'status' => SUCCESS
                  );
                }
                else
                {
                  return array (
                      'msg' => array('error'=>$change_password->msg),
                      'status' => FAIL
                  );
                }
              }
        }
        else
        {
          return array (
              'msg' =>$check_mandatory['data'],
              'status' => FAIL
          );
        }
      }
      catch ( Exception $exception )
      {
        Log::error ( ' [LoginBO] ' . ' [changePassword] ' . $exception->getMessage () );
      }
}

/**
*
* Description:check reset password flag
* 05-oct-2017
* 01:22:29 pm
* @author Manjiri Parab
*/
public function checkResetPasswordFlag($email)
{
      Log::debug ( ' [LoginBO] ' . ' [checkResetPasswordFlag] ' . 'Entered in LoginBO of checkResetPasswordFlag function' );
      try
      {
        return  $this->login_dao->checkResetPasswordFlag($email);
      }
      catch ( Exception $exception )
      {
        Log::error ( ' [LoginBO] ' . ' [changePassword] ' . $exception->getMessage () );
      }
}
/**
*
* Description:get user's current profile page url
* 09-oct-2017
* 01:10:29 pm
* @author Manjiri Parab
*/
public function getCurrentPage()
{
      Log::debug ( ' [LoginBO] ' . ' [getCurrentPage] ' . 'Entered in LoginBO of getCurrentPage function' );
      try
      {
          return $this->login_dao->getCurrentPage();
      }
      catch ( Exception $exception )
      {
          Log::error ( ' [LoginBO] ' . ' [getDetails] ' . $exception->getMessage () );
      }
}

/**
*
* Description: update page status
* 02-Oct-2017
* 04:33:29 pm
* @author Manjiri Parab
*/

public function updatePageStatus()
{
      Log::debug ( ' [LoginChangePasswordController] ' . ' [viewLoginChangePasswordController] ' . 'Entered in LoginChangePasswordController of viewLoginChangePassword function' );
      try
      {
        return $this->login_dao->updatePageStatus();
      }
      catch ( Exception $exception )
      {
        Log::error ( ' [LoginController] ' . ' [getDetails] ' . $exception->getMessage () );
      }
}

}
?>
