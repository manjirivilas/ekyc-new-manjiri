<?php

namespace App\BO;
use App\DAO\LoginDAO;
use App\DAO\SelectPlanDAO;
use Auth;
use Config;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Log;
use Request;
use Response;

use Session;

require_once app_path().'/helpers/Constants.php';
require_once app_path().'/helpers/validation.php';
require_once app_path().'/helpers/sanitizer.php';

use Illuminate\Foundation\Validation\ValidatesRequests;
use Validator;
use App\BO\EkycApiCaller;


class SelectPlanBO extends Model
{
  public function __construct()
  {
      $this->login_dao = new LoginDAO ();
      $this->select_plan_dao = new SelectPlanDAO ();
      $this->common_curl_function = new EkycApiCaller();
  }
  /**
  *
  * Description: get trading/mf details
  * 22-Aug-2017
  * 05:57:29 pm
  * @author Manjiri Parab
  */
  public function getSelectPlan($product)
  {
    try
    {
        Log::debug ( ' [LoginBO] ' . ' [getSelectPlan] ' . 'Entered in selectplanBO of getSelectPlan function' );
        $user_plan=required(PLAN_DATA,$product);

        if(empty($user_plan['trading']))
        {
          $user_plan['trading']=0;
        }
        if(empty($user_plan['mutual_fund']))
        {
          $user_plan['mutual_fund']=0;
        }
        if(!($user_plan['trading'] || $user_plan['mutual_fund']))
        {
          return array('msg'=>SELECT_PRODUCT_MSG,'status'=>FAIL);
        }
        else
        {
          $product = $this->select_plan_dao->saveTradingPlan($user_plan);

          if($product)
          {
            if($user_plan['trading'])
            {
              $url =$this->login_dao->incrementPageStatus(7);
            }
            else
            {
              $url = $this->login_dao->incrementPageStatus(9);
            }
             // to update user's page status
            return array('msg'=>$url,'status'=>SUCCESS);
          }
          else
          {
            return array('msg'=>ERROR_MSG,'status'=>FAIL);
          }
        }
    }
    catch (Exception $exception)
    {
      throw $exception;
    }

  }
  /**
  *
  * Description: ekyc-signup page post data to password set page
  * 22-Aug-2017
  * 05:57:29 pm
  * @author Manjiri Parab
  */
  public function checkTradingFlag()
  {
    try
    {
        return $this->select_plan_dao->checkTradingFlag();

    }
    catch (Exception $exception)
    {
      throw $exception;
    }
  }
  /**
  *
  * Description: ekyc-signup page post data to password set page
  * 22-Aug-2017
  * 05:57:29 pm
  * @author Manjiri Parab
  */
  public function getSegment($segment)
  {
    try
    {
      Log::debug ( ' [SelectSegmentBO] ' . ' [getSegment] ' . 'Entered in SelectSegmentBO of getSegment function' );

      $user_plan=required(SEGMENT,$segment);

      if(empty($user_plan['equity_seg']) && empty($user_plan['commodity_seg']) && empty($user_plan['currency_seg']))
      {
        return array('msg'=>array('error'=>SELECT_SEGMENT_MSG),'status'=>FAIL);
      }
      else
      {
        $user_plan['equity_flag']=  $user_plan['commodity_flag']=$user_plan['currency_flag']=0;
        if(!empty($user_plan['equity_seg']))
        {
          $user_plan['equity_flag']= 1;
          $user_plan['equity_seg'] = implode(', ',$user_plan['equity_seg']);
        }
        if(!empty($user_plan['commodity_seg']))
        {
          $user_plan['commodity_flag']= 1;
          $user_plan['commodity_seg'] = implode(', ',$user_plan['commodity_seg']);
        }
        if(!empty($user_plan['currency_seg']))
        {
          $user_plan['currency_flag']= 1;
          $user_plan['currency_seg'] = implode(', ',$user_plan['currency_seg']);
        }
        $client_segment_data= $this->select_plan_dao->saveSegment($user_plan);
        if($client_segment_data)
        {
          if(!empty($user_plan['equity_seg']) && strpos($user_plan['equity_seg'], "CASH"))
          {
            $url=$this->login_dao->incrementPageStatus(8);
          }
          else
          {
              $check_plan = $this->select_plan_dao->checkTradingFlag();
              if($check_plan['mutual_fund'])
              {
                $url=$this->login_dao->incrementPageStatus(9);
              }
              else
              {
                $url=$this->login_dao->incrementPageStatus(10);
              }
          }
          return array('msg'=>$url,'status'=>SUCCESS);
        }
        else
        {
           return array('msg'=>ERROR_MSG,'status'=>FAIL);
        }

      }
    } catch (Exception $exception) {
  throw $exception;
    }
  }

  /**
  *
  * Description:get demat account
  * 12-Oct-2017
  * 02:52:29 pm
  * @author Manjiri Parab
  */
  public function getProduct($product)
  {
    try
    {
      Log::debug ( ' [SelectProductBO] ' . ' [getProduct] ' . 'Entered in SelectProductBO of getProduct function' );

      $user_plan=required(DEMAT_PRODUCT,$product);

      if($user_plan['demat_acc_flag']=='')
      {
          $user_plan['demat_acc_flag'] = 0;
          $validator = Validator::make ($user_plan,V_DEMAT_DATA,V_DEMAT_DATA_MSG);
          if ($validator->fails ())
          {
              return array ('msg' =>$validator->messages ()->toArray (),'status' => FAIL);
          }
      }
      else
      {
        $user_plan['dp_id'] = 0;
      }

      $save_demat = $this->select_plan_dao->saveDematDetails($user_plan);
      // echo 1;
      if($save_demat)
      {
        $check_plan = $this->select_plan_dao->checkTradingFlag();
        if($check_plan['mutual_fund'])
        {
            $url=$this->login_dao->incrementPageStatus(9);
        }
        else
        {
            $url=$this->login_dao->incrementPageStatus(10);
        }
        return array ('msg' =>$url,'status' => SUCCESS);
      }
      else
      {
        return array ('msg' =>ERROR_MSG,'status' => FAIL);
      }
    }
    catch (Exception $exception)
    {
      throw $exception;
    }
  }

  /**
  *
  * Description:get demat account
  * 12-Oct-2017
  * 02:52:29 pm
  * @author Manjiri Parab
  */
public function saveNomineeDetails($nominee_details)
{
  try
  {
    $nominee_details=required(NOMINEE_DATA,$nominee_details);
    $validator = Validator::make ($nominee_details,V_NOMINEE_DATA,V_NOMINEE_DATA_MSG);
    if ($validator->fails ())
    {
        return array ('msg' =>$validator->messages ()->toArray (),'status' => FAIL);
    }
    else
    {
      $remove_nominee = $this->select_plan_dao->removeNominee();
      $this->select_plan_dao->saveNominee($nominee_details['f_name'],$nominee_details['f_relation'],$nominee_details['f_dob'],$nominee_details['f_address'],$nominee_details['nominee_flag']);
      $this->select_plan_dao->saveNominee($nominee_details['s_name'],$nominee_details['s_relation'],$nominee_details['s_dob'],$nominee_details['s_address'],$nominee_details['nominee_flag']);
      $this->select_plan_dao->saveNominee($nominee_details['t_name'],$nominee_details['t_relation'],$nominee_details['t_dob'],$nominee_details['t_address'],$nominee_details['nominee_flag']);
      if($remove_nominee)
      {
        $url=$this->login_dao->incrementPageStatus(10);
        return array ('msg' =>$url,'status' => SUCCESS);
      }
      else
      {
        return array ('msg' =>ERROR_MSG,'status' => FAIL);
      }
    }
  }
  catch (Exception $exception)
  {
    throw $exception;
  }
}
/**
*
* Description:get demat account
* 12-Oct-2017
* 02:52:29 pm
* @author Manjiri Parab
*/
public function getNomineeDetails()
{
  try
  {
    return $this->select_plan_dao->getNomineeDetails();
  }
  catch (Exception $exception)
  {
    throw $exception;
  }
}

}
