<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PageMaster extends Model
{
    //
    protected $table = 'page_master';
    protected $connection = 'mysql';

    protected $fillable = [ 'id'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    static public function userData()
    {
      return array();
    }
}
