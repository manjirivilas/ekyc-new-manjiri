<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TradingPlan extends Model
{

  protected $table = 'trading_plan_details';
  protected $connection = 'mysql';



  protected $fillable = [ 'user_id'];


  public function user()
  {
      return $this->belongsTo('App\User');
  }
    //
}
