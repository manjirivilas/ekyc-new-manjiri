<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class BankDetails extends Model
{
   protected $table = 'bank_details';
   protected $connection = 'mysql';



   protected $fillable = [
       'bankname', 'branch', 'user_id','IFSC','MICR','acc_no','acc_type',
       'sec_bankname', 'sec_branch','sec_IFSC','sec_MICR','sec_acc_no','sec_acc_type'
   ];


   public function user()
   {
       return $this->belongsTo('App\User');
   }
}
