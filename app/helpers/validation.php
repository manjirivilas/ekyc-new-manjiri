<?php
//name regex
Validator::extend('alpha_spaces', function ($attribute, $value) {
    return preg_match('/^[\pL\s]+$/u', $value);
});
Validator::extend('bank_branch_regex', function ($attribute, $value) {
    return preg_match('/^(\w*\s*[\#\-\,\/\.\(\)\&\:\+]*)+$/', $value);///^[a-zA-Z0-9 ()-]+$/
});

//name regex
Validator::extend('name_regex', function ($attribute, $value) {
    return preg_match('/^[a-zA-Z ]+$/', $value);///^[a-zA-Z0-9 ()-]+$/
});

//ifsc regex
Validator::extend('ifsc_regex', function ($attribute, $value) {
    return preg_match('/^[A-Za-z]{4}\d{7}$/', $value);
});

//pan regex
Validator::extend('pan_regex', function ($attribute, $value) {
    return preg_match('/^[a-zA-z]{5}\d{4}[a-zA-Z]{1}$/', $value);
});

// //trading details validation
// Validator::extend('trading_exp_rule', function ($attribute, $value) {
//     print_r($value);
//     print_r($attribute);exit;
//     return preg_match('/^[a-zA-z]{5}\d{4}[a-zA-Z]{1}$/', $value);
// });



//Name-validation
define('V_REG_USER_DATA', array('name' => 'required|max:100|min:6|alpha_spaces',
'email' => 'required|email|max:255',
'mobile' => 'required|regex:'.REGEX_MOBILE));

//validation messages
define('V_REG_USER_DATA_MSG', array(
  'name.required' =>BLANK_NAME,
  'name.alpha_spaces' =>INVALID_NAME,
  'email.required' =>BLANK_EMAIL,
  'email.email' =>INVALID_EMAIL,
  'mobile.required' =>BLANK_MOBILE,
  'mobile.regex' =>INVALID_MOBILE));

//Password-validation
define('V_REG_USER_PASSWORD', array('password' => 'required|min:6','password_confirmation' => 'required|min:6|same:password',));

//Validation-messages
define('V_REG_USER_PASSWORD_MSG', array(  'password.required' =>BLANK_PASSWORD,
  'password.min' =>INVALID_PASSWORD,
  'password_confirmation.same' =>MISMATCH_PASSWORD));

//login details validation
define('V_LOGIN_USER', array('email' => 'required|email|max:255','password' => 'required|min:6'));

//login validation messages
define('V_LOGIN_USER_MSG', array(
  'email.required' =>BLANK_EMAIL,
  'email.email' =>INVALID_EMAIL,
  'password.min' =>INVALID_PASSWORD
  ));

//forgot password-validation
define('V_REG_USER_EMAIL', array('email' => 'required|email|max:255'));

//forgot password-Validation-messages
define('V_REG_USER_EMAIL_MSG', array('email.required' =>BLANK_EMAIL,'email.email' =>INVALID_EMAIL));

//Bank details
define('V_BANK_DETAILS', array(
    'api_token' =>'required|max:300',
    'ifsc'=>'required|ifsc_regex',
    'bankname'=>'required|max:100|bank_branch_regex',
    'branch'=>'required|min:3|max:50|bank_branch_regex',
    'micr'=>'required|digits:9',
    'acc_type'=>'required|min:7|max:10|alpha|in:Savings,Current',
    'accno'=>'required|AlphaNum|min:8|max:21',
    'accno_confirmation'=>'required|min:8|same:accno',


));

define('V_PRI_SEC_BANK_DETAILS', array(
  'api_token' =>'required|max:300',
  'ifsc'=>'required|ifsc_regex',
  'bankname'=>'required|max:100|bank_branch_regex',
  'branch'=>'required|min:3|max:50|bank_branch_regex',
  'micr'=>'required|digits:9',
  'acc_type'=>'required|min:7|max:10|alpha|in:Savings,Current',
  'accno'=>'required|AlphaNum|min:8|max:21',
  'accno_confirmation'=>'required|min:8|same:accno',
  'secifsc'=>'required|ifsc_regex',
  'secbankname'=>'required|max:100|bank_branch_regex',
  'secbranch'=>'required|min:3|max:50|bank_branch_regex',
  'secmicr'=>'required|digits:9',
  'secacc_type'=>'required|min:7|max:10|alpha|in:Savings,Current',
  'secaccno'=>'required|AlphaNum|min:8|max:21|different:accno',
  'secaccno_confirmation'=>'required|min:8|same:secaccno'
));


//bank details messages
define('V_BANK_DETAILS_MSG', array(
  'ifsc.ifsc_regex' => INVALID_IFSC,
  'bankname.bank_branch_regex'=>INVALID_BANK_NAME,
  'secifsc.ifsc_regex' => INVALID_IFSC,
));

//aadhar

define('V_AADHAR', array(
  'adhaar' => 'required|digits:12',
));

define('V_AADHAR_MSG', array(
  'adhaar.digits' => INVALID_AADHAR,
));


//aadhar-otp
define('V_OTP', array(
 'api_token' =>'required|max:300',
 'pin'=>'required|digits:6',
 'adhaar'=>'required|digits:12',
 'txn_id'=>'required|max:45'
));

define('V_OTP_MSG', array(
  'adhaar.digits' => INVALID_OTP,
));


//pan
define('V_PAN', array(
  'pan_no' => 'required|pan_regex',
  'other_residency' => 'required|in:1,0',
  'identification_country_1' => 'required_if:other_residency,1|required_with_all:identification_type_1,identification_number_1|max:60',
  'identification_number_1' => 'required_with_all:identification_country_1,identification_type_1|max:20',
  'identification_type_1' => 'required_with_all:identification_country_1,identification_number_1|max:20',

  'identification_country_2' => 'required_with_all:identification_number_2,identification_type_2|max:60',
  'identification_number_2' => 'required_with_all:identification_type_2,identification_country_2|max:20',
  'identification_type_2' => 'required_with_all:identification_number_2,identification_country_2|max:20',

  'identification_country_3' => 'required_with_all:identification_number_3,identification_type_3|max:60',
  'identification_number_3' => 'required_with_all:identification_country_3,identification_type_3|max:20',
  'identification_type_3' => 'required_with_all:identification_country_3,identification_number_3|max:20',

));

define('V_PAN_MSG', array(
  'pan_no.required' => BLANK_PAN,
  'pan_no.pan_regex' => INVALID_PAN,
  'other_residency.required' => BLANK_RESIDENCY,

  'identification_country_1.required_if' => REQUIRED_COUNTRY,
  'identification_number_1.required_with_all'=>REQUIRED_DATA,
  'identification_type_1.required_with_all'=>REQUIRED_DATA,

  'identification_country_2.required_with_all'=>REQUIRED_DATA,
  'identification_number_2.required_with_all'=>REQUIRED_DATA,
  'identification_type_2.required_with_all'=>REQUIRED_DATA,

  'identification_country_3.required_with_all'=>REQUIRED_DATA,
  'identification_number_3.required_with_all'=>REQUIRED_DATA,
  'identification_type_3.required_with_all'=>REQUIRED_DATA,

));

//personal details
define('V_USER_DATA', array(
  'api_token' =>'required|max:300',
  'fathername'=>'required|min:3|max:100|name_regex',
  'mothername'=>'required|min:3|max:100|name_regex',
  'birthdate'=>'required',
  'gender'=>'required',
  'email'=>'required|email|max:255',
  'mobile'=>'required|regex:'.REGEX_MOBILE,
  'marrital_status'=>'required',
  'address'=>'required',
  'address1'=>'required',
  'address2'=>'required',
  'city'=>'required',
  'state'=>'required',
  'pincode'=>'required|digits:6',
  'address_proof'=>'required',
  'permanent_address'=>'required',
  'permanent_address1'=>'required',
  'permanent_address2'=>'required',
  'permanent_city'=>'required',
  'permanent_state'=>'required',
  'permanent_pincode'=>'required|digits:6',
  'permanent_address_proof'=>'required',
  'annual_income'=>'required',
  'net_worth'=>'required|numeric',
  'as_on'=>'required',
  'occupation'=>'required'
));

//aadhar user with same address
define('V_USER_DATA_MSG', array(
  'mobile.regex' =>INVALID_MOBILE
));


//aadhar user with same address
define('V_ADHAAR_USER_SAME_ADDRS_DATA', array(
  'api_token' =>'required|max:300',
  'mothername'=>'required|min:3|max:100|name_regex',
  'marrital_status'=>'required',
  'annual_income'=>'required',
  'net_worth'=>'required|numeric',
  'as_on'=>'required',
  'occupation'=>'required'
));

//aadhar user with different address
define('V_ADHAAR_USER_DIFF_ADDRS_DATA', array(
  'api_token' =>'required|max:300',
  'mothername'=>'required|min:3|max:100|name_regex',
  'marrital_status'=>'required',
  'permanent_address'=>'required',
  'permanent_address1'=>'required',
  'permanent_address2'=>'required',
  'permanent_city'=>'required',
  'permanent_state'=>'required',
  'permanent_pincode'=>'required|digits:6',
  'permanent_address_proof'=>'required',
  'annual_income'=>'required',
  'net_worth'=>'required|numeric',
  'as_on'=>'required',
  'occupation'=>'required'
));

//change password validation
define('V_CHANGEPASSWORD', array(
 'api_token' =>'required|max:300',
 'old_password'=>'required|min:6|max:300',
 'password' => 'required|min:6',
 'password_confirmation' => 'required|min:6|same:password'
));

define('V_CHANGEPASSWORD_MSG', array(
 'old_password'=>INVALID_PASSWORD,
 'password' =>INVALID_PASSWORD,
));

//demat account validation
define('V_DEMAT_DATA', array(
 'demat_acc_flag' =>'required|in:1,0',
 'demat_name'=>'required|max:100',
 'dp_id' => 'required|digits:8',
 'bo_id' => 'required|AlphaNum|size:8'
));

define('V_DEMAT_DATA_MSG', array(
 'demat_name.required'=>'Please write your existing Dmat account name ',
 'dp_id.required' => 'Please mention your DP id',
 'bo_id.AlphaNum' => 'Invalid BO id',
 'bo_id.required' =>'Please mention your BO id'
));

//validation for nominee_details
define('V_NOMINEE_DATA', array(
  'nominee_flag' => 'in:0,1,2',
  'f_name' =>'required|max:70',
  'f_relation'=>'required|max:15',
  'f_dob' => 'required',
  'f_address' => 'required|max:100',
  's_name' =>'required_if:nominee_flag,1|max:70',
  's_relation'=>'required_if:nominee_flag,1|max:15',
  's_dob' => 'required_if:nominee_flag,1',
  's_address' => 'required_if:nominee_flag,1|max:100',
  't_name' =>'required_if:nominee_flag,2|max:70',
  't_relation'=>'required_if:nominee_flag,2|max:15',
  't_dob' => 'required_if:nominee_flag,2',
  't_address' => 'required_if:nominee_flag,2|max:100'
));

define('V_NOMINEE_DATA_MSG', array(
 'f_name.required'=>'Please enter a name',
 's_name.required_if'=>'Please enter a name',
 't_name.required_if'=>'Please enter a name',

 'f_relation.required'=>'Please mention relation with nominee',
 's_relation.required_if'=>'Please mention relation with nominee',
 't_relation.required_if'=>'Please mention relation with nominee',

 'f_dob.required'=>'Please mention date of birth',
 's_dob.required_if'=>'Please mention date of birth',
 't_dob.required_if'=>'Please mention date of birth',

 'f_address.required'=>'Please mention the address',
 's_address.required_if'=>'Please mention the address',
 't_address.required_if'=>'Please mention the address'

));
 ?>
