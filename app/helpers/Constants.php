<?php

//ekyc sign-up
define('REGEX_NAME','/^[a-zA-Z ]*$/');
define('BLANK_NAME', 'Please enter your name');
define('INVALID_NAME', 'Special charecters and numbers are not allowed');

define('BLANK_EMAIL', 'Please enter your mail id');
define('INVALID_EMAIL', 'Invalid Email id');

define('REGEX_MOBILE','/^[7-9]\d{9}$/');
define('BLANK_MOBILE', 'Please enter your mobile number');
define('INVALID_MOBILE','Invalid Mobile number');

define('BLANK_PASSWORD', 'Please enter password');
define('INVALID_PASSWORD', 'Password should be minimum of 6 charecters');
define('MISMATCH_PASSWORD', 'password and confirmed password did not match');

define('USERDATA',array('name','email','mobile'));
define('POSTDATA', array('name','email','mobile','password','password_confirmation'));
define('POST_LOGIN_DATA', array('email','password'));
define('POST_EMAIL_URL_DATA', array('email','url_path'));
define('POST_PASSWORD_DATA', array('password','password_confirmation','token'));

define('POST_BANKDETAILS', array('api_token','bankname','branch','ifsc', 'micr','accno','accno_confirmation','acc_type'));
define('POST_ALL_BANKDETAILS', array('api_token','bankname','branch','ifsc', 'micr','accno','accno_confirmation','acc_type',
                                            'secbankname','secbranch','secmicr','secifsc','secaccno','secaccno_confirmation','secacc_type'));
define('PLAN_DATA',array('trading','mutual_fund'));



define('POST_PAN', array('pan_no','other_residency','identification_country_1','identification_number_1','identification_type_1',
'identification_country_2','identification_number_2','identification_type_2','identification_country_3','identification_number_3','identification_type_3'));

define('POST_OTP', array('api_token','pin','adhaar','txn_id'));

define('POST_USER_DATA', array('api_token','fathername','same_address_flag','mothername','birthdate','gender','email','mobile','marrital_status','address',
'address1','address2','city','state','pincode','address_proof','permanent_address','permanent_address1','permanent_address2','permanent_city',
'permanent_state','permanent_pincode','permanent_address_proof','annual_income','net_worth','as_on','occupation'));

//trading details
define('POST_USER_TRADING_DETIALS', array('api_token','Trading_exp','year','month'));
define('MENTION_EXP', array('Trading_exp'=>'Please mention the trading experience'));

//change password
define('POST_CHANGE_PASSWORD', array('api_token','old_password','password','password_confirmation'));

//segment
define('SEGMENT', array('equity_flag','commodity_flag','currency_flag','equity_plan','commodity_plan','currency_plan','equity_seg','commodity_seg','currency_seg'));

//demat product
define('DEMAT_PRODUCT', array('demat_acc_flag','demat_name','dp_id','bo_id','instruction_slip'));

//nominee details
define('NOMINEE_DATA', array('nominee_flag','f_name','f_relation','f_dob','f_address','s_name','s_relation','s_dob','s_address','t_name','t_relation','t_dob','t_address'));


define('ERROR_MSG', array('error'=>'Something went wrong,please try again later !!!'));
define('API_ERROR_MSG', array('error'=>'Server is temporarily unavailable, please try again later'));
define('BANK_API_ERROR_MSG', array('error'=>'Unable to fetch data, server is temporarily unavailable, please try again later'));
define('FAIL', 1);
define('ERROR', 2);
define('SUCCESS',3);
define('DATA_FAIL',0);

define('NOT_USER','User not found');
define('REGISTERED_USER','The email has already been used');
define('PSD_MAIL_SENT_MSG','The password reset link has been sent to your Email Address');
define('RESET_PASSWORD_LINK','/user-reset-password');
define('RESET_PASSWORD_SUCCESS_MSG','password changed successfully');
define('INVALID_IFSC','Invalid Ifsc Code');
define('INVALID_BANK_NAME','Special charecters are not allowed');
define('MISMATCH_EMAIL_PASSWORD',"Incorrect Email id or Password");
define('SUCCESS_PWD_CHANGE',"Password has been changed successfully !!!");
define('LINK_EXPIRED'," The link you have clicked was invalid or has expired. Please try again");
define('LINK_SENT'," The link has been sent to your account !!!");
define('LINK_SENT_FAIL'," Some error occurred, please try again later !!!");
define('SELECT_PRODUCT_MSG',"Please select the product !!!");
define('SELECT_SEGMENT_MSG',"Please select atleast one segment !!!");


//aadhar
define('POST_AADHAR', array('api_token','adhaar'));
define('INVALID_AADHAR','Invalid Aadhar number');
define('INVALID_OTP','Invalid OTP');

//pan
define('BLANK_PAN','Please enter Pan number');
define('INVALID_PAN','Invalid Pan number');
define('BLANK_RESIDENCY','Please select tax residency status');
define('REQUIRED_COUNTRY','Please write country name');
define('REQUIRED_DATA','Please fill the details');
