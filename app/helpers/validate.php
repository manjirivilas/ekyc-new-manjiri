<?php
Validator::extend('alpha_spaces', function ($attribute, $value) {
    return preg_match('/^[\pL\s]+$/u', $value);
});
define("V_REG_USER_MSG", array(
  'name.alpha_spaces'=>"Accept only alphabets."
));

define('V_EMAIL_AVAILABLE', array('email' => 'required|email|max:255|unique:users'));

define("V_REG_USER", array (
     'name' => 'required|max:100|min:6|alpha_spaces',
     'email' => 'required|email|max:255|unique:users',
     'phone' => 'required|digits:10',
     'password' => 'required|min:6|confirmed',
     'password_confirmation' => 'required|min:6'
));

define('V_CHANGEPASSWORD', array(
  'api_token' =>'required',
  'password' => 'required|min:6|confirmed',
  'password_confirmation' => 'required|min:6'
));

define("V_LOGIN_USER", array (
      'email' => 'required|email|max:255',
      'password' => 'required|min:6'
));
