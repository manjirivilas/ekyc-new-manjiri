<?php

function mandatory($keys, $postdata)
{

    foreach ($keys as $key) {
        if ((!array_key_exists($key, $postdata)) || empty(trim($postdata[$key]))) {
            return ["status"=>"fail","data"=>array($key=>$key." is required")];
        }
    }
    return ["status"=>"success",'data'=>null];
}

function required($keys, $postdata)
{
    $temp=array();
    foreach ($keys as $key) {
        if (array_key_exists($key, $postdata)) {
            $temp[$key]=$postdata[$key];
        }
        else
        {
            $temp[$key]="";
        }
    }
    return $temp;
}


 ?>
