<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FatcaDetails extends Model
{
  protected $table = 'fatca_details';
  protected $connection = 'mysql';

  protected $fillable = [ 'user_id'];

  public function user()
  {
      return $this->belongsTo('App\User');
  }
}
